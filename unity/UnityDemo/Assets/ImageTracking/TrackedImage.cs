﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
//import the libraries below
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
 
public class TrackedImage : MonoBehaviour
{
    //create the “trackable” manager to detect 2D images
    ARTrackedImageManager m_TrackedImageManager;
 
    //call Awake method to initialize tracked image manager 
    void Awake()
    {
        //initialized tracked image manager  
        m_TrackedImageManager = GetComponent<ARTrackedImageManager>();
    }
 
 
    //when the tracked image manager is enabled add binding to the tracked 
    //image changed event handler by calling a method to iterate through 
   //image reference’s changes 
    void OnEnable()
    {
        m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }
 
 
   //when the tracked image manager is disabled remove binding to the 
   //tracked image changed event handler by calling a method to iterate 
   //through image reference’s changes
    void OnDisable()
    {
        m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }
 
 
    //method to iterate tracked image changed event handler arguments 
    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        // for each tracked image that has been added
        foreach (var trackedImage in eventArgs.added)
        {
            // Give the initial image a reasonable default scale
           trackedImage.transform.localScale = new Vector3(1f, 1f, 1f);
 
        }
        
        // for each tracked image that has been updated
        foreach (var trackedImage in eventArgs.updated)
           //throw tracked image to check tracking state
            UpdateGameObject(trackedImage);
 
 
        // for each tracked image that has been removed  
        foreach (var trackedImage in eventArgs.removed)
        {
            // destroy the AR object associated with the tracked image
            Destroy(trackedImage.gameObject);
        }
    }
 
    // method to update image tracked game object visibility
    void UpdateGameObject(ARTrackedImage trackedImage)
    {
        Debug.Log("Tracking State: " + trackedImage.trackingState);
        //if tracked image tracking state is comparable to tracking
        if (trackedImage.trackingState == TrackingState.Tracking)
        {
            //set the image tracked ar object to active 
            trackedImage.gameObject.SetActive(true);
        }
        else //if tracked image tracking state is limited or none 
        {
            //deactivate the image tracked ar object 
            trackedImage.gameObject.SetActive(false);
        }
    }
}