﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 ();
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000033 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003E TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000041 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000044 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000046 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000047 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000004B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004D System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004E System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004F System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000050 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000051 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000052 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000054 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000059 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000005C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000005D System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000005E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000062 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000064 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000067 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000069 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006B System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000006C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006D System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000070 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000071 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000072 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000073 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000074 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000075 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000076 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000077 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007A System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000007B System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000007C System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000007D TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000007E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000007F System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000080 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000081 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000082 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000083 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000084 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000085 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000086 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000087 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000088 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000089 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000008A System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x0000008B System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000008C System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000008D TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000095 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000096 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000097 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000098 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000099 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000009A System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000009C System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x0000009D System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000009E System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000009F TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x000000A0 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A1 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A2 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000A3 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A4 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000A5 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000A6 System.Void System.Linq.Lookup`2::Resize()
// 0x000000A7 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000AB System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000AC System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000AD System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000AE System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000AF System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000B0 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000B1 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000B2 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000B3 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000B4 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000B5 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000B6 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000B7 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000B8 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000B9 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000BA TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000BB System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000BC System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000BD System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000C0 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000C1 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000C2 System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000C3 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000C4 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000C5 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000C6 System.Void System.Linq.Set`1::Resize()
// 0x000000C7 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000C8 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000C9 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000CA System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CB System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000CC System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000CD System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CE System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000CF System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000D0 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000D1 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000D2 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000D3 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000D6 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000D7 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000D8 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000D9 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000DA System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000DB System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000DC System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000DD System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000DE System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000DF System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000E0 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000E6 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000E8 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000E9 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000EA System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000EB System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000EC System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000ED System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EE System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000EF System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000F0 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000F1 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000F2 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000F3 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000F4 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000F5 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000F6 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000F7 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000F8 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000F9 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000FA System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000FB T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000FC System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000FD System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[253] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[253] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	931,
	27,
	37,
	206,
	206,
	3,
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[62] = 
{
	{ 0x02000008, { 91, 4 } },
	{ 0x02000009, { 95, 9 } },
	{ 0x0200000A, { 106, 7 } },
	{ 0x0200000B, { 115, 10 } },
	{ 0x0200000C, { 127, 11 } },
	{ 0x0200000D, { 141, 9 } },
	{ 0x0200000E, { 153, 12 } },
	{ 0x0200000F, { 168, 1 } },
	{ 0x02000010, { 169, 2 } },
	{ 0x02000011, { 171, 12 } },
	{ 0x02000012, { 183, 12 } },
	{ 0x02000013, { 195, 6 } },
	{ 0x02000014, { 201, 2 } },
	{ 0x02000015, { 203, 4 } },
	{ 0x02000016, { 207, 3 } },
	{ 0x02000019, { 210, 17 } },
	{ 0x0200001A, { 231, 5 } },
	{ 0x0200001B, { 236, 1 } },
	{ 0x0200001D, { 237, 8 } },
	{ 0x0200001F, { 245, 4 } },
	{ 0x02000020, { 249, 3 } },
	{ 0x02000021, { 252, 5 } },
	{ 0x02000022, { 257, 7 } },
	{ 0x02000023, { 264, 3 } },
	{ 0x02000024, { 267, 7 } },
	{ 0x02000025, { 274, 4 } },
	{ 0x02000026, { 278, 21 } },
	{ 0x02000028, { 299, 2 } },
	{ 0x06000034, { 0, 10 } },
	{ 0x06000035, { 10, 10 } },
	{ 0x06000036, { 20, 5 } },
	{ 0x06000037, { 25, 5 } },
	{ 0x06000038, { 30, 1 } },
	{ 0x06000039, { 31, 2 } },
	{ 0x0600003A, { 33, 2 } },
	{ 0x0600003B, { 35, 4 } },
	{ 0x0600003C, { 39, 1 } },
	{ 0x0600003D, { 40, 2 } },
	{ 0x0600003E, { 42, 3 } },
	{ 0x0600003F, { 45, 2 } },
	{ 0x06000040, { 47, 1 } },
	{ 0x06000041, { 48, 7 } },
	{ 0x06000042, { 55, 2 } },
	{ 0x06000043, { 57, 2 } },
	{ 0x06000044, { 59, 4 } },
	{ 0x06000045, { 63, 4 } },
	{ 0x06000046, { 67, 3 } },
	{ 0x06000047, { 70, 4 } },
	{ 0x06000048, { 74, 4 } },
	{ 0x06000049, { 78, 3 } },
	{ 0x0600004A, { 81, 1 } },
	{ 0x0600004B, { 82, 1 } },
	{ 0x0600004C, { 83, 3 } },
	{ 0x0600004D, { 86, 3 } },
	{ 0x0600004E, { 89, 2 } },
	{ 0x0600005E, { 104, 2 } },
	{ 0x06000063, { 113, 2 } },
	{ 0x06000068, { 125, 2 } },
	{ 0x0600006E, { 138, 3 } },
	{ 0x06000073, { 150, 3 } },
	{ 0x06000078, { 165, 3 } },
	{ 0x060000A0, { 227, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[301] = 
{
	{ (Il2CppRGCTXDataType)2, 24650 },
	{ (Il2CppRGCTXDataType)3, 18953 },
	{ (Il2CppRGCTXDataType)2, 24651 },
	{ (Il2CppRGCTXDataType)2, 24652 },
	{ (Il2CppRGCTXDataType)3, 18954 },
	{ (Il2CppRGCTXDataType)2, 24653 },
	{ (Il2CppRGCTXDataType)2, 24654 },
	{ (Il2CppRGCTXDataType)3, 18955 },
	{ (Il2CppRGCTXDataType)2, 24655 },
	{ (Il2CppRGCTXDataType)3, 18956 },
	{ (Il2CppRGCTXDataType)2, 24656 },
	{ (Il2CppRGCTXDataType)3, 18957 },
	{ (Il2CppRGCTXDataType)2, 24657 },
	{ (Il2CppRGCTXDataType)2, 24658 },
	{ (Il2CppRGCTXDataType)3, 18958 },
	{ (Il2CppRGCTXDataType)2, 24659 },
	{ (Il2CppRGCTXDataType)2, 24660 },
	{ (Il2CppRGCTXDataType)3, 18959 },
	{ (Il2CppRGCTXDataType)2, 24661 },
	{ (Il2CppRGCTXDataType)3, 18960 },
	{ (Il2CppRGCTXDataType)2, 24662 },
	{ (Il2CppRGCTXDataType)3, 18961 },
	{ (Il2CppRGCTXDataType)3, 18962 },
	{ (Il2CppRGCTXDataType)2, 19700 },
	{ (Il2CppRGCTXDataType)3, 18963 },
	{ (Il2CppRGCTXDataType)2, 24663 },
	{ (Il2CppRGCTXDataType)3, 18964 },
	{ (Il2CppRGCTXDataType)3, 18965 },
	{ (Il2CppRGCTXDataType)2, 19707 },
	{ (Il2CppRGCTXDataType)3, 18966 },
	{ (Il2CppRGCTXDataType)3, 18967 },
	{ (Il2CppRGCTXDataType)2, 24664 },
	{ (Il2CppRGCTXDataType)3, 18968 },
	{ (Il2CppRGCTXDataType)2, 24665 },
	{ (Il2CppRGCTXDataType)3, 18969 },
	{ (Il2CppRGCTXDataType)3, 18970 },
	{ (Il2CppRGCTXDataType)2, 24666 },
	{ (Il2CppRGCTXDataType)2, 24667 },
	{ (Il2CppRGCTXDataType)3, 18971 },
	{ (Il2CppRGCTXDataType)3, 18972 },
	{ (Il2CppRGCTXDataType)2, 24668 },
	{ (Il2CppRGCTXDataType)3, 18973 },
	{ (Il2CppRGCTXDataType)2, 24669 },
	{ (Il2CppRGCTXDataType)3, 18974 },
	{ (Il2CppRGCTXDataType)3, 18975 },
	{ (Il2CppRGCTXDataType)2, 19739 },
	{ (Il2CppRGCTXDataType)3, 18976 },
	{ (Il2CppRGCTXDataType)3, 18977 },
	{ (Il2CppRGCTXDataType)2, 19754 },
	{ (Il2CppRGCTXDataType)3, 18978 },
	{ (Il2CppRGCTXDataType)2, 19747 },
	{ (Il2CppRGCTXDataType)2, 24670 },
	{ (Il2CppRGCTXDataType)3, 18979 },
	{ (Il2CppRGCTXDataType)3, 18980 },
	{ (Il2CppRGCTXDataType)3, 18981 },
	{ (Il2CppRGCTXDataType)2, 19755 },
	{ (Il2CppRGCTXDataType)3, 18982 },
	{ (Il2CppRGCTXDataType)2, 24671 },
	{ (Il2CppRGCTXDataType)3, 18983 },
	{ (Il2CppRGCTXDataType)2, 24672 },
	{ (Il2CppRGCTXDataType)2, 24673 },
	{ (Il2CppRGCTXDataType)2, 19759 },
	{ (Il2CppRGCTXDataType)2, 24674 },
	{ (Il2CppRGCTXDataType)2, 24675 },
	{ (Il2CppRGCTXDataType)2, 24676 },
	{ (Il2CppRGCTXDataType)2, 19761 },
	{ (Il2CppRGCTXDataType)2, 24677 },
	{ (Il2CppRGCTXDataType)2, 19763 },
	{ (Il2CppRGCTXDataType)2, 24678 },
	{ (Il2CppRGCTXDataType)3, 18984 },
	{ (Il2CppRGCTXDataType)2, 24679 },
	{ (Il2CppRGCTXDataType)2, 24680 },
	{ (Il2CppRGCTXDataType)2, 19766 },
	{ (Il2CppRGCTXDataType)2, 24681 },
	{ (Il2CppRGCTXDataType)2, 24682 },
	{ (Il2CppRGCTXDataType)2, 24683 },
	{ (Il2CppRGCTXDataType)2, 19768 },
	{ (Il2CppRGCTXDataType)2, 24684 },
	{ (Il2CppRGCTXDataType)2, 19770 },
	{ (Il2CppRGCTXDataType)2, 24685 },
	{ (Il2CppRGCTXDataType)3, 18985 },
	{ (Il2CppRGCTXDataType)2, 24686 },
	{ (Il2CppRGCTXDataType)2, 19775 },
	{ (Il2CppRGCTXDataType)2, 19777 },
	{ (Il2CppRGCTXDataType)2, 24687 },
	{ (Il2CppRGCTXDataType)3, 18986 },
	{ (Il2CppRGCTXDataType)2, 19780 },
	{ (Il2CppRGCTXDataType)2, 24688 },
	{ (Il2CppRGCTXDataType)3, 18987 },
	{ (Il2CppRGCTXDataType)2, 24689 },
	{ (Il2CppRGCTXDataType)2, 19783 },
	{ (Il2CppRGCTXDataType)3, 18988 },
	{ (Il2CppRGCTXDataType)3, 18989 },
	{ (Il2CppRGCTXDataType)2, 19787 },
	{ (Il2CppRGCTXDataType)3, 18990 },
	{ (Il2CppRGCTXDataType)3, 18991 },
	{ (Il2CppRGCTXDataType)2, 19799 },
	{ (Il2CppRGCTXDataType)2, 24690 },
	{ (Il2CppRGCTXDataType)3, 18992 },
	{ (Il2CppRGCTXDataType)3, 18993 },
	{ (Il2CppRGCTXDataType)2, 19801 },
	{ (Il2CppRGCTXDataType)2, 24516 },
	{ (Il2CppRGCTXDataType)3, 18994 },
	{ (Il2CppRGCTXDataType)3, 18995 },
	{ (Il2CppRGCTXDataType)2, 24691 },
	{ (Il2CppRGCTXDataType)3, 18996 },
	{ (Il2CppRGCTXDataType)3, 18997 },
	{ (Il2CppRGCTXDataType)2, 19811 },
	{ (Il2CppRGCTXDataType)2, 24692 },
	{ (Il2CppRGCTXDataType)3, 18998 },
	{ (Il2CppRGCTXDataType)3, 18999 },
	{ (Il2CppRGCTXDataType)3, 18194 },
	{ (Il2CppRGCTXDataType)3, 19000 },
	{ (Il2CppRGCTXDataType)2, 24693 },
	{ (Il2CppRGCTXDataType)3, 19001 },
	{ (Il2CppRGCTXDataType)3, 19002 },
	{ (Il2CppRGCTXDataType)2, 19823 },
	{ (Il2CppRGCTXDataType)2, 24694 },
	{ (Il2CppRGCTXDataType)3, 19003 },
	{ (Il2CppRGCTXDataType)3, 19004 },
	{ (Il2CppRGCTXDataType)3, 19005 },
	{ (Il2CppRGCTXDataType)3, 19006 },
	{ (Il2CppRGCTXDataType)3, 19007 },
	{ (Il2CppRGCTXDataType)3, 18200 },
	{ (Il2CppRGCTXDataType)3, 19008 },
	{ (Il2CppRGCTXDataType)2, 24695 },
	{ (Il2CppRGCTXDataType)3, 19009 },
	{ (Il2CppRGCTXDataType)3, 19010 },
	{ (Il2CppRGCTXDataType)2, 19836 },
	{ (Il2CppRGCTXDataType)2, 24696 },
	{ (Il2CppRGCTXDataType)3, 19011 },
	{ (Il2CppRGCTXDataType)3, 19012 },
	{ (Il2CppRGCTXDataType)2, 19838 },
	{ (Il2CppRGCTXDataType)2, 24697 },
	{ (Il2CppRGCTXDataType)3, 19013 },
	{ (Il2CppRGCTXDataType)3, 19014 },
	{ (Il2CppRGCTXDataType)2, 24698 },
	{ (Il2CppRGCTXDataType)3, 19015 },
	{ (Il2CppRGCTXDataType)3, 19016 },
	{ (Il2CppRGCTXDataType)2, 24699 },
	{ (Il2CppRGCTXDataType)3, 19017 },
	{ (Il2CppRGCTXDataType)3, 19018 },
	{ (Il2CppRGCTXDataType)2, 19853 },
	{ (Il2CppRGCTXDataType)2, 24700 },
	{ (Il2CppRGCTXDataType)3, 19019 },
	{ (Il2CppRGCTXDataType)3, 19020 },
	{ (Il2CppRGCTXDataType)3, 19021 },
	{ (Il2CppRGCTXDataType)3, 18211 },
	{ (Il2CppRGCTXDataType)2, 24701 },
	{ (Il2CppRGCTXDataType)3, 19022 },
	{ (Il2CppRGCTXDataType)3, 19023 },
	{ (Il2CppRGCTXDataType)2, 24702 },
	{ (Il2CppRGCTXDataType)3, 19024 },
	{ (Il2CppRGCTXDataType)3, 19025 },
	{ (Il2CppRGCTXDataType)2, 19869 },
	{ (Il2CppRGCTXDataType)2, 24703 },
	{ (Il2CppRGCTXDataType)3, 19026 },
	{ (Il2CppRGCTXDataType)3, 19027 },
	{ (Il2CppRGCTXDataType)3, 19028 },
	{ (Il2CppRGCTXDataType)3, 19029 },
	{ (Il2CppRGCTXDataType)3, 19030 },
	{ (Il2CppRGCTXDataType)3, 19031 },
	{ (Il2CppRGCTXDataType)3, 18217 },
	{ (Il2CppRGCTXDataType)2, 24704 },
	{ (Il2CppRGCTXDataType)3, 19032 },
	{ (Il2CppRGCTXDataType)3, 19033 },
	{ (Il2CppRGCTXDataType)2, 24705 },
	{ (Il2CppRGCTXDataType)3, 19034 },
	{ (Il2CppRGCTXDataType)3, 19035 },
	{ (Il2CppRGCTXDataType)3, 19036 },
	{ (Il2CppRGCTXDataType)3, 19037 },
	{ (Il2CppRGCTXDataType)3, 19038 },
	{ (Il2CppRGCTXDataType)3, 19039 },
	{ (Il2CppRGCTXDataType)2, 24706 },
	{ (Il2CppRGCTXDataType)2, 24707 },
	{ (Il2CppRGCTXDataType)3, 19040 },
	{ (Il2CppRGCTXDataType)2, 19904 },
	{ (Il2CppRGCTXDataType)2, 19898 },
	{ (Il2CppRGCTXDataType)3, 19041 },
	{ (Il2CppRGCTXDataType)2, 19897 },
	{ (Il2CppRGCTXDataType)2, 24708 },
	{ (Il2CppRGCTXDataType)3, 19042 },
	{ (Il2CppRGCTXDataType)3, 19043 },
	{ (Il2CppRGCTXDataType)3, 19044 },
	{ (Il2CppRGCTXDataType)3, 19045 },
	{ (Il2CppRGCTXDataType)2, 24709 },
	{ (Il2CppRGCTXDataType)3, 19046 },
	{ (Il2CppRGCTXDataType)2, 19920 },
	{ (Il2CppRGCTXDataType)2, 19912 },
	{ (Il2CppRGCTXDataType)3, 19047 },
	{ (Il2CppRGCTXDataType)3, 19048 },
	{ (Il2CppRGCTXDataType)2, 19911 },
	{ (Il2CppRGCTXDataType)2, 24710 },
	{ (Il2CppRGCTXDataType)3, 19049 },
	{ (Il2CppRGCTXDataType)3, 19050 },
	{ (Il2CppRGCTXDataType)3, 19051 },
	{ (Il2CppRGCTXDataType)2, 19924 },
	{ (Il2CppRGCTXDataType)3, 19052 },
	{ (Il2CppRGCTXDataType)2, 24711 },
	{ (Il2CppRGCTXDataType)3, 19053 },
	{ (Il2CppRGCTXDataType)3, 19054 },
	{ (Il2CppRGCTXDataType)2, 24712 },
	{ (Il2CppRGCTXDataType)2, 24713 },
	{ (Il2CppRGCTXDataType)2, 24714 },
	{ (Il2CppRGCTXDataType)3, 19055 },
	{ (Il2CppRGCTXDataType)2, 19936 },
	{ (Il2CppRGCTXDataType)3, 19056 },
	{ (Il2CppRGCTXDataType)2, 24715 },
	{ (Il2CppRGCTXDataType)3, 19057 },
	{ (Il2CppRGCTXDataType)2, 24715 },
	{ (Il2CppRGCTXDataType)2, 19960 },
	{ (Il2CppRGCTXDataType)3, 19058 },
	{ (Il2CppRGCTXDataType)3, 19059 },
	{ (Il2CppRGCTXDataType)3, 19060 },
	{ (Il2CppRGCTXDataType)3, 19061 },
	{ (Il2CppRGCTXDataType)2, 24716 },
	{ (Il2CppRGCTXDataType)2, 24717 },
	{ (Il2CppRGCTXDataType)2, 24718 },
	{ (Il2CppRGCTXDataType)3, 19062 },
	{ (Il2CppRGCTXDataType)3, 19063 },
	{ (Il2CppRGCTXDataType)2, 19956 },
	{ (Il2CppRGCTXDataType)2, 19959 },
	{ (Il2CppRGCTXDataType)3, 19064 },
	{ (Il2CppRGCTXDataType)3, 19065 },
	{ (Il2CppRGCTXDataType)2, 19963 },
	{ (Il2CppRGCTXDataType)3, 19066 },
	{ (Il2CppRGCTXDataType)2, 24719 },
	{ (Il2CppRGCTXDataType)2, 19953 },
	{ (Il2CppRGCTXDataType)2, 24720 },
	{ (Il2CppRGCTXDataType)3, 19067 },
	{ (Il2CppRGCTXDataType)3, 19068 },
	{ (Il2CppRGCTXDataType)3, 19069 },
	{ (Il2CppRGCTXDataType)2, 24721 },
	{ (Il2CppRGCTXDataType)3, 19070 },
	{ (Il2CppRGCTXDataType)3, 19071 },
	{ (Il2CppRGCTXDataType)3, 19072 },
	{ (Il2CppRGCTXDataType)2, 19978 },
	{ (Il2CppRGCTXDataType)3, 19073 },
	{ (Il2CppRGCTXDataType)2, 24722 },
	{ (Il2CppRGCTXDataType)2, 24723 },
	{ (Il2CppRGCTXDataType)3, 19074 },
	{ (Il2CppRGCTXDataType)3, 19075 },
	{ (Il2CppRGCTXDataType)2, 19999 },
	{ (Il2CppRGCTXDataType)3, 19076 },
	{ (Il2CppRGCTXDataType)2, 20000 },
	{ (Il2CppRGCTXDataType)3, 19077 },
	{ (Il2CppRGCTXDataType)2, 24724 },
	{ (Il2CppRGCTXDataType)3, 19078 },
	{ (Il2CppRGCTXDataType)3, 19079 },
	{ (Il2CppRGCTXDataType)2, 24725 },
	{ (Il2CppRGCTXDataType)3, 19080 },
	{ (Il2CppRGCTXDataType)3, 19081 },
	{ (Il2CppRGCTXDataType)2, 24726 },
	{ (Il2CppRGCTXDataType)3, 19082 },
	{ (Il2CppRGCTXDataType)3, 19083 },
	{ (Il2CppRGCTXDataType)3, 19084 },
	{ (Il2CppRGCTXDataType)2, 20031 },
	{ (Il2CppRGCTXDataType)3, 19085 },
	{ (Il2CppRGCTXDataType)2, 20040 },
	{ (Il2CppRGCTXDataType)3, 19086 },
	{ (Il2CppRGCTXDataType)2, 24727 },
	{ (Il2CppRGCTXDataType)2, 24728 },
	{ (Il2CppRGCTXDataType)3, 19087 },
	{ (Il2CppRGCTXDataType)3, 19088 },
	{ (Il2CppRGCTXDataType)3, 19089 },
	{ (Il2CppRGCTXDataType)3, 19090 },
	{ (Il2CppRGCTXDataType)3, 19091 },
	{ (Il2CppRGCTXDataType)3, 19092 },
	{ (Il2CppRGCTXDataType)2, 20056 },
	{ (Il2CppRGCTXDataType)2, 24729 },
	{ (Il2CppRGCTXDataType)3, 19093 },
	{ (Il2CppRGCTXDataType)3, 19094 },
	{ (Il2CppRGCTXDataType)2, 20060 },
	{ (Il2CppRGCTXDataType)3, 19095 },
	{ (Il2CppRGCTXDataType)2, 24730 },
	{ (Il2CppRGCTXDataType)2, 20070 },
	{ (Il2CppRGCTXDataType)2, 20068 },
	{ (Il2CppRGCTXDataType)2, 24731 },
	{ (Il2CppRGCTXDataType)3, 19096 },
	{ (Il2CppRGCTXDataType)2, 24732 },
	{ (Il2CppRGCTXDataType)3, 19097 },
	{ (Il2CppRGCTXDataType)3, 19098 },
	{ (Il2CppRGCTXDataType)3, 19099 },
	{ (Il2CppRGCTXDataType)2, 20073 },
	{ (Il2CppRGCTXDataType)3, 19100 },
	{ (Il2CppRGCTXDataType)3, 19101 },
	{ (Il2CppRGCTXDataType)2, 20076 },
	{ (Il2CppRGCTXDataType)3, 19102 },
	{ (Il2CppRGCTXDataType)1, 24733 },
	{ (Il2CppRGCTXDataType)2, 20075 },
	{ (Il2CppRGCTXDataType)3, 19103 },
	{ (Il2CppRGCTXDataType)1, 20075 },
	{ (Il2CppRGCTXDataType)1, 20073 },
	{ (Il2CppRGCTXDataType)2, 24734 },
	{ (Il2CppRGCTXDataType)2, 20075 },
	{ (Il2CppRGCTXDataType)3, 19104 },
	{ (Il2CppRGCTXDataType)3, 19105 },
	{ (Il2CppRGCTXDataType)3, 19106 },
	{ (Il2CppRGCTXDataType)2, 20074 },
	{ (Il2CppRGCTXDataType)3, 19107 },
	{ (Il2CppRGCTXDataType)2, 20087 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	253,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	62,
	s_rgctxIndices,
	301,
	s_rgctxValues,
	NULL,
};
