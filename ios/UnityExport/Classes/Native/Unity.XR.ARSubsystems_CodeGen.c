﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.XR.ARSubsystems.XRAnchor UnityEngine.XR.ARSubsystems.XRAnchor::get_defaultValue()
extern void XRAnchor_get_defaultValue_m457A914338467F05B7928AF1657C2447DDD38B96 ();
// 0x00000002 System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRAnchor__ctor_m0A6A007D3FC6C125D82C56EF0034197973E14A14_AdjustorThunk ();
// 0x00000003 System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRAnchor__ctor_m8D7A2BE809D9C967C1D12C7E00648FF642AEA4DF_AdjustorThunk ();
// 0x00000004 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRAnchor::get_trackableId()
extern void XRAnchor_get_trackableId_m7BD89E3F1664C126D09D8DD141EA18E8A9933711_AdjustorThunk ();
// 0x00000005 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRAnchor::get_pose()
extern void XRAnchor_get_pose_m7CA50F0FCB9FE7A6FB60C6AFD33B62AF4BE0CB1A_AdjustorThunk ();
// 0x00000006 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRAnchor::get_trackingState()
extern void XRAnchor_get_trackingState_m6C73E20FCB9A2E33666BD07816D04099D61EF3EF_AdjustorThunk ();
// 0x00000007 System.IntPtr UnityEngine.XR.ARSubsystems.XRAnchor::get_nativePtr()
extern void XRAnchor_get_nativePtr_mE26418E703B946C28F977AA10BF36063485EB2F6_AdjustorThunk ();
// 0x00000008 System.Guid UnityEngine.XR.ARSubsystems.XRAnchor::get_sessionId()
extern void XRAnchor_get_sessionId_m80299CA4593E150BF0D645DAEAC03855C131183E_AdjustorThunk ();
// 0x00000009 System.Int32 UnityEngine.XR.ARSubsystems.XRAnchor::GetHashCode()
extern void XRAnchor_GetHashCode_m2E347D72B6CE87F263419D78199C5623BEEA7EAC_AdjustorThunk ();
// 0x0000000A System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::Equals(UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_Equals_m4BB0A471E6C5ADEFBE1863BD9CACC6C6F7325DF5_AdjustorThunk ();
// 0x0000000B System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::Equals(System.Object)
extern void XRAnchor_Equals_mC6E13454F78E1E4B0FB5CC71A7147D988C8608BF_AdjustorThunk ();
// 0x0000000C System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::op_Equality(UnityEngine.XR.ARSubsystems.XRAnchor,UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_op_Equality_m2AD61748921E3CAFB8F748F60ED8493A4A60C153 ();
// 0x0000000D System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::op_Inequality(UnityEngine.XR.ARSubsystems.XRAnchor,UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_op_Inequality_m4698017121926343900DD5B710E449888A5AEEDA ();
// 0x0000000E System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.cctor()
extern void XRAnchor__cctor_mEE7F56270E97E82B96F344CC218F2C78E49645ED ();
// 0x0000000F System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::.ctor()
extern void XRAnchorSubsystem__ctor_m658A4253C51FC407AB5588D44AB923A624DA3218 ();
// 0x00000010 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnStart()
extern void XRAnchorSubsystem_OnStart_mF3FFAB8CB0BC7BFAEB03D9B008ABAC8F30AFB46C ();
// 0x00000011 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnStop()
extern void XRAnchorSubsystem_OnStop_mA451A62E5FD46EB63E3AE777039EAC45B8D4E5D8 ();
// 0x00000012 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnDestroyed()
extern void XRAnchorSubsystem_OnDestroyed_m08E9324005A7527978BDA2F1AB19830D97307CBF ();
// 0x00000013 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRAnchorSubsystem_GetChanges_mA9F45640A583B5E123F075733A25637AC4444D25 ();
// 0x00000014 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void XRAnchorSubsystem_TryAddAnchor_mB2A0A082C26E3E20DF0A097E8AF9F67B4C31D51D ();
// 0x00000015 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void XRAnchorSubsystem_TryAttachAnchor_mBC1C13FBB753A7F7B4BB81CDB41D4C9670ABF132 ();
// 0x00000016 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRAnchorSubsystem_TryRemoveAnchor_m09EE4CB777C6C27E06FEFE52D22BD523CF3B1DE6 ();
// 0x00000017 UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::CreateProvider()
// 0x00000018 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::get_supportsTrackableAttachments()
extern void XRAnchorSubsystemDescriptor_get_supportsTrackableAttachments_m7F2711CC17F581951A3729BCD1BEFFDB8C6E265D ();
// 0x00000019 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::set_supportsTrackableAttachments(System.Boolean)
extern void XRAnchorSubsystemDescriptor_set_supportsTrackableAttachments_m0F7AF1D7E0CF98758531572208E2BB1A2065B6EE ();
// 0x0000001A System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void XRAnchorSubsystemDescriptor_Create_m3F5431E9EE74F05E361D1BBD657727AF75EBC8DF ();
// 0x0000001B System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void XRAnchorSubsystemDescriptor__ctor_mABB3CDF46BA26182C701A3AB0AEF91053E58F396 ();
// 0x0000001C System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversionStatusExtensions::IsDone(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus)
extern void XRAsyncCameraImageConversionStatusExtensions_IsDone_m8CF9AE693207B45BE8D14CCE6320FF8FC048A10E ();
// 0x0000001D System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversionStatusExtensions::IsError(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus)
extern void XRAsyncCameraImageConversionStatusExtensions_IsError_mD9CA202791F0CF667E51EE6CEC35717F4C518ED1 ();
// 0x0000001E UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::get_conversionParams()
extern void XRAsyncCameraImageConversion_get_conversionParams_m2BEEC8F4DD17AF95D5883EA8AE7C6EFC61590877_AdjustorThunk ();
// 0x0000001F System.Void UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::set_conversionParams(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRAsyncCameraImageConversion_set_conversionParams_m60290CD24C0AE8EECCE83387E12AB02C2DA54244_AdjustorThunk ();
// 0x00000020 UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::get_status()
extern void XRAsyncCameraImageConversion_get_status_mC084247856B30468671D2C43783E60E643F36270_AdjustorThunk ();
// 0x00000021 System.Void UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystem,System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRAsyncCameraImageConversion__ctor_mA4D75E954B713EA430F8D35A533587CA2CDE6968_AdjustorThunk ();
// 0x00000022 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::GetData()
// 0x00000023 System.Void UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::Dispose()
extern void XRAsyncCameraImageConversion_Dispose_m2DCD2014A91F54E500277CD4F6EE4DD587B69933_AdjustorThunk ();
// 0x00000024 System.Int32 UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::GetHashCode()
extern void XRAsyncCameraImageConversion_GetHashCode_m15B844354D09525711A8F19B99B56967A993A0A8_AdjustorThunk ();
// 0x00000025 System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::Equals(System.Object)
extern void XRAsyncCameraImageConversion_Equals_m728765267CBAD3F774E4258038F3861F67077DDF_AdjustorThunk ();
// 0x00000026 System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::Equals(UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion)
extern void XRAsyncCameraImageConversion_Equals_m2B27ED670A34C32531F241CCEB1A15DD22B89E87_AdjustorThunk ();
// 0x00000027 System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::op_Equality(UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion,UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion)
extern void XRAsyncCameraImageConversion_op_Equality_m86C260BBA7D9CD34DF7FD183741960E35D07DAD7 ();
// 0x00000028 System.Boolean UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::op_Inequality(UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion,UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion)
extern void XRAsyncCameraImageConversion_op_Inequality_m81792AC215117FE5002B3717643475109C0501FB ();
// 0x00000029 System.String UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion::ToString()
extern void XRAsyncCameraImageConversion_ToString_mCCCE35B02E3179FBBE6D021DB239C61D3AF4C845_AdjustorThunk ();
// 0x0000002A System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_width()
extern void XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4_AdjustorThunk ();
// 0x0000002B System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_height()
extern void XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140_AdjustorThunk ();
// 0x0000002C UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_resolution()
extern void XRCameraConfiguration_get_resolution_mDED625C9D21911EF0D05C49DCBC589FE7915C2B2_AdjustorThunk ();
// 0x0000002D System.Nullable`1<System.Int32> UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_framerate()
extern void XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B_AdjustorThunk ();
// 0x0000002E System.Void UnityEngine.XR.ARSubsystems.XRCameraConfiguration::.ctor(UnityEngine.Vector2Int,System.Int32)
extern void XRCameraConfiguration__ctor_m1E9BF53E84D084D10548F54D77E74070B0E84AD8_AdjustorThunk ();
// 0x0000002F System.Void UnityEngine.XR.ARSubsystems.XRCameraConfiguration::.ctor(UnityEngine.Vector2Int)
extern void XRCameraConfiguration__ctor_mB9EC5F31736A0ABFFB56660DB9AAEF869E3BBEA1_AdjustorThunk ();
// 0x00000030 System.String UnityEngine.XR.ARSubsystems.XRCameraConfiguration::ToString()
extern void XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30_AdjustorThunk ();
// 0x00000031 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::GetHashCode()
extern void XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC_AdjustorThunk ();
// 0x00000032 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(System.Object)
extern void XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C_AdjustorThunk ();
// 0x00000033 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC_AdjustorThunk ();
// 0x00000034 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_op_Equality_mAAD75A6749A62B4DA3A57C30B40111A648C94C2D ();
// 0x00000035 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_op_Inequality_m9472B9522744C80469C6CF0731F1443BF1BA1075 ();
// 0x00000036 System.Int64 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_timestampNs()
extern void XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C_AdjustorThunk ();
// 0x00000037 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageBrightness()
extern void XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C_AdjustorThunk ();
// 0x00000038 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageColorTemperature()
extern void XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234_AdjustorThunk ();
// 0x00000039 UnityEngine.Color UnityEngine.XR.ARSubsystems.XRCameraFrame::get_colorCorrection()
extern void XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk ();
// 0x0000003A UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_projectionMatrix()
extern void XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk ();
// 0x0000003B UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_displayMatrix()
extern void XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk ();
// 0x0000003C UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRCameraFrame::get_trackingState()
extern void XRCameraFrame_get_trackingState_mB45FB220A1EDE8A180217EC6F710A9651E61682A_AdjustorThunk ();
// 0x0000003D System.IntPtr UnityEngine.XR.ARSubsystems.XRCameraFrame::get_nativePtr()
extern void XRCameraFrame_get_nativePtr_mB6380FE0EE01853F55087ACE6C56CB1961384373_AdjustorThunk ();
// 0x0000003E UnityEngine.XR.ARSubsystems.XRCameraFrameProperties UnityEngine.XR.ARSubsystems.XRCameraFrame::get_properties()
extern void XRCameraFrame_get_properties_mE2BF04FF350BC7B1FF375CB5C4E703211133DEF5_AdjustorThunk ();
// 0x0000003F System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageIntensityInLumens()
extern void XRCameraFrame_get_averageIntensityInLumens_m88D3A6D420FE826CD53467E3039475601685DA8F_AdjustorThunk ();
// 0x00000040 System.Double UnityEngine.XR.ARSubsystems.XRCameraFrame::get_exposureDuration()
extern void XRCameraFrame_get_exposureDuration_m5DD81A33C2564756BF8B76FB260E1C1070E3BC7F_AdjustorThunk ();
// 0x00000041 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_exposureOffset()
extern void XRCameraFrame_get_exposureOffset_mA3A2A7C3DFE532AEDA6F1A77C91AA92A2D30FA1C_AdjustorThunk ();
// 0x00000042 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasTimestamp()
extern void XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B_AdjustorThunk ();
// 0x00000043 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageBrightness()
extern void XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5_AdjustorThunk ();
// 0x00000044 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageColorTemperature()
extern void XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15_AdjustorThunk ();
// 0x00000045 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasColorCorrection()
extern void XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597_AdjustorThunk ();
// 0x00000046 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasProjectionMatrix()
extern void XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1_AdjustorThunk ();
// 0x00000047 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasDisplayMatrix()
extern void XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9_AdjustorThunk ();
// 0x00000048 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageIntensityInLumens()
extern void XRCameraFrame_get_hasAverageIntensityInLumens_m2E261BC18F6DA1887D84CFD6C3ABB2135FDC5A99_AdjustorThunk ();
// 0x00000049 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasExposureDuration()
extern void XRCameraFrame_get_hasExposureDuration_mD2581A0F791D7471D153286395F36733CC943DA4_AdjustorThunk ();
// 0x0000004A System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasExposureOffset()
extern void XRCameraFrame_get_hasExposureOffset_m69EB36FB07594B62BC4DE731A007B7EE2E61040A_AdjustorThunk ();
// 0x0000004B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetTimestamp(System.Int64&)
extern void XRCameraFrame_TryGetTimestamp_m73E78FEDC2B5A689D2A4FA0F14796F11E02E8D7E_AdjustorThunk ();
// 0x0000004C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageBrightness(System.Single&)
extern void XRCameraFrame_TryGetAverageBrightness_m1C88B42C26BB03CF446C436E137529BB5488A704_AdjustorThunk ();
// 0x0000004D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageColorTemperature(System.Single&)
extern void XRCameraFrame_TryGetAverageColorTemperature_m3CBA822338324D045A2514043B8684264C5A99DA_AdjustorThunk ();
// 0x0000004E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetProjectionMatrix(UnityEngine.Matrix4x4&)
extern void XRCameraFrame_TryGetProjectionMatrix_mFB3605638456E9DB8C8B2795BB9907FB5CCEB54D_AdjustorThunk ();
// 0x0000004F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetDisplayMatrix(UnityEngine.Matrix4x4&)
extern void XRCameraFrame_TryGetDisplayMatrix_mF94A6FD5819E5E6F463BF53F201C6C88AE5158B9_AdjustorThunk ();
// 0x00000050 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageIntensityInLumens(System.Single&)
extern void XRCameraFrame_TryGetAverageIntensityInLumens_m7B50523B4EFC67EF7B37191798F4A6EA5AEAE714_AdjustorThunk ();
// 0x00000051 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E_AdjustorThunk ();
// 0x00000052 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(System.Object)
extern void XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61_AdjustorThunk ();
// 0x00000053 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraFrame,UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_op_Equality_m5A5BC3941CB1BF81BBFED8AB5DFD9F59F37CA272 ();
// 0x00000054 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraFrame,UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_op_Inequality_m542E0BDA5EB653B7527803339CEFDE4384EC30D2 ();
// 0x00000055 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraFrame::GetHashCode()
extern void XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C_AdjustorThunk ();
// 0x00000056 System.String UnityEngine.XR.ARSubsystems.XRCameraFrame::ToString()
extern void XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA_AdjustorThunk ();
// 0x00000057 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraImage::get_dimensions()
extern void XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC_AdjustorThunk ();
// 0x00000058 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_dimensions(UnityEngine.Vector2Int)
extern void XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD_AdjustorThunk ();
// 0x00000059 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_width()
extern void XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A_AdjustorThunk ();
// 0x0000005A System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_height()
extern void XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE_AdjustorThunk ();
// 0x0000005B System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::get_planeCount()
extern void XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6_AdjustorThunk ();
// 0x0000005C System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_planeCount(System.Int32)
extern void XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5_AdjustorThunk ();
// 0x0000005D UnityEngine.XR.ARSubsystems.CameraImageFormat UnityEngine.XR.ARSubsystems.XRCameraImage::get_format()
extern void XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9_AdjustorThunk ();
// 0x0000005E System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_format(UnityEngine.XR.ARSubsystems.CameraImageFormat)
extern void XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC_AdjustorThunk ();
// 0x0000005F System.Double UnityEngine.XR.ARSubsystems.XRCameraImage::get_timestamp()
extern void XRCameraImage_get_timestamp_m189929FA10F1C355B5763E25232750613F2FD9B7_AdjustorThunk ();
// 0x00000060 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::set_timestamp(System.Double)
extern void XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F_AdjustorThunk ();
// 0x00000061 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::get_valid()
extern void XRCameraImage_get_valid_m9C9C80282451A3D92E3EA46615BFBE948CAE017B_AdjustorThunk ();
// 0x00000062 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::.cctor()
extern void XRCameraImage__cctor_mCE4936C86EAC70BB3FBC1690BE39DED4D063FFFA ();
// 0x00000063 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystem,System.Int32,UnityEngine.Vector2Int,System.Int32,System.Double,UnityEngine.XR.ARSubsystems.CameraImageFormat)
extern void XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0_AdjustorThunk ();
// 0x00000064 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::FormatSupported(UnityEngine.TextureFormat)
extern void XRCameraImage_FormatSupported_m2913E01D5D0DC66B027D49DC2B365A98693075D2 ();
// 0x00000065 UnityEngine.XR.ARSubsystems.XRCameraImagePlane UnityEngine.XR.ARSubsystems.XRCameraImage::GetPlane(System.Int32)
extern void XRCameraImage_GetPlane_m25B263847B085967E2B36AA03C6B411FC6EBCD66_AdjustorThunk ();
// 0x00000066 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::GetConvertedDataSize(UnityEngine.Vector2Int,UnityEngine.TextureFormat)
extern void XRCameraImage_GetConvertedDataSize_m3BB1342E4075EB814DC6D5BD5CD13F07309FE8F4_AdjustorThunk ();
// 0x00000067 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::GetConvertedDataSize(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImage_GetConvertedDataSize_m27A3F5121CA92DD8C4A7800F0844E0C23F1AD368_AdjustorThunk ();
// 0x00000068 System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::Convert(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32)
extern void XRCameraImage_Convert_m75508BB6601557724C90E14C66A8A9706A8CACAA_AdjustorThunk ();
// 0x00000069 UnityEngine.XR.ARSubsystems.XRAsyncCameraImageConversion UnityEngine.XR.ARSubsystems.XRCameraImage::ConvertAsync(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImage_ConvertAsync_mDC2A930F5646533A1F13AD4B9CA659507057522A_AdjustorThunk ();
// 0x0000006A System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::ConvertAsync(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.Action`3<UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,Unity.Collections.NativeArray`1<System.Byte>>)
extern void XRCameraImage_ConvertAsync_m52EDCE064D29031680FE7149DBCD7D2600475FD4_AdjustorThunk ();
// 0x0000006B System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::OnAsyncConversionComplete(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B ();
// 0x0000006C System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::ValidateNativeHandleAndThrow()
extern void XRCameraImage_ValidateNativeHandleAndThrow_mD0E79FE634E6E5431B5222CFB1D8AC833D2BE13B_AdjustorThunk ();
// 0x0000006D System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::ValidateConversionParamsAndThrow(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImage_ValidateConversionParamsAndThrow_m8618EACFC53F5916704F64774882F50F8B4E38E9_AdjustorThunk ();
// 0x0000006E System.Void UnityEngine.XR.ARSubsystems.XRCameraImage::Dispose()
extern void XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A_AdjustorThunk ();
// 0x0000006F System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImage::GetHashCode()
extern void XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E_AdjustorThunk ();
// 0x00000070 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::Equals(System.Object)
extern void XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6_AdjustorThunk ();
// 0x00000071 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::Equals(UnityEngine.XR.ARSubsystems.XRCameraImage)
extern void XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431_AdjustorThunk ();
// 0x00000072 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraImage,UnityEngine.XR.ARSubsystems.XRCameraImage)
extern void XRCameraImage_op_Equality_mEC3CFAA34E277FABD84A140E2D84AE5C1096B87E ();
// 0x00000073 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraImage,UnityEngine.XR.ARSubsystems.XRCameraImage)
extern void XRCameraImage_op_Inequality_m2F2E759DF204BE5ABEFEF0B3232DB5F37B4B15EF ();
// 0x00000074 System.String UnityEngine.XR.ARSubsystems.XRCameraImage::ToString()
extern void XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0_AdjustorThunk ();
// 0x00000075 UnityEngine.RectInt UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_inputRect()
extern void XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833_AdjustorThunk ();
// 0x00000076 System.Void UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::set_inputRect(UnityEngine.RectInt)
extern void XRCameraImageConversionParams_set_inputRect_m1FE1903DB0E679A5C18E03A5E714D362D2D356E5_AdjustorThunk ();
// 0x00000077 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_outputDimensions()
extern void XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2_AdjustorThunk ();
// 0x00000078 System.Void UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::set_outputDimensions(UnityEngine.Vector2Int)
extern void XRCameraImageConversionParams_set_outputDimensions_m7099D526B85FA979380DE028FA860D9F46BD8E81_AdjustorThunk ();
// 0x00000079 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_outputFormat()
extern void XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F_AdjustorThunk ();
// 0x0000007A System.Void UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::set_outputFormat(UnityEngine.TextureFormat)
extern void XRCameraImageConversionParams_set_outputFormat_m97AED6CDFC10BD39F0588EB44DD72B1338A90438_AdjustorThunk ();
// 0x0000007B UnityEngine.XR.ARSubsystems.CameraImageTransformation UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::get_transformation()
extern void XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013_AdjustorThunk ();
// 0x0000007C System.Void UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::set_transformation(UnityEngine.XR.ARSubsystems.CameraImageTransformation)
extern void XRCameraImageConversionParams_set_transformation_mBE056F5598B1FBE0072A78ADEBD1CBB60EA2B4BA_AdjustorThunk ();
// 0x0000007D System.Void UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::.ctor(UnityEngine.XR.ARSubsystems.XRCameraImage,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.CameraImageTransformation)
extern void XRCameraImageConversionParams__ctor_m8D586B09EAB9827D70E6550EB67872CA2AAFC4B1_AdjustorThunk ();
// 0x0000007E System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::GetHashCode()
extern void XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876_AdjustorThunk ();
// 0x0000007F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::Equals(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E_AdjustorThunk ();
// 0x00000080 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::Equals(System.Object)
extern void XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06_AdjustorThunk ();
// 0x00000081 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImageConversionParams_op_Equality_mE076A48C022D29334A622B6E7A0D67BA9755A02B ();
// 0x00000082 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraImageConversionParams_op_Inequality_mDD615881884A83AB8ADE968F7AA1CB998402FF16 ();
// 0x00000083 System.String UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams::ToString()
extern void XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97_AdjustorThunk ();
// 0x00000084 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImagePlane::get_rowStride()
extern void XRCameraImagePlane_get_rowStride_m8551CC63400DE51639214B8E190A5274A84FF11B_AdjustorThunk ();
// 0x00000085 System.Void UnityEngine.XR.ARSubsystems.XRCameraImagePlane::set_rowStride(System.Int32)
extern void XRCameraImagePlane_set_rowStride_m83E1AFB0D313992A2A83C24C4A9EE9B8E095F8CB_AdjustorThunk ();
// 0x00000086 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImagePlane::get_pixelStride()
extern void XRCameraImagePlane_get_pixelStride_mEB04E71CAA5C3FB38099BF40CE07D176D781CD5C_AdjustorThunk ();
// 0x00000087 System.Void UnityEngine.XR.ARSubsystems.XRCameraImagePlane::set_pixelStride(System.Int32)
extern void XRCameraImagePlane_set_pixelStride_m1A467C173D5D73BDA30B1FBF815A8BC977947C65_AdjustorThunk ();
// 0x00000088 Unity.Collections.NativeArray`1<System.Byte> UnityEngine.XR.ARSubsystems.XRCameraImagePlane::get_data()
extern void XRCameraImagePlane_get_data_m7A4B4236191D73BB3B62D2367642C6BCD14EACE6_AdjustorThunk ();
// 0x00000089 System.Void UnityEngine.XR.ARSubsystems.XRCameraImagePlane::set_data(Unity.Collections.NativeArray`1<System.Byte>)
extern void XRCameraImagePlane_set_data_m7B7487B42D2914063EE88E699A4BDFCD07A2FB55_AdjustorThunk ();
// 0x0000008A System.Int32 UnityEngine.XR.ARSubsystems.XRCameraImagePlane::GetHashCode()
extern void XRCameraImagePlane_GetHashCode_m0B82CBAB063DEC9C941CA4C1BA033AE485E0519F_AdjustorThunk ();
// 0x0000008B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImagePlane::Equals(System.Object)
extern void XRCameraImagePlane_Equals_mF26614D5995021938AADB7DC19CB6DD827F68698_AdjustorThunk ();
// 0x0000008C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImagePlane::Equals(UnityEngine.XR.ARSubsystems.XRCameraImagePlane)
extern void XRCameraImagePlane_Equals_m33CD260CF0A3F593C2400FE3B04E4A17C5BB1E56_AdjustorThunk ();
// 0x0000008D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImagePlane::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraImagePlane,UnityEngine.XR.ARSubsystems.XRCameraImagePlane)
extern void XRCameraImagePlane_op_Equality_m5E4E7095E52415F1F3CEC32D00DAEE3906C60571 ();
// 0x0000008E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraImagePlane::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraImagePlane,UnityEngine.XR.ARSubsystems.XRCameraImagePlane)
extern void XRCameraImagePlane_op_Inequality_mA36691A1244BE582C0E10C53391A41BDEA80E1C3 ();
// 0x0000008F System.String UnityEngine.XR.ARSubsystems.XRCameraImagePlane::ToString()
extern void XRCameraImagePlane_ToString_m5BBAE2E31FBCECC4B33EEA0B630B3F176AB38EBA_AdjustorThunk ();
// 0x00000090 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_focalLength()
extern void XRCameraIntrinsics_get_focalLength_m9D090B0B207598F353860CB5735B85B78827C93F_AdjustorThunk ();
// 0x00000091 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_principalPoint()
extern void XRCameraIntrinsics_get_principalPoint_mC66F07CA90FAA4A8A94BDF2A62641196C9DD6DEC_AdjustorThunk ();
// 0x00000092 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_resolution()
extern void XRCameraIntrinsics_get_resolution_m6572536639CC3A6F1A1E1DE50971522B2933355D_AdjustorThunk ();
// 0x00000093 System.Void UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2Int)
extern void XRCameraIntrinsics__ctor_mADA992B91D743E1417DD1C46B16566D1BD72B681_AdjustorThunk ();
// 0x00000094 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2_AdjustorThunk ();
// 0x00000095 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(System.Object)
extern void XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F_AdjustorThunk ();
// 0x00000096 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics,UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_op_Equality_m7214DEF9B315C54F812C1332C9260CE4BBBC4BE2 ();
// 0x00000097 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics,UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_op_Inequality_mE794A593D63C81CD9779F585EB28FC7FB3026E09 ();
// 0x00000098 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::GetHashCode()
extern void XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98_AdjustorThunk ();
// 0x00000099 System.String UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::ToString()
extern void XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983_AdjustorThunk ();
// 0x0000009A System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_zNear()
extern void XRCameraParams_get_zNear_mD6245E7FAC0B8F427F9353E018F61E231B8733FC_AdjustorThunk ();
// 0x0000009B System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zNear(System.Single)
extern void XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB_AdjustorThunk ();
// 0x0000009C System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_zFar()
extern void XRCameraParams_get_zFar_mBA07EC688D770BDBCAC55E50DC309AEF6D85A679_AdjustorThunk ();
// 0x0000009D System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zFar(System.Single)
extern void XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF_AdjustorThunk ();
// 0x0000009E System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenWidth()
extern void XRCameraParams_get_screenWidth_m9CE9B72CBE1FD3CD7F0D856B741D742E5069067E_AdjustorThunk ();
// 0x0000009F System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenWidth(System.Single)
extern void XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919_AdjustorThunk ();
// 0x000000A0 System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenHeight()
extern void XRCameraParams_get_screenHeight_m1305736438CFC1A019C2AB2DF3CA11F2BFBB5B57_AdjustorThunk ();
// 0x000000A1 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenHeight(System.Single)
extern void XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8_AdjustorThunk ();
// 0x000000A2 UnityEngine.ScreenOrientation UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenOrientation()
extern void XRCameraParams_get_screenOrientation_mF47570D9A01E6E868BD0A77E89942B47B9A5A86B_AdjustorThunk ();
// 0x000000A3 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117_AdjustorThunk ();
// 0x000000A4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586_AdjustorThunk ();
// 0x000000A5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(System.Object)
extern void XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E_AdjustorThunk ();
// 0x000000A6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_op_Equality_mCBE3894A8724D8370C56D72C6E78B46219E68162 ();
// 0x000000A7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_op_Inequality_mC226CEC2EC0BF9FF430F4BDE844417236A65F865 ();
// 0x000000A8 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraParams::GetHashCode()
extern void XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4_AdjustorThunk ();
// 0x000000A9 System.String UnityEngine.XR.ARSubsystems.XRCameraParams::ToString()
extern void XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD_AdjustorThunk ();
// 0x000000AA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::.ctor()
extern void XRCameraSubsystem__ctor_m4DB65C1288A29F049A4A362B9CD81B60970A73AE ();
// 0x000000AB UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_focusMode()
extern void XRCameraSubsystem_get_focusMode_mD7F796B4784759D129D32699DEA4ABE7A8E54A71 ();
// 0x000000AC System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_focusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void XRCameraSubsystem_set_focusMode_m4230E264ADA524A210DDE27D926F0DE66918C07A ();
// 0x000000AD UnityEngine.XR.ARSubsystems.LightEstimationMode UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_lightEstimationMode()
extern void XRCameraSubsystem_get_lightEstimationMode_mFED2EBF1F5E52B48935CD4EB4978C2A04A82F5A2 ();
// 0x000000AE System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_lightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void XRCameraSubsystem_set_lightEstimationMode_m0B04B0E3D3E8F73CA5FC6868DE86D6CFFF2263B5 ();
// 0x000000AF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnStart()
extern void XRCameraSubsystem_OnStart_m283BED6AC2AB793BA7E71C07469544C3D6ACBF67 ();
// 0x000000B0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnStop()
extern void XRCameraSubsystem_OnStop_m5FA255D23089DF33D74682EB69B6FC6F8FD39041 ();
// 0x000000B1 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnDestroyed()
extern void XRCameraSubsystem_OnDestroyed_m75D8C8094D3F73E008F80183E3A63ED305B07BC4 ();
// 0x000000B2 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetTextureDescriptors(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21 ();
// 0x000000B3 UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_cameraMaterial()
extern void XRCameraSubsystem_get_cameraMaterial_m5B8CE90E2D4F8AF83D5D8134B1CB57D38065EBF5 ();
// 0x000000B4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void XRCameraSubsystem_TryGetIntrinsics_mA8696FD477463BD57E5EF328AA276439C556BD3F ();
// 0x000000B5 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetConfigurations(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA ();
// 0x000000B6 System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_currentConfiguration()
extern void XRCameraSubsystem_get_currentConfiguration_m95C8ED2F04B9AD10A1C2498C5D32DF03821A1DBF ();
// 0x000000B7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void XRCameraSubsystem_set_currentConfiguration_mC6AAFA4CCF55F2293DB2F8E0A16A9239438ED70D ();
// 0x000000B8 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_invertCulling()
extern void XRCameraSubsystem_get_invertCulling_mB2F2CD061D9065757EDFB6EAD361FB0B1782BAF4 ();
// 0x000000B9 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider UnityEngine.XR.ARSubsystems.XRCameraSubsystem::CreateProvider()
// 0x000000BA System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void XRCameraSubsystem_TryGetLatestFrame_m7BDBD516A8A2832ECBD9334E4D7B0D38ED2D191F ();
// 0x000000BB System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_permissionGranted()
extern void XRCameraSubsystem_get_permissionGranted_m4BE44093A85FE550B9DEFAA70E21CD84C3E99BFF ();
// 0x000000BC System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestImage(UnityEngine.XR.ARSubsystems.XRCameraImage&)
extern void XRCameraSubsystem_TryGetLatestImage_mA9EEBC8B2BC3D9F1DC25513B25BFECA07CE45ABF ();
// 0x000000BD System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::Register(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystem_Register_m471B8039B86BACC07C48A926544DF6C1415C1DA7 ();
// 0x000000BE UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetAsyncRequestStatus(System.Int32)
extern void XRCameraSubsystem_GetAsyncRequestStatus_m6268EBBA8A4DF7A4836B50EAD3A3908025F2F5DA ();
// 0x000000BF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::DisposeImage(System.Int32)
extern void XRCameraSubsystem_DisposeImage_m45A6B78356D402A5528F4F023FA33BD395F1E958 ();
// 0x000000C0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::DisposeAsyncRequest(System.Int32)
extern void XRCameraSubsystem_DisposeAsyncRequest_mF175176F7D374FAEB7F5BD1ACED82A6F463226F5 ();
// 0x000000C1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo&)
extern void XRCameraSubsystem_TryGetPlane_m415B3B07D89C4804AA7B6E14237FCBA8EC83BF03 ();
// 0x000000C2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::NativeHandleValid(System.Int32)
extern void XRCameraSubsystem_NativeHandleValid_m72536C711FE26C43825B05D173B353F75377E33B ();
// 0x000000C3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void XRCameraSubsystem_TryGetConvertedDataSize_m8DE2AA592ED5BFF97D3DCAB38B1441BFD9B18AFA ();
// 0x000000C4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32)
extern void XRCameraSubsystem_TryConvert_mFD08FDC95473E8FC144E257541CB093A1BC5BB39 ();
// 0x000000C5 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void XRCameraSubsystem_ConvertAsync_m1D08E5F3A08DBD2571BB28B93200385C42319498 ();
// 0x000000C6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetAsyncRequestData(System.Int32,System.IntPtr&,System.Int32&)
extern void XRCameraSubsystem_TryGetAsyncRequestData_m2C31AFADC07F7BFEB787EE29104E2CA2B86B683F ();
// 0x000000C7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate,System.IntPtr)
extern void XRCameraSubsystem_ConvertAsync_mC717FF778303227C1073EB9F169F62E1075E0E36 ();
// 0x000000C8 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_id()
extern void XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C_AdjustorThunk ();
// 0x000000C9 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_id(System.String)
extern void XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8_AdjustorThunk ();
// 0x000000CA System.Type UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_implementationType()
extern void XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5_AdjustorThunk ();
// 0x000000CB System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_implementationType(System.Type)
extern void XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E_AdjustorThunk ();
// 0x000000CC System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageBrightness()
extern void XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5_AdjustorThunk ();
// 0x000000CD System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA_AdjustorThunk ();
// 0x000000CE System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageColorTemperature()
extern void XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D_AdjustorThunk ();
// 0x000000CF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E_AdjustorThunk ();
// 0x000000D0 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsColorCorrection()
extern void XRCameraSubsystemCinfo_get_supportsColorCorrection_m3B9E39E84E9C061ACE40853B381B6DFB1DFE03AE_AdjustorThunk ();
// 0x000000D1 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsColorCorrection(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F_AdjustorThunk ();
// 0x000000D2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsDisplayMatrix()
extern void XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C_AdjustorThunk ();
// 0x000000D3 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F_AdjustorThunk ();
// 0x000000D4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsProjectionMatrix()
extern void XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E_AdjustorThunk ();
// 0x000000D5 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66_AdjustorThunk ();
// 0x000000D6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsTimestamp()
extern void XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA_AdjustorThunk ();
// 0x000000D7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28_AdjustorThunk ();
// 0x000000D8 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraConfigurations()
extern void XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E_AdjustorThunk ();
// 0x000000D9 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906_AdjustorThunk ();
// 0x000000DA System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraImage()
extern void XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445_AdjustorThunk ();
// 0x000000DB System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951_AdjustorThunk ();
// 0x000000DC System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageIntensityInLumens()
extern void XRCameraSubsystemCinfo_get_supportsAverageIntensityInLumens_m13362159DD3F8A1AB2C9E9CFFF6DB405374EB174_AdjustorThunk ();
// 0x000000DD System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageIntensityInLumens(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageIntensityInLumens_m0672F68F42398824EB559650DE318B6E0C9414BD_AdjustorThunk ();
// 0x000000DE System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsFocusModes()
extern void XRCameraSubsystemCinfo_get_supportsFocusModes_mE631781ECBAFD54B7F567A62039CC4CC25C9BC2D_AdjustorThunk ();
// 0x000000DF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsFocusModes(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsFocusModes_mCE0E9DF8C54F831E713E751A93BC83144A8849F2_AdjustorThunk ();
// 0x000000E0 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F_AdjustorThunk ();
// 0x000000E1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(System.Object)
extern void XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15_AdjustorThunk ();
// 0x000000E2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_op_Equality_m1FB80C7F9E88CD19AB137F45DD5DC1CCD2E3A35A ();
// 0x000000E3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_op_Inequality_m109F8E812D78490A4522698532D3D3A27EBFD882 ();
// 0x000000E4 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::GetHashCode()
extern void XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3_AdjustorThunk ();
// 0x000000E5 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor__ctor_m218E5DC6846EF28CD00B48704E59A327949C3CAE ();
// 0x000000E6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageBrightness()
extern void XRCameraSubsystemDescriptor_get_supportsAverageBrightness_mE3B7483DD61AE2D5AA8AAA60C31EC2D430DA1DD5 ();
// 0x000000E7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m90F7E1B3733B5D1975217E7FBEEFC2EC6DFDFBF6 ();
// 0x000000E8 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageColorTemperature()
extern void XRCameraSubsystemDescriptor_get_supportsAverageColorTemperature_m1AEBC59200F29D7BE0B619CC8BCFD669501F7EB6 ();
// 0x000000E9 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_m617CC63B166A095A8F873C73BAF39F86D579CD69 ();
// 0x000000EA System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsDisplayMatrix()
extern void XRCameraSubsystemDescriptor_get_supportsDisplayMatrix_m66F85F0187565E0CE7A504CCB21B5F614B90033F ();
// 0x000000EB System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m67FC7675BE452E50E68679516708CEF60B36C061 ();
// 0x000000EC System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsProjectionMatrix()
extern void XRCameraSubsystemDescriptor_get_supportsProjectionMatrix_m75295E7DB06B9C3D81C1B89EC318EDF323091D0C ();
// 0x000000ED System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mC5EBFF2790C6672050DD1781F0026AA82CDE0A3C ();
// 0x000000EE System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsTimestamp()
extern void XRCameraSubsystemDescriptor_get_supportsTimestamp_m8B204E3D78ED417621176FF6BAC73EBBBCC0B341 ();
// 0x000000EF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsTimestamp_m675F2A889C4A694BB5F72569DC697BDE0455B3E1 ();
// 0x000000F0 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsCameraConfigurations()
extern void XRCameraSubsystemDescriptor_get_supportsCameraConfigurations_m8AB0532CD36CFA0A804E5F8489D4BB0C18488C15 ();
// 0x000000F1 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_m2EFBEC5D6FC11ED5D546B7B75090B56A960F57F7 ();
// 0x000000F2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsCameraImage()
extern void XRCameraSubsystemDescriptor_get_supportsCameraImage_m1647B3EC91C215B91FEA91FB7F33A6949360287D ();
// 0x000000F3 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraImage_m84810E8929E44D5C62EEC38D5AAA7ED2F8434979 ();
// 0x000000F4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageIntensityInLumens()
extern void XRCameraSubsystemDescriptor_get_supportsAverageIntensityInLumens_m1E8F760E57616EE5EC09CF603F3C965BE249B671 ();
// 0x000000F5 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageIntensityInLumens(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageIntensityInLumens_m7AD0C0781B44225B04DC966D489E602DA22E8204 ();
// 0x000000F6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsFocusModes()
extern void XRCameraSubsystemDescriptor_get_supportsFocusModes_mF9DEE1BFA76128D329B8B40FCCEAEAB5D1AC5AAF ();
// 0x000000F7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsFocusModes(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsFocusModes_m5BE62BAB571CBEE76F22459E060CA96265B3187A ();
// 0x000000F8 UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3 ();
// 0x000000F9 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::.ctor()
extern void XRDepthSubsystem__ctor_m24423F4A0EF54A1EDA98684496E5973E192C097B ();
// 0x000000FA System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnStart()
extern void XRDepthSubsystem_OnStart_m68DDDE968810C2B434B5484E12D4A5FC655889BC ();
// 0x000000FB System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnDestroyed()
extern void XRDepthSubsystem_OnDestroyed_m85CABCBD92C957237999A4A75E1C59E2061430D0 ();
// 0x000000FC System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnStop()
extern void XRDepthSubsystem_OnStop_m0D73B1F233BD846651091FD0D99CB73F76F9E119 ();
// 0x000000FD UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3 ();
// 0x000000FE UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8 ();
// 0x000000FF UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider UnityEngine.XR.ARSubsystems.XRDepthSubsystem::CreateProvider()
// 0x00000100 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void XRDepthSubsystemDescriptor__ctor_mA74AF85046BBA65B77FA333055F82C2E3F6AD05D ();
// 0x00000101 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsFeaturePoints()
extern void XRDepthSubsystemDescriptor_get_supportsFeaturePoints_m741540583DFCEFCA891B728007A03FCF2405EE0E ();
// 0x00000102 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsFeaturePoints(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m7974B23484377F9C2698B1981BCE830C7BEC6C73 ();
// 0x00000103 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsUniqueIds()
extern void XRDepthSubsystemDescriptor_get_supportsUniqueIds_m52B935BE586195AB5FC081CC299C2DA3035781B5 ();
// 0x00000104 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsUniqueIds(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsUniqueIds_mF89B9E95F36EB311A0C512A7C20BD7CCADB1489B ();
// 0x00000105 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsConfidence()
extern void XRDepthSubsystemDescriptor_get_supportsConfidence_m0CACDD3DFE0D8AB4CDFDC351E034CB118251B015 ();
// 0x00000106 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsConfidence(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsConfidence_mBC66AFF12E25475139457FA6DEB1093A57065068 ();
// 0x00000107 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void XRDepthSubsystemDescriptor_RegisterDescriptor_m9F40B303586BE45F7AACB8B0AA408D242B34F4EC ();
// 0x00000108 UnityEngine.XR.ARSubsystems.XRPointCloud UnityEngine.XR.ARSubsystems.XRPointCloud::get_defaultValue()
extern void XRPointCloud_get_defaultValue_m71EFAD95365CFFB007E85B39F6CCEB2182FCEEDC ();
// 0x00000109 System.Void UnityEngine.XR.ARSubsystems.XRPointCloud::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F_AdjustorThunk ();
// 0x0000010A UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackableId()
extern void XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk ();
// 0x0000010B UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::get_pose()
extern void XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk ();
// 0x0000010C UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackingState()
extern void XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk ();
// 0x0000010D System.IntPtr UnityEngine.XR.ARSubsystems.XRPointCloud::get_nativePtr()
extern void XRPointCloud_get_nativePtr_m993688CDDE348BBADB52795B39A4FDEBF2273557_AdjustorThunk ();
// 0x0000010E System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloud::GetHashCode()
extern void XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C_AdjustorThunk ();
// 0x0000010F System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1_AdjustorThunk ();
// 0x00000110 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(System.Object)
extern void XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57_AdjustorThunk ();
// 0x00000111 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::op_Equality(UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_op_Equality_m3FB26131D214362187D1D66BF8A54B835EB593C8 ();
// 0x00000112 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::op_Inequality(UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_op_Inequality_m257A0AC3D7C4F48B96F9FA9C2C8D20B529844006 ();
// 0x00000113 System.Void UnityEngine.XR.ARSubsystems.XRPointCloud::.cctor()
extern void XRPointCloud__cctor_m64FEF8DF3A914F2770A088FFBE1F169FFDE42486 ();
// 0x00000114 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_positions()
extern void XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk ();
// 0x00000115 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_positions(Unity.Collections.NativeArray`1<UnityEngine.Vector3>)
extern void XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C_AdjustorThunk ();
// 0x00000116 Unity.Collections.NativeArray`1<System.Single> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_confidenceValues()
extern void XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C_AdjustorThunk ();
// 0x00000117 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_confidenceValues(Unity.Collections.NativeArray`1<System.Single>)
extern void XRPointCloudData_set_confidenceValues_m46BFC94CE988C46E7FBA1CC34CB0A493AF4E82B1_AdjustorThunk ();
// 0x00000118 Unity.Collections.NativeArray`1<System.UInt64> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_identifiers()
extern void XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479_AdjustorThunk ();
// 0x00000119 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_identifiers(Unity.Collections.NativeArray`1<System.UInt64>)
extern void XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B_AdjustorThunk ();
// 0x0000011A System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::Dispose()
extern void XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259_AdjustorThunk ();
// 0x0000011B System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloudData::GetHashCode()
extern void XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC_AdjustorThunk ();
// 0x0000011C System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(System.Object)
extern void XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357_AdjustorThunk ();
// 0x0000011D System.String UnityEngine.XR.ARSubsystems.XRPointCloudData::ToString()
extern void XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20_AdjustorThunk ();
// 0x0000011E System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C_AdjustorThunk ();
// 0x0000011F System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::op_Equality(UnityEngine.XR.ARSubsystems.XRPointCloudData,UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_op_Equality_mFA9DB6C392E640D637EED794FEFEBB8338742C07 ();
// 0x00000120 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::op_Inequality(UnityEngine.XR.ARSubsystems.XRPointCloudData,UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_op_Inequality_m42CFE606B979DB81ACF472E711A2C58FF9DE5DF2 ();
// 0x00000121 UnityEngine.XR.ARSubsystems.XREnvironmentProbe UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_defaultValue()
extern void XREnvironmentProbe_get_defaultValue_m14B351BF8F54FCFCDA803FA3C29D1590BC0148E4 ();
// 0x00000122 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackableId()
extern void XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk ();
// 0x00000123 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2_AdjustorThunk ();
// 0x00000124 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_scale()
extern void XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk ();
// 0x00000125 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_scale(UnityEngine.Vector3)
extern void XREnvironmentProbe_set_scale_m75D217DE6DFDA886AAE23446E2CA43C215C178B4_AdjustorThunk ();
// 0x00000126 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_pose()
extern void XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk ();
// 0x00000127 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_pose(UnityEngine.Pose)
extern void XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E_AdjustorThunk ();
// 0x00000128 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_size()
extern void XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk ();
// 0x00000129 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_size(UnityEngine.Vector3)
extern void XREnvironmentProbe_set_size_m341BE92AC4DE11F2CBA00CDE45E6494D9F1D3853_AdjustorThunk ();
// 0x0000012A UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_textureDescriptor()
extern void XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk ();
// 0x0000012B System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_textureDescriptor(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XREnvironmentProbe_set_textureDescriptor_m4E112C52FAE45845367E6BB9F9803F5ACF63B474_AdjustorThunk ();
// 0x0000012C UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackingState()
extern void XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk ();
// 0x0000012D System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_trackingState(UnityEngine.XR.ARSubsystems.TrackingState)
extern void XREnvironmentProbe_set_trackingState_m2E217867A0F5124889681873154F54F5A855A186_AdjustorThunk ();
// 0x0000012E System.IntPtr UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_nativePtr()
extern void XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66_AdjustorThunk ();
// 0x0000012F System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_nativePtr(System.IntPtr)
extern void XREnvironmentProbe_set_nativePtr_mCA6F60033040EF29236438F2EB9B8AFAB9497F84_AdjustorThunk ();
// 0x00000130 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0_AdjustorThunk ();
// 0x00000131 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(System.Object)
extern void XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170_AdjustorThunk ();
// 0x00000132 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::op_Equality(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_op_Equality_mE4F0DFE44C9C32C3A92C6DAE81B2EB84C0CFCA54 ();
// 0x00000133 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::op_Inequality(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_op_Inequality_m8A390AF78A9078214D4ED6570C8FF981B4312964 ();
// 0x00000134 System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::GetHashCode()
extern void XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B_AdjustorThunk ();
// 0x00000135 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString()
extern void XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2_AdjustorThunk ();
// 0x00000136 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString(System.String)
extern void XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19_AdjustorThunk ();
// 0x00000137 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::.cctor()
extern void XREnvironmentProbe__cctor_m4865D642B00DDA22FCC86BF709D5E99DFCAB8279 ();
// 0x00000138 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::.ctor()
extern void XREnvironmentProbeSubsystem__ctor_m7A3AE7794DA58FE53C8EE9F47F8B84F3D5DF47B6 ();
// 0x00000139 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_automaticPlacement()
extern void XREnvironmentProbeSubsystem_get_automaticPlacement_m4D1AC97BF886DFA4851A85797CE2FB34246BAA43 ();
// 0x0000013A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::set_automaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystem_set_automaticPlacement_m1B62556B219E960E694D903D797ED5D9E1E998C4 ();
// 0x0000013B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_environmentTextureHDR()
extern void XREnvironmentProbeSubsystem_get_environmentTextureHDR_mF1FDCF466DA39A5A1376F5337B8AFAB9C0022E4E ();
// 0x0000013C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::set_environmentTextureHDR(System.Boolean)
extern void XREnvironmentProbeSubsystem_set_environmentTextureHDR_mDA7E8015CC7ED7B5312EDFD33769027C9871CC9B ();
// 0x0000013D UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5 ();
// 0x0000013E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnStart()
extern void XREnvironmentProbeSubsystem_OnStart_m599490F20ACA420EE977709ABBE76CC44BB66A89 ();
// 0x0000013F System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnStop()
extern void XREnvironmentProbeSubsystem_OnStop_m2A5FF05318BF41E42FDA607E8476DC4DDEF7A1CC ();
// 0x00000140 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnDestroyed()
extern void XREnvironmentProbeSubsystem_OnDestroyed_mBE024AE290ACB5B14118E53E20329A158E1012D9 ();
// 0x00000141 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_mC5653C998195D72D7A048BE3F4CC07BE81B6438B ();
// 0x00000142 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m045B4A7307B5CBAFFEF3DBAFC78DF05F551B6801 ();
// 0x00000143 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::CreateProvider()
// 0x00000144 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::Register(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystem_Register_mF52AA36EB4EAA59C932C43E45DA567A3EA6D55FD ();
// 0x00000145 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_id()
extern void XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882_AdjustorThunk ();
// 0x00000146 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_id(System.String)
extern void XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24_AdjustorThunk ();
// 0x00000147 System.Type UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_implementationType()
extern void XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22_AdjustorThunk ();
// 0x00000148 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_implementationType(System.Type)
extern void XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E_AdjustorThunk ();
// 0x00000149 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A_AdjustorThunk ();
// 0x0000014A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59_AdjustorThunk ();
// 0x0000014B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE_AdjustorThunk ();
// 0x0000014C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F_AdjustorThunk ();
// 0x0000014D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67_AdjustorThunk ();
// 0x0000014E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582_AdjustorThunk ();
// 0x0000014F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645_AdjustorThunk ();
// 0x00000150 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3_AdjustorThunk ();
// 0x00000151 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsEnvironmentTexture()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210_AdjustorThunk ();
// 0x00000152 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662_AdjustorThunk ();
// 0x00000153 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsEnvironmentTextureHDR()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTextureHDR_m88EEFF6EC5EE0395A2C338FC18F8A2DF92AC0E30_AdjustorThunk ();
// 0x00000154 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsEnvironmentTextureHDR(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTextureHDR_m772ACB32612EFAF491C33F78DE942FD9E0D89FEC_AdjustorThunk ();
// 0x00000155 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809_AdjustorThunk ();
// 0x00000156 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(System.Object)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E_AdjustorThunk ();
// 0x00000157 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_op_Equality_m88D26DC5B25259A6AD95D0F384BF2058E0B48133 ();
// 0x00000158 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_op_Inequality_m262DEAE6D9933DD29F52F6C37435E1FE7EA001F0 ();
// 0x00000159 System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::GetHashCode()
extern void XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B_AdjustorThunk ();
// 0x0000015A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor__ctor_m91F1A02FF56AC51ABA801B94687E393C9AB82F74 ();
// 0x0000015B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_mE198AF9A486EED050CB81241F91BE6F7DA074632 ();
// 0x0000015C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_mFB3D77F497F4DF6722A1B7C66DE6F4D9505763A7 ();
// 0x0000015D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m1FCEBFF75F2F521CC101940F5F8A1022B45B64E1 ();
// 0x0000015E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_m4E909B728829BACB3845873C9D0A2F41ED14D531 ();
// 0x0000015F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mA944EEE25188EC82048232D81023516D1298080A ();
// 0x00000160 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m2374B2B4F3CF3959F87F0DD13C8EE4895BC9AE25 ();
// 0x00000161 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_m25ACC17AE99D2D056B11C7BAD34981D0A7444550 ();
// 0x00000162 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mECFC5E6081D92D77590B981F055E86AE6557873B ();
// 0x00000163 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsEnvironmentTexture()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTexture_m5A0F9ABC531CAA2844AE6F07A8F8DD7EF81F76D5 ();
// 0x00000164 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_m31201D249DB3E60DA61A739A66D19BEFB89B4B7B ();
// 0x00000165 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsEnvironmentTextureHDR()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTextureHDR_m0031706D58FD3E85CC2371B096C3AF5884F9E047 ();
// 0x00000166 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsEnvironmentTextureHDR(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTextureHDR_mD1B9BA66047AA649756F9E13B8AA81456A7E7C5A ();
// 0x00000167 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E ();
// 0x00000168 UnityEngine.XR.ARSubsystems.XRFace UnityEngine.XR.ARSubsystems.XRFace::get_defaultValue()
extern void XRFace_get_defaultValue_m10A49DFCC1786C0E8F3244200F1AC9696C16AD34 ();
// 0x00000169 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRFace::get_trackableId()
extern void XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk ();
// 0x0000016A UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_pose()
extern void XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk ();
// 0x0000016B UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRFace::get_trackingState()
extern void XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk ();
// 0x0000016C System.IntPtr UnityEngine.XR.ARSubsystems.XRFace::get_nativePtr()
extern void XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06_AdjustorThunk ();
// 0x0000016D UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_leftEyePose()
extern void XRFace_get_leftEyePose_mB6508142768ACD1B9C5EA05224DEF9E690C7F0F1_AdjustorThunk ();
// 0x0000016E UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_rightEyePose()
extern void XRFace_get_rightEyePose_m8A78A727975AA070F197E566C8135B2CA45E4996_AdjustorThunk ();
// 0x0000016F UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XRFace::get_fixationPoint()
extern void XRFace_get_fixationPoint_mBC16DB0D6E29A8DCAEB022097B502398BF106405_AdjustorThunk ();
// 0x00000170 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(System.Object)
extern void XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A_AdjustorThunk ();
// 0x00000171 System.Int32 UnityEngine.XR.ARSubsystems.XRFace::GetHashCode()
extern void XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0_AdjustorThunk ();
// 0x00000172 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::op_Equality(UnityEngine.XR.ARSubsystems.XRFace,UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_op_Equality_m6C4A7F979B051CF75D03F1084305D6E42BCF4EC0 ();
// 0x00000173 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::op_Inequality(UnityEngine.XR.ARSubsystems.XRFace,UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_op_Inequality_mCFA0FEE8A708FDD9EEF569212A10DA76095B4CA7 ();
// 0x00000174 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6_AdjustorThunk ();
// 0x00000175 System.Void UnityEngine.XR.ARSubsystems.XRFace::.cctor()
extern void XRFace__cctor_m5D1A0A4AD53355FE60D9ACA97DE83DE702C386CB ();
// 0x00000176 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Resize(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRFaceMesh_Attributes,Unity.Collections.Allocator)
extern void XRFaceMesh_Resize_m6546D0617427A8BDD68E2F53EB85F67B80ED4DE7_AdjustorThunk ();
// 0x00000177 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_vertices()
extern void XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk ();
// 0x00000178 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_normals()
extern void XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk ();
// 0x00000179 Unity.Collections.NativeArray`1<System.Int32> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_indices()
extern void XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D_AdjustorThunk ();
// 0x0000017A Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_uvs()
extern void XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk ();
// 0x0000017B System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Dispose()
extern void XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E_AdjustorThunk ();
// 0x0000017C System.Int32 UnityEngine.XR.ARSubsystems.XRFaceMesh::GetHashCode()
extern void XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E_AdjustorThunk ();
// 0x0000017D System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(System.Object)
extern void XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC_AdjustorThunk ();
// 0x0000017E System.String UnityEngine.XR.ARSubsystems.XRFaceMesh::ToString()
extern void XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3_AdjustorThunk ();
// 0x0000017F System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D_AdjustorThunk ();
// 0x00000180 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::op_Equality(UnityEngine.XR.ARSubsystems.XRFaceMesh,UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_op_Equality_mE8CF70FC8B019862A4A4408DCF1323CCD8DEDE9A ();
// 0x00000181 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::op_Inequality(UnityEngine.XR.ARSubsystems.XRFaceMesh,UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_op_Inequality_mA63E23602313310CE2C5DF67CADE3C7D4B9DECBB ();
// 0x00000182 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Resize(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&,System.Boolean)
// 0x00000183 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::.ctor()
extern void XRFaceSubsystem__ctor_m69A0FE81F7D83567E0B1F70FE0CC9B37AE5BB7EB ();
// 0x00000184 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnStart()
extern void XRFaceSubsystem_OnStart_m5206B8B5819B5B818EABC714EE362A3C8F543338 ();
// 0x00000185 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnDestroyed()
extern void XRFaceSubsystem_OnDestroyed_mF97D40C2D112EF388BBF9D902A8B15C993872077 ();
// 0x00000186 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnStop()
extern void XRFaceSubsystem_OnStop_m28860BE62A1CE3037538BAF91072A5E51756D8A3 ();
// 0x00000187 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_maximumFaceCount()
extern void XRFaceSubsystem_get_maximumFaceCount_m1C2E12E9FB9B0EBB26F5167D2C60823463B5D9FE ();
// 0x00000188 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::set_maximumFaceCount(System.Int32)
extern void XRFaceSubsystem_set_maximumFaceCount_mFD5EDEB83AE800C0D3C9FD9FDD4C59E3A046056B ();
// 0x00000189 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_supportedFaceCount()
extern void XRFaceSubsystem_get_supportedFaceCount_mE7D95AB41DF2408E3121F1107671A5D410A9E33D ();
// 0x0000018A UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7 ();
// 0x0000018B System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void XRFaceSubsystem_GetFaceMesh_m4E1417F6B00F84DB66EFECFE29A8FA2D3ECBBD12 ();
// 0x0000018C UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider UnityEngine.XR.ARSubsystems.XRFaceSubsystem::CreateProvider()
// 0x0000018D System.String UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_id()
extern void FaceSubsystemParams_get_id_mE496E36DA79D6680FDD501331A40FEE5ACA4844F_AdjustorThunk ();
// 0x0000018E System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_id(System.String)
extern void FaceSubsystemParams_set_id_m8F4D745F751A5DBAA600928FE0FC088197F17F02_AdjustorThunk ();
// 0x0000018F System.Type UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_subsystemImplementationType()
extern void FaceSubsystemParams_get_subsystemImplementationType_mF277DA4D914188A29C632C0A4EF01B833A145DD0_AdjustorThunk ();
// 0x00000190 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_subsystemImplementationType(System.Type)
extern void FaceSubsystemParams_set_subsystemImplementationType_mD47C85784BA65BD39301E4EEF8CADEC22233E431_AdjustorThunk ();
// 0x00000191 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFacePose()
extern void FaceSubsystemParams_get_supportsFacePose_mF7D44A52EA89803B802EFF223FB73F366CBA23F4_AdjustorThunk ();
// 0x00000192 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFacePose(System.Boolean)
extern void FaceSubsystemParams_set_supportsFacePose_m4D057DB195C5497D6430139C6CCE73553EB30B15_AdjustorThunk ();
// 0x00000193 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshVerticesAndIndices()
extern void FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_mFC3A1CCB5F0F3BDD8FAE922CCA8AEFADF31BCD56_AdjustorThunk ();
// 0x00000194 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshVerticesAndIndices(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_mA9BB345D83A8BDB50511C19E759B115ECEFFD111_AdjustorThunk ();
// 0x00000195 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshUVs()
extern void FaceSubsystemParams_get_supportsFaceMeshUVs_mB44CFFAC24415AA94DBF91C346A700D5CAF7E101_AdjustorThunk ();
// 0x00000196 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshUVs(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshUVs_mA7FB3F027FBFD77CE4E217C456D7C7ACE2A8E557_AdjustorThunk ();
// 0x00000197 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshNormals()
extern void FaceSubsystemParams_get_supportsFaceMeshNormals_m32FEB03508B5BB7DFC02E78BAC4EC2C6A67632AF_AdjustorThunk ();
// 0x00000198 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshNormals(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshNormals_m078C141E010316EB93E3620DE9C36B795EE17994_AdjustorThunk ();
// 0x00000199 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsEyeTracking()
extern void FaceSubsystemParams_get_supportsEyeTracking_m7894CCA2A243A2DE020C5CDD9D82CAD31159C461_AdjustorThunk ();
// 0x0000019A System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsEyeTracking(System.Boolean)
extern void FaceSubsystemParams_set_supportsEyeTracking_m297F9ABAA20371C328C1A41D1A54CCB53A5E33EC_AdjustorThunk ();
// 0x0000019B System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::Equals(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_Equals_mCC1F4FAA16C7991A05D3EB146ECB4D2864C5131D_AdjustorThunk ();
// 0x0000019C System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::Equals(System.Object)
extern void FaceSubsystemParams_Equals_m382A90011B4E67AF4BBDD015282293128A10DCB9_AdjustorThunk ();
// 0x0000019D System.Int32 UnityEngine.XR.ARSubsystems.FaceSubsystemParams::GetHashCode()
extern void FaceSubsystemParams_GetHashCode_mD2C1F94EFB85920F5801FE988D8E5857DE801F4D_AdjustorThunk ();
// 0x0000019E System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::op_Equality(UnityEngine.XR.ARSubsystems.FaceSubsystemParams,UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_op_Equality_mB3E1C58B8B66732CFB7B71CCE38665314DFA45C4 ();
// 0x0000019F System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::op_Inequality(UnityEngine.XR.ARSubsystems.FaceSubsystemParams,UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_op_Inequality_mC94E0022A973D6D7FCCEB8DEBE541A3D6C86ACE7 ();
// 0x000001A0 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void XRFaceSubsystemDescriptor__ctor_mA1CA67656903ECE685C5546EFF3C208B7FFF9D10 ();
// 0x000001A1 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFacePose()
extern void XRFaceSubsystemDescriptor_get_supportsFacePose_mB43A745C7907B946DD061435294C5C883A252891 ();
// 0x000001A2 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshVerticesAndIndices()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshVerticesAndIndices_mE4E13309BDD7E3560ADA94222DC272269A5E2581 ();
// 0x000001A3 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshUVs()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshUVs_m3A0F0F232DD7B477E4CF45CFB7497D2BB7F7647C ();
// 0x000001A4 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshNormals()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshNormals_mCCBCA63B368220DD0BFCCBBDD7BA5EC5AF605B48 ();
// 0x000001A5 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsEyeTracking()
extern void XRFaceSubsystemDescriptor_get_supportsEyeTracking_mFB1F49080EC91D12B4F6FCF06654E4954BD034C6 ();
// 0x000001A6 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void XRFaceSubsystemDescriptor_Create_m7CE3BB12E0100FD0BB3D56E3BCE603BCF62C78AA ();
// 0x000001A7 System.Guid UnityEngine.XR.ARSubsystems.GuidUtil::Compose(System.UInt64,System.UInt64)
extern void GuidUtil_Compose_m23689A8CCFCDF3904D23BE11760E58DC662E35C1 ();
// 0x000001A8 System.Int32 UnityEngine.XR.ARSubsystems.IReferenceImageLibrary::get_count()
// 0x000001A9 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.IReferenceImageLibrary::get_Item(System.Int32)
// 0x000001AA Unity.Jobs.JobHandle UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::ScheduleAddImageJobImpl(Unity.Collections.NativeSlice`1<System.Byte>,UnityEngine.Vector2Int,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRReferenceImage,Unity.Jobs.JobHandle)
// 0x000001AB Unity.Jobs.JobHandle UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::ScheduleAddImageJob(Unity.Collections.NativeSlice`1<System.Byte>,UnityEngine.Vector2Int,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRReferenceImage,Unity.Jobs.JobHandle)
extern void MutableRuntimeReferenceImageLibrary_ScheduleAddImageJob_m96F4D42BCF3F1E53B44805A62411FDE2D63C12F1 ();
// 0x000001AC System.Int32 UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::get_supportedTextureFormatCount()
// 0x000001AD UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetSupportedTextureFormatAt(System.Int32)
extern void MutableRuntimeReferenceImageLibrary_GetSupportedTextureFormatAt_m62439D0A5E15B37AA9E91A7BF080F4773F7C2A49 ();
// 0x000001AE UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetSupportedTextureFormatAtImpl(System.Int32)
// 0x000001AF System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::IsTextureFormatSupported(UnityEngine.TextureFormat)
extern void MutableRuntimeReferenceImageLibrary_IsTextureFormatSupported_mEA4103D5399A43A7FDFD985ADA91AF9906ECCC68 ();
// 0x000001B0 UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetEnumerator()
extern void MutableRuntimeReferenceImageLibrary_GetEnumerator_mF0AD39CFBE88F86EC1F8D6D905C8AAB4E22F6349 ();
// 0x000001B1 UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GenerateNewGuid()
extern void MutableRuntimeReferenceImageLibrary_GenerateNewGuid_m9E2A885618C9EDD289C6891D181B6BF8002C6732 ();
// 0x000001B2 System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::.ctor()
extern void MutableRuntimeReferenceImageLibrary__ctor_m41DFC85BF0771073834499AED09267690A77DAD1 ();
// 0x000001B3 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::get_Item(System.Int32)
extern void RuntimeReferenceImageLibrary_get_Item_m2A36BD94D16026E02040BF91C477938A6B318EEC ();
// 0x000001B4 System.Int32 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::get_count()
// 0x000001B5 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::GetReferenceImageAt(System.Int32)
// 0x000001B6 System.Void UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::.ctor()
extern void RuntimeReferenceImageLibrary__ctor_m4079D1B22201413ED1E4846D4382B1594B58FFE1 ();
// 0x000001B7 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::.ctor()
extern void XRImageTrackingSubsystem__ctor_mEE7E27E4FDC18721F1D9CDDAAC8FFAACC782D4CF ();
// 0x000001B8 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnStart()
extern void XRImageTrackingSubsystem_OnStart_m556B2CED62A708CCEE2040EE04DF1ACC2660C0EC ();
// 0x000001B9 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnStop()
extern void XRImageTrackingSubsystem_OnStop_mB7F9EF0F3DE842CF24BEBFA23642CEEB793C3979 ();
// 0x000001BA System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnDestroyed()
extern void XRImageTrackingSubsystem_OnDestroyed_mE87276BA554E6C042C660A051A6EA51B42CFF85C ();
// 0x000001BB UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::get_imageLibrary()
extern void XRImageTrackingSubsystem_get_imageLibrary_mCFF108F9559826539F0EEC73BA0092F7544537B8 ();
// 0x000001BC System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
extern void XRImageTrackingSubsystem_set_imageLibrary_m02A6BB33E8C255D9CFEEF478DD6E39BFBD0F036A ();
// 0x000001BD UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void XRImageTrackingSubsystem_CreateRuntimeLibrary_m26FE1D40056DEED2BE3D7A090B375184076792AE ();
// 0x000001BE UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0 ();
// 0x000001BF System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_maxNumberOfMovingImages(System.Int32)
extern void XRImageTrackingSubsystem_set_maxNumberOfMovingImages_m59B8A966406E06A35D55565FE6628158006A32BB ();
// 0x000001C0 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::CreateProvider()
// 0x000001C1 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_supportsMovingImages()
extern void XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_mB0B37BABA6FEECB44860DFFD2A388BBFF48F4F20 ();
// 0x000001C2 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_supportsMovingImages(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_mB13C74324DB6B9D7E3A69826726BFEBA2E403D69 ();
// 0x000001C3 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_requiresPhysicalImageDimensions()
extern void XRImageTrackingSubsystemDescriptor_get_requiresPhysicalImageDimensions_m5A3C84C2BFF8A6B8C0089793F936950F9790CA27 ();
// 0x000001C4 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_requiresPhysicalImageDimensions(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_requiresPhysicalImageDimensions_m1B830E3AA0EEA77F6E7151B6E29E1ABC565D6951 ();
// 0x000001C5 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_supportsMutableLibrary()
extern void XRImageTrackingSubsystemDescriptor_get_supportsMutableLibrary_m52E13375BBF9591EB70335833AF85F0FB1557FEE ();
// 0x000001C6 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_supportsMutableLibrary(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_supportsMutableLibrary_m054D64646762EC2F122B247322EEBCE0005FB4B3 ();
// 0x000001C7 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void XRImageTrackingSubsystemDescriptor_Create_m1049DA7C21F27833846D6C6E699DD2DA964522A9 ();
// 0x000001C8 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void XRImageTrackingSubsystemDescriptor__ctor_mCD5CADD6B43C92838FA1A87926CC23D53A9715C4 ();
// 0x000001C9 System.Void UnityEngine.XR.ARSubsystems.XRReferenceImage::.ctor(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid,System.Nullable`1<UnityEngine.Vector2>,System.String,UnityEngine.Texture2D)
extern void XRReferenceImage__ctor_m65A67C3DF0FBA638484D01E1936B29F79D574E20_AdjustorThunk ();
// 0x000001CA System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImage::get_guid()
extern void XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C_AdjustorThunk ();
// 0x000001CB System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImage::get_textureGuid()
extern void XRReferenceImage_get_textureGuid_m7B247E9E12DB4017B0D4427E59DA33614AD258A5_AdjustorThunk ();
// 0x000001CC System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::get_specifySize()
extern void XRReferenceImage_get_specifySize_mB51499BC0F76BF575ACEB77018EA2BB0AB25CE61_AdjustorThunk ();
// 0x000001CD UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRReferenceImage::get_size()
extern void XRReferenceImage_get_size_m29A6DA526141F214BE2949524305EFE91C07FA32_AdjustorThunk ();
// 0x000001CE System.Single UnityEngine.XR.ARSubsystems.XRReferenceImage::get_width()
extern void XRReferenceImage_get_width_m70D16B42866C9058944D138F6EC2099182790E1F_AdjustorThunk ();
// 0x000001CF System.Single UnityEngine.XR.ARSubsystems.XRReferenceImage::get_height()
extern void XRReferenceImage_get_height_m6107C8991299296D2DB27A312E3D341EE654C414_AdjustorThunk ();
// 0x000001D0 System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::get_name()
extern void XRReferenceImage_get_name_mB454E9E3452D93AC8CCF83A2D1EB1EFA8FD535A9_AdjustorThunk ();
// 0x000001D1 UnityEngine.Texture2D UnityEngine.XR.ARSubsystems.XRReferenceImage::get_texture()
extern void XRReferenceImage_get_texture_m97887B57DD747DCE051484D1C97F1240B673FE16_AdjustorThunk ();
// 0x000001D2 System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::ToString()
extern void XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA_AdjustorThunk ();
// 0x000001D3 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImage::GetHashCode()
extern void XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09_AdjustorThunk ();
// 0x000001D4 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(System.Object)
extern void XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726_AdjustorThunk ();
// 0x000001D5 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6_AdjustorThunk ();
// 0x000001D6 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::op_Equality(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_op_Equality_m1583BEC4A3B41BA40F6B04104595A0A153A1D2DC ();
// 0x000001D7 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_op_Inequality_mA7B9B718C7547A588378425FA39490C701018C3E ();
// 0x000001D8 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_count()
extern void XRReferenceImageLibrary_get_count_mFC2EABE3C3D8966005C0AB2E74836BDC998995DD ();
// 0x000001D9 System.Collections.Generic.List`1_Enumerator<UnityEngine.XR.ARSubsystems.XRReferenceImage> UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::GetEnumerator()
extern void XRReferenceImageLibrary_GetEnumerator_mE48D64D91D797FCFFA8D582B720C68F14D521710 ();
// 0x000001DA UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_Item(System.Int32)
extern void XRReferenceImageLibrary_get_Item_mD672D0FB305F5209E867F2361EAA542524E3A199 ();
// 0x000001DB System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::indexOf(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImageLibrary_indexOf_mCF228BB01B5658DAD82FF40FE4B0086C2FE2AC52 ();
// 0x000001DC System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_guid()
extern void XRReferenceImageLibrary_get_guid_m101D8AFC1E328EBF5DBDED74F7EA8863A3468418 ();
// 0x000001DD System.Void UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::.ctor()
extern void XRReferenceImageLibrary__ctor_mF39B0A6A9C1B2777ADD98CAFF08762B601EA5691 ();
// 0x000001DE System.Void UnityEngine.XR.ARSubsystems.XRTrackedImage::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,System.Guid,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F_AdjustorThunk ();
// 0x000001DF UnityEngine.XR.ARSubsystems.XRTrackedImage UnityEngine.XR.ARSubsystems.XRTrackedImage::get_defaultValue()
extern void XRTrackedImage_get_defaultValue_mC27C0C8BAC99DFBD1900C92FBA0D4940D86468EE ();
// 0x000001E0 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackableId()
extern void XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk ();
// 0x000001E1 System.Guid UnityEngine.XR.ARSubsystems.XRTrackedImage::get_sourceImageId()
extern void XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A_AdjustorThunk ();
// 0x000001E2 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedImage::get_pose()
extern void XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk ();
// 0x000001E3 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRTrackedImage::get_size()
extern void XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk ();
// 0x000001E4 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackingState()
extern void XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk ();
// 0x000001E5 System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedImage::get_nativePtr()
extern void XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF_AdjustorThunk ();
// 0x000001E6 System.Int32 UnityEngine.XR.ARSubsystems.XRTrackedImage::GetHashCode()
extern void XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D_AdjustorThunk ();
// 0x000001E7 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069_AdjustorThunk ();
// 0x000001E8 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(System.Object)
extern void XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF_AdjustorThunk ();
// 0x000001E9 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::op_Equality(UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_op_Equality_m9C6903C11D04AACBB3ADD17C5C41D65724C0D708 ();
// 0x000001EA System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_op_Inequality_mD8EB9C084A8CAA1CDE7AD880A552010BE3693CF0 ();
// 0x000001EB System.Void UnityEngine.XR.ARSubsystems.XRTrackedImage::.cctor()
extern void XRTrackedImage__cctor_m4E42C0412A7516B29FD431E93E6783C09E1F570F ();
// 0x000001EC Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.NativeCopyUtility::PtrToNativeArrayWithDefault(T,System.Void*,System.Int32,System.Int32,Unity.Collections.Allocator)
// 0x000001ED System.Void UnityEngine.XR.ARSubsystems.NativeCopyUtility::FillArrayWithValue(Unity.Collections.NativeArray`1<T>,T)
// 0x000001EE Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.NativeCopyUtility::CreateArrayFilledWithValue(T,System.Int32,Unity.Collections.Allocator)
// 0x000001EF System.Void UnityEngine.XR.ARSubsystems.XRParticipant::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRParticipant__ctor_mAD579360C7F9E8080480CEF09D82A11308281ECF_AdjustorThunk ();
// 0x000001F0 UnityEngine.XR.ARSubsystems.XRParticipant UnityEngine.XR.ARSubsystems.XRParticipant::get_defaultParticipant()
extern void XRParticipant_get_defaultParticipant_m1ACE083807AB7AC1D3C5C6B008B21D6235E0C90D ();
// 0x000001F1 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRParticipant::get_trackableId()
extern void XRParticipant_get_trackableId_mAF0DAE2613E96C830102678EA49DA306402C7700_AdjustorThunk ();
// 0x000001F2 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRParticipant::get_pose()
extern void XRParticipant_get_pose_m9FDF90F628DF1FC812226F06F196A113644C1717_AdjustorThunk ();
// 0x000001F3 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRParticipant::get_trackingState()
extern void XRParticipant_get_trackingState_m759EEC47B61486F19F9312FBBD6B29DD2F0C46FB_AdjustorThunk ();
// 0x000001F4 System.IntPtr UnityEngine.XR.ARSubsystems.XRParticipant::get_nativePtr()
extern void XRParticipant_get_nativePtr_mF0456897D2F54CC3590C2869FF086F6E2B17D123_AdjustorThunk ();
// 0x000001F5 System.Guid UnityEngine.XR.ARSubsystems.XRParticipant::get_sessionId()
extern void XRParticipant_get_sessionId_mA6719BC03C781B0E4A4F303E877BF8DA05916D51_AdjustorThunk ();
// 0x000001F6 System.Int32 UnityEngine.XR.ARSubsystems.XRParticipant::GetHashCode()
extern void XRParticipant_GetHashCode_mA6FFFED9EFF3DA3118B4EA9931BFCFB8AB457CED_AdjustorThunk ();
// 0x000001F7 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::Equals(UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_Equals_m67B3B04FF423328CAAD56ECC253CD4844EC887DD_AdjustorThunk ();
// 0x000001F8 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::Equals(System.Object)
extern void XRParticipant_Equals_mEAC638071C5C48AAB3D4D671F12F7836C7A86F97_AdjustorThunk ();
// 0x000001F9 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::op_Equality(UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_op_Equality_m4BB4AAA3AEE332ABAA42551F45F0132FD0876C38 ();
// 0x000001FA System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::op_Inequality(UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_op_Inequality_mC586A7747F2A9F1D462DC377EA72C065B5CF9352 ();
// 0x000001FB System.Void UnityEngine.XR.ARSubsystems.XRParticipant::.cctor()
extern void XRParticipant__cctor_m76240ACBBABF354611F29C081AC90C77594D43C5 ();
// 0x000001FC System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::.ctor()
extern void XRParticipantSubsystem__ctor_mCDFFB9E0DE58E9CCF9676B99C891CA350E25AADA ();
// 0x000001FD System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnStart()
extern void XRParticipantSubsystem_OnStart_m6E5309E4FAB42A84ADF9EFACFD562B8F5D6F9AE0 ();
// 0x000001FE System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnStop()
extern void XRParticipantSubsystem_OnStop_mC0FF0C8B8390C6C39CF7ECA6AE7FF67EA426F5E5 ();
// 0x000001FF System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnDestroyed()
extern void XRParticipantSubsystem_OnDestroyed_mEE7B0AE9632BEE7C248922680D30B6EAB50CE266 ();
// 0x00000200 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRParticipant> UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRParticipantSubsystem_GetChanges_mAFC1419DAF4C1E3FEDD4B14C2561C4413AFA47BA ();
// 0x00000201 UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::CreateProvider()
// 0x00000202 UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::get_capabilities()
extern void XRParticipantSubsystemDescriptor_get_capabilities_mC77B52797D0A552DE7073B23C3BB84682B700B21 ();
// 0x00000203 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::set_capabilities(UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
extern void XRParticipantSubsystemDescriptor_set_capabilities_m358224840BB1F68335AF4C04FD24F8A025632039 ();
// 0x00000204 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::Register(System.String,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
// 0x00000205 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::.ctor(System.String,System.Type,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
extern void XRParticipantSubsystemDescriptor__ctor_m7F1C1756522C4D4E02ABEDBDC32040B796577F0C ();
// 0x00000206 UnityEngine.XR.ARSubsystems.BoundedPlane UnityEngine.XR.ARSubsystems.BoundedPlane::get_defaultValue()
extern void BoundedPlane_get_defaultValue_mD9C5DCC9919CFB735B2D62B8F4BEF1DAEBA37E89 ();
// 0x00000207 System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.PlaneAlignment,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,UnityEngine.XR.ARSubsystems.PlaneClassification)
extern void BoundedPlane__ctor_mAC435BBCE5DE883B3BC4AD679D399247096419A1_AdjustorThunk ();
// 0x00000208 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackableId()
extern void BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk ();
// 0x00000209 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_subsumedById()
extern void BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk ();
// 0x0000020A UnityEngine.Pose UnityEngine.XR.ARSubsystems.BoundedPlane::get_pose()
extern void BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk ();
// 0x0000020B UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_center()
extern void BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk ();
// 0x0000020C UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_extents()
extern void BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk ();
// 0x0000020D UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_size()
extern void BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk ();
// 0x0000020E UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARSubsystems.BoundedPlane::get_alignment()
extern void BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk ();
// 0x0000020F UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackingState()
extern void BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk ();
// 0x00000210 System.IntPtr UnityEngine.XR.ARSubsystems.BoundedPlane::get_nativePtr()
extern void BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361_AdjustorThunk ();
// 0x00000211 UnityEngine.XR.ARSubsystems.PlaneClassification UnityEngine.XR.ARSubsystems.BoundedPlane::get_classification()
extern void BoundedPlane_get_classification_mBC7152460D4441EE38BE0A9ACC26F31AC810C373_AdjustorThunk ();
// 0x00000212 System.Single UnityEngine.XR.ARSubsystems.BoundedPlane::get_width()
extern void BoundedPlane_get_width_m1E26F5383BD65FF68259DE31FB497B4B376CFA50_AdjustorThunk ();
// 0x00000213 System.Single UnityEngine.XR.ARSubsystems.BoundedPlane::get_height()
extern void BoundedPlane_get_height_mB1F3A68A91EBB34C8206E0DE9EB95D170CCC1E01_AdjustorThunk ();
// 0x00000214 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.BoundedPlane::get_normal()
extern void BoundedPlane_get_normal_m1DBB621B1447071A5C7C5F2966A90459B9481078_AdjustorThunk ();
// 0x00000215 UnityEngine.Plane UnityEngine.XR.ARSubsystems.BoundedPlane::get_plane()
extern void BoundedPlane_get_plane_mB634B619F93280612D0D395F8BD42B8533CEA787_AdjustorThunk ();
// 0x00000216 System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::GetCorners(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void BoundedPlane_GetCorners_m3F9CDB18FF9647D6C37D2FE993578C5EE9C3E125_AdjustorThunk ();
// 0x00000217 System.String UnityEngine.XR.ARSubsystems.BoundedPlane::ToString()
extern void BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1_AdjustorThunk ();
// 0x00000218 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(System.Object)
extern void BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E_AdjustorThunk ();
// 0x00000219 System.Int32 UnityEngine.XR.ARSubsystems.BoundedPlane::GetHashCode()
extern void BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637_AdjustorThunk ();
// 0x0000021A System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::op_Equality(UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_op_Equality_m3D7772E6F97621B26E620EEB821A446C367C0FC2 ();
// 0x0000021B System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::op_Inequality(UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_op_Inequality_m81F75E9FF3A08BAC48BB998A4DCB979E8362E318 ();
// 0x0000021C System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D_AdjustorThunk ();
// 0x0000021D System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::.cctor()
extern void BoundedPlane__cctor_m1357E51DCD027D6C83B0F1289A93F70653CA92B4 ();
// 0x0000021E System.Boolean UnityEngine.XR.ARSubsystems.PlaneAlignmentExtensions::IsHorizontal(UnityEngine.XR.ARSubsystems.PlaneAlignment)
extern void PlaneAlignmentExtensions_IsHorizontal_m291A9197CC11F985A33F496091D22AE0B9C42D14 ();
// 0x0000021F System.Boolean UnityEngine.XR.ARSubsystems.PlaneAlignmentExtensions::IsVertical(UnityEngine.XR.ARSubsystems.PlaneAlignment)
extern void PlaneAlignmentExtensions_IsVertical_m0E43C68ABD9D0FF5AF5424EDAD403FAAFBB6B2E0 ();
// 0x00000220 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::.ctor()
extern void XRPlaneSubsystem__ctor_mC6CCE81B1FE634A34E37D1595EC6189A6D5B28E1 ();
// 0x00000221 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnStart()
extern void XRPlaneSubsystem_OnStart_mEC0E5DACF25139BDB8E43291D30B0C89E4AEDFFA ();
// 0x00000222 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnDestroyed()
extern void XRPlaneSubsystem_OnDestroyed_m2E16F97A27B3BA56A7B4D30640CBA36598685B11 ();
// 0x00000223 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnStop()
extern void XRPlaneSubsystem_OnStop_mC82629B43719E1ED60448DFB754F41B140ED005F ();
// 0x00000224 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void XRPlaneSubsystem_set_planeDetectionMode_mC7B2B3A8A0FB7853FBA4227F3A4DFD8A155E14DD ();
// 0x00000225 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B ();
// 0x00000226 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void XRPlaneSubsystem_GetBoundary_mB724A7CF46B3AF8C7E01A0A56854F46C595314CC ();
// 0x00000227 UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateProvider()
// 0x00000228 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateOrResizeNativeArrayIfNecessary(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&)
// 0x00000229 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsHorizontalPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsHorizontalPlaneDetection_mED6CA8897E68F81C48C753D71DC8644CBBE7C350 ();
// 0x0000022A System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m834174F0D747E2D8D8C34EB20D879FFA3E607849 ();
// 0x0000022B System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsVerticalPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsVerticalPlaneDetection_m074CCD30970971A023FAFEB246FD59B546C21374 ();
// 0x0000022C System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsVerticalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_mF2E7FC43C5A6D048BE8A13F7824CC76543C28AB4 ();
// 0x0000022D System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsArbitraryPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsArbitraryPlaneDetection_m016EA52D18A21472F28FBAACD4A30969E0891290 ();
// 0x0000022E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_mCC9B359F247DFD0928E3D968CBD7713C744403F6 ();
// 0x0000022F System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsBoundaryVertices()
extern void XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m455A7A695D7C4F238405B4B50472BE0A8118F611 ();
// 0x00000230 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsBoundaryVertices(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_m79DA28E3ED06B14E1FEC3EEB72986EA1A420A15E ();
// 0x00000231 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsClassification()
extern void XRPlaneSubsystemDescriptor_get_supportsClassification_m7714830FB71FEFF2AD8C7A35E73E25E9CA9209D8 ();
// 0x00000232 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsClassification(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsClassification_mD90B7F7175BF1868A8203B2D91BEDC498469F53C ();
// 0x00000233 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void XRPlaneSubsystemDescriptor_Create_mE7A8E8E49F7EB078CE4D76C9F0D883634157EC9C ();
// 0x00000234 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void XRPlaneSubsystemDescriptor__ctor_mFF66A6FD68D7DBC951F675E15E230CEADC37B42C ();
// 0x00000235 System.Boolean UnityEngine.XR.ARSubsystems.Promise`1::get_keepWaiting()
// 0x00000236 T UnityEngine.XR.ARSubsystems.Promise`1::get_result()
// 0x00000237 System.Void UnityEngine.XR.ARSubsystems.Promise`1::set_result(T)
// 0x00000238 UnityEngine.XR.ARSubsystems.Promise`1<T> UnityEngine.XR.ARSubsystems.Promise`1::CreateResolvedPromise(T)
// 0x00000239 System.Void UnityEngine.XR.ARSubsystems.Promise`1::Resolve(T)
// 0x0000023A System.Void UnityEngine.XR.ARSubsystems.Promise`1::OnKeepWaiting()
// 0x0000023B System.Void UnityEngine.XR.ARSubsystems.Promise`1::.ctor()
// 0x0000023C UnityEngine.XR.ARSubsystems.XRRaycastHit UnityEngine.XR.ARSubsystems.XRRaycastHit::get_defaultValue()
extern void XRRaycastHit_get_defaultValue_m17AEBDAC971A56C3FC4C7C4E2E14ECC357658DFA ();
// 0x0000023D UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycastHit::get_trackableId()
extern void XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk ();
// 0x0000023E System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRRaycastHit_set_trackableId_m8EE8F31C6CC0A5F22C206BB1B020029E9D6E4E3F_AdjustorThunk ();
// 0x0000023F UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRRaycastHit::get_pose()
extern void XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk ();
// 0x00000240 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_pose(UnityEngine.Pose)
extern void XRRaycastHit_set_pose_mF79DD76A43B1E6075095517E645F0C2C38864A54_AdjustorThunk ();
// 0x00000241 System.Single UnityEngine.XR.ARSubsystems.XRRaycastHit::get_distance()
extern void XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE_AdjustorThunk ();
// 0x00000242 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_distance(System.Single)
extern void XRRaycastHit_set_distance_mBE7929E6C3D4F4AA7AA6B834CDB1E9A3DAB5C90A_AdjustorThunk ();
// 0x00000243 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastHit::get_hitType()
extern void XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk ();
// 0x00000244 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_hitType(UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastHit_set_hitType_m6B306C7B344FD45B9343DA72C16354A2A0C1F781_AdjustorThunk ();
// 0x00000245 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,System.Single,UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5_AdjustorThunk ();
// 0x00000246 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastHit::GetHashCode()
extern void XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA_AdjustorThunk ();
// 0x00000247 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(System.Object)
extern void XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862_AdjustorThunk ();
// 0x00000248 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C_AdjustorThunk ();
// 0x00000249 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::op_Equality(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_op_Equality_mC953B64E3B0A0A69D69F0E438FA88D26A537764F ();
// 0x0000024A System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::op_Inequality(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_op_Inequality_m76F437956D65EFE583A6C3FC80534CABEE699EA3 ();
// 0x0000024B System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::.cctor()
extern void XRRaycastHit__cctor_mF74B94EA0D60B0112CDB7F37D439239F187E55C4 ();
// 0x0000024C System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::.ctor()
extern void XRRaycastSubsystem__ctor_mD6FC049FC72B869A2A78EE56093819D989AD4021 ();
// 0x0000024D System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnStart()
extern void XRRaycastSubsystem_OnStart_mDCBB868298D7E902CD0F451B6269D6335393667D ();
// 0x0000024E System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnStop()
extern void XRRaycastSubsystem_OnStop_m7D9BA78977F77076C34E8827033AB470D454DBDE ();
// 0x0000024F System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnDestroyed()
extern void XRRaycastSubsystem_OnDestroyed_m7C95CCEF2EF0D515353FFA806CACFE3C02BD8BB4 ();
// 0x00000250 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA ();
// 0x00000251 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF ();
// 0x00000252 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::CreateProvider()
// 0x00000253 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsViewportBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_m58C8F3A796EEB498A31A9BEE387A37E935121388 ();
// 0x00000254 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsViewportBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_mD39B9FA29B589E0DF23DE7A21012058C0505402C ();
// 0x00000255 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsWorldBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_mA8C8F4A9E9B0B85E6BE432488CBCF9A97A5E5F4A ();
// 0x00000256 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsWorldBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m4D35B87B7D4284B470A41F46E5261FE953B75035 ();
// 0x00000257 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportedTrackableTypes()
extern void XRRaycastSubsystemDescriptor_get_supportedTrackableTypes_m02F17127CFA033A9D6D84C7F0D53D0BA3FE379C4 ();
// 0x00000258 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mE17AF0F588A87E741576518F5C851286937BFE27 ();
// 0x00000259 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void XRRaycastSubsystemDescriptor_RegisterDescriptor_mA164B987D51AD208D957753220E5B1D1A2DB0650 ();
// 0x0000025A System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void XRRaycastSubsystemDescriptor__ctor_m69D7B8410396340E3D54A19B7EE73B4D24562C3C ();
// 0x0000025B System.Void UnityEngine.XR.ARSubsystems.SerializableGuid::.ctor(System.UInt64,System.UInt64)
extern void SerializableGuid__ctor_mCB8E3B766CF1C54AA9D17E738C65E47B16ECE296_AdjustorThunk ();
// 0x0000025C UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.SerializableGuid::get_empty()
extern void SerializableGuid_get_empty_mCDC698E4D3EE9F3B311588C6FC1EE7CC9E820892 ();
// 0x0000025D System.Guid UnityEngine.XR.ARSubsystems.SerializableGuid::get_guid()
extern void SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B_AdjustorThunk ();
// 0x0000025E System.Int32 UnityEngine.XR.ARSubsystems.SerializableGuid::GetHashCode()
extern void SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D_AdjustorThunk ();
// 0x0000025F System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(System.Object)
extern void SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994_AdjustorThunk ();
// 0x00000260 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString()
extern void SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C_AdjustorThunk ();
// 0x00000261 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString(System.String)
extern void SerializableGuid_ToString_m5A32B3DAD7464E66B832B85D8E625D32A634B21C_AdjustorThunk ();
// 0x00000262 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString(System.String,System.IFormatProvider)
extern void SerializableGuid_ToString_m221CA635D117768CCA6A603C67FE1C391954F338_AdjustorThunk ();
// 0x00000263 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC_AdjustorThunk ();
// 0x00000264 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::op_Equality(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_op_Equality_mFA22287F5A8B93FA4C8897FAE942491C9D72D621 ();
// 0x00000265 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::op_Inequality(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_op_Inequality_mBEDA8B91A7B6D53F2CF4009020AE2501A7451C63 ();
// 0x00000266 System.Void UnityEngine.XR.ARSubsystems.SerializableGuid::.cctor()
extern void SerializableGuid__cctor_mBD80AFE51A38B55A6C19129375843F41B9FAA51F ();
// 0x00000267 System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsSupported(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsSupported_mE7271B43B00DD99C98FA00AAAE3A948E0822D624 ();
// 0x00000268 System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsInstalled(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsInstalled_m199B1D6F5729DE0A1BC7144BE38DEF9896B68A33 ();
// 0x00000269 System.IntPtr UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_nativePtr()
extern void XRSessionSubsystem_get_nativePtr_m7FF5CD90265F723DEA0F552BD0D0E96BFF4011BA ();
// 0x0000026A System.Guid UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_sessionId()
extern void XRSessionSubsystem_get_sessionId_mE74B6851048615893D7D4368D3F292314D158D0B ();
// 0x0000026B UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::GetAvailabilityAsync()
extern void XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED ();
// 0x0000026C UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::InstallAsync()
extern void XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC ();
// 0x0000026D System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::.ctor()
extern void XRSessionSubsystem__ctor_m0E7B9E65E53B03A65C53F87CF55E76528E9AF62A ();
// 0x0000026E System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnStart()
extern void XRSessionSubsystem_OnStart_m13F2A47C585607D4B8EB53841958FDFD610256E0 ();
// 0x0000026F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Reset()
extern void XRSessionSubsystem_Reset_m09A1C906E574CD27377602632572058C88958773 ();
// 0x00000270 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnStop()
extern void XRSessionSubsystem_OnStop_m7D5FBBD4A3BA07ECF6D319F972FD09048D339D72 ();
// 0x00000271 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnDestroyed()
extern void XRSessionSubsystem_OnDestroyed_m9698BDE2A34F1C84EC4F1ED3E5582EA8953172BE ();
// 0x00000272 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionSubsystem_Update_m40F8405ECB47FDC56B0B203F09655E3E5F637EFB ();
// 0x00000273 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationPause()
extern void XRSessionSubsystem_OnApplicationPause_m5030BDDD5E7BB722D7259D98C988911335751945 ();
// 0x00000274 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationResume()
extern void XRSessionSubsystem_OnApplicationResume_m47B8B29B90F5DDF27221EE1E32D5DBA22C347FBD ();
// 0x00000275 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_trackingState()
extern void XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30 ();
// 0x00000276 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_notTrackingReason()
extern void XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7 ();
// 0x00000277 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_matchFrameRate()
extern void XRSessionSubsystem_get_matchFrameRate_mC750A3D6F4EA06E45880B76305178E568E04C82A ();
// 0x00000278 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_matchFrameRate(System.Boolean)
extern void XRSessionSubsystem_set_matchFrameRate_mAE3C92CA2CA959912C6AF7AB1BD10B2B9A9FC54C ();
// 0x00000279 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_frameRate()
extern void XRSessionSubsystem_get_frameRate_m0E723AB90D900D24D7DA2F5A2A8009CB4946A62B ();
// 0x0000027A UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider UnityEngine.XR.ARSubsystems.XRSessionSubsystem::CreateProvider()
// 0x0000027B System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsInstall()
extern void XRSessionSubsystemDescriptor_get_supportsInstall_m6A46D829025B00AFFCEE06A8A66A3D383AF2757E ();
// 0x0000027C System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsInstall(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsInstall_m8660DC05AA837607AC160D4300E7D1D204BD1E18 ();
// 0x0000027D System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsMatchFrameRate()
extern void XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m331D3B1192D04F6EF98D9172D70EC35A5B24A02B ();
// 0x0000027E System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsMatchFrameRate(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_m9371FEB427307794FC0EB7DF361E2F47E6E3F378 ();
// 0x0000027F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void XRSessionSubsystemDescriptor_RegisterDescriptor_m3EF9E7985B16FFF8FE15FBEDFC87FF1BB811D49E ();
// 0x00000280 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void XRSessionSubsystemDescriptor__ctor_m1956ED91DA6DA8D4EAA62906C378B69F5F3E8C1E ();
// 0x00000281 UnityEngine.ScreenOrientation UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenOrientation()
extern void XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk ();
// 0x00000282 System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12_AdjustorThunk ();
// 0x00000283 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenDimensions()
extern void XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk ();
// 0x00000284 System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenDimensions(UnityEngine.Vector2Int)
extern void XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635_AdjustorThunk ();
// 0x00000285 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::GetHashCode()
extern void XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70_AdjustorThunk ();
// 0x00000286 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(System.Object)
extern void XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92_AdjustorThunk ();
// 0x00000287 System.String UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::ToString()
extern void XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D_AdjustorThunk ();
// 0x00000288 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8_AdjustorThunk ();
// 0x00000289 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::op_Equality(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_op_Equality_m5E717326C2C77A0DB0CA78CA5C1FF63B50955293 ();
// 0x0000028A System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_op_Inequality_m9E436001D2E4D4B649F432824BFB803BE6C7B86D ();
// 0x0000028B UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::get_invalidId()
extern void TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD ();
// 0x0000028C System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId1()
extern void TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D_AdjustorThunk ();
// 0x0000028D System.Void UnityEngine.XR.ARSubsystems.TrackableId::set_subId1(System.UInt64)
extern void TrackableId_set_subId1_mB5663FA9DA4D0036732A967F0C36F8A5B6604E06_AdjustorThunk ();
// 0x0000028E System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId2()
extern void TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0_AdjustorThunk ();
// 0x0000028F System.Void UnityEngine.XR.ARSubsystems.TrackableId::set_subId2(System.UInt64)
extern void TrackableId_set_subId2_mEBB08EA635B97F94BD22E8034E35CB0CFDF0FA8E_AdjustorThunk ();
// 0x00000290 System.Void UnityEngine.XR.ARSubsystems.TrackableId::.ctor(System.UInt64,System.UInt64)
extern void TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8_AdjustorThunk ();
// 0x00000291 System.String UnityEngine.XR.ARSubsystems.TrackableId::ToString()
extern void TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2_AdjustorThunk ();
// 0x00000292 System.Int32 UnityEngine.XR.ARSubsystems.TrackableId::GetHashCode()
extern void TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328_AdjustorThunk ();
// 0x00000293 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(System.Object)
extern void TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28_AdjustorThunk ();
// 0x00000294 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2_AdjustorThunk ();
// 0x00000295 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::op_Equality(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_op_Equality_mDC7BF74A9CA0E8F34E4BD3C584918380A624D3E8 ();
// 0x00000296 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::op_Inequality(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_op_Inequality_mFBEAC6D64B8228DF26E71F7F78CE2B192C55A4BF ();
// 0x00000297 System.Void UnityEngine.XR.ARSubsystems.TrackableId::.cctor()
extern void TrackableId__cctor_m874A4E0F53AF39A7D95AEA9A0E8164BA2B6EA32F ();
// 0x00000298 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.ITrackable::get_trackableId()
// 0x00000299 UnityEngine.Pose UnityEngine.XR.ARSubsystems.ITrackable::get_pose()
// 0x0000029A UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.ITrackable::get_trackingState()
// 0x0000029B System.IntPtr UnityEngine.XR.ARSubsystems.ITrackable::get_nativePtr()
// 0x0000029C Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_added()
// 0x0000029D Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_updated()
// 0x0000029E Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_removed()
// 0x0000029F System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_isCreated()
// 0x000002A0 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::set_isCreated(System.Boolean)
// 0x000002A1 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
// 0x000002A2 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator,T)
// 0x000002A3 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
// 0x000002A4 UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
// 0x000002A5 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::Dispose()
// 0x000002A6 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
// 0x000002A7 UnityEngine.XR.ARSubsystems.TrackableChanges`1<TTrackable> UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::GetChanges(Unity.Collections.Allocator)
// 0x000002A8 System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::.ctor()
// 0x000002A9 System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::ValidateAndThrow(UnityEngine.XR.ARSubsystems.TrackableChanges`1<T>)
// 0x000002AA System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::ValidateAndDisposeIfThrown(UnityEngine.XR.ARSubsystems.TrackableChanges`1<T>)
// 0x000002AB System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::AddToSetAndThrowIfDuplicate(UnityEngine.XR.ARSubsystems.TrackableId,System.Boolean,System.String)
// 0x000002AC System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::.ctor()
// 0x000002AD System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::.cctor()
// 0x000002AE System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnStart()
// 0x000002AF System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnStop()
// 0x000002B0 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnDestroyed()
// 0x000002B1 System.Boolean UnityEngine.XR.ARSubsystems.XRSubsystem`1::get_running()
// 0x000002B2 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnDestroy()
// 0x000002B3 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::Start()
// 0x000002B4 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::Stop()
// 0x000002B5 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::.ctor()
// 0x000002B6 System.IntPtr UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_nativeTexture()
extern void XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B_AdjustorThunk ();
// 0x000002B7 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_nativeTexture(System.IntPtr)
extern void XRTextureDescriptor_set_nativeTexture_m037F49B441D20D89651EFC2D76B7303094FB01F6_AdjustorThunk ();
// 0x000002B8 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_width()
extern void XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04_AdjustorThunk ();
// 0x000002B9 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_width(System.Int32)
extern void XRTextureDescriptor_set_width_m43B69CA429B7BEF091AAA695EDB6C96B6F575B97_AdjustorThunk ();
// 0x000002BA System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_height()
extern void XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755_AdjustorThunk ();
// 0x000002BB System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_height(System.Int32)
extern void XRTextureDescriptor_set_height_m294373970C66CDC1A8ACDCBBCC49F19E39FB6F61_AdjustorThunk ();
// 0x000002BC System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_mipmapCount()
extern void XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3_AdjustorThunk ();
// 0x000002BD System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_mipmapCount(System.Int32)
extern void XRTextureDescriptor_set_mipmapCount_m9EEB76516BF044C734C20D98400B628711ECC1F1_AdjustorThunk ();
// 0x000002BE UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_format()
extern void XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk ();
// 0x000002BF System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_format(UnityEngine.TextureFormat)
extern void XRTextureDescriptor_set_format_m50B0219DA4339E2C4D270FCE5CE5645D192D785B_AdjustorThunk ();
// 0x000002C0 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_propertyNameId()
extern void XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80_AdjustorThunk ();
// 0x000002C1 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_propertyNameId(System.Int32)
extern void XRTextureDescriptor_set_propertyNameId_m05A26233A1485B768874953F567B2EF7AFA2C39C_AdjustorThunk ();
// 0x000002C2 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_valid()
extern void XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE_AdjustorThunk ();
// 0x000002C3 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::hasIdenticalTextureMetadata(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8_AdjustorThunk ();
// 0x000002C4 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Reset()
extern void XRTextureDescriptor_Reset_mACE0F00A599DCF65581C7363C1B80C178242B065_AdjustorThunk ();
// 0x000002C5 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB_AdjustorThunk ();
// 0x000002C6 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(System.Object)
extern void XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5_AdjustorThunk ();
// 0x000002C7 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::op_Equality(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_op_Equality_mFD1888CDCE7DB12A55D71764C96D78106AEFF04D ();
// 0x000002C8 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::op_Inequality(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_op_Inequality_m41878BCABB8BA2BFF564EADBB0B277A06A4D50D4 ();
// 0x000002C9 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::GetHashCode()
extern void XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD_AdjustorThunk ();
// 0x000002CA System.String UnityEngine.XR.ARSubsystems.XRTextureDescriptor::ToString()
extern void XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2_AdjustorThunk ();
// 0x000002CB System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Start()
extern void Provider_Start_m8C851B71F2BA5DB2E35E21A5C246D51D014E7BCE ();
// 0x000002CC System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Stop()
extern void Provider_Stop_m9354EC0691E8052B903375FFC605B0D9CAF96B76 ();
// 0x000002CD System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Destroy()
extern void Provider_Destroy_m23E1FFE47E35B396EF1442C2982D65C61E33D505 ();
// 0x000002CE UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
// 0x000002CF System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void Provider_TryAddAnchor_m33AF5D97A8A579C4C7052688B990BD23B5412599 ();
// 0x000002D0 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void Provider_TryAttachAnchor_m593EAA09284A4A0B28C4916A477A1BC3E4E37015 ();
// 0x000002D1 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_TryRemoveAnchor_m137C88B267C5579CCC2CF50D13AD6F718C801EE2 ();
// 0x000002D2 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::.ctor()
extern void Provider__ctor_m52F6A325C30DBE037094A0F03FE674C11362C77A ();
// 0x000002D3 System.String UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_mAC0D6F0EE0BFBCF1981F31E4CC1D7FBB50058223_AdjustorThunk ();
// 0x000002D4 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mC90D1044C7FE3F23A39E37B9AB475BCAE7F60E6D_AdjustorThunk ();
// 0x000002D5 System.Type UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_mABD347CF43B54CF9519A4D574ECE2BCFCF7856EC_AdjustorThunk ();
// 0x000002D6 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_mB078866AA16F3E677F5E35E16549748990903BC2_AdjustorThunk ();
// 0x000002D7 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_supportsTrackableAttachments()
extern void Cinfo_get_supportsTrackableAttachments_m63525509E033418A2D2E8D8DD2DB4F1617F54002_AdjustorThunk ();
// 0x000002D8 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_supportsTrackableAttachments(System.Boolean)
extern void Cinfo_set_supportsTrackableAttachments_m5B26FD00AEE893C614CE7E9E6C4356D4F69A015D_AdjustorThunk ();
// 0x000002D9 System.Int32 UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mE01CBCB65F9B293FFF906D9CB9ADA20C6F9F8B61_AdjustorThunk ();
// 0x000002DA System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mFFB6723FA7AC8AA1E1E8884493D0CDF35817F734_AdjustorThunk ();
// 0x000002DB System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m0B77F3FC1837038B76DA69AD8663F3CC55D49610_AdjustorThunk ();
// 0x000002DC System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m48A2F12F777C202965C797DA3C698B71A0D74B3D ();
// 0x000002DD System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m2817AAD7A107F0F76F21A6A4D8FAB09988EAD5C1 ();
// 0x000002DE UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_cameraMaterial()
extern void Provider_get_cameraMaterial_m82824265BAB7CF990BA7F189A7DBAC583B06D7C4 ();
// 0x000002DF System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_permissionGranted()
extern void Provider_get_permissionGranted_mDE63F618AFC3A029DE7E385BBBA9E76440848945 ();
// 0x000002E0 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_invertCulling()
extern void Provider_get_invertCulling_m59869AA95B9EBE03DE2D27823F628D18566A500A ();
// 0x000002E1 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Start()
extern void Provider_Start_m1E551A4059E6BDC166AE2D8FEB5A9076D623AE1D ();
// 0x000002E2 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Stop()
extern void Provider_Stop_mFCF6A1C3AC86A90AC583045364AE614AC6100E1F ();
// 0x000002E3 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Destroy()
extern void Provider_Destroy_m9A3E04CAE42529F3587873CB8BA73B5217293BE4 ();
// 0x000002E4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void Provider_TryGetFrame_mADCD409D367FCAD1D91F0AC1ED9A3CC836CB134E ();
// 0x000002E5 UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_cameraFocusMode()
extern void Provider_get_cameraFocusMode_m7EE0C12F43F40239D19ABB3F9DEB3F786E7AC027 ();
// 0x000002E6 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_cameraFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void Provider_set_cameraFocusMode_m517F19394BAA8F41728780B42E448F36AEED9F78 ();
// 0x000002E7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void Provider_TrySetLightEstimationMode_m14CDBB2644C0ED1433C4CEA6FB10C3EBA4C16560 ();
// 0x000002E8 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void Provider_TryGetIntrinsics_m8A592B09EC5B6DB37A665C2B276736932B63FCA1 ();
// 0x000002E9 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void Provider_GetConfigurations_m9FD5BBC0B7C6270E7371BC88EC5EB0B38CBEACE2 ();
// 0x000002EA System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_currentConfiguration()
extern void Provider_get_currentConfiguration_m7F407DC6CC63A4DA3D60FB4CE62D2C80F97A9D9B ();
// 0x000002EB System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void Provider_set_currentConfiguration_mCBE318AC26E52C03758632E78AD1F1EF5291B4C5 ();
// 0x000002EC Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void Provider_GetTextureDescriptors_mFC6F6C5A3B399ACAD7A8E78A93E73ABA8197F4D3 ();
// 0x000002ED System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo&)
extern void Provider_TryAcquireLatestImage_m49208EE7FA4A3104978EBEA865DB3CDED56B1679 ();
// 0x000002EE UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetAsyncRequestStatus(System.Int32)
extern void Provider_GetAsyncRequestStatus_mC46D89C12B6EBD90BC37DC4007B9A5462DE01E56 ();
// 0x000002EF System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::DisposeImage(System.Int32)
extern void Provider_DisposeImage_m2E937A0EAEB2AD3EE55875A4F80EDBF9C099A914 ();
// 0x000002F0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::DisposeAsyncRequest(System.Int32)
extern void Provider_DisposeAsyncRequest_mAE9436B72F3EA27F1118B6142D6FD78DFAE95669 ();
// 0x000002F1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo&)
extern void Provider_TryGetPlane_m58B3A21E6D2D01DC824153FB44A9E71C08334F07 ();
// 0x000002F2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::NativeHandleValid(System.Int32)
extern void Provider_NativeHandleValid_mA0213BCC7EACEAA8CDFACC686485CFA60702C729 ();
// 0x000002F3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void Provider_TryGetConvertedDataSize_mA677B9758E7F5B9E4388F484050B039164072FA3 ();
// 0x000002F4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32)
extern void Provider_TryConvert_m5C2DEE74EE49310B195FF0D7E8C6ACAD3DE9F057 ();
// 0x000002F5 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void Provider_ConvertAsync_mB43A1E6B21B6B136EC2685CBD67FB9C6CA0ADE7F ();
// 0x000002F6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetAsyncRequestData(System.Int32,System.IntPtr&,System.Int32&)
extern void Provider_TryGetAsyncRequestData_mD5AB278261ECB97E7C00D143600D26557858C367 ();
// 0x000002F7 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate,System.IntPtr)
extern void Provider_ConvertAsync_m701AE2B6FBFA7D6F4783D178FCB7B0598AE6C016 ();
// 0x000002F8 UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::CreateCameraMaterial(System.String)
extern void Provider_CreateCameraMaterial_mC7C8A8DB289C964715A06F0BB71AB3160FDBA1D9 ();
// 0x000002F9 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::.ctor()
extern void Provider__ctor_m7B7ACE576C7DDE24A0AAEF774AC6AA35834FC8B0 ();
// 0x000002FA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void OnImageRequestCompleteDelegate__ctor_mD9BE79ADAAEE0A44060E897728EA37196AF5ED47 ();
// 0x000002FB System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate::Invoke(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void OnImageRequestCompleteDelegate_Invoke_m835EE17741E50584491577BD5B9061FB12EB95F0 ();
// 0x000002FC System.IAsyncResult UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate::BeginInvoke(UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnImageRequestCompleteDelegate_BeginInvoke_m3102FE931688E2734845C0CB9FED184CA15B48AC ();
// 0x000002FD System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate::EndInvoke(System.IAsyncResult)
extern void OnImageRequestCompleteDelegate_EndInvoke_m17DDA25840DD080989CEC9304CBD369D65AC82AD ();
// 0x000002FE System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::get_nativeHandle()
extern void CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419_AdjustorThunk ();
// 0x000002FF UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::get_dimensions()
extern void CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED_AdjustorThunk ();
// 0x00000300 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::get_planeCount()
extern void CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6_AdjustorThunk ();
// 0x00000301 System.Double UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::get_timestamp()
extern void CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2_AdjustorThunk ();
// 0x00000302 UnityEngine.XR.ARSubsystems.CameraImageFormat UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::get_format()
extern void CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F_AdjustorThunk ();
// 0x00000303 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::.ctor(System.Int32,UnityEngine.Vector2Int,System.Int32,System.Double,UnityEngine.XR.ARSubsystems.CameraImageFormat)
extern void CameraImageCinfo__ctor_m0D4E4A0EDDB0E69C7090DEB9D83D5E509051AA4E_AdjustorThunk ();
// 0x00000304 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo)
extern void CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8_AdjustorThunk ();
// 0x00000305 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::Equals(System.Object)
extern void CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8_AdjustorThunk ();
// 0x00000306 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo)
extern void CameraImageCinfo_op_Equality_m637A1532BF8201D778300760BBE07AD54798ED44 ();
// 0x00000307 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo)
extern void CameraImageCinfo_op_Inequality_mBC319EA25C09E3E56AF9CFDED4851A610118B599 ();
// 0x00000308 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::GetHashCode()
extern void CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE_AdjustorThunk ();
// 0x00000309 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo::ToString()
extern void CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7_AdjustorThunk ();
// 0x0000030A System.IntPtr UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::get_dataPtr()
extern void CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5_AdjustorThunk ();
// 0x0000030B System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::get_dataLength()
extern void CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22_AdjustorThunk ();
// 0x0000030C System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::get_rowStride()
extern void CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3_AdjustorThunk ();
// 0x0000030D System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::get_pixelStride()
extern void CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A_AdjustorThunk ();
// 0x0000030E System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::.ctor(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void CameraImagePlaneCinfo__ctor_m2623B9CDD7119BB1856EB40F5F53A52EC77338DF_AdjustorThunk ();
// 0x0000030F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo)
extern void CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A_AdjustorThunk ();
// 0x00000310 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::Equals(System.Object)
extern void CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE_AdjustorThunk ();
// 0x00000311 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo)
extern void CameraImagePlaneCinfo_op_Equality_mB630411D1A473447EA9D2B8147804C08E89A33D0 ();
// 0x00000312 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo)
extern void CameraImagePlaneCinfo_op_Inequality_m10D27EDCFFCEF3E8A490E753F5B40837E6C77F8D ();
// 0x00000313 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::GetHashCode()
extern void CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162_AdjustorThunk ();
// 0x00000314 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo::ToString()
extern void CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD_AdjustorThunk ();
// 0x00000315 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Start()
extern void Provider_Start_m55D0D69DA1EB55A51D5CA298C7159649513772FD ();
// 0x00000316 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Stop()
extern void Provider_Stop_m8E387BA8DE2006668F390169248D1B5EA4E5FE86 ();
// 0x00000317 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Destroy()
extern void Provider_Destroy_mAED2A0A426BCC36ED705D6A37AA9FC49826B15B5 ();
// 0x00000318 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRPointCloud,Unity.Collections.Allocator)
// 0x00000319 UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
// 0x0000031A System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::.ctor()
extern void Provider__ctor_m5138BEFB55280F1416C10DAF84C39ACE4AF3BCC1 ();
// 0x0000031B System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsFeaturePoints()
extern void Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385_AdjustorThunk ();
// 0x0000031C System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsFeaturePoints(System.Boolean)
extern void Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360_AdjustorThunk ();
// 0x0000031D System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsConfidence()
extern void Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0_AdjustorThunk ();
// 0x0000031E System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsConfidence(System.Boolean)
extern void Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C_AdjustorThunk ();
// 0x0000031F System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsUniqueIds()
extern void Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807_AdjustorThunk ();
// 0x00000320 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsUniqueIds(System.Boolean)
extern void Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4_AdjustorThunk ();
// 0x00000321 UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Capabilities UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_capabilities()
extern void Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk ();
// 0x00000322 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_capabilities(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Capabilities)
extern void Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E_AdjustorThunk ();
// 0x00000323 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED_AdjustorThunk ();
// 0x00000324 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E_AdjustorThunk ();
// 0x00000325 System.Int32 UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58_AdjustorThunk ();
// 0x00000326 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m62CDABCB830B7C6CB3374E9AC63B1F65DE2BAA13 ();
// 0x00000327 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m42029F02D73CBB73B559956028B31B4937C3D418 ();
// 0x00000328 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Start()
extern void Provider_Start_mAFCFA17EF4ACFE9FBC08B13F134090166C58769B ();
// 0x00000329 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Stop()
extern void Provider_Stop_m3E3C80C510852F9FB52EC7545A13B54966F74274 ();
// 0x0000032A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Destroy()
extern void Provider_Destroy_mC7C05E996737BBB1329DDF749D606F300C1B6997 ();
// 0x0000032B System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::SetAutomaticPlacement(System.Boolean)
extern void Provider_SetAutomaticPlacement_m23F1429651885D6F7C52D08A9CF830BFCA39E3F9 ();
// 0x0000032C System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::TrySetEnvironmentTextureHDREnabled(System.Boolean)
extern void Provider_TrySetEnvironmentTextureHDREnabled_mDB08C8517BE14AAA988ECE63B602E18F280A7511 ();
// 0x0000032D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void Provider_TryAddEnvironmentProbe_m2545C50FB75912508F2C47C3A21110B1DFC13CDD ();
// 0x0000032E System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_RemoveEnvironmentProbe_mD545F8CFF2F119ED73A599872B0334663BEC605D ();
// 0x0000032F UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,Unity.Collections.Allocator)
// 0x00000330 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::.ctor()
extern void Provider__ctor_mEC07F574A6B6CB7E8FB489D25BEECA335CA612EC ();
// 0x00000331 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Start()
extern void Provider_Start_m602C2D2B0EE7BB0D9ACD710E52701925452282E7 ();
// 0x00000332 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Stop()
extern void Provider_Stop_m5198663A8E3B916507D39C2DCE156332D374308F ();
// 0x00000333 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Destroy()
extern void Provider_Destroy_mB56D0D2459279C2AA9910C11CFAC112FCBBE3260 ();
// 0x00000334 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void Provider_GetFaceMesh_mE947DDC1126A9BD41BD9B252DDA7755200DF1A04 ();
// 0x00000335 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRFace,Unity.Collections.Allocator)
// 0x00000336 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::get_supportedFaceCount()
extern void Provider_get_supportedFaceCount_m3376126BA49A086BB0CAAD562498D6FE462C047C ();
// 0x00000337 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::get_maximumFaceCount()
extern void Provider_get_maximumFaceCount_m2A8549F526067C1CBD00975719F2FEC37D7365D8 ();
// 0x00000338 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::set_maximumFaceCount(System.Int32)
extern void Provider_set_maximumFaceCount_m91DF83E9C467C622B415B744C790B25D0C3F08FF ();
// 0x00000339 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::.ctor()
extern void Provider__ctor_m4BF4C3C6CA02410A5EC47A5C7FF50D08AE7498B0 ();
// 0x0000033A System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::.ctor(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary)
extern void Enumerator__ctor_mDA14A75D2CAC675AB6AE4FA7FDCBCD54AAE7CE39_AdjustorThunk ();
// 0x0000033B System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::MoveNext()
extern void Enumerator_MoveNext_mF47691FD5F45BCC88BDCB9D3E30B261CCEC7694F_AdjustorThunk ();
// 0x0000033C UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::get_Current()
extern void Enumerator_get_Current_mB2B2A92CE85E0846FEDDE76130EE8D6CD32A799C_AdjustorThunk ();
// 0x0000033D System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Dispose()
extern void Enumerator_Dispose_mD3CBE521602677CD1576DD33FF5F6F9EA90F867C_AdjustorThunk ();
// 0x0000033E System.Int32 UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::GetHashCode()
extern void Enumerator_GetHashCode_m76050DFEB3E8573E39DFCF7058E9939F0E059067_AdjustorThunk ();
// 0x0000033F System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Equals(System.Object)
extern void Enumerator_Equals_mB7BB7CC1A8D6E25C8506EE6E7ECB2CD93724EF54_AdjustorThunk ();
// 0x00000340 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Equals(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_Equals_m476069B8D9916F437C5D6AEEC13D21F0E2CF1C29_AdjustorThunk ();
// 0x00000341 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::op_Equality(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator,UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_op_Equality_mF9350929F4D0B5FF93149EA1CBADB8206BD912EB ();
// 0x00000342 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::op_Inequality(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator,UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_op_Inequality_m561C568546544E4EB9E74BC1F3F68826364FB950 ();
// 0x00000343 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::Destroy()
extern void Provider_Destroy_mF8BC4EBBD16FAE7168D856A68D59234CEA27ACD5 ();
// 0x00000344 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
// 0x00000345 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
// 0x00000346 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
// 0x00000347 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::set_maxNumberOfMovingImages(System.Int32)
extern void Provider_set_maxNumberOfMovingImages_m968F46966BD0E231991BF6834577536FB174FEC7 ();
// 0x00000348 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::.ctor()
extern void Provider__ctor_m12B077B91F113E69002DF00BFFF13DCAFC550C5F ();
// 0x00000349 System.String UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98_AdjustorThunk ();
// 0x0000034A System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6_AdjustorThunk ();
// 0x0000034B System.Type UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4_AdjustorThunk ();
// 0x0000034C System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF_AdjustorThunk ();
// 0x0000034D System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_supportsMovingImages()
extern void Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F_AdjustorThunk ();
// 0x0000034E System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_supportsMovingImages(System.Boolean)
extern void Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C_AdjustorThunk ();
// 0x0000034F System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_requiresPhysicalImageDimensions()
extern void Cinfo_get_requiresPhysicalImageDimensions_m548502FBF010625D9ACC2869E46D1CCEE4A8B83E_AdjustorThunk ();
// 0x00000350 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_requiresPhysicalImageDimensions(System.Boolean)
extern void Cinfo_set_requiresPhysicalImageDimensions_m57E69E5A56010C3A0237BA861BB39CF7C6CBCBFB_AdjustorThunk ();
// 0x00000351 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_supportsMutableLibrary()
extern void Cinfo_get_supportsMutableLibrary_mD86A73E2B7F9A03BBD9DBCF47BF097C2F25194BC_AdjustorThunk ();
// 0x00000352 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_supportsMutableLibrary(System.Boolean)
extern void Cinfo_set_supportsMutableLibrary_m0635990374A918BFBAA137EDBD9128C012E86CFC_AdjustorThunk ();
// 0x00000353 System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094_AdjustorThunk ();
// 0x00000354 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941_AdjustorThunk ();
// 0x00000355 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC_AdjustorThunk ();
// 0x00000356 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m4BFD25F1452FB829B4A52A36D4DA2F85473241F6 ();
// 0x00000357 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m7BC4DE6B4A18849F4559DB3552AD80381AFAB7A5 ();
// 0x00000358 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Start()
extern void Provider_Start_mC9D2F2E358AB53A83EF9221BC7A777065D529704 ();
// 0x00000359 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Stop()
extern void Provider_Stop_m43705D46DCCBAA2E9D22AD73CF01AF7D2706E373 ();
// 0x0000035A System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Destroy()
extern void Provider_Destroy_m4E9C920434482C4410E852E4DC7F300E52623EB3 ();
// 0x0000035B UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRParticipant> UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRParticipant,Unity.Collections.Allocator)
// 0x0000035C System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::.ctor()
extern void Provider__ctor_m9F9BA5B0BC7D73106B244AC16958977AFC8F5C64 ();
// 0x0000035D System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Start()
extern void Provider_Start_m585D4A4BA37DE9B97192C0678686AB867A50F883 ();
// 0x0000035E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Stop()
extern void Provider_Stop_m36967BE370E8E0CA5F5A2C7CC0C02A4F06B1173D ();
// 0x0000035F System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Destroy()
extern void Provider_Destroy_m66246D41CB9794304B34F89E0A9D9F2557BC55AB ();
// 0x00000360 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void Provider_GetBoundary_m20B566BA701A693EBED89F7F907C1C204F52B365 ();
// 0x00000361 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
// 0x00000362 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void Provider_set_planeDetectionMode_mE0054191CF7CC3CD46C4EB1F904714641B1DB92F ();
// 0x00000363 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::.ctor()
extern void Provider__ctor_m99298F30B946FE819FC58BD35A71C32A96C9883C ();
// 0x00000364 System.String UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99_AdjustorThunk ();
// 0x00000365 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF_AdjustorThunk ();
// 0x00000366 System.Type UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C_AdjustorThunk ();
// 0x00000367 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B_AdjustorThunk ();
// 0x00000368 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsHorizontalPlaneDetection()
extern void Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214_AdjustorThunk ();
// 0x00000369 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304_AdjustorThunk ();
// 0x0000036A System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsVerticalPlaneDetection()
extern void Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE_AdjustorThunk ();
// 0x0000036B System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsVerticalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B_AdjustorThunk ();
// 0x0000036C System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsArbitraryPlaneDetection()
extern void Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C_AdjustorThunk ();
// 0x0000036D System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06_AdjustorThunk ();
// 0x0000036E System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsBoundaryVertices()
extern void Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84_AdjustorThunk ();
// 0x0000036F System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsBoundaryVertices(System.Boolean)
extern void Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D_AdjustorThunk ();
// 0x00000370 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsClassification()
extern void Cinfo_get_supportsClassification_m815046C4B90F936F4736942441D020EBB8C57023_AdjustorThunk ();
// 0x00000371 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsClassification(System.Boolean)
extern void Cinfo_set_supportsClassification_m461A917AC95A1EB92C0A10334018D38C8D82348A_AdjustorThunk ();
// 0x00000372 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D_AdjustorThunk ();
// 0x00000373 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8_AdjustorThunk ();
// 0x00000374 System.Int32 UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0_AdjustorThunk ();
// 0x00000375 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m92CAE095C4DBAB20ED4E573E5885F7C0DE881B7B ();
// 0x00000376 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m8735071162549242673CD1298CFFA59E125EFD25 ();
// 0x00000377 System.Void UnityEngine.XR.ARSubsystems.Promise`1_ImmediatePromise::OnKeepWaiting()
// 0x00000378 System.Void UnityEngine.XR.ARSubsystems.Promise`1_ImmediatePromise::.ctor(T)
// 0x00000379 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Start()
extern void Provider_Start_mF5193E0D9A12A1286389F3B08F0FA5675ADF5CC5 ();
// 0x0000037A System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Stop()
extern void Provider_Stop_m23081AA73D0BE12116F4F83A63474305DFBC7E3B ();
// 0x0000037B System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Destroy()
extern void Provider_Destroy_m1228BCFCB0887BC20C2E5CC554CD29C1DE1931EB ();
// 0x0000037C Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void Provider_Raycast_m6063A859AC10ACF7F27A4AE0AAD82C3A0DF831AE ();
// 0x0000037D Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void Provider_Raycast_mF885EFF5FFD12196F53C9FE405EC523020C35CE2 ();
// 0x0000037E System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::.ctor()
extern void Provider__ctor_m15999B3E2C2B67AD264BEE04B753C69303256473 ();
// 0x0000037F System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA_AdjustorThunk ();
// 0x00000380 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B_AdjustorThunk ();
// 0x00000381 System.Type UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F_AdjustorThunk ();
// 0x00000382 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A_AdjustorThunk ();
// 0x00000383 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportsViewportBasedRaycast()
extern void Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514_AdjustorThunk ();
// 0x00000384 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportsViewportBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233_AdjustorThunk ();
// 0x00000385 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportsWorldBasedRaycast()
extern void Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F_AdjustorThunk ();
// 0x00000386 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportsWorldBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5_AdjustorThunk ();
// 0x00000387 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportedTrackableTypes()
extern void Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk ();
// 0x00000388 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652_AdjustorThunk ();
// 0x00000389 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636_AdjustorThunk ();
// 0x0000038A System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6_AdjustorThunk ();
// 0x0000038B System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::ToString()
extern void Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453_AdjustorThunk ();
// 0x0000038C System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B_AdjustorThunk ();
// 0x0000038D System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m6846FB5AFD2270316EB39A768A5E927FE4DFC82E ();
// 0x0000038E System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mA22683D29D9FC60CE5D84B46B2B9C0B8A9CE0920 ();
// 0x0000038F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Resume()
extern void Provider_Resume_m9A2A311FC87427ADA41FD6262222724BA56B1C71 ();
// 0x00000390 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Pause()
extern void Provider_Pause_m0BCA2AD4EC520FDDDD0B6E3698C0CD6343B35761 ();
// 0x00000391 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void Provider_Update_mD118C3E90A2CBFCC6486562F558B46C908768A91 ();
// 0x00000392 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Destroy()
extern void Provider_Destroy_m119B0D0E53B60B0F95778495FDF2939317AA30C5 ();
// 0x00000393 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Reset()
extern void Provider_Reset_mD263E65FDBC3D80860FAF5ED5AEE4C91E3D914D5 ();
// 0x00000394 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::OnApplicationPause()
extern void Provider_OnApplicationPause_m3EF5E05F1A459E50420B4B820CA19F06AC2F0735 ();
// 0x00000395 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::OnApplicationResume()
extern void Provider_OnApplicationResume_m7E5873546FD4386540C79DE2BD1CC918D8B1E65F ();
// 0x00000396 System.IntPtr UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_nativePtr()
extern void Provider_get_nativePtr_m87C8937571D73F8B99BA224439020760CEFF110F ();
// 0x00000397 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::GetAvailabilityAsync()
extern void Provider_GetAvailabilityAsync_m9CC6F74169601931E94DA1177C34A543048B0A01 ();
// 0x00000398 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::InstallAsync()
extern void Provider_InstallAsync_m074282442B17B260C246A17F8D7E1C4E947A6017 ();
// 0x00000399 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_trackingState()
extern void Provider_get_trackingState_m9E8D77D2BA6BD8F3508CFA530482C6D790DCFF35 ();
// 0x0000039A UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_notTrackingReason()
extern void Provider_get_notTrackingReason_m3EF060E8F7A23B6CD196E1B7FCF2F34457B065AA ();
// 0x0000039B System.Guid UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_sessionId()
extern void Provider_get_sessionId_mD2F3D74F1EB6B876B010BCA69CE0E29D80FB2BA7 ();
// 0x0000039C System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_matchFrameRate()
extern void Provider_get_matchFrameRate_m4DCFFB4B857699910A230A596664E86D68103583 ();
// 0x0000039D System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::set_matchFrameRate(System.Boolean)
extern void Provider_set_matchFrameRate_m166192F3055E7A5B35C8183215F15AB08DC8B08E ();
// 0x0000039E System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_frameRate()
extern void Provider_get_frameRate_m2037F0EC2A42B1295115701731DF1B8CC098F6D1 ();
// 0x0000039F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::.ctor()
extern void Provider__ctor_mFCC52955F6E8D4F49818B78E2F6E7B27CD0222F2 ();
// 0x000003A0 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_supportsInstall()
extern void Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E_AdjustorThunk ();
// 0x000003A1 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_supportsInstall(System.Boolean)
extern void Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80_AdjustorThunk ();
// 0x000003A2 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_supportsMatchFrameRate()
extern void Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3_AdjustorThunk ();
// 0x000003A3 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_supportsMatchFrameRate(System.Boolean)
extern void Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6_AdjustorThunk ();
// 0x000003A4 System.String UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870_AdjustorThunk ();
// 0x000003A5 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB_AdjustorThunk ();
// 0x000003A6 System.Type UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF_AdjustorThunk ();
// 0x000003A7 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85_AdjustorThunk ();
// 0x000003A8 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9_AdjustorThunk ();
// 0x000003A9 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B_AdjustorThunk ();
// 0x000003AA System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67_AdjustorThunk ();
// 0x000003AB System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m12DF3BFC611B815E5935F52A090B53F9575F3342 ();
// 0x000003AC System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m3D32A0D12104E275C89DCE4C40764818C082BCC6 ();
static Il2CppMethodPointer s_methodPointers[940] = 
{
	XRAnchor_get_defaultValue_m457A914338467F05B7928AF1657C2447DDD38B96,
	XRAnchor__ctor_m0A6A007D3FC6C125D82C56EF0034197973E14A14_AdjustorThunk,
	XRAnchor__ctor_m8D7A2BE809D9C967C1D12C7E00648FF642AEA4DF_AdjustorThunk,
	XRAnchor_get_trackableId_m7BD89E3F1664C126D09D8DD141EA18E8A9933711_AdjustorThunk,
	XRAnchor_get_pose_m7CA50F0FCB9FE7A6FB60C6AFD33B62AF4BE0CB1A_AdjustorThunk,
	XRAnchor_get_trackingState_m6C73E20FCB9A2E33666BD07816D04099D61EF3EF_AdjustorThunk,
	XRAnchor_get_nativePtr_mE26418E703B946C28F977AA10BF36063485EB2F6_AdjustorThunk,
	XRAnchor_get_sessionId_m80299CA4593E150BF0D645DAEAC03855C131183E_AdjustorThunk,
	XRAnchor_GetHashCode_m2E347D72B6CE87F263419D78199C5623BEEA7EAC_AdjustorThunk,
	XRAnchor_Equals_m4BB0A471E6C5ADEFBE1863BD9CACC6C6F7325DF5_AdjustorThunk,
	XRAnchor_Equals_mC6E13454F78E1E4B0FB5CC71A7147D988C8608BF_AdjustorThunk,
	XRAnchor_op_Equality_m2AD61748921E3CAFB8F748F60ED8493A4A60C153,
	XRAnchor_op_Inequality_m4698017121926343900DD5B710E449888A5AEEDA,
	XRAnchor__cctor_mEE7F56270E97E82B96F344CC218F2C78E49645ED,
	XRAnchorSubsystem__ctor_m658A4253C51FC407AB5588D44AB923A624DA3218,
	XRAnchorSubsystem_OnStart_mF3FFAB8CB0BC7BFAEB03D9B008ABAC8F30AFB46C,
	XRAnchorSubsystem_OnStop_mA451A62E5FD46EB63E3AE777039EAC45B8D4E5D8,
	XRAnchorSubsystem_OnDestroyed_m08E9324005A7527978BDA2F1AB19830D97307CBF,
	XRAnchorSubsystem_GetChanges_mA9F45640A583B5E123F075733A25637AC4444D25,
	XRAnchorSubsystem_TryAddAnchor_mB2A0A082C26E3E20DF0A097E8AF9F67B4C31D51D,
	XRAnchorSubsystem_TryAttachAnchor_mBC1C13FBB753A7F7B4BB81CDB41D4C9670ABF132,
	XRAnchorSubsystem_TryRemoveAnchor_m09EE4CB777C6C27E06FEFE52D22BD523CF3B1DE6,
	NULL,
	XRAnchorSubsystemDescriptor_get_supportsTrackableAttachments_m7F2711CC17F581951A3729BCD1BEFFDB8C6E265D,
	XRAnchorSubsystemDescriptor_set_supportsTrackableAttachments_m0F7AF1D7E0CF98758531572208E2BB1A2065B6EE,
	XRAnchorSubsystemDescriptor_Create_m3F5431E9EE74F05E361D1BBD657727AF75EBC8DF,
	XRAnchorSubsystemDescriptor__ctor_mABB3CDF46BA26182C701A3AB0AEF91053E58F396,
	XRAsyncCameraImageConversionStatusExtensions_IsDone_m8CF9AE693207B45BE8D14CCE6320FF8FC048A10E,
	XRAsyncCameraImageConversionStatusExtensions_IsError_mD9CA202791F0CF667E51EE6CEC35717F4C518ED1,
	XRAsyncCameraImageConversion_get_conversionParams_m2BEEC8F4DD17AF95D5883EA8AE7C6EFC61590877_AdjustorThunk,
	XRAsyncCameraImageConversion_set_conversionParams_m60290CD24C0AE8EECCE83387E12AB02C2DA54244_AdjustorThunk,
	XRAsyncCameraImageConversion_get_status_mC084247856B30468671D2C43783E60E643F36270_AdjustorThunk,
	XRAsyncCameraImageConversion__ctor_mA4D75E954B713EA430F8D35A533587CA2CDE6968_AdjustorThunk,
	NULL,
	XRAsyncCameraImageConversion_Dispose_m2DCD2014A91F54E500277CD4F6EE4DD587B69933_AdjustorThunk,
	XRAsyncCameraImageConversion_GetHashCode_m15B844354D09525711A8F19B99B56967A993A0A8_AdjustorThunk,
	XRAsyncCameraImageConversion_Equals_m728765267CBAD3F774E4258038F3861F67077DDF_AdjustorThunk,
	XRAsyncCameraImageConversion_Equals_m2B27ED670A34C32531F241CCEB1A15DD22B89E87_AdjustorThunk,
	XRAsyncCameraImageConversion_op_Equality_m86C260BBA7D9CD34DF7FD183741960E35D07DAD7,
	XRAsyncCameraImageConversion_op_Inequality_m81792AC215117FE5002B3717643475109C0501FB,
	XRAsyncCameraImageConversion_ToString_mCCCE35B02E3179FBBE6D021DB239C61D3AF4C845_AdjustorThunk,
	XRCameraConfiguration_get_width_m998994AB66DE43B180D472B2B703B524C69497B4_AdjustorThunk,
	XRCameraConfiguration_get_height_m13F96FED610F582C624331BD6394B88E4D055140_AdjustorThunk,
	XRCameraConfiguration_get_resolution_mDED625C9D21911EF0D05C49DCBC589FE7915C2B2_AdjustorThunk,
	XRCameraConfiguration_get_framerate_m97E323885AEFFC7E8FCAC41CBE3F72A34400440B_AdjustorThunk,
	XRCameraConfiguration__ctor_m1E9BF53E84D084D10548F54D77E74070B0E84AD8_AdjustorThunk,
	XRCameraConfiguration__ctor_mB9EC5F31736A0ABFFB56660DB9AAEF869E3BBEA1_AdjustorThunk,
	XRCameraConfiguration_ToString_m423574318A784468BA1055A28EC192FC34D4FA30_AdjustorThunk,
	XRCameraConfiguration_GetHashCode_m0826525E55CF9BD9E693F2D359F46A36E7FB08EC_AdjustorThunk,
	XRCameraConfiguration_Equals_m04BF698F8D3E743BEF15107AFF74AAE1347CE82C_AdjustorThunk,
	XRCameraConfiguration_Equals_m40024812F8E6090F4F18A6F0EBC055E9ED1B50BC_AdjustorThunk,
	XRCameraConfiguration_op_Equality_mAAD75A6749A62B4DA3A57C30B40111A648C94C2D,
	XRCameraConfiguration_op_Inequality_m9472B9522744C80469C6CF0731F1443BF1BA1075,
	XRCameraFrame_get_timestampNs_m490E96B453EB3AA7416F98013B47B551C003268C_AdjustorThunk,
	XRCameraFrame_get_averageBrightness_mA23D9FB95046E89792F9F70750E000FF30E2959C_AdjustorThunk,
	XRCameraFrame_get_averageColorTemperature_m2AA0B1BE3B939E9221507D9AFB1CB28AE9FF0234_AdjustorThunk,
	XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk,
	XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk,
	XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk,
	XRCameraFrame_get_trackingState_mB45FB220A1EDE8A180217EC6F710A9651E61682A_AdjustorThunk,
	XRCameraFrame_get_nativePtr_mB6380FE0EE01853F55087ACE6C56CB1961384373_AdjustorThunk,
	XRCameraFrame_get_properties_mE2BF04FF350BC7B1FF375CB5C4E703211133DEF5_AdjustorThunk,
	XRCameraFrame_get_averageIntensityInLumens_m88D3A6D420FE826CD53467E3039475601685DA8F_AdjustorThunk,
	XRCameraFrame_get_exposureDuration_m5DD81A33C2564756BF8B76FB260E1C1070E3BC7F_AdjustorThunk,
	XRCameraFrame_get_exposureOffset_mA3A2A7C3DFE532AEDA6F1A77C91AA92A2D30FA1C_AdjustorThunk,
	XRCameraFrame_get_hasTimestamp_m9463BC747994BF7E6AEFDA339D88F5EDB02AA83B_AdjustorThunk,
	XRCameraFrame_get_hasAverageBrightness_m86A574F4A162689E55C8B2C0F04B9528CFB738A5_AdjustorThunk,
	XRCameraFrame_get_hasAverageColorTemperature_mB177764FC2D9592E4CA15F5BEFD710EF5510DB15_AdjustorThunk,
	XRCameraFrame_get_hasColorCorrection_m8FE25191C55FF1FA09EE7EB0D1AC1ABB1CD87597_AdjustorThunk,
	XRCameraFrame_get_hasProjectionMatrix_mC15F4AD61C07287736EA10A7C7BE5F66EE0258D1_AdjustorThunk,
	XRCameraFrame_get_hasDisplayMatrix_mD63FE4D87F9D23F40217F3DA69CEF5D01F45B8A9_AdjustorThunk,
	XRCameraFrame_get_hasAverageIntensityInLumens_m2E261BC18F6DA1887D84CFD6C3ABB2135FDC5A99_AdjustorThunk,
	XRCameraFrame_get_hasExposureDuration_mD2581A0F791D7471D153286395F36733CC943DA4_AdjustorThunk,
	XRCameraFrame_get_hasExposureOffset_m69EB36FB07594B62BC4DE731A007B7EE2E61040A_AdjustorThunk,
	XRCameraFrame_TryGetTimestamp_m73E78FEDC2B5A689D2A4FA0F14796F11E02E8D7E_AdjustorThunk,
	XRCameraFrame_TryGetAverageBrightness_m1C88B42C26BB03CF446C436E137529BB5488A704_AdjustorThunk,
	XRCameraFrame_TryGetAverageColorTemperature_m3CBA822338324D045A2514043B8684264C5A99DA_AdjustorThunk,
	XRCameraFrame_TryGetProjectionMatrix_mFB3605638456E9DB8C8B2795BB9907FB5CCEB54D_AdjustorThunk,
	XRCameraFrame_TryGetDisplayMatrix_mF94A6FD5819E5E6F463BF53F201C6C88AE5158B9_AdjustorThunk,
	XRCameraFrame_TryGetAverageIntensityInLumens_m7B50523B4EFC67EF7B37191798F4A6EA5AEAE714_AdjustorThunk,
	XRCameraFrame_Equals_mD750801D16ED7C39E75B31B663A97F300513FE3E_AdjustorThunk,
	XRCameraFrame_Equals_mA3BFB0E406F27C0A40D0A8B79E02A17B8C06AA61_AdjustorThunk,
	XRCameraFrame_op_Equality_m5A5BC3941CB1BF81BBFED8AB5DFD9F59F37CA272,
	XRCameraFrame_op_Inequality_m542E0BDA5EB653B7527803339CEFDE4384EC30D2,
	XRCameraFrame_GetHashCode_mB60ADF713E8CA46B1629DA9EB361ED34127C358C_AdjustorThunk,
	XRCameraFrame_ToString_mE362631B14B4302E3AED73932E73C1D6AB5684FA_AdjustorThunk,
	XRCameraImage_get_dimensions_m396844BB3871C3CF359299C935A286F7EC9E07CC_AdjustorThunk,
	XRCameraImage_set_dimensions_m0EB351A10EB73AEB3BA66DEBB92CF6A482DF0FDD_AdjustorThunk,
	XRCameraImage_get_width_m9540469A8631057CDBA693ACA5B39F11F7F8F85A_AdjustorThunk,
	XRCameraImage_get_height_m3DFB851BE8723A821A3AB2A7F08F38E5C910A2BE_AdjustorThunk,
	XRCameraImage_get_planeCount_mE12B1F437995559D932808E1D5CA4838243802D6_AdjustorThunk,
	XRCameraImage_set_planeCount_mBC04A59810816A0E2B8F173F17A17627CB2177C5_AdjustorThunk,
	XRCameraImage_get_format_m363F742D003794B51229FEF58C6D24EF2E4174D9_AdjustorThunk,
	XRCameraImage_set_format_m587247101073030F7122D794E2C7C4ED3208D3EC_AdjustorThunk,
	XRCameraImage_get_timestamp_m189929FA10F1C355B5763E25232750613F2FD9B7_AdjustorThunk,
	XRCameraImage_set_timestamp_m817C23EC056A13E82BC33FD115C5DC4A40569C5F_AdjustorThunk,
	XRCameraImage_get_valid_m9C9C80282451A3D92E3EA46615BFBE948CAE017B_AdjustorThunk,
	XRCameraImage__cctor_mCE4936C86EAC70BB3FBC1690BE39DED4D063FFFA,
	XRCameraImage__ctor_m13AB438F2246EEA666CEB1A6C7CE9C32B7F867C0_AdjustorThunk,
	XRCameraImage_FormatSupported_m2913E01D5D0DC66B027D49DC2B365A98693075D2,
	XRCameraImage_GetPlane_m25B263847B085967E2B36AA03C6B411FC6EBCD66_AdjustorThunk,
	XRCameraImage_GetConvertedDataSize_m3BB1342E4075EB814DC6D5BD5CD13F07309FE8F4_AdjustorThunk,
	XRCameraImage_GetConvertedDataSize_m27A3F5121CA92DD8C4A7800F0844E0C23F1AD368_AdjustorThunk,
	XRCameraImage_Convert_m75508BB6601557724C90E14C66A8A9706A8CACAA_AdjustorThunk,
	XRCameraImage_ConvertAsync_mDC2A930F5646533A1F13AD4B9CA659507057522A_AdjustorThunk,
	XRCameraImage_ConvertAsync_m52EDCE064D29031680FE7149DBCD7D2600475FD4_AdjustorThunk,
	XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B,
	XRCameraImage_ValidateNativeHandleAndThrow_mD0E79FE634E6E5431B5222CFB1D8AC833D2BE13B_AdjustorThunk,
	XRCameraImage_ValidateConversionParamsAndThrow_m8618EACFC53F5916704F64774882F50F8B4E38E9_AdjustorThunk,
	XRCameraImage_Dispose_m01A1F3830038FD9ED9B35B6640A6CA57D383615A_AdjustorThunk,
	XRCameraImage_GetHashCode_m27BEDB780E2AF59CA888ED4A8A0DC2B120BC756E_AdjustorThunk,
	XRCameraImage_Equals_mE4A7D2162B33C795268692D0B745F9610B58D4F6_AdjustorThunk,
	XRCameraImage_Equals_m9408E719265D28710EF012AA4E2E63AF320C7431_AdjustorThunk,
	XRCameraImage_op_Equality_mEC3CFAA34E277FABD84A140E2D84AE5C1096B87E,
	XRCameraImage_op_Inequality_m2F2E759DF204BE5ABEFEF0B3232DB5F37B4B15EF,
	XRCameraImage_ToString_m2E0D10A11B0FAE6A42A572D9C23320587B53C9E0_AdjustorThunk,
	XRCameraImageConversionParams_get_inputRect_m6F91AAA4D0844E9A9E1391A2FA73F99CEADAC833_AdjustorThunk,
	XRCameraImageConversionParams_set_inputRect_m1FE1903DB0E679A5C18E03A5E714D362D2D356E5_AdjustorThunk,
	XRCameraImageConversionParams_get_outputDimensions_mE4BFDDB1E03C9024E392ABD2A0EF90398F1188D2_AdjustorThunk,
	XRCameraImageConversionParams_set_outputDimensions_m7099D526B85FA979380DE028FA860D9F46BD8E81_AdjustorThunk,
	XRCameraImageConversionParams_get_outputFormat_m7960F36DE5418DCFCBD0E780C483EDA42AB9405F_AdjustorThunk,
	XRCameraImageConversionParams_set_outputFormat_m97AED6CDFC10BD39F0588EB44DD72B1338A90438_AdjustorThunk,
	XRCameraImageConversionParams_get_transformation_m2DA4133406E43882B1B52CDB3867F2E29A70C013_AdjustorThunk,
	XRCameraImageConversionParams_set_transformation_mBE056F5598B1FBE0072A78ADEBD1CBB60EA2B4BA_AdjustorThunk,
	XRCameraImageConversionParams__ctor_m8D586B09EAB9827D70E6550EB67872CA2AAFC4B1_AdjustorThunk,
	XRCameraImageConversionParams_GetHashCode_mE4B9237F3C69745C28C8D0D958B353D80F33A876_AdjustorThunk,
	XRCameraImageConversionParams_Equals_m7E25A42871B1C10084E685AC78F4A2E53D42652E_AdjustorThunk,
	XRCameraImageConversionParams_Equals_mAB0C49294DC3FB284CB470DDC298B696C4B28A06_AdjustorThunk,
	XRCameraImageConversionParams_op_Equality_mE076A48C022D29334A622B6E7A0D67BA9755A02B,
	XRCameraImageConversionParams_op_Inequality_mDD615881884A83AB8ADE968F7AA1CB998402FF16,
	XRCameraImageConversionParams_ToString_mCD58F5C2B04509ADEDDB374DA4492E22F3D70E97_AdjustorThunk,
	XRCameraImagePlane_get_rowStride_m8551CC63400DE51639214B8E190A5274A84FF11B_AdjustorThunk,
	XRCameraImagePlane_set_rowStride_m83E1AFB0D313992A2A83C24C4A9EE9B8E095F8CB_AdjustorThunk,
	XRCameraImagePlane_get_pixelStride_mEB04E71CAA5C3FB38099BF40CE07D176D781CD5C_AdjustorThunk,
	XRCameraImagePlane_set_pixelStride_m1A467C173D5D73BDA30B1FBF815A8BC977947C65_AdjustorThunk,
	XRCameraImagePlane_get_data_m7A4B4236191D73BB3B62D2367642C6BCD14EACE6_AdjustorThunk,
	XRCameraImagePlane_set_data_m7B7487B42D2914063EE88E699A4BDFCD07A2FB55_AdjustorThunk,
	XRCameraImagePlane_GetHashCode_m0B82CBAB063DEC9C941CA4C1BA033AE485E0519F_AdjustorThunk,
	XRCameraImagePlane_Equals_mF26614D5995021938AADB7DC19CB6DD827F68698_AdjustorThunk,
	XRCameraImagePlane_Equals_m33CD260CF0A3F593C2400FE3B04E4A17C5BB1E56_AdjustorThunk,
	XRCameraImagePlane_op_Equality_m5E4E7095E52415F1F3CEC32D00DAEE3906C60571,
	XRCameraImagePlane_op_Inequality_mA36691A1244BE582C0E10C53391A41BDEA80E1C3,
	XRCameraImagePlane_ToString_m5BBAE2E31FBCECC4B33EEA0B630B3F176AB38EBA_AdjustorThunk,
	XRCameraIntrinsics_get_focalLength_m9D090B0B207598F353860CB5735B85B78827C93F_AdjustorThunk,
	XRCameraIntrinsics_get_principalPoint_mC66F07CA90FAA4A8A94BDF2A62641196C9DD6DEC_AdjustorThunk,
	XRCameraIntrinsics_get_resolution_m6572536639CC3A6F1A1E1DE50971522B2933355D_AdjustorThunk,
	XRCameraIntrinsics__ctor_mADA992B91D743E1417DD1C46B16566D1BD72B681_AdjustorThunk,
	XRCameraIntrinsics_Equals_mEA26D3BF6A90B7DC9E2FF626BEA72EB6C098D5D2_AdjustorThunk,
	XRCameraIntrinsics_Equals_mB82F3C59386B2169809615700C79BF3DC519C45F_AdjustorThunk,
	XRCameraIntrinsics_op_Equality_m7214DEF9B315C54F812C1332C9260CE4BBBC4BE2,
	XRCameraIntrinsics_op_Inequality_mE794A593D63C81CD9779F585EB28FC7FB3026E09,
	XRCameraIntrinsics_GetHashCode_mC38995C37469CCF20C3A155F7F033BD16EE36D98_AdjustorThunk,
	XRCameraIntrinsics_ToString_m6A86B906A1EB79BDC75582D45311E75323054983_AdjustorThunk,
	XRCameraParams_get_zNear_mD6245E7FAC0B8F427F9353E018F61E231B8733FC_AdjustorThunk,
	XRCameraParams_set_zNear_m0592AF26BE6AD3149462E71FB77B3838ACC3E9AB_AdjustorThunk,
	XRCameraParams_get_zFar_mBA07EC688D770BDBCAC55E50DC309AEF6D85A679_AdjustorThunk,
	XRCameraParams_set_zFar_m85FE4877910BD92BFE4F308F73EA2BE35F3538BF_AdjustorThunk,
	XRCameraParams_get_screenWidth_m9CE9B72CBE1FD3CD7F0D856B741D742E5069067E_AdjustorThunk,
	XRCameraParams_set_screenWidth_mF256E58C15E4E73B3675323C93D1F38E06438919_AdjustorThunk,
	XRCameraParams_get_screenHeight_m1305736438CFC1A019C2AB2DF3CA11F2BFBB5B57_AdjustorThunk,
	XRCameraParams_set_screenHeight_mFFFBD063E1AA590D9B5055287965EF5A0A0B92A8_AdjustorThunk,
	XRCameraParams_get_screenOrientation_mF47570D9A01E6E868BD0A77E89942B47B9A5A86B_AdjustorThunk,
	XRCameraParams_set_screenOrientation_m2F3BC04E753E945AD1AE69F2643301C5FDEA3117_AdjustorThunk,
	XRCameraParams_Equals_m0E44D78DF6343B56235F52CC885DC36ABD69F586_AdjustorThunk,
	XRCameraParams_Equals_m2AF39FA9619DC2B0C470AAEEDF35A05E85BD0A1E_AdjustorThunk,
	XRCameraParams_op_Equality_mCBE3894A8724D8370C56D72C6E78B46219E68162,
	XRCameraParams_op_Inequality_mC226CEC2EC0BF9FF430F4BDE844417236A65F865,
	XRCameraParams_GetHashCode_m4E01EC262AD8F277BB0793D9782904D78F3B46D4_AdjustorThunk,
	XRCameraParams_ToString_mBE56D42E3C4F978B3A61B67C346E4CB58BD293CD_AdjustorThunk,
	XRCameraSubsystem__ctor_m4DB65C1288A29F049A4A362B9CD81B60970A73AE,
	XRCameraSubsystem_get_focusMode_mD7F796B4784759D129D32699DEA4ABE7A8E54A71,
	XRCameraSubsystem_set_focusMode_m4230E264ADA524A210DDE27D926F0DE66918C07A,
	XRCameraSubsystem_get_lightEstimationMode_mFED2EBF1F5E52B48935CD4EB4978C2A04A82F5A2,
	XRCameraSubsystem_set_lightEstimationMode_m0B04B0E3D3E8F73CA5FC6868DE86D6CFFF2263B5,
	XRCameraSubsystem_OnStart_m283BED6AC2AB793BA7E71C07469544C3D6ACBF67,
	XRCameraSubsystem_OnStop_m5FA255D23089DF33D74682EB69B6FC6F8FD39041,
	XRCameraSubsystem_OnDestroyed_m75D8C8094D3F73E008F80183E3A63ED305B07BC4,
	XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21,
	XRCameraSubsystem_get_cameraMaterial_m5B8CE90E2D4F8AF83D5D8134B1CB57D38065EBF5,
	XRCameraSubsystem_TryGetIntrinsics_mA8696FD477463BD57E5EF328AA276439C556BD3F,
	XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA,
	XRCameraSubsystem_get_currentConfiguration_m95C8ED2F04B9AD10A1C2498C5D32DF03821A1DBF,
	XRCameraSubsystem_set_currentConfiguration_mC6AAFA4CCF55F2293DB2F8E0A16A9239438ED70D,
	XRCameraSubsystem_get_invertCulling_mB2F2CD061D9065757EDFB6EAD361FB0B1782BAF4,
	NULL,
	XRCameraSubsystem_TryGetLatestFrame_m7BDBD516A8A2832ECBD9334E4D7B0D38ED2D191F,
	XRCameraSubsystem_get_permissionGranted_m4BE44093A85FE550B9DEFAA70E21CD84C3E99BFF,
	XRCameraSubsystem_TryGetLatestImage_mA9EEBC8B2BC3D9F1DC25513B25BFECA07CE45ABF,
	XRCameraSubsystem_Register_m471B8039B86BACC07C48A926544DF6C1415C1DA7,
	XRCameraSubsystem_GetAsyncRequestStatus_m6268EBBA8A4DF7A4836B50EAD3A3908025F2F5DA,
	XRCameraSubsystem_DisposeImage_m45A6B78356D402A5528F4F023FA33BD395F1E958,
	XRCameraSubsystem_DisposeAsyncRequest_mF175176F7D374FAEB7F5BD1ACED82A6F463226F5,
	XRCameraSubsystem_TryGetPlane_m415B3B07D89C4804AA7B6E14237FCBA8EC83BF03,
	XRCameraSubsystem_NativeHandleValid_m72536C711FE26C43825B05D173B353F75377E33B,
	XRCameraSubsystem_TryGetConvertedDataSize_m8DE2AA592ED5BFF97D3DCAB38B1441BFD9B18AFA,
	XRCameraSubsystem_TryConvert_mFD08FDC95473E8FC144E257541CB093A1BC5BB39,
	XRCameraSubsystem_ConvertAsync_m1D08E5F3A08DBD2571BB28B93200385C42319498,
	XRCameraSubsystem_TryGetAsyncRequestData_m2C31AFADC07F7BFEB787EE29104E2CA2B86B683F,
	XRCameraSubsystem_ConvertAsync_mC717FF778303227C1073EB9F169F62E1075E0E36,
	XRCameraSubsystemCinfo_get_id_m8C63E6A41979D1A0201ADACA9F984088D06F488C_AdjustorThunk,
	XRCameraSubsystemCinfo_set_id_mEA5E0B21781D8AAF0FB30E9E506AA4D7C392E2A8_AdjustorThunk,
	XRCameraSubsystemCinfo_get_implementationType_m6F9012A33C47D026F5070C2F64B444A93709B5C5_AdjustorThunk,
	XRCameraSubsystemCinfo_set_implementationType_mAEA2151AEC9F31C5726795200B63D4BA53F2721E_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageBrightness_m1DDB2CC0BE14F5C77E7ECC2FF1BA5C712C08CCA5_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageBrightness_m0851BD298973A23FCE8D87B5B8AB389562D255FA_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_m16E2A9E1B564001C9A7D2F5EA20C0101DC97218D_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m427080856A860B42B3FC21139B990F19BE0AD87E_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsColorCorrection_m3B9E39E84E9C061ACE40853B381B6DFB1DFE03AE_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsColorCorrection_mFC3AED27787017D69ABA73FF60D1E20DDF5E674F_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsDisplayMatrix_mE79B1E6401467F3320D1AFF9678041FC2B5EC36C_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsDisplayMatrix_mB5BF43F49F4D64AA3DFE174574D386D99A96F92F_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m820DB18EBD58445C4976C05E25C3E2A8B04B388E_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m4CC64D264746A394D8186CCDD583CFCC637C8E66_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsTimestamp_mC9A551933743EC5171B9EAC41D5222067B56CAEA_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsTimestamp_m901DA9F41D9CEE062F7A054738E5382E2A825F28_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsCameraConfigurations_m125606F11D5A7521099E70525019DA50BBB4334E_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mCCA48E46B902EEABAA94FB7A2A668097E06D4906_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsCameraImage_m247ED6F4E54DC010F7506E353AD06E5D62EB4445_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsCameraImage_m9B592584A2C27917CC80AB290F0A2600FD275951_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageIntensityInLumens_m13362159DD3F8A1AB2C9E9CFFF6DB405374EB174_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageIntensityInLumens_m0672F68F42398824EB559650DE318B6E0C9414BD_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsFocusModes_mE631781ECBAFD54B7F567A62039CC4CC25C9BC2D_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsFocusModes_mCE0E9DF8C54F831E713E751A93BC83144A8849F2_AdjustorThunk,
	XRCameraSubsystemCinfo_Equals_mC0F2554FBD6C4944FE9956C0843718D3D423D86F_AdjustorThunk,
	XRCameraSubsystemCinfo_Equals_mDC6FD25003FEA123FF4D83BAAED03854B0DF2B15_AdjustorThunk,
	XRCameraSubsystemCinfo_op_Equality_m1FB80C7F9E88CD19AB137F45DD5DC1CCD2E3A35A,
	XRCameraSubsystemCinfo_op_Inequality_m109F8E812D78490A4522698532D3D3A27EBFD882,
	XRCameraSubsystemCinfo_GetHashCode_m41B5C3CD9B0D9101488D0D63E8557909BE239EE3_AdjustorThunk,
	XRCameraSubsystemDescriptor__ctor_m218E5DC6846EF28CD00B48704E59A327949C3CAE,
	XRCameraSubsystemDescriptor_get_supportsAverageBrightness_mE3B7483DD61AE2D5AA8AAA60C31EC2D430DA1DD5,
	XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m90F7E1B3733B5D1975217E7FBEEFC2EC6DFDFBF6,
	XRCameraSubsystemDescriptor_get_supportsAverageColorTemperature_m1AEBC59200F29D7BE0B619CC8BCFD669501F7EB6,
	XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_m617CC63B166A095A8F873C73BAF39F86D579CD69,
	XRCameraSubsystemDescriptor_get_supportsDisplayMatrix_m66F85F0187565E0CE7A504CCB21B5F614B90033F,
	XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m67FC7675BE452E50E68679516708CEF60B36C061,
	XRCameraSubsystemDescriptor_get_supportsProjectionMatrix_m75295E7DB06B9C3D81C1B89EC318EDF323091D0C,
	XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mC5EBFF2790C6672050DD1781F0026AA82CDE0A3C,
	XRCameraSubsystemDescriptor_get_supportsTimestamp_m8B204E3D78ED417621176FF6BAC73EBBBCC0B341,
	XRCameraSubsystemDescriptor_set_supportsTimestamp_m675F2A889C4A694BB5F72569DC697BDE0455B3E1,
	XRCameraSubsystemDescriptor_get_supportsCameraConfigurations_m8AB0532CD36CFA0A804E5F8489D4BB0C18488C15,
	XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_m2EFBEC5D6FC11ED5D546B7B75090B56A960F57F7,
	XRCameraSubsystemDescriptor_get_supportsCameraImage_m1647B3EC91C215B91FEA91FB7F33A6949360287D,
	XRCameraSubsystemDescriptor_set_supportsCameraImage_m84810E8929E44D5C62EEC38D5AAA7ED2F8434979,
	XRCameraSubsystemDescriptor_get_supportsAverageIntensityInLumens_m1E8F760E57616EE5EC09CF603F3C965BE249B671,
	XRCameraSubsystemDescriptor_set_supportsAverageIntensityInLumens_m7AD0C0781B44225B04DC966D489E602DA22E8204,
	XRCameraSubsystemDescriptor_get_supportsFocusModes_mF9DEE1BFA76128D329B8B40FCCEAEAB5D1AC5AAF,
	XRCameraSubsystemDescriptor_set_supportsFocusModes_m5BE62BAB571CBEE76F22459E060CA96265B3187A,
	XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3,
	XRDepthSubsystem__ctor_m24423F4A0EF54A1EDA98684496E5973E192C097B,
	XRDepthSubsystem_OnStart_m68DDDE968810C2B434B5484E12D4A5FC655889BC,
	XRDepthSubsystem_OnDestroyed_m85CABCBD92C957237999A4A75E1C59E2061430D0,
	XRDepthSubsystem_OnStop_m0D73B1F233BD846651091FD0D99CB73F76F9E119,
	XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3,
	XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8,
	NULL,
	XRDepthSubsystemDescriptor__ctor_mA74AF85046BBA65B77FA333055F82C2E3F6AD05D,
	XRDepthSubsystemDescriptor_get_supportsFeaturePoints_m741540583DFCEFCA891B728007A03FCF2405EE0E,
	XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m7974B23484377F9C2698B1981BCE830C7BEC6C73,
	XRDepthSubsystemDescriptor_get_supportsUniqueIds_m52B935BE586195AB5FC081CC299C2DA3035781B5,
	XRDepthSubsystemDescriptor_set_supportsUniqueIds_mF89B9E95F36EB311A0C512A7C20BD7CCADB1489B,
	XRDepthSubsystemDescriptor_get_supportsConfidence_m0CACDD3DFE0D8AB4CDFDC351E034CB118251B015,
	XRDepthSubsystemDescriptor_set_supportsConfidence_mBC66AFF12E25475139457FA6DEB1093A57065068,
	XRDepthSubsystemDescriptor_RegisterDescriptor_m9F40B303586BE45F7AACB8B0AA408D242B34F4EC,
	XRPointCloud_get_defaultValue_m71EFAD95365CFFB007E85B39F6CCEB2182FCEEDC,
	XRPointCloud__ctor_m049E9ED3F776B9F25BD217D0B2D714464A5F6C9F_AdjustorThunk,
	XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk,
	XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk,
	XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk,
	XRPointCloud_get_nativePtr_m993688CDDE348BBADB52795B39A4FDEBF2273557_AdjustorThunk,
	XRPointCloud_GetHashCode_mC2934047C0D733F372E51C52E2837DAE9E13259C_AdjustorThunk,
	XRPointCloud_Equals_mC56FA4F7B07E704C529E144B073920A79E599CC1_AdjustorThunk,
	XRPointCloud_Equals_m66CC3D8FEDF0226F8D4F0B6574449E334D849F57_AdjustorThunk,
	XRPointCloud_op_Equality_m3FB26131D214362187D1D66BF8A54B835EB593C8,
	XRPointCloud_op_Inequality_m257A0AC3D7C4F48B96F9FA9C2C8D20B529844006,
	XRPointCloud__cctor_m64FEF8DF3A914F2770A088FFBE1F169FFDE42486,
	XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk,
	XRPointCloudData_set_positions_mC99C23E8AE61A1A3333C1A2F7E0F9DBD6C9F771C_AdjustorThunk,
	XRPointCloudData_get_confidenceValues_mE672D23FB62CF42CA475CF82A678DA16729D618C_AdjustorThunk,
	XRPointCloudData_set_confidenceValues_m46BFC94CE988C46E7FBA1CC34CB0A493AF4E82B1_AdjustorThunk,
	XRPointCloudData_get_identifiers_m6F0A88EEB7DD58C82662346098A4E20CA111C479_AdjustorThunk,
	XRPointCloudData_set_identifiers_m3CDA83EC60EC5AAB982B3C5E0F9AC9E94D41992B_AdjustorThunk,
	XRPointCloudData_Dispose_mB2C769355B6385CE3F8F47E720087F7B7C726259_AdjustorThunk,
	XRPointCloudData_GetHashCode_m0ACAD17C55503EE73995A9DF2A1110FBEF0145DC_AdjustorThunk,
	XRPointCloudData_Equals_mBDA7D40FB197B84AF2E8EB4C5CF6015D09E74357_AdjustorThunk,
	XRPointCloudData_ToString_m91E31F94CB96601D2BA320BE4B993728D1A8DE20_AdjustorThunk,
	XRPointCloudData_Equals_mBE92CAA314FFE99803718F2EABEA3A1B7AE8A99C_AdjustorThunk,
	XRPointCloudData_op_Equality_mFA9DB6C392E640D637EED794FEFEBB8338742C07,
	XRPointCloudData_op_Inequality_m42CFE606B979DB81ACF472E711A2C58FF9DE5DF2,
	XREnvironmentProbe_get_defaultValue_m14B351BF8F54FCFCDA803FA3C29D1590BC0148E4,
	XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk,
	XREnvironmentProbe_set_trackableId_m8E02AF983995D8D544C83C7F170989AFABC13AA2_AdjustorThunk,
	XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk,
	XREnvironmentProbe_set_scale_m75D217DE6DFDA886AAE23446E2CA43C215C178B4_AdjustorThunk,
	XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk,
	XREnvironmentProbe_set_pose_m19121B0DDCFC795C1541A378F506B899BD2F8D0E_AdjustorThunk,
	XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk,
	XREnvironmentProbe_set_size_m341BE92AC4DE11F2CBA00CDE45E6494D9F1D3853_AdjustorThunk,
	XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk,
	XREnvironmentProbe_set_textureDescriptor_m4E112C52FAE45845367E6BB9F9803F5ACF63B474_AdjustorThunk,
	XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk,
	XREnvironmentProbe_set_trackingState_m2E217867A0F5124889681873154F54F5A855A186_AdjustorThunk,
	XREnvironmentProbe_get_nativePtr_mC9CB253F77A64FCD5D1ADC64590E91A793DC8D66_AdjustorThunk,
	XREnvironmentProbe_set_nativePtr_mCA6F60033040EF29236438F2EB9B8AFAB9497F84_AdjustorThunk,
	XREnvironmentProbe_Equals_mABEF3AB481CB2191DE4C790E3A5A245DE1D347D0_AdjustorThunk,
	XREnvironmentProbe_Equals_m8DFEE5B51820BCC164FDFA7F5F4996074A9C5170_AdjustorThunk,
	XREnvironmentProbe_op_Equality_mE4F0DFE44C9C32C3A92C6DAE81B2EB84C0CFCA54,
	XREnvironmentProbe_op_Inequality_m8A390AF78A9078214D4ED6570C8FF981B4312964,
	XREnvironmentProbe_GetHashCode_m638CB5F2CF52A8ABA8778B6B5EB7F13E57CD7B1B_AdjustorThunk,
	XREnvironmentProbe_ToString_m8B856D8579587102C1F500A2F2180361CD7770D2_AdjustorThunk,
	XREnvironmentProbe_ToString_m20D40F2265F40337C75C536779CC935885222B19_AdjustorThunk,
	XREnvironmentProbe__cctor_m4865D642B00DDA22FCC86BF709D5E99DFCAB8279,
	XREnvironmentProbeSubsystem__ctor_m7A3AE7794DA58FE53C8EE9F47F8B84F3D5DF47B6,
	XREnvironmentProbeSubsystem_get_automaticPlacement_m4D1AC97BF886DFA4851A85797CE2FB34246BAA43,
	XREnvironmentProbeSubsystem_set_automaticPlacement_m1B62556B219E960E694D903D797ED5D9E1E998C4,
	XREnvironmentProbeSubsystem_get_environmentTextureHDR_mF1FDCF466DA39A5A1376F5337B8AFAB9C0022E4E,
	XREnvironmentProbeSubsystem_set_environmentTextureHDR_mDA7E8015CC7ED7B5312EDFD33769027C9871CC9B,
	XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5,
	XREnvironmentProbeSubsystem_OnStart_m599490F20ACA420EE977709ABBE76CC44BB66A89,
	XREnvironmentProbeSubsystem_OnStop_m2A5FF05318BF41E42FDA607E8476DC4DDEF7A1CC,
	XREnvironmentProbeSubsystem_OnDestroyed_mBE024AE290ACB5B14118E53E20329A158E1012D9,
	XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_mC5653C998195D72D7A048BE3F4CC07BE81B6438B,
	XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m045B4A7307B5CBAFFEF3DBAFC78DF05F551B6801,
	NULL,
	XREnvironmentProbeSubsystem_Register_mF52AA36EB4EAA59C932C43E45DA567A3EA6D55FD,
	XREnvironmentProbeSubsystemCinfo_get_id_mADBD2988DA174EE595955008050CB74CB19C4882_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_id_m65F71E8D97413215944F75C52F6F9F2088644E24_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_implementationType_m2AAB6F75B1588A46DC09034244ED3C4CEF0BDD22_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_implementationType_m5CDE58834E022AEB4B9E6FD826D2A6140D3D1B3E_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m38F2FBCF91D735F7ACD339C2C6FD013B62C0DC0A_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_mF3AA42AAE10CC81DF831404F415BD34694B08C59_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_m1F2CFC423F2D37975488C822E76CD175A589E8AE_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m200BBBC11A1580CAA151ED498A8B24E27BAB646F_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_mFA0D6FB52DF9C8ACCF6DC3B9C8A11FDB37877C67_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mBC81F9BC67A3FF73D0EB679BEDDFE1D3DA918582_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mF9F46814201602562ACCF4E7AD63E20B0B7C1645_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mC43F8E59BF70D73AC0EB8EE5A9B2D6F92966B3B3_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m444E17E86B43838E6BC1279C1793F40E21E0C210_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m9C630C891056E5E1187AA2433DDC8D7E0F3FF662_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTextureHDR_m88EEFF6EC5EE0395A2C338FC18F8A2DF92AC0E30_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTextureHDR_m772ACB32612EFAF491C33F78DE942FD9E0D89FEC_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_Equals_m87EE67B01CB7C502E31CBBB818CA4C6D12DAB809_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_Equals_m79E504F27913015A7C97F1E64E16A02DC1C4D73E_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_op_Equality_m88D26DC5B25259A6AD95D0F384BF2058E0B48133,
	XREnvironmentProbeSubsystemCinfo_op_Inequality_m262DEAE6D9933DD29F52F6C37435E1FE7EA001F0,
	XREnvironmentProbeSubsystemCinfo_GetHashCode_mFCD281E4EF9FD74889EF9754EF53341C79988F1B_AdjustorThunk,
	XREnvironmentProbeSubsystemDescriptor__ctor_m91F1A02FF56AC51ABA801B94687E393C9AB82F74,
	XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_mE198AF9A486EED050CB81241F91BE6F7DA074632,
	XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_mFB3D77F497F4DF6722A1B7C66DE6F4D9505763A7,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m1FCEBFF75F2F521CC101940F5F8A1022B45B64E1,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_m4E909B728829BACB3845873C9D0A2F41ED14D531,
	XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mA944EEE25188EC82048232D81023516D1298080A,
	XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m2374B2B4F3CF3959F87F0DD13C8EE4895BC9AE25,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_m25ACC17AE99D2D056B11C7BAD34981D0A7444550,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mECFC5E6081D92D77590B981F055E86AE6557873B,
	XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTexture_m5A0F9ABC531CAA2844AE6F07A8F8DD7EF81F76D5,
	XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_m31201D249DB3E60DA61A739A66D19BEFB89B4B7B,
	XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTextureHDR_m0031706D58FD3E85CC2371B096C3AF5884F9E047,
	XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTextureHDR_mD1B9BA66047AA649756F9E13B8AA81456A7E7C5A,
	XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E,
	XRFace_get_defaultValue_m10A49DFCC1786C0E8F3244200F1AC9696C16AD34,
	XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk,
	XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk,
	XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk,
	XRFace_get_nativePtr_m1EDCB59CD67423A2951DBA0DD0C98AB848183F06_AdjustorThunk,
	XRFace_get_leftEyePose_mB6508142768ACD1B9C5EA05224DEF9E690C7F0F1_AdjustorThunk,
	XRFace_get_rightEyePose_m8A78A727975AA070F197E566C8135B2CA45E4996_AdjustorThunk,
	XRFace_get_fixationPoint_mBC16DB0D6E29A8DCAEB022097B502398BF106405_AdjustorThunk,
	XRFace_Equals_mB68B812AD74B02F7B8B82DE62EAE30146238DD3A_AdjustorThunk,
	XRFace_GetHashCode_mB3AC95DBCA3308827747473BF3A6044FCD2DD7B0_AdjustorThunk,
	XRFace_op_Equality_m6C4A7F979B051CF75D03F1084305D6E42BCF4EC0,
	XRFace_op_Inequality_mCFA0FEE8A708FDD9EEF569212A10DA76095B4CA7,
	XRFace_Equals_m1FF3F979A7F289C6CA77DD3290F4A50CAF889ED6_AdjustorThunk,
	XRFace__cctor_m5D1A0A4AD53355FE60D9ACA97DE83DE702C386CB,
	XRFaceMesh_Resize_m6546D0617427A8BDD68E2F53EB85F67B80ED4DE7_AdjustorThunk,
	XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk,
	XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk,
	XRFaceMesh_get_indices_m7EA9FB6B6CE3484262F74546455CB08BA5B5B00D_AdjustorThunk,
	XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk,
	XRFaceMesh_Dispose_m3E7A416718B532DFD7D100D5D0F1F3A9AED96F7E_AdjustorThunk,
	XRFaceMesh_GetHashCode_mBB4E68260D980EAFD29333F2B0EE9B10FAA8040E_AdjustorThunk,
	XRFaceMesh_Equals_m466E0A0D30D307B956CB677D1103AD4F4C86E3BC_AdjustorThunk,
	XRFaceMesh_ToString_mC0D08232897F11B687916F6B1975927411A783B3_AdjustorThunk,
	XRFaceMesh_Equals_m7F90AA84BD56C74C8C6472CE390C706E8AB8120D_AdjustorThunk,
	XRFaceMesh_op_Equality_mE8CF70FC8B019862A4A4408DCF1323CCD8DEDE9A,
	XRFaceMesh_op_Inequality_mA63E23602313310CE2C5DF67CADE3C7D4B9DECBB,
	NULL,
	XRFaceSubsystem__ctor_m69A0FE81F7D83567E0B1F70FE0CC9B37AE5BB7EB,
	XRFaceSubsystem_OnStart_m5206B8B5819B5B818EABC714EE362A3C8F543338,
	XRFaceSubsystem_OnDestroyed_mF97D40C2D112EF388BBF9D902A8B15C993872077,
	XRFaceSubsystem_OnStop_m28860BE62A1CE3037538BAF91072A5E51756D8A3,
	XRFaceSubsystem_get_maximumFaceCount_m1C2E12E9FB9B0EBB26F5167D2C60823463B5D9FE,
	XRFaceSubsystem_set_maximumFaceCount_mFD5EDEB83AE800C0D3C9FD9FDD4C59E3A046056B,
	XRFaceSubsystem_get_supportedFaceCount_mE7D95AB41DF2408E3121F1107671A5D410A9E33D,
	XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7,
	XRFaceSubsystem_GetFaceMesh_m4E1417F6B00F84DB66EFECFE29A8FA2D3ECBBD12,
	NULL,
	FaceSubsystemParams_get_id_mE496E36DA79D6680FDD501331A40FEE5ACA4844F_AdjustorThunk,
	FaceSubsystemParams_set_id_m8F4D745F751A5DBAA600928FE0FC088197F17F02_AdjustorThunk,
	FaceSubsystemParams_get_subsystemImplementationType_mF277DA4D914188A29C632C0A4EF01B833A145DD0_AdjustorThunk,
	FaceSubsystemParams_set_subsystemImplementationType_mD47C85784BA65BD39301E4EEF8CADEC22233E431_AdjustorThunk,
	FaceSubsystemParams_get_supportsFacePose_mF7D44A52EA89803B802EFF223FB73F366CBA23F4_AdjustorThunk,
	FaceSubsystemParams_set_supportsFacePose_m4D057DB195C5497D6430139C6CCE73553EB30B15_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_mFC3A1CCB5F0F3BDD8FAE922CCA8AEFADF31BCD56_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_mA9BB345D83A8BDB50511C19E759B115ECEFFD111_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshUVs_mB44CFFAC24415AA94DBF91C346A700D5CAF7E101_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshUVs_mA7FB3F027FBFD77CE4E217C456D7C7ACE2A8E557_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshNormals_m32FEB03508B5BB7DFC02E78BAC4EC2C6A67632AF_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshNormals_m078C141E010316EB93E3620DE9C36B795EE17994_AdjustorThunk,
	FaceSubsystemParams_get_supportsEyeTracking_m7894CCA2A243A2DE020C5CDD9D82CAD31159C461_AdjustorThunk,
	FaceSubsystemParams_set_supportsEyeTracking_m297F9ABAA20371C328C1A41D1A54CCB53A5E33EC_AdjustorThunk,
	FaceSubsystemParams_Equals_mCC1F4FAA16C7991A05D3EB146ECB4D2864C5131D_AdjustorThunk,
	FaceSubsystemParams_Equals_m382A90011B4E67AF4BBDD015282293128A10DCB9_AdjustorThunk,
	FaceSubsystemParams_GetHashCode_mD2C1F94EFB85920F5801FE988D8E5857DE801F4D_AdjustorThunk,
	FaceSubsystemParams_op_Equality_mB3E1C58B8B66732CFB7B71CCE38665314DFA45C4,
	FaceSubsystemParams_op_Inequality_mC94E0022A973D6D7FCCEB8DEBE541A3D6C86ACE7,
	XRFaceSubsystemDescriptor__ctor_mA1CA67656903ECE685C5546EFF3C208B7FFF9D10,
	XRFaceSubsystemDescriptor_get_supportsFacePose_mB43A745C7907B946DD061435294C5C883A252891,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshVerticesAndIndices_mE4E13309BDD7E3560ADA94222DC272269A5E2581,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshUVs_m3A0F0F232DD7B477E4CF45CFB7497D2BB7F7647C,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshNormals_mCCBCA63B368220DD0BFCCBBDD7BA5EC5AF605B48,
	XRFaceSubsystemDescriptor_get_supportsEyeTracking_mFB1F49080EC91D12B4F6FCF06654E4954BD034C6,
	XRFaceSubsystemDescriptor_Create_m7CE3BB12E0100FD0BB3D56E3BCE603BCF62C78AA,
	GuidUtil_Compose_m23689A8CCFCDF3904D23BE11760E58DC662E35C1,
	NULL,
	NULL,
	NULL,
	MutableRuntimeReferenceImageLibrary_ScheduleAddImageJob_m96F4D42BCF3F1E53B44805A62411FDE2D63C12F1,
	NULL,
	MutableRuntimeReferenceImageLibrary_GetSupportedTextureFormatAt_m62439D0A5E15B37AA9E91A7BF080F4773F7C2A49,
	NULL,
	MutableRuntimeReferenceImageLibrary_IsTextureFormatSupported_mEA4103D5399A43A7FDFD985ADA91AF9906ECCC68,
	MutableRuntimeReferenceImageLibrary_GetEnumerator_mF0AD39CFBE88F86EC1F8D6D905C8AAB4E22F6349,
	MutableRuntimeReferenceImageLibrary_GenerateNewGuid_m9E2A885618C9EDD289C6891D181B6BF8002C6732,
	MutableRuntimeReferenceImageLibrary__ctor_m41DFC85BF0771073834499AED09267690A77DAD1,
	RuntimeReferenceImageLibrary_get_Item_m2A36BD94D16026E02040BF91C477938A6B318EEC,
	NULL,
	NULL,
	RuntimeReferenceImageLibrary__ctor_m4079D1B22201413ED1E4846D4382B1594B58FFE1,
	XRImageTrackingSubsystem__ctor_mEE7E27E4FDC18721F1D9CDDAAC8FFAACC782D4CF,
	XRImageTrackingSubsystem_OnStart_m556B2CED62A708CCEE2040EE04DF1ACC2660C0EC,
	XRImageTrackingSubsystem_OnStop_mB7F9EF0F3DE842CF24BEBFA23642CEEB793C3979,
	XRImageTrackingSubsystem_OnDestroyed_mE87276BA554E6C042C660A051A6EA51B42CFF85C,
	XRImageTrackingSubsystem_get_imageLibrary_mCFF108F9559826539F0EEC73BA0092F7544537B8,
	XRImageTrackingSubsystem_set_imageLibrary_m02A6BB33E8C255D9CFEEF478DD6E39BFBD0F036A,
	XRImageTrackingSubsystem_CreateRuntimeLibrary_m26FE1D40056DEED2BE3D7A090B375184076792AE,
	XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0,
	XRImageTrackingSubsystem_set_maxNumberOfMovingImages_m59B8A966406E06A35D55565FE6628158006A32BB,
	NULL,
	XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_mB0B37BABA6FEECB44860DFFD2A388BBFF48F4F20,
	XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_mB13C74324DB6B9D7E3A69826726BFEBA2E403D69,
	XRImageTrackingSubsystemDescriptor_get_requiresPhysicalImageDimensions_m5A3C84C2BFF8A6B8C0089793F936950F9790CA27,
	XRImageTrackingSubsystemDescriptor_set_requiresPhysicalImageDimensions_m1B830E3AA0EEA77F6E7151B6E29E1ABC565D6951,
	XRImageTrackingSubsystemDescriptor_get_supportsMutableLibrary_m52E13375BBF9591EB70335833AF85F0FB1557FEE,
	XRImageTrackingSubsystemDescriptor_set_supportsMutableLibrary_m054D64646762EC2F122B247322EEBCE0005FB4B3,
	XRImageTrackingSubsystemDescriptor_Create_m1049DA7C21F27833846D6C6E699DD2DA964522A9,
	XRImageTrackingSubsystemDescriptor__ctor_mCD5CADD6B43C92838FA1A87926CC23D53A9715C4,
	XRReferenceImage__ctor_m65A67C3DF0FBA638484D01E1936B29F79D574E20_AdjustorThunk,
	XRReferenceImage_get_guid_m646CE46068C4BA601BC23772D3D807D18836B80C_AdjustorThunk,
	XRReferenceImage_get_textureGuid_m7B247E9E12DB4017B0D4427E59DA33614AD258A5_AdjustorThunk,
	XRReferenceImage_get_specifySize_mB51499BC0F76BF575ACEB77018EA2BB0AB25CE61_AdjustorThunk,
	XRReferenceImage_get_size_m29A6DA526141F214BE2949524305EFE91C07FA32_AdjustorThunk,
	XRReferenceImage_get_width_m70D16B42866C9058944D138F6EC2099182790E1F_AdjustorThunk,
	XRReferenceImage_get_height_m6107C8991299296D2DB27A312E3D341EE654C414_AdjustorThunk,
	XRReferenceImage_get_name_mB454E9E3452D93AC8CCF83A2D1EB1EFA8FD535A9_AdjustorThunk,
	XRReferenceImage_get_texture_m97887B57DD747DCE051484D1C97F1240B673FE16_AdjustorThunk,
	XRReferenceImage_ToString_m6BC181F25F28FCFB70B2035D7AAEFB1E6DDC6FAA_AdjustorThunk,
	XRReferenceImage_GetHashCode_m5178E851EB53F2D106ACBB7C8330F889E1581C09_AdjustorThunk,
	XRReferenceImage_Equals_mE3432D5A1D715669F9AEC65EB524B925549F4726_AdjustorThunk,
	XRReferenceImage_Equals_m001D4B708546DF448C4243079684F6FDEE76FBC6_AdjustorThunk,
	XRReferenceImage_op_Equality_m1583BEC4A3B41BA40F6B04104595A0A153A1D2DC,
	XRReferenceImage_op_Inequality_mA7B9B718C7547A588378425FA39490C701018C3E,
	XRReferenceImageLibrary_get_count_mFC2EABE3C3D8966005C0AB2E74836BDC998995DD,
	XRReferenceImageLibrary_GetEnumerator_mE48D64D91D797FCFFA8D582B720C68F14D521710,
	XRReferenceImageLibrary_get_Item_mD672D0FB305F5209E867F2361EAA542524E3A199,
	XRReferenceImageLibrary_indexOf_mCF228BB01B5658DAD82FF40FE4B0086C2FE2AC52,
	XRReferenceImageLibrary_get_guid_m101D8AFC1E328EBF5DBDED74F7EA8863A3468418,
	XRReferenceImageLibrary__ctor_mF39B0A6A9C1B2777ADD98CAFF08762B601EA5691,
	XRTrackedImage__ctor_m0D4DB0925EB1FBEC466A9D19C627F747A400408F_AdjustorThunk,
	XRTrackedImage_get_defaultValue_mC27C0C8BAC99DFBD1900C92FBA0D4940D86468EE,
	XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk,
	XRTrackedImage_get_sourceImageId_mFEBFE1A21956E0CBF6828407DE0F2209610BF60A_AdjustorThunk,
	XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk,
	XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk,
	XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk,
	XRTrackedImage_get_nativePtr_mE90A65D3EDE7F0190F36BD4BDF2E06FEAD113DAF_AdjustorThunk,
	XRTrackedImage_GetHashCode_mFF30CD39BC82F7A636BF9E0ACF96967C46F07B5D_AdjustorThunk,
	XRTrackedImage_Equals_m626B512ECA4BFBB14918EF13969F8789C3A8A069_AdjustorThunk,
	XRTrackedImage_Equals_mF94BFA9B373C9899F29EBD1F01A15ADA2D6E47AF_AdjustorThunk,
	XRTrackedImage_op_Equality_m9C6903C11D04AACBB3ADD17C5C41D65724C0D708,
	XRTrackedImage_op_Inequality_mD8EB9C084A8CAA1CDE7AD880A552010BE3693CF0,
	XRTrackedImage__cctor_m4E42C0412A7516B29FD431E93E6783C09E1F570F,
	NULL,
	NULL,
	NULL,
	XRParticipant__ctor_mAD579360C7F9E8080480CEF09D82A11308281ECF_AdjustorThunk,
	XRParticipant_get_defaultParticipant_m1ACE083807AB7AC1D3C5C6B008B21D6235E0C90D,
	XRParticipant_get_trackableId_mAF0DAE2613E96C830102678EA49DA306402C7700_AdjustorThunk,
	XRParticipant_get_pose_m9FDF90F628DF1FC812226F06F196A113644C1717_AdjustorThunk,
	XRParticipant_get_trackingState_m759EEC47B61486F19F9312FBBD6B29DD2F0C46FB_AdjustorThunk,
	XRParticipant_get_nativePtr_mF0456897D2F54CC3590C2869FF086F6E2B17D123_AdjustorThunk,
	XRParticipant_get_sessionId_mA6719BC03C781B0E4A4F303E877BF8DA05916D51_AdjustorThunk,
	XRParticipant_GetHashCode_mA6FFFED9EFF3DA3118B4EA9931BFCFB8AB457CED_AdjustorThunk,
	XRParticipant_Equals_m67B3B04FF423328CAAD56ECC253CD4844EC887DD_AdjustorThunk,
	XRParticipant_Equals_mEAC638071C5C48AAB3D4D671F12F7836C7A86F97_AdjustorThunk,
	XRParticipant_op_Equality_m4BB4AAA3AEE332ABAA42551F45F0132FD0876C38,
	XRParticipant_op_Inequality_mC586A7747F2A9F1D462DC377EA72C065B5CF9352,
	XRParticipant__cctor_m76240ACBBABF354611F29C081AC90C77594D43C5,
	XRParticipantSubsystem__ctor_mCDFFB9E0DE58E9CCF9676B99C891CA350E25AADA,
	XRParticipantSubsystem_OnStart_m6E5309E4FAB42A84ADF9EFACFD562B8F5D6F9AE0,
	XRParticipantSubsystem_OnStop_mC0FF0C8B8390C6C39CF7ECA6AE7FF67EA426F5E5,
	XRParticipantSubsystem_OnDestroyed_mEE7B0AE9632BEE7C248922680D30B6EAB50CE266,
	XRParticipantSubsystem_GetChanges_mAFC1419DAF4C1E3FEDD4B14C2561C4413AFA47BA,
	NULL,
	XRParticipantSubsystemDescriptor_get_capabilities_mC77B52797D0A552DE7073B23C3BB84682B700B21,
	XRParticipantSubsystemDescriptor_set_capabilities_m358224840BB1F68335AF4C04FD24F8A025632039,
	NULL,
	XRParticipantSubsystemDescriptor__ctor_m7F1C1756522C4D4E02ABEDBDC32040B796577F0C,
	BoundedPlane_get_defaultValue_mD9C5DCC9919CFB735B2D62B8F4BEF1DAEBA37E89,
	BoundedPlane__ctor_mAC435BBCE5DE883B3BC4AD679D399247096419A1_AdjustorThunk,
	BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk,
	BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk,
	BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk,
	BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk,
	BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk,
	BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk,
	BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk,
	BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk,
	BoundedPlane_get_nativePtr_mF0C7299B1CD00C40972DE1BE13C411594A59D361_AdjustorThunk,
	BoundedPlane_get_classification_mBC7152460D4441EE38BE0A9ACC26F31AC810C373_AdjustorThunk,
	BoundedPlane_get_width_m1E26F5383BD65FF68259DE31FB497B4B376CFA50_AdjustorThunk,
	BoundedPlane_get_height_mB1F3A68A91EBB34C8206E0DE9EB95D170CCC1E01_AdjustorThunk,
	BoundedPlane_get_normal_m1DBB621B1447071A5C7C5F2966A90459B9481078_AdjustorThunk,
	BoundedPlane_get_plane_mB634B619F93280612D0D395F8BD42B8533CEA787_AdjustorThunk,
	BoundedPlane_GetCorners_m3F9CDB18FF9647D6C37D2FE993578C5EE9C3E125_AdjustorThunk,
	BoundedPlane_ToString_m0EE069A6B9579B564EFD59C300277FB7D824E7D1_AdjustorThunk,
	BoundedPlane_Equals_m74FDA713E8EBBF9256546B4B04A1CCF214ED7D8E_AdjustorThunk,
	BoundedPlane_GetHashCode_m39BDD70727BE818F86E2DEEF4E264FE13368B637_AdjustorThunk,
	BoundedPlane_op_Equality_m3D7772E6F97621B26E620EEB821A446C367C0FC2,
	BoundedPlane_op_Inequality_m81F75E9FF3A08BAC48BB998A4DCB979E8362E318,
	BoundedPlane_Equals_mF06B1E1B2C53F0BF0FA541CD4828DFD16E8D789D_AdjustorThunk,
	BoundedPlane__cctor_m1357E51DCD027D6C83B0F1289A93F70653CA92B4,
	PlaneAlignmentExtensions_IsHorizontal_m291A9197CC11F985A33F496091D22AE0B9C42D14,
	PlaneAlignmentExtensions_IsVertical_m0E43C68ABD9D0FF5AF5424EDAD403FAAFBB6B2E0,
	XRPlaneSubsystem__ctor_mC6CCE81B1FE634A34E37D1595EC6189A6D5B28E1,
	XRPlaneSubsystem_OnStart_mEC0E5DACF25139BDB8E43291D30B0C89E4AEDFFA,
	XRPlaneSubsystem_OnDestroyed_m2E16F97A27B3BA56A7B4D30640CBA36598685B11,
	XRPlaneSubsystem_OnStop_mC82629B43719E1ED60448DFB754F41B140ED005F,
	XRPlaneSubsystem_set_planeDetectionMode_mC7B2B3A8A0FB7853FBA4227F3A4DFD8A155E14DD,
	XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B,
	XRPlaneSubsystem_GetBoundary_mB724A7CF46B3AF8C7E01A0A56854F46C595314CC,
	NULL,
	NULL,
	XRPlaneSubsystemDescriptor_get_supportsHorizontalPlaneDetection_mED6CA8897E68F81C48C753D71DC8644CBBE7C350,
	XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m834174F0D747E2D8D8C34EB20D879FFA3E607849,
	XRPlaneSubsystemDescriptor_get_supportsVerticalPlaneDetection_m074CCD30970971A023FAFEB246FD59B546C21374,
	XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_mF2E7FC43C5A6D048BE8A13F7824CC76543C28AB4,
	XRPlaneSubsystemDescriptor_get_supportsArbitraryPlaneDetection_m016EA52D18A21472F28FBAACD4A30969E0891290,
	XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_mCC9B359F247DFD0928E3D968CBD7713C744403F6,
	XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m455A7A695D7C4F238405B4B50472BE0A8118F611,
	XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_m79DA28E3ED06B14E1FEC3EEB72986EA1A420A15E,
	XRPlaneSubsystemDescriptor_get_supportsClassification_m7714830FB71FEFF2AD8C7A35E73E25E9CA9209D8,
	XRPlaneSubsystemDescriptor_set_supportsClassification_mD90B7F7175BF1868A8203B2D91BEDC498469F53C,
	XRPlaneSubsystemDescriptor_Create_mE7A8E8E49F7EB078CE4D76C9F0D883634157EC9C,
	XRPlaneSubsystemDescriptor__ctor_mFF66A6FD68D7DBC951F675E15E230CEADC37B42C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRRaycastHit_get_defaultValue_m17AEBDAC971A56C3FC4C7C4E2E14ECC357658DFA,
	XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk,
	XRRaycastHit_set_trackableId_m8EE8F31C6CC0A5F22C206BB1B020029E9D6E4E3F_AdjustorThunk,
	XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk,
	XRRaycastHit_set_pose_mF79DD76A43B1E6075095517E645F0C2C38864A54_AdjustorThunk,
	XRRaycastHit_get_distance_mCD38ECEDD0FA6EAFEFEC71DB7EE3CF1B82B5CEFE_AdjustorThunk,
	XRRaycastHit_set_distance_mBE7929E6C3D4F4AA7AA6B834CDB1E9A3DAB5C90A_AdjustorThunk,
	XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk,
	XRRaycastHit_set_hitType_m6B306C7B344FD45B9343DA72C16354A2A0C1F781_AdjustorThunk,
	XRRaycastHit__ctor_mFE2DF7F0A38F4507B3C5684B78F0694266BB76B5_AdjustorThunk,
	XRRaycastHit_GetHashCode_mB0A7A65C634E1CA9C70DC17D2B31A6E082D349EA_AdjustorThunk,
	XRRaycastHit_Equals_m80D2CC8EEC73127B553C601D9B6A3CEDCFBCF862_AdjustorThunk,
	XRRaycastHit_Equals_mD3774307DB6D9200AE2C4703CF2CB2D90616051C_AdjustorThunk,
	XRRaycastHit_op_Equality_mC953B64E3B0A0A69D69F0E438FA88D26A537764F,
	XRRaycastHit_op_Inequality_m76F437956D65EFE583A6C3FC80534CABEE699EA3,
	XRRaycastHit__cctor_mF74B94EA0D60B0112CDB7F37D439239F187E55C4,
	XRRaycastSubsystem__ctor_mD6FC049FC72B869A2A78EE56093819D989AD4021,
	XRRaycastSubsystem_OnStart_mDCBB868298D7E902CD0F451B6269D6335393667D,
	XRRaycastSubsystem_OnStop_m7D9BA78977F77076C34E8827033AB470D454DBDE,
	XRRaycastSubsystem_OnDestroyed_m7C95CCEF2EF0D515353FFA806CACFE3C02BD8BB4,
	XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA,
	XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF,
	NULL,
	XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_m58C8F3A796EEB498A31A9BEE387A37E935121388,
	XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_mD39B9FA29B589E0DF23DE7A21012058C0505402C,
	XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_mA8C8F4A9E9B0B85E6BE432488CBCF9A97A5E5F4A,
	XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m4D35B87B7D4284B470A41F46E5261FE953B75035,
	XRRaycastSubsystemDescriptor_get_supportedTrackableTypes_m02F17127CFA033A9D6D84C7F0D53D0BA3FE379C4,
	XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mE17AF0F588A87E741576518F5C851286937BFE27,
	XRRaycastSubsystemDescriptor_RegisterDescriptor_mA164B987D51AD208D957753220E5B1D1A2DB0650,
	XRRaycastSubsystemDescriptor__ctor_m69D7B8410396340E3D54A19B7EE73B4D24562C3C,
	SerializableGuid__ctor_mCB8E3B766CF1C54AA9D17E738C65E47B16ECE296_AdjustorThunk,
	SerializableGuid_get_empty_mCDC698E4D3EE9F3B311588C6FC1EE7CC9E820892,
	SerializableGuid_get_guid_mBBAACA6CC4257BB65BB6178DB4B9B8273462D71B_AdjustorThunk,
	SerializableGuid_GetHashCode_m2220A2C721DACD85F1271B8D17D334323B0CAD2D_AdjustorThunk,
	SerializableGuid_Equals_m0775AC30655B576CF3FC2E92FCD210A864264994_AdjustorThunk,
	SerializableGuid_ToString_mDC25F3F328C27C046B6BAD13E5839C363B4BF98C_AdjustorThunk,
	SerializableGuid_ToString_m5A32B3DAD7464E66B832B85D8E625D32A634B21C_AdjustorThunk,
	SerializableGuid_ToString_m221CA635D117768CCA6A603C67FE1C391954F338_AdjustorThunk,
	SerializableGuid_Equals_mE819A7AFF3AC4EE0DBF8AC4811E3E930367DDFEC_AdjustorThunk,
	SerializableGuid_op_Equality_mFA22287F5A8B93FA4C8897FAE942491C9D72D621,
	SerializableGuid_op_Inequality_mBEDA8B91A7B6D53F2CF4009020AE2501A7451C63,
	SerializableGuid__cctor_mBD80AFE51A38B55A6C19129375843F41B9FAA51F,
	SessionAvailabilityExtensions_IsSupported_mE7271B43B00DD99C98FA00AAAE3A948E0822D624,
	SessionAvailabilityExtensions_IsInstalled_m199B1D6F5729DE0A1BC7144BE38DEF9896B68A33,
	XRSessionSubsystem_get_nativePtr_m7FF5CD90265F723DEA0F552BD0D0E96BFF4011BA,
	XRSessionSubsystem_get_sessionId_mE74B6851048615893D7D4368D3F292314D158D0B,
	XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED,
	XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC,
	XRSessionSubsystem__ctor_m0E7B9E65E53B03A65C53F87CF55E76528E9AF62A,
	XRSessionSubsystem_OnStart_m13F2A47C585607D4B8EB53841958FDFD610256E0,
	XRSessionSubsystem_Reset_m09A1C906E574CD27377602632572058C88958773,
	XRSessionSubsystem_OnStop_m7D5FBBD4A3BA07ECF6D319F972FD09048D339D72,
	XRSessionSubsystem_OnDestroyed_m9698BDE2A34F1C84EC4F1ED3E5582EA8953172BE,
	XRSessionSubsystem_Update_m40F8405ECB47FDC56B0B203F09655E3E5F637EFB,
	XRSessionSubsystem_OnApplicationPause_m5030BDDD5E7BB722D7259D98C988911335751945,
	XRSessionSubsystem_OnApplicationResume_m47B8B29B90F5DDF27221EE1E32D5DBA22C347FBD,
	XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30,
	XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7,
	XRSessionSubsystem_get_matchFrameRate_mC750A3D6F4EA06E45880B76305178E568E04C82A,
	XRSessionSubsystem_set_matchFrameRate_mAE3C92CA2CA959912C6AF7AB1BD10B2B9A9FC54C,
	XRSessionSubsystem_get_frameRate_m0E723AB90D900D24D7DA2F5A2A8009CB4946A62B,
	NULL,
	XRSessionSubsystemDescriptor_get_supportsInstall_m6A46D829025B00AFFCEE06A8A66A3D383AF2757E,
	XRSessionSubsystemDescriptor_set_supportsInstall_m8660DC05AA837607AC160D4300E7D1D204BD1E18,
	XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m331D3B1192D04F6EF98D9172D70EC35A5B24A02B,
	XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_m9371FEB427307794FC0EB7DF361E2F47E6E3F378,
	XRSessionSubsystemDescriptor_RegisterDescriptor_m3EF9E7985B16FFF8FE15FBEDFC87FF1BB811D49E,
	XRSessionSubsystemDescriptor__ctor_m1956ED91DA6DA8D4EAA62906C378B69F5F3E8C1E,
	XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk,
	XRSessionUpdateParams_set_screenOrientation_m977BF9AC1B8FF7224144F0979A8A30325256EE12_AdjustorThunk,
	XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk,
	XRSessionUpdateParams_set_screenDimensions_m74048D3192BAF559FEFAB878921C3EBB68ACB635_AdjustorThunk,
	XRSessionUpdateParams_GetHashCode_m5D06AEAC2DD5497E37C5C8A1E06952186ACDCC70_AdjustorThunk,
	XRSessionUpdateParams_Equals_m0D01CBAE986724E42B3FF0EBE51808915F873B92_AdjustorThunk,
	XRSessionUpdateParams_ToString_mC2C61A95F598C42B879A6E20982DA96B16E23B6D_AdjustorThunk,
	XRSessionUpdateParams_Equals_mAA0877F7CE8BCEC50F568C257794F50C3A4BFDB8_AdjustorThunk,
	XRSessionUpdateParams_op_Equality_m5E717326C2C77A0DB0CA78CA5C1FF63B50955293,
	XRSessionUpdateParams_op_Inequality_m9E436001D2E4D4B649F432824BFB803BE6C7B86D,
	TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD,
	TrackableId_get_subId1_mC31404D1705A36B5554158D187B8E91107425D4D_AdjustorThunk,
	TrackableId_set_subId1_mB5663FA9DA4D0036732A967F0C36F8A5B6604E06_AdjustorThunk,
	TrackableId_get_subId2_m42D794430D6C6163D2F5F839A58383F5BB628AC0_AdjustorThunk,
	TrackableId_set_subId2_mEBB08EA635B97F94BD22E8034E35CB0CFDF0FA8E_AdjustorThunk,
	TrackableId__ctor_m620606AC246BEEC637A748B22FAB588CB93120B8_AdjustorThunk,
	TrackableId_ToString_m9C04F12E2DA81C481BAABC989B14E8B3509DADB2_AdjustorThunk,
	TrackableId_GetHashCode_m6F1171936847F6A193255FABDCA3772D7AE57328_AdjustorThunk,
	TrackableId_Equals_m513A2978A536C42A1AFE1A4D42BEE78EE056BD28_AdjustorThunk,
	TrackableId_Equals_mB8FF48A7F895DEE1E826EDD6126475258BE9ADC2_AdjustorThunk,
	TrackableId_op_Equality_mDC7BF74A9CA0E8F34E4BD3C584918380A624D3E8,
	TrackableId_op_Inequality_mFBEAC6D64B8228DF26E71F7F78CE2B192C55A4BF,
	TrackableId__cctor_m874A4E0F53AF39A7D95AEA9A0E8164BA2B6EA32F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRTextureDescriptor_get_nativeTexture_mC7FFC8C9D5E8C5BBD93F7A7E95B29253FD59770B_AdjustorThunk,
	XRTextureDescriptor_set_nativeTexture_m037F49B441D20D89651EFC2D76B7303094FB01F6_AdjustorThunk,
	XRTextureDescriptor_get_width_m66B9E821EBE5FEBAB7A9B589A056462FD2E35D04_AdjustorThunk,
	XRTextureDescriptor_set_width_m43B69CA429B7BEF091AAA695EDB6C96B6F575B97_AdjustorThunk,
	XRTextureDescriptor_get_height_mFC30414502C03B7BDD149DFFC374ACE0BD472755_AdjustorThunk,
	XRTextureDescriptor_set_height_m294373970C66CDC1A8ACDCBBCC49F19E39FB6F61_AdjustorThunk,
	XRTextureDescriptor_get_mipmapCount_m206E924935A23EB8B06CE8FC1E98BC74B09247F3_AdjustorThunk,
	XRTextureDescriptor_set_mipmapCount_m9EEB76516BF044C734C20D98400B628711ECC1F1_AdjustorThunk,
	XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk,
	XRTextureDescriptor_set_format_m50B0219DA4339E2C4D270FCE5CE5645D192D785B_AdjustorThunk,
	XRTextureDescriptor_get_propertyNameId_mCF70D57DDDB24C2F3521DA86DFC1131DB29B8D80_AdjustorThunk,
	XRTextureDescriptor_set_propertyNameId_m05A26233A1485B768874953F567B2EF7AFA2C39C_AdjustorThunk,
	XRTextureDescriptor_get_valid_mBDA97DA3C73C0B8282A29606BFD70F1C29C0B4AE_AdjustorThunk,
	XRTextureDescriptor_hasIdenticalTextureMetadata_m7EF6E9887C77831839D7AECC3B8E51022821A5A8_AdjustorThunk,
	XRTextureDescriptor_Reset_mACE0F00A599DCF65581C7363C1B80C178242B065_AdjustorThunk,
	XRTextureDescriptor_Equals_m8198CAFC2D9A7FA7941C2B587F6FB1B9FE5918EB_AdjustorThunk,
	XRTextureDescriptor_Equals_m162675AC01545EA2A8149CE27A70E811C9A7B3D5_AdjustorThunk,
	XRTextureDescriptor_op_Equality_mFD1888CDCE7DB12A55D71764C96D78106AEFF04D,
	XRTextureDescriptor_op_Inequality_m41878BCABB8BA2BFF564EADBB0B277A06A4D50D4,
	XRTextureDescriptor_GetHashCode_m686D69173CDCFB2CB96E32C948A0EEC176ED19AD_AdjustorThunk,
	XRTextureDescriptor_ToString_m8F80DF64DD7FC44FBFB6FFD42FA3B7265FB016C2_AdjustorThunk,
	Provider_Start_m8C851B71F2BA5DB2E35E21A5C246D51D014E7BCE,
	Provider_Stop_m9354EC0691E8052B903375FFC605B0D9CAF96B76,
	Provider_Destroy_m23E1FFE47E35B396EF1442C2982D65C61E33D505,
	NULL,
	Provider_TryAddAnchor_m33AF5D97A8A579C4C7052688B990BD23B5412599,
	Provider_TryAttachAnchor_m593EAA09284A4A0B28C4916A477A1BC3E4E37015,
	Provider_TryRemoveAnchor_m137C88B267C5579CCC2CF50D13AD6F718C801EE2,
	Provider__ctor_m52F6A325C30DBE037094A0F03FE674C11362C77A,
	Cinfo_get_id_mAC0D6F0EE0BFBCF1981F31E4CC1D7FBB50058223_AdjustorThunk,
	Cinfo_set_id_mC90D1044C7FE3F23A39E37B9AB475BCAE7F60E6D_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_mABD347CF43B54CF9519A4D574ECE2BCFCF7856EC_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_mB078866AA16F3E677F5E35E16549748990903BC2_AdjustorThunk,
	Cinfo_get_supportsTrackableAttachments_m63525509E033418A2D2E8D8DD2DB4F1617F54002_AdjustorThunk,
	Cinfo_set_supportsTrackableAttachments_m5B26FD00AEE893C614CE7E9E6C4356D4F69A015D_AdjustorThunk,
	Cinfo_GetHashCode_mE01CBCB65F9B293FFF906D9CB9ADA20C6F9F8B61_AdjustorThunk,
	Cinfo_Equals_mFFB6723FA7AC8AA1E1E8884493D0CDF35817F734_AdjustorThunk,
	Cinfo_Equals_m0B77F3FC1837038B76DA69AD8663F3CC55D49610_AdjustorThunk,
	Cinfo_op_Equality_m48A2F12F777C202965C797DA3C698B71A0D74B3D,
	Cinfo_op_Inequality_m2817AAD7A107F0F76F21A6A4D8FAB09988EAD5C1,
	Provider_get_cameraMaterial_m82824265BAB7CF990BA7F189A7DBAC583B06D7C4,
	Provider_get_permissionGranted_mDE63F618AFC3A029DE7E385BBBA9E76440848945,
	Provider_get_invertCulling_m59869AA95B9EBE03DE2D27823F628D18566A500A,
	Provider_Start_m1E551A4059E6BDC166AE2D8FEB5A9076D623AE1D,
	Provider_Stop_mFCF6A1C3AC86A90AC583045364AE614AC6100E1F,
	Provider_Destroy_m9A3E04CAE42529F3587873CB8BA73B5217293BE4,
	Provider_TryGetFrame_mADCD409D367FCAD1D91F0AC1ED9A3CC836CB134E,
	Provider_get_cameraFocusMode_m7EE0C12F43F40239D19ABB3F9DEB3F786E7AC027,
	Provider_set_cameraFocusMode_m517F19394BAA8F41728780B42E448F36AEED9F78,
	Provider_TrySetLightEstimationMode_m14CDBB2644C0ED1433C4CEA6FB10C3EBA4C16560,
	Provider_TryGetIntrinsics_m8A592B09EC5B6DB37A665C2B276736932B63FCA1,
	Provider_GetConfigurations_m9FD5BBC0B7C6270E7371BC88EC5EB0B38CBEACE2,
	Provider_get_currentConfiguration_m7F407DC6CC63A4DA3D60FB4CE62D2C80F97A9D9B,
	Provider_set_currentConfiguration_mCBE318AC26E52C03758632E78AD1F1EF5291B4C5,
	Provider_GetTextureDescriptors_mFC6F6C5A3B399ACAD7A8E78A93E73ABA8197F4D3,
	Provider_TryAcquireLatestImage_m49208EE7FA4A3104978EBEA865DB3CDED56B1679,
	Provider_GetAsyncRequestStatus_mC46D89C12B6EBD90BC37DC4007B9A5462DE01E56,
	Provider_DisposeImage_m2E937A0EAEB2AD3EE55875A4F80EDBF9C099A914,
	Provider_DisposeAsyncRequest_mAE9436B72F3EA27F1118B6142D6FD78DFAE95669,
	Provider_TryGetPlane_m58B3A21E6D2D01DC824153FB44A9E71C08334F07,
	Provider_NativeHandleValid_mA0213BCC7EACEAA8CDFACC686485CFA60702C729,
	Provider_TryGetConvertedDataSize_mA677B9758E7F5B9E4388F484050B039164072FA3,
	Provider_TryConvert_m5C2DEE74EE49310B195FF0D7E8C6ACAD3DE9F057,
	Provider_ConvertAsync_mB43A1E6B21B6B136EC2685CBD67FB9C6CA0ADE7F,
	Provider_TryGetAsyncRequestData_mD5AB278261ECB97E7C00D143600D26557858C367,
	Provider_ConvertAsync_m701AE2B6FBFA7D6F4783D178FCB7B0598AE6C016,
	Provider_CreateCameraMaterial_mC7C8A8DB289C964715A06F0BB71AB3160FDBA1D9,
	Provider__ctor_m7B7ACE576C7DDE24A0AAEF774AC6AA35834FC8B0,
	OnImageRequestCompleteDelegate__ctor_mD9BE79ADAAEE0A44060E897728EA37196AF5ED47,
	OnImageRequestCompleteDelegate_Invoke_m835EE17741E50584491577BD5B9061FB12EB95F0,
	OnImageRequestCompleteDelegate_BeginInvoke_m3102FE931688E2734845C0CB9FED184CA15B48AC,
	OnImageRequestCompleteDelegate_EndInvoke_m17DDA25840DD080989CEC9304CBD369D65AC82AD,
	CameraImageCinfo_get_nativeHandle_mD1CE0F2F44CFBA90188A3DD71970DC947D548419_AdjustorThunk,
	CameraImageCinfo_get_dimensions_m55D78C58843F9F6AB11D634BBD0A4F17FB4CBFED_AdjustorThunk,
	CameraImageCinfo_get_planeCount_m261902854A879D48561867655A57295A2B7E44D6_AdjustorThunk,
	CameraImageCinfo_get_timestamp_m482C0B93963CCBB4953A1F375D5CF5862F758ED2_AdjustorThunk,
	CameraImageCinfo_get_format_mE92FD0C4F73255F6CDCCDC1CACBCAEF1E5E5605F_AdjustorThunk,
	CameraImageCinfo__ctor_m0D4E4A0EDDB0E69C7090DEB9D83D5E509051AA4E_AdjustorThunk,
	CameraImageCinfo_Equals_mED7B9DECD14313D8D33586A50B22F2B3CFD81EC8_AdjustorThunk,
	CameraImageCinfo_Equals_m5410D9B5B20D2CD87049EB0A89362E8777B02DF8_AdjustorThunk,
	CameraImageCinfo_op_Equality_m637A1532BF8201D778300760BBE07AD54798ED44,
	CameraImageCinfo_op_Inequality_mBC319EA25C09E3E56AF9CFDED4851A610118B599,
	CameraImageCinfo_GetHashCode_m9E0E4A3A6091C9FF9D72E70AEB5820DD1A0959FE_AdjustorThunk,
	CameraImageCinfo_ToString_mD15A4DF77EC11F2BFC6E4A94AC781CDDF14A7BB7_AdjustorThunk,
	CameraImagePlaneCinfo_get_dataPtr_m0C0144ED8D52B8456B579FA31C3159780E5161D5_AdjustorThunk,
	CameraImagePlaneCinfo_get_dataLength_m6CD246242017D808B54EC7E57C276CCA724AFA22_AdjustorThunk,
	CameraImagePlaneCinfo_get_rowStride_m5594C13CC656157844AF9045800D49868C1A65B3_AdjustorThunk,
	CameraImagePlaneCinfo_get_pixelStride_m83C1A6151625091937C7DD696C10C39CBE4C084A_AdjustorThunk,
	CameraImagePlaneCinfo__ctor_m2623B9CDD7119BB1856EB40F5F53A52EC77338DF_AdjustorThunk,
	CameraImagePlaneCinfo_Equals_mA5382E8A5C97636DBB423D83FDBDC6E6C4AD2F6A_AdjustorThunk,
	CameraImagePlaneCinfo_Equals_mFA864D2AFF1CCC3F62ED3B107F6B50A8D66466CE_AdjustorThunk,
	CameraImagePlaneCinfo_op_Equality_mB630411D1A473447EA9D2B8147804C08E89A33D0,
	CameraImagePlaneCinfo_op_Inequality_m10D27EDCFFCEF3E8A490E753F5B40837E6C77F8D,
	CameraImagePlaneCinfo_GetHashCode_mBB8FCE75397290CF3F692965D989A11927B16162_AdjustorThunk,
	CameraImagePlaneCinfo_ToString_mD7AEB170BA8CAA9EABF9245793A993DB435584BD_AdjustorThunk,
	Provider_Start_m55D0D69DA1EB55A51D5CA298C7159649513772FD,
	Provider_Stop_m8E387BA8DE2006668F390169248D1B5EA4E5FE86,
	Provider_Destroy_mAED2A0A426BCC36ED705D6A37AA9FC49826B15B5,
	NULL,
	NULL,
	Provider__ctor_m5138BEFB55280F1416C10DAF84C39ACE4AF3BCC1,
	Cinfo_get_supportsFeaturePoints_m167B1DCD4173C6CB2AAC5E8FA35759AE4D4BF385_AdjustorThunk,
	Cinfo_set_supportsFeaturePoints_mB3633125ACFBA430C6EC66F3FF8E5BFEC72EC360_AdjustorThunk,
	Cinfo_get_supportsConfidence_m2AB397C1B754341CC18399C796866FDC8FF597F0_AdjustorThunk,
	Cinfo_set_supportsConfidence_mD7DE3DC81C6783C66AAE15A10301DE202520605C_AdjustorThunk,
	Cinfo_get_supportsUniqueIds_m72987F3CF2CB4081D548EF34F13876D607CBD807_AdjustorThunk,
	Cinfo_set_supportsUniqueIds_m416FF5EC15306E37DC3436BBB02B4998D64B62C4_AdjustorThunk,
	Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk,
	Cinfo_set_capabilities_m2FFCFE5025136EFB71DDF302E4B81E9287D9A39E_AdjustorThunk,
	Cinfo_Equals_mDAB1544CED67FAD91D9F273A32EBD01B18EC87ED_AdjustorThunk,
	Cinfo_Equals_mF847824268A42C4CC910934F8454B95DD30C4C1E_AdjustorThunk,
	Cinfo_GetHashCode_m114731847A78B883C2EC5BC32DE8C412D4684A58_AdjustorThunk,
	Cinfo_op_Equality_m62CDABCB830B7C6CB3374E9AC63B1F65DE2BAA13,
	Cinfo_op_Inequality_m42029F02D73CBB73B559956028B31B4937C3D418,
	Provider_Start_mAFCFA17EF4ACFE9FBC08B13F134090166C58769B,
	Provider_Stop_m3E3C80C510852F9FB52EC7545A13B54966F74274,
	Provider_Destroy_mC7C05E996737BBB1329DDF749D606F300C1B6997,
	Provider_SetAutomaticPlacement_m23F1429651885D6F7C52D08A9CF830BFCA39E3F9,
	Provider_TrySetEnvironmentTextureHDREnabled_mDB08C8517BE14AAA988ECE63B602E18F280A7511,
	Provider_TryAddEnvironmentProbe_m2545C50FB75912508F2C47C3A21110B1DFC13CDD,
	Provider_RemoveEnvironmentProbe_mD545F8CFF2F119ED73A599872B0334663BEC605D,
	NULL,
	Provider__ctor_mEC07F574A6B6CB7E8FB489D25BEECA335CA612EC,
	Provider_Start_m602C2D2B0EE7BB0D9ACD710E52701925452282E7,
	Provider_Stop_m5198663A8E3B916507D39C2DCE156332D374308F,
	Provider_Destroy_mB56D0D2459279C2AA9910C11CFAC112FCBBE3260,
	Provider_GetFaceMesh_mE947DDC1126A9BD41BD9B252DDA7755200DF1A04,
	NULL,
	Provider_get_supportedFaceCount_m3376126BA49A086BB0CAAD562498D6FE462C047C,
	Provider_get_maximumFaceCount_m2A8549F526067C1CBD00975719F2FEC37D7365D8,
	Provider_set_maximumFaceCount_m91DF83E9C467C622B415B744C790B25D0C3F08FF,
	Provider__ctor_m4BF4C3C6CA02410A5EC47A5C7FF50D08AE7498B0,
	Enumerator__ctor_mDA14A75D2CAC675AB6AE4FA7FDCBCD54AAE7CE39_AdjustorThunk,
	Enumerator_MoveNext_mF47691FD5F45BCC88BDCB9D3E30B261CCEC7694F_AdjustorThunk,
	Enumerator_get_Current_mB2B2A92CE85E0846FEDDE76130EE8D6CD32A799C_AdjustorThunk,
	Enumerator_Dispose_mD3CBE521602677CD1576DD33FF5F6F9EA90F867C_AdjustorThunk,
	Enumerator_GetHashCode_m76050DFEB3E8573E39DFCF7058E9939F0E059067_AdjustorThunk,
	Enumerator_Equals_mB7BB7CC1A8D6E25C8506EE6E7ECB2CD93724EF54_AdjustorThunk,
	Enumerator_Equals_m476069B8D9916F437C5D6AEEC13D21F0E2CF1C29_AdjustorThunk,
	Enumerator_op_Equality_mF9350929F4D0B5FF93149EA1CBADB8206BD912EB,
	Enumerator_op_Inequality_m561C568546544E4EB9E74BC1F3F68826364FB950,
	Provider_Destroy_mF8BC4EBBD16FAE7168D856A68D59234CEA27ACD5,
	NULL,
	NULL,
	NULL,
	Provider_set_maxNumberOfMovingImages_m968F46966BD0E231991BF6834577536FB174FEC7,
	Provider__ctor_m12B077B91F113E69002DF00BFFF13DCAFC550C5F,
	Cinfo_get_id_m146AEBFC1F34906EF7CB853DB90B28AAA8449D98_AdjustorThunk,
	Cinfo_set_id_mFA87FA52172846CBD4587F2E207D65A097B842E6_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m3715508999808DEC6A812A1D228A2472ECEA48C4_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_mBBC866C6F4207A69F7351857F66D683E0EAF8FAF_AdjustorThunk,
	Cinfo_get_supportsMovingImages_mAB2286D926C059743ACE6E6DABCFAAB7939AC80F_AdjustorThunk,
	Cinfo_set_supportsMovingImages_m2898F758BCCE6DDC222E828D855CEFC89C58EF2C_AdjustorThunk,
	Cinfo_get_requiresPhysicalImageDimensions_m548502FBF010625D9ACC2869E46D1CCEE4A8B83E_AdjustorThunk,
	Cinfo_set_requiresPhysicalImageDimensions_m57E69E5A56010C3A0237BA861BB39CF7C6CBCBFB_AdjustorThunk,
	Cinfo_get_supportsMutableLibrary_mD86A73E2B7F9A03BBD9DBCF47BF097C2F25194BC_AdjustorThunk,
	Cinfo_set_supportsMutableLibrary_m0635990374A918BFBAA137EDBD9128C012E86CFC_AdjustorThunk,
	Cinfo_GetHashCode_m30FEF663E95BCA50174085CAE5EC3E6B207CF094_AdjustorThunk,
	Cinfo_Equals_mB17F6F32907C286C2B660B6E9E1BCC20A651F941_AdjustorThunk,
	Cinfo_Equals_mB207ED3E11BCF5AEC1A56818611A0082753E8DBC_AdjustorThunk,
	Cinfo_op_Equality_m4BFD25F1452FB829B4A52A36D4DA2F85473241F6,
	Cinfo_op_Inequality_m7BC4DE6B4A18849F4559DB3552AD80381AFAB7A5,
	Provider_Start_mC9D2F2E358AB53A83EF9221BC7A777065D529704,
	Provider_Stop_m43705D46DCCBAA2E9D22AD73CF01AF7D2706E373,
	Provider_Destroy_m4E9C920434482C4410E852E4DC7F300E52623EB3,
	NULL,
	Provider__ctor_m9F9BA5B0BC7D73106B244AC16958977AFC8F5C64,
	Provider_Start_m585D4A4BA37DE9B97192C0678686AB867A50F883,
	Provider_Stop_m36967BE370E8E0CA5F5A2C7CC0C02A4F06B1173D,
	Provider_Destroy_m66246D41CB9794304B34F89E0A9D9F2557BC55AB,
	Provider_GetBoundary_m20B566BA701A693EBED89F7F907C1C204F52B365,
	NULL,
	Provider_set_planeDetectionMode_mE0054191CF7CC3CD46C4EB1F904714641B1DB92F,
	Provider__ctor_m99298F30B946FE819FC58BD35A71C32A96C9883C,
	Cinfo_get_id_m68FCB4621B357B690E4C43846221E241455A1D99_AdjustorThunk,
	Cinfo_set_id_m9211F9ADC4DCFA1AAB5AA9F662EE6510D6FE01EF_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_mF924CAAE546DCF3AB899CD4ED8AC2B75B4B0706C_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m74075897385685A6E0753F2EE29CD77A90A22E6B_AdjustorThunk,
	Cinfo_get_supportsHorizontalPlaneDetection_m0FB9CC965AD3A99988175C8D1669D341BFB72214_AdjustorThunk,
	Cinfo_set_supportsHorizontalPlaneDetection_m6BA5B6FD1C2FDF236AEE15957FD1F1837C394304_AdjustorThunk,
	Cinfo_get_supportsVerticalPlaneDetection_m3C7470BF80CEB7F507540383C2BB02A5B2201AAE_AdjustorThunk,
	Cinfo_set_supportsVerticalPlaneDetection_m386B3816E8C1538AB58318D55D9C64D1113C1B3B_AdjustorThunk,
	Cinfo_get_supportsArbitraryPlaneDetection_mC75F86CCCBA2ED8AD6283EF458F3897CB78DAB3C_AdjustorThunk,
	Cinfo_set_supportsArbitraryPlaneDetection_m625EDF8616A904C1D2C3B9DB1B52A28A0D3EAF06_AdjustorThunk,
	Cinfo_get_supportsBoundaryVertices_m6E514112FA9F617953047AF6C2CD98895D587E84_AdjustorThunk,
	Cinfo_set_supportsBoundaryVertices_mFC986523905272E58728731CEE06B47DD4ECAC3D_AdjustorThunk,
	Cinfo_get_supportsClassification_m815046C4B90F936F4736942441D020EBB8C57023_AdjustorThunk,
	Cinfo_set_supportsClassification_m461A917AC95A1EB92C0A10334018D38C8D82348A_AdjustorThunk,
	Cinfo_Equals_mE08723A87EF2FC94FD64BDD93A176F3497518F7D_AdjustorThunk,
	Cinfo_Equals_m3B3C974BB1055B352CAAE2961A7374DA559049F8_AdjustorThunk,
	Cinfo_GetHashCode_m9F3BA097C011CC6998676138C52EF6306C09BBA0_AdjustorThunk,
	Cinfo_op_Equality_m92CAE095C4DBAB20ED4E573E5885F7C0DE881B7B,
	Cinfo_op_Inequality_m8735071162549242673CD1298CFFA59E125EFD25,
	NULL,
	NULL,
	Provider_Start_mF5193E0D9A12A1286389F3B08F0FA5675ADF5CC5,
	Provider_Stop_m23081AA73D0BE12116F4F83A63474305DFBC7E3B,
	Provider_Destroy_m1228BCFCB0887BC20C2E5CC554CD29C1DE1931EB,
	Provider_Raycast_m6063A859AC10ACF7F27A4AE0AAD82C3A0DF831AE,
	Provider_Raycast_mF885EFF5FFD12196F53C9FE405EC523020C35CE2,
	Provider__ctor_m15999B3E2C2B67AD264BEE04B753C69303256473,
	Cinfo_get_id_mCCD575E1E7E6E7E7166A4B7F0AF9E7F023FC3FCA_AdjustorThunk,
	Cinfo_set_id_mDBC061879B3E989FF064E7E31CFC85ACD142199B_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m65BDDAA14217AD17F0928287A8428D7B33A3634F_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m9670297F5DC91608B606E2B8A7E4C2643236D65A_AdjustorThunk,
	Cinfo_get_supportsViewportBasedRaycast_m8D0A14E0E43F99FCB89A3D87F954D657ADC0C514_AdjustorThunk,
	Cinfo_set_supportsViewportBasedRaycast_m42B64A1095C52F16217EBF1D5ABFD7353DA35233_AdjustorThunk,
	Cinfo_get_supportsWorldBasedRaycast_m327E8B0EE9C3103FBF490CA62FE5E1A51EF62C9F_AdjustorThunk,
	Cinfo_set_supportsWorldBasedRaycast_mBF04DD8B3208A7D9C98419FEDC8CB012F7253DF5_AdjustorThunk,
	Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk,
	Cinfo_set_supportedTrackableTypes_m13138A57079E692472B33A4B216D5568852BE652_AdjustorThunk,
	Cinfo_GetHashCode_m155C084A788BBE99BBD85B2B06D7CC74DF76D636_AdjustorThunk,
	Cinfo_Equals_m434EDD8246018244C8CBBD881D91CCDBF5437EF6_AdjustorThunk,
	Cinfo_ToString_m2DFF2387C99F28931C7E802878564A3694D51453_AdjustorThunk,
	Cinfo_Equals_m21B31135C0E69990482BE0286436FF51748CF92B_AdjustorThunk,
	Cinfo_op_Equality_m6846FB5AFD2270316EB39A768A5E927FE4DFC82E,
	Cinfo_op_Inequality_mA22683D29D9FC60CE5D84B46B2B9C0B8A9CE0920,
	Provider_Resume_m9A2A311FC87427ADA41FD6262222724BA56B1C71,
	Provider_Pause_m0BCA2AD4EC520FDDDD0B6E3698C0CD6343B35761,
	Provider_Update_mD118C3E90A2CBFCC6486562F558B46C908768A91,
	Provider_Destroy_m119B0D0E53B60B0F95778495FDF2939317AA30C5,
	Provider_Reset_mD263E65FDBC3D80860FAF5ED5AEE4C91E3D914D5,
	Provider_OnApplicationPause_m3EF5E05F1A459E50420B4B820CA19F06AC2F0735,
	Provider_OnApplicationResume_m7E5873546FD4386540C79DE2BD1CC918D8B1E65F,
	Provider_get_nativePtr_m87C8937571D73F8B99BA224439020760CEFF110F,
	Provider_GetAvailabilityAsync_m9CC6F74169601931E94DA1177C34A543048B0A01,
	Provider_InstallAsync_m074282442B17B260C246A17F8D7E1C4E947A6017,
	Provider_get_trackingState_m9E8D77D2BA6BD8F3508CFA530482C6D790DCFF35,
	Provider_get_notTrackingReason_m3EF060E8F7A23B6CD196E1B7FCF2F34457B065AA,
	Provider_get_sessionId_mD2F3D74F1EB6B876B010BCA69CE0E29D80FB2BA7,
	Provider_get_matchFrameRate_m4DCFFB4B857699910A230A596664E86D68103583,
	Provider_set_matchFrameRate_m166192F3055E7A5B35C8183215F15AB08DC8B08E,
	Provider_get_frameRate_m2037F0EC2A42B1295115701731DF1B8CC098F6D1,
	Provider__ctor_mFCC52955F6E8D4F49818B78E2F6E7B27CD0222F2,
	Cinfo_get_supportsInstall_m010F6B6254015F5F477114A35C4F11F4A3334E2E_AdjustorThunk,
	Cinfo_set_supportsInstall_m4295AB46C19802B003C61D7EB79DC8D02CF14B80_AdjustorThunk,
	Cinfo_get_supportsMatchFrameRate_m7DD61A2A7B767E475C97DF33FA4785C2DC12B6E3_AdjustorThunk,
	Cinfo_set_supportsMatchFrameRate_mE43FF83622414EA44D02418EC98B1DA8DDFFDBD6_AdjustorThunk,
	Cinfo_get_id_m038FCFC448004E0F21D8DF6F22FBFA0F5AE14870_AdjustorThunk,
	Cinfo_set_id_m8E2A1220FE77B46B870237AE788DFEE34F6C29CB_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m27F97E21F5CC1903B5358E2C386343D53F3379BF_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m9591600428F1364957BEDD8C12C1B734BBA2BF85_AdjustorThunk,
	Cinfo_GetHashCode_m10FAF407C61975E8F03E5F4B961BC2583EA369A9_AdjustorThunk,
	Cinfo_Equals_m2AB456C7BFFE6923D76AE087F4548493143B1B7B_AdjustorThunk,
	Cinfo_Equals_m1FEB5A86DE5F73249471CC686193B327ED687B67_AdjustorThunk,
	Cinfo_op_Equality_m12DF3BFC611B815E5935F52A090B53F9575F3342,
	Cinfo_op_Inequality_m3D32A0D12104E275C89DCE4C40764818C082BCC6,
};
static const int32_t s_InvokerIndices[940] = 
{
	1828,
	1829,
	1830,
	1831,
	1832,
	10,
	15,
	728,
	10,
	1833,
	9,
	1834,
	1834,
	3,
	23,
	23,
	23,
	23,
	1835,
	1836,
	1837,
	1838,
	14,
	89,
	31,
	1840,
	1841,
	46,
	46,
	1844,
	1845,
	10,
	1846,
	-1,
	23,
	10,
	9,
	1847,
	1848,
	1848,
	14,
	10,
	10,
	1849,
	788,
	1850,
	1851,
	14,
	10,
	9,
	1852,
	1853,
	1853,
	181,
	739,
	739,
	1517,
	1622,
	1622,
	10,
	15,
	10,
	739,
	466,
	739,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	859,
	859,
	859,
	859,
	859,
	859,
	1854,
	9,
	1855,
	1855,
	10,
	14,
	1849,
	1851,
	10,
	10,
	10,
	32,
	10,
	32,
	466,
	344,
	89,
	3,
	1856,
	46,
	1857,
	1858,
	1859,
	1860,
	1861,
	1862,
	1863,
	23,
	1845,
	23,
	10,
	9,
	1864,
	1865,
	1865,
	14,
	1866,
	1867,
	1849,
	1851,
	10,
	32,
	10,
	32,
	1868,
	10,
	1869,
	9,
	1870,
	1870,
	14,
	10,
	32,
	10,
	32,
	1871,
	1627,
	10,
	9,
	1872,
	1873,
	1873,
	14,
	1501,
	1501,
	1849,
	1874,
	1875,
	9,
	1876,
	1876,
	10,
	14,
	739,
	343,
	739,
	343,
	739,
	343,
	739,
	343,
	10,
	32,
	1877,
	9,
	1878,
	1878,
	10,
	14,
	23,
	10,
	32,
	10,
	32,
	23,
	23,
	23,
	1879,
	14,
	859,
	1880,
	1881,
	1882,
	89,
	14,
	1883,
	89,
	859,
	1884,
	37,
	32,
	32,
	898,
	30,
	1885,
	1886,
	1887,
	1385,
	1888,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1899,
	9,
	1900,
	1900,
	10,
	1901,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1902,
	23,
	23,
	23,
	23,
	1903,
	1904,
	14,
	1906,
	89,
	31,
	89,
	31,
	89,
	31,
	1907,
	1910,
	1829,
	1831,
	1832,
	10,
	15,
	10,
	1911,
	9,
	1912,
	1912,
	3,
	1913,
	1914,
	1915,
	1916,
	1917,
	1918,
	23,
	10,
	9,
	14,
	1919,
	1920,
	1920,
	1921,
	1831,
	1922,
	1490,
	1491,
	1832,
	1923,
	1490,
	1491,
	1924,
	1925,
	10,
	32,
	15,
	7,
	1926,
	9,
	1927,
	1927,
	10,
	14,
	28,
	3,
	23,
	89,
	31,
	89,
	31,
	1928,
	23,
	23,
	23,
	1929,
	1838,
	14,
	1930,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1932,
	9,
	1933,
	1933,
	10,
	1934,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1935,
	1936,
	1831,
	1832,
	10,
	15,
	1832,
	1832,
	1490,
	9,
	10,
	1937,
	1937,
	1938,
	3,
	347,
	1913,
	1913,
	1939,
	1940,
	23,
	10,
	9,
	14,
	1941,
	1942,
	1942,
	-1,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	1943,
	1944,
	14,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1946,
	9,
	10,
	1947,
	1947,
	1948,
	89,
	89,
	89,
	89,
	89,
	1949,
	1950,
	10,
	1951,
	1952,
	1952,
	10,
	37,
	37,
	30,
	1953,
	1954,
	23,
	1951,
	10,
	1951,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	28,
	1958,
	32,
	14,
	89,
	31,
	89,
	31,
	89,
	31,
	1960,
	1961,
	1964,
	728,
	728,
	89,
	1501,
	739,
	739,
	14,
	14,
	14,
	10,
	9,
	1965,
	1966,
	1966,
	10,
	1967,
	1951,
	1968,
	728,
	23,
	1969,
	1970,
	1831,
	728,
	1832,
	1501,
	10,
	15,
	10,
	1971,
	9,
	1972,
	1972,
	3,
	-1,
	-1,
	-1,
	1830,
	1973,
	1831,
	1832,
	10,
	15,
	728,
	10,
	1974,
	9,
	1975,
	1975,
	3,
	23,
	23,
	23,
	23,
	1976,
	14,
	10,
	32,
	-1,
	118,
	1978,
	1979,
	1831,
	1831,
	1832,
	1501,
	1501,
	1501,
	10,
	10,
	15,
	10,
	739,
	739,
	1490,
	1980,
	1981,
	14,
	9,
	10,
	1982,
	1982,
	1983,
	3,
	46,
	46,
	23,
	23,
	23,
	23,
	32,
	1984,
	1944,
	14,
	-1,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1986,
	1987,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1990,
	1831,
	1922,
	1832,
	1923,
	739,
	343,
	10,
	32,
	1991,
	10,
	9,
	1992,
	1993,
	1993,
	3,
	23,
	23,
	23,
	23,
	1994,
	1995,
	14,
	89,
	31,
	89,
	31,
	10,
	32,
	1998,
	1999,
	990,
	1954,
	728,
	10,
	9,
	14,
	28,
	105,
	2002,
	2003,
	2003,
	3,
	46,
	46,
	15,
	728,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	2004,
	23,
	23,
	10,
	10,
	89,
	31,
	10,
	14,
	89,
	31,
	89,
	31,
	2005,
	2006,
	10,
	32,
	1849,
	1851,
	10,
	9,
	14,
	2009,
	2010,
	2010,
	2011,
	181,
	209,
	181,
	209,
	990,
	14,
	10,
	9,
	1838,
	2012,
	2012,
	3,
	1831,
	1832,
	10,
	15,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	15,
	7,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	2013,
	23,
	2013,
	9,
	2014,
	2014,
	10,
	14,
	23,
	23,
	23,
	1839,
	1836,
	1837,
	1838,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	10,
	9,
	1842,
	1843,
	1843,
	14,
	89,
	89,
	23,
	23,
	23,
	1883,
	10,
	32,
	30,
	859,
	1889,
	1881,
	1882,
	1890,
	859,
	37,
	32,
	32,
	898,
	30,
	1885,
	1886,
	1887,
	1385,
	1888,
	28,
	23,
	124,
	1891,
	1892,
	26,
	10,
	1849,
	10,
	466,
	10,
	1893,
	1894,
	9,
	1895,
	1895,
	10,
	14,
	15,
	10,
	10,
	10,
	1896,
	1897,
	9,
	1898,
	1898,
	10,
	14,
	23,
	23,
	23,
	1905,
	1904,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	1908,
	9,
	10,
	1909,
	1909,
	23,
	23,
	23,
	31,
	223,
	1929,
	1838,
	1931,
	23,
	23,
	23,
	23,
	1944,
	1945,
	10,
	10,
	32,
	23,
	26,
	89,
	1955,
	23,
	10,
	9,
	1956,
	1957,
	1957,
	23,
	1959,
	26,
	28,
	32,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	1962,
	9,
	1963,
	1963,
	23,
	23,
	23,
	1977,
	23,
	23,
	23,
	23,
	1944,
	1985,
	32,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	1988,
	9,
	10,
	1989,
	1989,
	-1,
	-1,
	23,
	23,
	23,
	1996,
	1997,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	10,
	32,
	10,
	9,
	14,
	2000,
	2001,
	2001,
	23,
	23,
	2004,
	23,
	23,
	23,
	23,
	15,
	14,
	14,
	10,
	10,
	728,
	89,
	31,
	10,
	23,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	9,
	2007,
	2008,
	2008,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x0600006B, 8,  (void**)&XRCameraImage_OnAsyncConversionComplete_m8C475A6334B19FAC20896F0DFEFBDEAC6E5EE53B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[13] = 
{
	{ 0x0200003A, { 23, 4 } },
	{ 0x02000049, { 30, 11 } },
	{ 0x0200004A, { 41, 3 } },
	{ 0x0200004B, { 44, 12 } },
	{ 0x0200004C, { 56, 5 } },
	{ 0x02000061, { 27, 3 } },
	{ 0x06000022, { 0, 2 } },
	{ 0x06000182, { 2, 5 } },
	{ 0x060001EC, { 7, 3 } },
	{ 0x060001ED, { 10, 4 } },
	{ 0x060001EE, { 14, 3 } },
	{ 0x06000204, { 17, 1 } },
	{ 0x06000228, { 18, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[61] = 
{
	{ (Il2CppRGCTXDataType)3, 19206 },
	{ (Il2CppRGCTXDataType)3, 19207 },
	{ (Il2CppRGCTXDataType)3, 19208 },
	{ (Il2CppRGCTXDataType)3, 19209 },
	{ (Il2CppRGCTXDataType)3, 19210 },
	{ (Il2CppRGCTXDataType)2, 22215 },
	{ (Il2CppRGCTXDataType)3, 19211 },
	{ (Il2CppRGCTXDataType)3, 19212 },
	{ (Il2CppRGCTXDataType)3, 19213 },
	{ (Il2CppRGCTXDataType)3, 19214 },
	{ (Il2CppRGCTXDataType)3, 19215 },
	{ (Il2CppRGCTXDataType)3, 19216 },
	{ (Il2CppRGCTXDataType)3, 19217 },
	{ (Il2CppRGCTXDataType)3, 19218 },
	{ (Il2CppRGCTXDataType)2, 22264 },
	{ (Il2CppRGCTXDataType)3, 19219 },
	{ (Il2CppRGCTXDataType)3, 19220 },
	{ (Il2CppRGCTXDataType)1, 24766 },
	{ (Il2CppRGCTXDataType)3, 19221 },
	{ (Il2CppRGCTXDataType)3, 19222 },
	{ (Il2CppRGCTXDataType)3, 19223 },
	{ (Il2CppRGCTXDataType)2, 22300 },
	{ (Il2CppRGCTXDataType)3, 19224 },
	{ (Il2CppRGCTXDataType)3, 19225 },
	{ (Il2CppRGCTXDataType)2, 24767 },
	{ (Il2CppRGCTXDataType)3, 19226 },
	{ (Il2CppRGCTXDataType)3, 19227 },
	{ (Il2CppRGCTXDataType)3, 19228 },
	{ (Il2CppRGCTXDataType)2, 22313 },
	{ (Il2CppRGCTXDataType)3, 19229 },
	{ (Il2CppRGCTXDataType)2, 22348 },
	{ (Il2CppRGCTXDataType)3, 19230 },
	{ (Il2CppRGCTXDataType)3, 19231 },
	{ (Il2CppRGCTXDataType)3, 19232 },
	{ (Il2CppRGCTXDataType)3, 19233 },
	{ (Il2CppRGCTXDataType)3, 19234 },
	{ (Il2CppRGCTXDataType)3, 19235 },
	{ (Il2CppRGCTXDataType)2, 22350 },
	{ (Il2CppRGCTXDataType)3, 19236 },
	{ (Il2CppRGCTXDataType)3, 19237 },
	{ (Il2CppRGCTXDataType)3, 19238 },
	{ (Il2CppRGCTXDataType)3, 19239 },
	{ (Il2CppRGCTXDataType)2, 22355 },
	{ (Il2CppRGCTXDataType)2, 24768 },
	{ (Il2CppRGCTXDataType)2, 24769 },
	{ (Il2CppRGCTXDataType)3, 19240 },
	{ (Il2CppRGCTXDataType)3, 19241 },
	{ (Il2CppRGCTXDataType)3, 19242 },
	{ (Il2CppRGCTXDataType)2, 22362 },
	{ (Il2CppRGCTXDataType)3, 19243 },
	{ (Il2CppRGCTXDataType)3, 19244 },
	{ (Il2CppRGCTXDataType)2, 24770 },
	{ (Il2CppRGCTXDataType)3, 19245 },
	{ (Il2CppRGCTXDataType)3, 19246 },
	{ (Il2CppRGCTXDataType)3, 19247 },
	{ (Il2CppRGCTXDataType)3, 19248 },
	{ (Il2CppRGCTXDataType)3, 19249 },
	{ (Il2CppRGCTXDataType)3, 19250 },
	{ (Il2CppRGCTXDataType)3, 19251 },
	{ (Il2CppRGCTXDataType)3, 19252 },
	{ (Il2CppRGCTXDataType)2, 22367 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule = 
{
	"Unity.XR.ARSubsystems.dll",
	940,
	s_methodPointers,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	13,
	s_rgctxIndices,
	61,
	s_rgctxValues,
	NULL,
};
