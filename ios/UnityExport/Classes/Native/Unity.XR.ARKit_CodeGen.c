﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.XR.ARKit.ARCollaborationData::.ctor(System.Byte[])
extern void ARCollaborationData__ctor_mA8801622DBC87812580E672FBED6A0E7AA70CBFB_AdjustorThunk ();
// 0x00000002 System.Void UnityEngine.XR.ARKit.ARCollaborationData::.ctor(System.Byte[],System.Int32,System.Int32)
extern void ARCollaborationData__ctor_mDC9EFE0861A9160434B1619D5D812313F6BD33E9_AdjustorThunk ();
// 0x00000003 System.Void UnityEngine.XR.ARKit.ARCollaborationData::.ctor(Unity.Collections.NativeSlice`1<System.Byte>)
extern void ARCollaborationData__ctor_mBCE6B3BE0FBCD54FACDC15D9399F9F383542B171_AdjustorThunk ();
// 0x00000004 System.Boolean UnityEngine.XR.ARKit.ARCollaborationData::get_valid()
extern void ARCollaborationData_get_valid_mE4CAB210F23A37A61E1A521A2FDAAD46F8A6512F_AdjustorThunk ();
// 0x00000005 UnityEngine.XR.ARKit.ARCollaborationDataPriority UnityEngine.XR.ARKit.ARCollaborationData::get_priority()
extern void ARCollaborationData_get_priority_m697D851F51AA30CCA702C8034E620535BD17BDD6_AdjustorThunk ();
// 0x00000006 System.Void UnityEngine.XR.ARKit.ARCollaborationData::Dispose()
extern void ARCollaborationData_Dispose_m4F408B1A35120FCF93A095EBEA19401BA176889F_AdjustorThunk ();
// 0x00000007 UnityEngine.XR.ARKit.SerializedARCollaborationData UnityEngine.XR.ARKit.ARCollaborationData::ToSerialized()
extern void ARCollaborationData_ToSerialized_mA7D388B24320043601C54F15AFD2AD04889CEEB7_AdjustorThunk ();
// 0x00000008 System.Int32 UnityEngine.XR.ARKit.ARCollaborationData::GetHashCode()
extern void ARCollaborationData_GetHashCode_m6CB5AB6E64E20081AB461FD69825A53DDB4F6B40_AdjustorThunk ();
// 0x00000009 System.Boolean UnityEngine.XR.ARKit.ARCollaborationData::Equals(System.Object)
extern void ARCollaborationData_Equals_m2EB8A50C23709F272ACF85A0EA7A34BA4D8DA8FE_AdjustorThunk ();
// 0x0000000A System.Boolean UnityEngine.XR.ARKit.ARCollaborationData::Equals(UnityEngine.XR.ARKit.ARCollaborationData)
extern void ARCollaborationData_Equals_mCF87707CDF6A1EE12FF4E73002A6491465EA235C_AdjustorThunk ();
// 0x0000000B System.Boolean UnityEngine.XR.ARKit.ARCollaborationData::op_Equality(UnityEngine.XR.ARKit.ARCollaborationData,UnityEngine.XR.ARKit.ARCollaborationData)
extern void ARCollaborationData_op_Equality_m1E921049437F4E563482D4946FDB909438D69CAD ();
// 0x0000000C System.Boolean UnityEngine.XR.ARKit.ARCollaborationData::op_Inequality(UnityEngine.XR.ARKit.ARCollaborationData,UnityEngine.XR.ARKit.ARCollaborationData)
extern void ARCollaborationData_op_Inequality_mBAE1F7A82EF713E3E371ECE54BDE0B935AFAFE68 ();
// 0x0000000D System.Void UnityEngine.XR.ARKit.ARCollaborationData::.ctor(System.IntPtr)
extern void ARCollaborationData__ctor_m743A990ED9B051DA8CEBEBDA6165A3031BA0AFA7_AdjustorThunk ();
// 0x0000000E System.Void UnityEngine.XR.ARKit.ARCollaborationData::.ctor(UnityEngine.XR.ARKit.NSData)
extern void ARCollaborationData__ctor_m4F3BF86E1076C66410C7BE6A1A2AE421DD4198EE_AdjustorThunk ();
// 0x0000000F System.Void UnityEngine.XR.ARKit.ARCollaborationData::ValidateAndThrow()
extern void ARCollaborationData_ValidateAndThrow_m2412260BCDAF65DCDC4C92D69579BAB8E7B3A86E_AdjustorThunk ();
// 0x00000010 System.IntPtr UnityEngine.XR.ARKit.ARCollaborationData::ConstructUnchecked(System.Void*,System.Int32)
extern void ARCollaborationData_ConstructUnchecked_mE36A07B80180D818F2E489F421950F63BA081A8E ();
// 0x00000011 System.IntPtr UnityEngine.XR.ARKit.ARCollaborationData::ConstructUnchecked(System.Byte[],System.Int32,System.Int32)
extern void ARCollaborationData_ConstructUnchecked_m3E2363E2E0DDCFAFB6AF02D6B0673DD0C98D6BCD ();
// 0x00000012 System.Void UnityEngine.XR.ARKit.ARCollaborationData::UnityARKit_CFRelease(System.IntPtr)
extern void ARCollaborationData_UnityARKit_CFRelease_m3FDFA75E2B0D0B02E6608B661E81D3350CC28773 ();
// 0x00000013 System.IntPtr UnityEngine.XR.ARKit.ARCollaborationData::UnityARKit_session_deserializeCollaborationDataFromNSData(System.IntPtr)
extern void ARCollaborationData_UnityARKit_session_deserializeCollaborationDataFromNSData_m7811442A62DA0DD626074F68DEF920DB7D428166 ();
// 0x00000014 System.IntPtr UnityEngine.XR.ARKit.ARCollaborationData::UnityARKit_session_serializeCollaborationDataToNSData(System.IntPtr)
extern void ARCollaborationData_UnityARKit_session_serializeCollaborationDataToNSData_m12A6F966BC80BB847759E2C50E4927F509692ED6 ();
// 0x00000015 UnityEngine.XR.ARKit.ARCollaborationDataPriority UnityEngine.XR.ARKit.ARCollaborationData::UnityARKit_session_getCollaborationDataPriority(System.IntPtr)
extern void ARCollaborationData_UnityARKit_session_getCollaborationDataPriority_mE36E65BEA8F9BF6493AAEC17A483D1F1DA0EE6BE ();
// 0x00000016 System.Boolean UnityEngine.XR.ARKit.ARCollaborationDataBuilder::get_hasData()
extern void ARCollaborationDataBuilder_get_hasData_mDA85E8BE053C940508B6875CF5310FF5FF6578D7_AdjustorThunk ();
// 0x00000017 System.Int32 UnityEngine.XR.ARKit.ARCollaborationDataBuilder::get_length()
extern void ARCollaborationDataBuilder_get_length_m34DEDD9E47CA7FA9065B4F07EEABA2A35614E9AB_AdjustorThunk ();
// 0x00000018 UnityEngine.XR.ARKit.ARCollaborationData UnityEngine.XR.ARKit.ARCollaborationDataBuilder::ToCollaborationData()
extern void ARCollaborationDataBuilder_ToCollaborationData_mF5FBFCB380133ECB14B169F5B570528BF053AFF8_AdjustorThunk ();
// 0x00000019 System.Void UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Append(System.Byte[],System.Int32,System.Int32)
extern void ARCollaborationDataBuilder_Append_mAAC5FABB782E0D811AD5D6756DFCE152F7A515DA_AdjustorThunk ();
// 0x0000001A System.Void UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Append(System.Byte[])
extern void ARCollaborationDataBuilder_Append_m844A03A45AC50DB065D71045C115B7843CC2E199_AdjustorThunk ();
// 0x0000001B System.Void UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Append(Unity.Collections.NativeSlice`1<System.Byte>)
extern void ARCollaborationDataBuilder_Append_mFE88865CACD6EB7450AF4A59DF4863277B6803C6_AdjustorThunk ();
// 0x0000001C System.Void UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Dispose()
extern void ARCollaborationDataBuilder_Dispose_m8873F4FE4305DABBDFA232AEDD617C7BA2B26D9E_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.XR.ARKit.ARCollaborationDataBuilder::AppendUnchecked(System.Void*,System.Int32)
extern void ARCollaborationDataBuilder_AppendUnchecked_mC602BCE02D597CD71F308FD9CBB4E490A6D30392_AdjustorThunk ();
// 0x0000001E System.Int32 UnityEngine.XR.ARKit.ARCollaborationDataBuilder::GetHashCode()
extern void ARCollaborationDataBuilder_GetHashCode_m4EE38AEAF1AD7A7A3AE8F4C99BFFD92596C9FBEF_AdjustorThunk ();
// 0x0000001F System.Boolean UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Equals(System.Object)
extern void ARCollaborationDataBuilder_Equals_mCBF064ACE8FCB2EDBAD48335BB33F7D0091B3DBF_AdjustorThunk ();
// 0x00000020 System.Boolean UnityEngine.XR.ARKit.ARCollaborationDataBuilder::Equals(UnityEngine.XR.ARKit.ARCollaborationDataBuilder)
extern void ARCollaborationDataBuilder_Equals_m31095618C272CAA89694E0E880A649E0F618AF54_AdjustorThunk ();
// 0x00000021 System.Boolean UnityEngine.XR.ARKit.ARCollaborationDataBuilder::op_Equality(UnityEngine.XR.ARKit.ARCollaborationDataBuilder,UnityEngine.XR.ARKit.ARCollaborationDataBuilder)
extern void ARCollaborationDataBuilder_op_Equality_m33534EFB61AF43BFC80898CAF3311546F820DBD9 ();
// 0x00000022 System.Boolean UnityEngine.XR.ARKit.ARCollaborationDataBuilder::op_Inequality(UnityEngine.XR.ARKit.ARCollaborationDataBuilder,UnityEngine.XR.ARKit.ARCollaborationDataBuilder)
extern void ARCollaborationDataBuilder_op_Inequality_m5B646EBA82D0882BDEC81490C2FA61A6D48D50D8 ();
// 0x00000023 UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider UnityEngine.XR.ARKit.ARKitAnchorSubsystem::CreateProvider()
extern void ARKitAnchorSubsystem_CreateProvider_mD7F43AB383497B78DAD76DC871BCF4147BF142D9 ();
// 0x00000024 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem::RegisterDescriptor()
extern void ARKitAnchorSubsystem_RegisterDescriptor_m563C875B34BEDCB4973833AFCC1EA1A60E9D0234 ();
// 0x00000025 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem::.ctor()
extern void ARKitAnchorSubsystem__ctor_mFE8F6F6F67B8253619540C0742C75A3134C3E9AE ();
// 0x00000026 System.IntPtr UnityEngine.XR.ARKit.Api::UnityARKit_TrackableProvider_start(System.IntPtr)
extern void Api_UnityARKit_TrackableProvider_start_m3A9E9F96C54B9D88A81D7BE64DC7905D0CA280A3 ();
// 0x00000027 System.IntPtr UnityEngine.XR.ARKit.Api::UnityARKit_TrackableProvider_stop(System.IntPtr)
extern void Api_UnityARKit_TrackableProvider_stop_mB2234DA5EEC25825C3941A26B64D8DA0EEB32AE1 ();
// 0x00000028 UnityEngine.XR.ARKit.NativeChanges UnityEngine.XR.ARKit.Api::UnityARKit_TrackableProvider_acquireChanges(System.IntPtr)
extern void Api_UnityARKit_TrackableProvider_acquireChanges_m2559830EE33C4DABF75C059D5EB5CAC3413903C6 ();
// 0x00000029 System.Void UnityEngine.XR.ARKit.Api::UnityARKit_TrackableProvider_copyChanges(System.IntPtr,UnityEngine.XR.ARKit.NativeChanges,System.Int32,System.Void*,System.Void*,System.Void*)
extern void Api_UnityARKit_TrackableProvider_copyChanges_mE4792AF38E41101B4F60631127E37B6AB392D55B ();
// 0x0000002A UnityEngine.XR.ARKit.ARWorldMapRequestStatus UnityEngine.XR.ARKit.Api::UnityARKit_getWorldMapRequestStatus(System.Int32)
extern void Api_UnityARKit_getWorldMapRequestStatus_mD2F538AB59442DBF7690EE9975FE22ED8ADF4654 ();
// 0x0000002B System.Void UnityEngine.XR.ARKit.Api::UnityARKit_disposeWorldMap(System.Int32)
extern void Api_UnityARKit_disposeWorldMap_mDB310DE6F820E095FB2B2D6ECD586FB7617026B9 ();
// 0x0000002C System.Void UnityEngine.XR.ARKit.Api::UnityARKit_disposeWorldMapRequest(System.Int32)
extern void Api_UnityARKit_disposeWorldMapRequest_m552CC414C42D2EB81B337AFAAA66D509FCD2D609 ();
// 0x0000002D System.Int32 UnityEngine.XR.ARKit.Api::UnityARKit_getWorldMapIdFromRequestId(System.Int32)
extern void Api_UnityARKit_getWorldMapIdFromRequestId_m501FE31EF9CD0C628AFC54D3790111E340855005 ();
// 0x0000002E System.Boolean UnityEngine.XR.ARKit.Api::UnityARKit_isWorldMapValid(System.Int32)
extern void Api_UnityARKit_isWorldMapValid_m03ED705763A5E3C562D69409DA5125716F55ECCF ();
// 0x0000002F System.Boolean UnityEngine.XR.ARKit.Api::UnityARKit_trySerializeWorldMap(System.Int32,System.IntPtr&,System.Int32&)
extern void Api_UnityARKit_trySerializeWorldMap_m626F02DB778347619427DF1F570702CE6D7F46D2 ();
// 0x00000030 System.Int32 UnityEngine.XR.ARKit.Api::UnityARKit_copyAndReleaseNsData(System.IntPtr,System.IntPtr,System.Int32)
extern void Api_UnityARKit_copyAndReleaseNsData_m1196DA7BA1FD8BDE407C5F4E5595FE5FEE7824C5 ();
// 0x00000031 System.Int32 UnityEngine.XR.ARKit.Api::UnityARKit_deserializeWorldMap(System.IntPtr,System.Int32)
extern void Api_UnityARKit_deserializeWorldMap_m6F213F566D0E3C21F87F79B61DCCB8FAF590631B ();
// 0x00000032 System.Void UnityEngine.XR.ARKit.Api::CFRelease(System.IntPtr&)
extern void Api_CFRelease_m722B29376E99768260B799C5776E0B906B277C09 ();
// 0x00000033 System.Void UnityEngine.XR.ARKit.Api::UnityARKit_CFRelease(System.IntPtr)
extern void Api_UnityARKit_CFRelease_m048AD7191576F85CD81DD1DE41A41379083DDEA7 ();
// 0x00000034 System.String UnityEngine.XR.ARKit.ARKitCameraSubsystem::get_backgroundShaderName()
extern void ARKitCameraSubsystem_get_backgroundShaderName_mF68B51768DCDA3A949F5E5BF4E89688C5170363E ();
// 0x00000035 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem::Register()
extern void ARKitCameraSubsystem_Register_m0B242BF988E92E626979B845EE749A1794E9E642 ();
// 0x00000036 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider UnityEngine.XR.ARKit.ARKitCameraSubsystem::CreateProvider()
extern void ARKitCameraSubsystem_CreateProvider_m0B81146A5AAC0EFB8C64C1B3F870E9DED4578142 ();
// 0x00000037 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem::.ctor()
extern void ARKitCameraSubsystem__ctor_mE0B24FB4ED5FC603CB22E220A161A00177B9B8B9 ();
// 0x00000038 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeRegistration::Register()
extern void ARKitEnvironmentProbeRegistration_Register_m99E02E59582A0F880422D128FEEF2BEE553EE20E ();
// 0x00000039 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem::CreateProvider()
extern void ARKitEnvironmentProbeSubsystem_CreateProvider_m0546A1ACD5AA45AA8A1C0D64579057FAF95CD5ED ();
// 0x0000003A System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem::.ctor()
extern void ARKitEnvironmentProbeSubsystem__ctor_m957799C8DE48100EF21E8429209271292EADFA95 ();
// 0x0000003B UnityEngine.XR.ARSubsystems.XRSessionSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_sessionSubsystem()
extern void ARKitLoader_get_sessionSubsystem_mA7515BBCDF9232D049CBF1B191BAC905DC66755F ();
// 0x0000003C UnityEngine.XR.ARSubsystems.XRCameraSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_cameraSubsystem()
extern void ARKitLoader_get_cameraSubsystem_mD12107697F3E7D434978BA85E0B37013E502FDB7 ();
// 0x0000003D UnityEngine.XR.ARSubsystems.XRDepthSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_depthSubsystem()
extern void ARKitLoader_get_depthSubsystem_mCC4A7A2EA5F0C3929C3E70D2A1868834F60D90DA ();
// 0x0000003E UnityEngine.XR.ARSubsystems.XRPlaneSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_planeSubsystem()
extern void ARKitLoader_get_planeSubsystem_m9AA4DF25AEAF1D2C3FA0E192344C5E99475BEDA1 ();
// 0x0000003F UnityEngine.XR.ARSubsystems.XRAnchorSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_anchorSubsystem()
extern void ARKitLoader_get_anchorSubsystem_mD4ECBB36A396B8F38EBF36253D347168F7F3AC25 ();
// 0x00000040 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_raycastSubsystem()
extern void ARKitLoader_get_raycastSubsystem_mDD7D230E26224F56A80066A06D97A15735F27C12 ();
// 0x00000041 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_environmentProbeSubsystem()
extern void ARKitLoader_get_environmentProbeSubsystem_mE4BF29A12A134D23A8F5E640A596C6CD76549D2B ();
// 0x00000042 UnityEngine.XR.XRInputSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_inputSubsystem()
extern void ARKitLoader_get_inputSubsystem_m4F58892B9B2842ACD60D9C633AA2002A4DC65D8A ();
// 0x00000043 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_imageTrackingSubsystem()
extern void ARKitLoader_get_imageTrackingSubsystem_m6EA1221F3B7DD4A5438AAA4CC58D545025EDCA7A ();
// 0x00000044 UnityEngine.XR.ARSubsystems.XRFaceSubsystem UnityEngine.XR.ARKit.ARKitLoader::get_faceSubsystem()
extern void ARKitLoader_get_faceSubsystem_m99A0CBBD032BF9E347C53BA32C80DF8F29F8FCBB ();
// 0x00000045 System.Boolean UnityEngine.XR.ARKit.ARKitLoader::Initialize()
extern void ARKitLoader_Initialize_m07F6EAAEBF5DD33535C326D9CEAF0BF9568E6F42 ();
// 0x00000046 System.Boolean UnityEngine.XR.ARKit.ARKitLoader::Start()
extern void ARKitLoader_Start_m64F1E97F17BDD984CF68D372EC73DA7E58FA493D ();
// 0x00000047 System.Boolean UnityEngine.XR.ARKit.ARKitLoader::Stop()
extern void ARKitLoader_Stop_m1050851FB75721A0E9904DBDC1A44943B5C85510 ();
// 0x00000048 System.Boolean UnityEngine.XR.ARKit.ARKitLoader::Deinitialize()
extern void ARKitLoader_Deinitialize_m8A9CF38E7A1BA52B4EADE0FCEDA8A60DEEADD486 ();
// 0x00000049 UnityEngine.XR.ARKit.ARKitLoaderSettings UnityEngine.XR.ARKit.ARKitLoader::GetSettings()
extern void ARKitLoader_GetSettings_mFA9176AE469580FF0BC04BF25B5E608045186FE3 ();
// 0x0000004A System.Void UnityEngine.XR.ARKit.ARKitLoader::.ctor()
extern void ARKitLoader__ctor_mD91F0A69BDCB27DEE299AE30552166FD679EC98A ();
// 0x0000004B System.Void UnityEngine.XR.ARKit.ARKitLoader::.cctor()
extern void ARKitLoader__cctor_m2F2390E6BE89D5F246595C1733D7A4FC2F0328C4 ();
// 0x0000004C System.Boolean UnityEngine.XR.ARKit.ARKitLoaderSettings::get_startAndStopSubsystems()
extern void ARKitLoaderSettings_get_startAndStopSubsystems_mC3EC3BC6C40190BF99DA2D9461BCDE1A4A42644F ();
// 0x0000004D System.Void UnityEngine.XR.ARKit.ARKitLoaderSettings::set_startAndStopSubsystems(System.Boolean)
extern void ARKitLoaderSettings_set_startAndStopSubsystems_mE22F3D40AAD40FAB7FCD2E9DFE0FF6D66CA2F6B7 ();
// 0x0000004E System.Void UnityEngine.XR.ARKit.ARKitLoaderSettings::Awake()
extern void ARKitLoaderSettings_Awake_m294A6CE5BBCD6DC1537B10E5F8599AEB2AF0AEDA ();
// 0x0000004F System.Void UnityEngine.XR.ARKit.ARKitLoaderSettings::.ctor()
extern void ARKitLoaderSettings__ctor_m2174ECFDC33B981CB04D95B3622D07D5A2A0F590 ();
// 0x00000050 System.Void UnityEngine.XR.ARKit.ARKitLoaderSettings::.cctor()
extern void ARKitLoaderSettings__cctor_m40D2F9FD2EACD15321D90E0AD51E3C3B812C8273 ();
// 0x00000051 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider UnityEngine.XR.ARKit.ARKitRaycastSubsystem::CreateProvider()
extern void ARKitRaycastSubsystem_CreateProvider_mC5E83538555A160082D762DA48ABD7BF87831EFF ();
// 0x00000052 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem::RegisterDescriptor()
extern void ARKitRaycastSubsystem_RegisterDescriptor_m1442393B3D2D07C4E6F1FBD355695BCEB4DCF63E ();
// 0x00000053 System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem::.ctor()
extern void ARKitRaycastSubsystem__ctor_m8FE9A2A756806FD0166A60C2347D88F0BB38307F ();
// 0x00000054 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_coachingOverlaySupported()
extern void ARKitSessionSubsystem_get_coachingOverlaySupported_m691AF2E35A3F2C3F14AF9AE86F0EBEC2A3FC7E45 ();
// 0x00000055 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_coachingActivatesAutomatically()
extern void ARKitSessionSubsystem_get_coachingActivatesAutomatically_m89E0B3DC0DC069BD8001B4F8CB8C807CC6B6F4A4 ();
// 0x00000056 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::set_coachingActivatesAutomatically(System.Boolean)
extern void ARKitSessionSubsystem_set_coachingActivatesAutomatically_mE690DEAD6678332A5039C1AB318F27D49D607739 ();
// 0x00000057 UnityEngine.XR.ARKit.ARCoachingGoal UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_coachingGoal()
extern void ARKitSessionSubsystem_get_coachingGoal_mE0E000624179A69AA375BD935047FF013104236B ();
// 0x00000058 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::set_coachingGoal(UnityEngine.XR.ARKit.ARCoachingGoal)
extern void ARKitSessionSubsystem_set_coachingGoal_mBEFD3504793A2644D37EE00EE410F29FA36A533B ();
// 0x00000059 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_coachingActive()
extern void ARKitSessionSubsystem_get_coachingActive_m1F3D6A334AF8F481E8B04D741770CB7FA4D024D1 ();
// 0x0000005A System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::SetCoachingActive(System.Boolean,UnityEngine.XR.ARKit.ARCoachingOverlayTransition)
extern void ARKitSessionSubsystem_SetCoachingActive_m1180E8D2081C7C4584DA0A2D4225A4028B98046B ();
// 0x0000005B UnityEngine.XR.ARKit.ARWorldMapRequest UnityEngine.XR.ARKit.ARKitSessionSubsystem::GetARWorldMapAsync()
extern void ARKitSessionSubsystem_GetARWorldMapAsync_m767616E7906AC991039AE185E3ADD5ACB3AE1B10 ();
// 0x0000005C System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::GetARWorldMapAsync(System.Action`2<UnityEngine.XR.ARKit.ARWorldMapRequestStatus,UnityEngine.XR.ARKit.ARWorldMap>)
extern void ARKitSessionSubsystem_GetARWorldMapAsync_mF0736B99BBD6949AC7A00FEC9CB1204A5284E6B0 ();
// 0x0000005D System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_worldMapSupported()
extern void ARKitSessionSubsystem_get_worldMapSupported_mBA665DC3901CBA775BABD2DA7F02E9FFAE8BEA20 ();
// 0x0000005E UnityEngine.XR.ARKit.ARWorldMappingStatus UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_worldMappingStatus()
extern void ARKitSessionSubsystem_get_worldMappingStatus_mCCC90F7BE712B10BCD985D7E464AA5755CE3F045 ();
// 0x0000005F System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::ApplyWorldMap(UnityEngine.XR.ARKit.ARWorldMap)
extern void ARKitSessionSubsystem_ApplyWorldMap_m43FB6CD517FF823EF08047B8BA4B2B774CFB83E2 ();
// 0x00000060 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_collaborationEnabled()
extern void ARKitSessionSubsystem_get_collaborationEnabled_m1A72B522F81FFF9B4D0DD6F1DD3F638B36F335E2 ();
// 0x00000061 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::set_collaborationEnabled(System.Boolean)
extern void ARKitSessionSubsystem_set_collaborationEnabled_m8D7BAAFEC89962C1BC9E822F4F89066D3E398E7A ();
// 0x00000062 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_supportsCollaboration()
extern void ARKitSessionSubsystem_get_supportsCollaboration_m32A2801F58B78052A987A18A818191AD857C3F95 ();
// 0x00000063 System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem::get_collaborationDataCount()
extern void ARKitSessionSubsystem_get_collaborationDataCount_m518124EDC7BC5530DAE3A9DDA3277718E2913823 ();
// 0x00000064 UnityEngine.XR.ARKit.ARCollaborationData UnityEngine.XR.ARKit.ARKitSessionSubsystem::DequeueCollaborationData()
extern void ARKitSessionSubsystem_DequeueCollaborationData_m52BA35A7996DF823BA6839F87CDD4AD909C372E5 ();
// 0x00000065 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::UpdateWithCollaborationData(UnityEngine.XR.ARKit.ARCollaborationData)
extern void ARKitSessionSubsystem_UpdateWithCollaborationData_m4D9C2D04512630C4EDFDF22CD51950034C07E741 ();
// 0x00000066 UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider UnityEngine.XR.ARKit.ARKitSessionSubsystem::CreateProvider()
extern void ARKitSessionSubsystem_CreateProvider_m7F7FBC5E6D8F60E7F8360E750EEE41A342A9C4F5 ();
// 0x00000067 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::.cctor()
extern void ARKitSessionSubsystem__cctor_mF21044D0935B2CA8F1FCD42BF839812255101E2A ();
// 0x00000068 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::OnAsyncConversionComplete(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr)
extern void ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F ();
// 0x00000069 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::RegisterDescriptor()
extern void ARKitSessionSubsystem_RegisterDescriptor_m359F3EC534DDADAE498376C22BE8BB24813B1CC9 ();
// 0x0000006A System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem::.ctor()
extern void ARKitSessionSubsystem__ctor_m551ABE75E099FDA75C75967077DEFE540A41DC19 ();
// 0x0000006B System.Void UnityEngine.XR.ARKit.ARWorldMap::Dispose()
extern void ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757_AdjustorThunk ();
// 0x0000006C System.Boolean UnityEngine.XR.ARKit.ARWorldMap::get_valid()
extern void ARWorldMap_get_valid_m46E6C15ED1A84309B0B80B17B61025F22D89CBD9_AdjustorThunk ();
// 0x0000006D Unity.Collections.NativeArray`1<System.Byte> UnityEngine.XR.ARKit.ARWorldMap::Serialize(Unity.Collections.Allocator)
extern void ARWorldMap_Serialize_m4DABF47BF52F0FEDBD9DF306B76A20E62C14E1DC_AdjustorThunk ();
// 0x0000006E System.Boolean UnityEngine.XR.ARKit.ARWorldMap::TryDeserialize(Unity.Collections.NativeArray`1<System.Byte>,UnityEngine.XR.ARKit.ARWorldMap&)
extern void ARWorldMap_TryDeserialize_mA27A7AA88E6F36FCBCBFD2E72C2683E980193099 ();
// 0x0000006F System.Int32 UnityEngine.XR.ARKit.ARWorldMap::GetHashCode()
extern void ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573_AdjustorThunk ();
// 0x00000070 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::Equals(System.Object)
extern void ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688_AdjustorThunk ();
// 0x00000071 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::Equals(UnityEngine.XR.ARKit.ARWorldMap)
extern void ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A_AdjustorThunk ();
// 0x00000072 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::op_Equality(UnityEngine.XR.ARKit.ARWorldMap,UnityEngine.XR.ARKit.ARWorldMap)
extern void ARWorldMap_op_Equality_m13BBDD287F1E6CC05115F3CCAFF4DE31A198DF50 ();
// 0x00000073 System.Boolean UnityEngine.XR.ARKit.ARWorldMap::op_Inequality(UnityEngine.XR.ARKit.ARWorldMap,UnityEngine.XR.ARKit.ARWorldMap)
extern void ARWorldMap_op_Inequality_m40D90E68ED4F82A88B637B39943D5A9579D9C615 ();
// 0x00000074 System.Void UnityEngine.XR.ARKit.ARWorldMap::.ctor(System.Int32)
extern void ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7_AdjustorThunk ();
// 0x00000075 System.Int32 UnityEngine.XR.ARKit.ARWorldMap::get_nativeHandle()
extern void ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22_AdjustorThunk ();
// 0x00000076 System.Void UnityEngine.XR.ARKit.ARWorldMap::set_nativeHandle(System.Int32)
extern void ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE_AdjustorThunk ();
// 0x00000077 UnityEngine.XR.ARKit.ARWorldMapRequestStatus UnityEngine.XR.ARKit.ARWorldMapRequest::get_status()
extern void ARWorldMapRequest_get_status_mEF94818913FCF6416CF29C6BFBCC84EEF39C3867_AdjustorThunk ();
// 0x00000078 UnityEngine.XR.ARKit.ARWorldMap UnityEngine.XR.ARKit.ARWorldMapRequest::GetWorldMap()
extern void ARWorldMapRequest_GetWorldMap_m2113D2DECC0E8F9CB67D7A3EC480CBD3F2AA5A4E_AdjustorThunk ();
// 0x00000079 System.Void UnityEngine.XR.ARKit.ARWorldMapRequest::Dispose()
extern void ARWorldMapRequest_Dispose_mBD67970EA0A71C29390F56841D0357E699987A8B_AdjustorThunk ();
// 0x0000007A System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::GetHashCode()
extern void ARWorldMapRequest_GetHashCode_m0257226B87FC520585883D513F8B69D664755003_AdjustorThunk ();
// 0x0000007B System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequest::Equals(System.Object)
extern void ARWorldMapRequest_Equals_m667C3E4FB1E8F795834A62E93F66A13958C4C4E7_AdjustorThunk ();
// 0x0000007C System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequest::Equals(UnityEngine.XR.ARKit.ARWorldMapRequest)
extern void ARWorldMapRequest_Equals_m62A44971FAF312C53F44D445377CEC8737720BA1_AdjustorThunk ();
// 0x0000007D System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequest::op_Equality(UnityEngine.XR.ARKit.ARWorldMapRequest,UnityEngine.XR.ARKit.ARWorldMapRequest)
extern void ARWorldMapRequest_op_Equality_m4CE393991E42963EB2464762473D649C52B781E4 ();
// 0x0000007E System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequest::op_Inequality(UnityEngine.XR.ARKit.ARWorldMapRequest,UnityEngine.XR.ARKit.ARWorldMapRequest)
extern void ARWorldMapRequest_op_Inequality_mA3D7102DF1B3FD71F9C535A4DC9496A14757DBD2 ();
// 0x0000007F System.Void UnityEngine.XR.ARKit.ARWorldMapRequest::.ctor(System.Int32)
extern void ARWorldMapRequest__ctor_m6800C690A81676D3F26CF9D7A4E1099B6BF4DAEF_AdjustorThunk ();
// 0x00000080 System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions::IsDone(UnityEngine.XR.ARKit.ARWorldMapRequestStatus)
extern void ARWorldMapRequestStatusExtensions_IsDone_mFBAF3BFF35C5A43A711E50C5D6A939BADB89D3E5 ();
// 0x00000081 System.Boolean UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions::IsError(UnityEngine.XR.ARKit.ARWorldMapRequestStatus)
extern void ARWorldMapRequestStatusExtensions_IsError_m4324D3418C82C5B1C955A72DD0E9BA5805286540 ();
// 0x00000082 UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::CreateProvider()
extern void ARKitXRDepthSubsystem_CreateProvider_m4D1BD576DBC081378970382782CA3719CD283A5B ();
// 0x00000083 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::RegisterDescriptor()
extern void ARKitXRDepthSubsystem_RegisterDescriptor_mEB1B5F8A8CF06C7E6F00CEE8B8A5D8F2CB86B5E7 ();
// 0x00000084 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem::.ctor()
extern void ARKitXRDepthSubsystem__ctor_m81EFFCC78EA40E790162C25FD9DC64DF4557BD46 ();
// 0x00000085 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Construct()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Construct_m389EF901E5A84DE58098B34F3159AFAA1B83C6E5 ();
// 0x00000086 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Destruct()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Destruct_mA74881A49FA8DB131E3613E414E54215205799C2 ();
// 0x00000087 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Start()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Start_mD464FCD2D9A5C50791D7B9D38AAD43D97ACA6227 ();
// 0x00000088 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_Stop()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Stop_m80FF6427ACD878991E6F3649C32A4C948AD834C9 ();
// 0x00000089 System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled(System.Boolean)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled_m58CF7C2D525E820536C7143D42E84C7EFC4805A9 ();
// 0x0000008A System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_TrySetEnvironmentTextureHDREnabled(System.Boolean)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TrySetEnvironmentTextureHDREnabled_mA0623A69CD42A71C9ACB5B3FFDA122774F915D4F ();
// 0x0000008B System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe_m7EF9192A23F0E6CBB565BCB89D9D3AC55024F272 ();
// 0x0000008C System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe_m823D591063A7E9D9F2186D3AE0FB2AA96C03E1D6 ();
// 0x0000008D System.IntPtr UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_AcquireChanges(System.Int32&,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_AcquireChanges_mF3B06C982FBD823D7B03F3DB870E22846FFA834C ();
// 0x0000008E System.Void UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_ReleaseChanges(System.IntPtr)
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_ReleaseChanges_m7357747C89D2E0CC5380F76F4965A3EECE4D1539 ();
// 0x0000008F System.Boolean UnityEngine.XR.ARKit.EnvironmentProbeApi::UnityARKit_EnvironmentProbeProvider_IsSupported()
extern void EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_IsSupported_m2FAAB3704CE8490F8B7C332A4C89EDABC53A37D8 ();
// 0x00000090 System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase::get_nativePtr()
extern void ARKitImageDatabase_get_nativePtr_m0627E0F10ED56C59222DD5597F5E0D562C7379D6 ();
// 0x00000091 System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::set_nativePtr(System.IntPtr)
extern void ARKitImageDatabase_set_nativePtr_m3EB571EADC2FFE172D2F6B49C6F9E85D4C1B737D ();
// 0x00000092 System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::Finalize()
extern void ARKitImageDatabase_Finalize_m6C9CFDE06F7E6520CA5A5600B5F4CBB1A303359E ();
// 0x00000093 System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::.ctor(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void ARKitImageDatabase__ctor_m577A4B9755C838DD1D7A71F14614DEC22B263FD6 ();
// 0x00000094 Unity.Jobs.JobHandle UnityEngine.XR.ARKit.ARKitImageDatabase::ScheduleAddImageJobImpl(Unity.Collections.NativeSlice`1<System.Byte>,UnityEngine.Vector2Int,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRReferenceImage,Unity.Jobs.JobHandle)
extern void ARKitImageDatabase_ScheduleAddImageJobImpl_mAC332D56D90815AF396570124E8D5D121017C01B ();
// 0x00000095 System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase::get_supportedTextureFormatCount()
extern void ARKitImageDatabase_get_supportedTextureFormatCount_m1F27D2C4CC29F345ABFE24F0E71820D0BC8A0A6B ();
// 0x00000096 UnityEngine.TextureFormat UnityEngine.XR.ARKit.ARKitImageDatabase::GetSupportedTextureFormatAtImpl(System.Int32)
extern void ARKitImageDatabase_GetSupportedTextureFormatAtImpl_m2D528D1C9DCBDE029C52747B30554A69B6999002 ();
// 0x00000097 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARKit.ARKitImageDatabase::GetReferenceImageAt(System.Int32)
extern void ARKitImageDatabase_GetReferenceImageAt_m32E2DD4E43C4B525821B3ED239FDF04EBA4D93C7 ();
// 0x00000098 System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase::get_count()
extern void ARKitImageDatabase_get_count_m41F4C1B60D3F3DDF656CDC87952490CF5C617801 ();
// 0x00000099 System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_CFRetain(System.IntPtr)
extern void ARKitImageDatabase_UnityARKit_CFRetain_m368A08124CC503F4ABDABC9BE37C61F600186451 ();
// 0x0000009A System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_CFRelease(System.IntPtr)
extern void ARKitImageDatabase_UnityARKit_CFRelease_m08FF854E77AD642DDC7BD585EED991F0AB71F98A ();
// 0x0000009B System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_ImageDatabase_createEmpty()
extern void ARKitImageDatabase_UnityARKit_ImageDatabase_createEmpty_m033397E063140EE34ECD85964D1C152072C73D83 ();
// 0x0000009C UnityEngine.XR.ARKit.SetReferenceLibraryResult UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_ImageDatabase_tryCreateFromResourceGroup(System.String,System.Int32,System.Guid,System.Void*,System.Int32,System.IntPtr&)
extern void ARKitImageDatabase_UnityARKit_ImageDatabase_tryCreateFromResourceGroup_m79FB16BABD9059F390B4FAAA9A89D56016DD1F80 ();
// 0x0000009D UnityEngine.XR.ARKit.ManagedReferenceImage UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_ImageDatabase_GetReferenceImage(System.IntPtr,System.Int32)
extern void ARKitImageDatabase_UnityARKit_ImageDatabase_GetReferenceImage_m500C9EF31E89D4060082DF8B40B005B0B3E10F90 ();
// 0x0000009E System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase::UnityARKit_ImageDatabase_GetReferenceImageCount(System.IntPtr)
extern void ARKitImageDatabase_UnityARKit_ImageDatabase_GetReferenceImageCount_m8DFA4A6501DEF6C46763560A0CDA7198818C3768 ();
// 0x0000009F System.Void UnityEngine.XR.ARKit.ARKitImageDatabase::.cctor()
extern void ARKitImageDatabase__cctor_m329B57B53D16428AB34ECDD5D50464224D186EEE ();
// 0x000000A0 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_setMaximumNumberOfTrackedImages(System.Int32)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setMaximumNumberOfTrackedImages_mBF0FA67787BE559D28A4949D256D5824B8F6F1F4 ();
// 0x000000A1 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_setDatabase(System.IntPtr)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setDatabase_m03198A009BB5D5BB9005BCFF293556850745FC14 ();
// 0x000000A2 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_stop()
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_stop_mA5E30442ACDDB68089B08B3F6928E3BDC55E85B6 ();
// 0x000000A3 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_destroy()
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_destroy_m73470876BEDDE85C215EABCD3555D73571E5F772 ();
// 0x000000A4 System.Void* UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_acquireChanges_mB4BFD5D1B49A8BA6443E7F08FE7BA8EE68A0BCFB ();
// 0x000000A5 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::UnityARKit_imageTracking_releaseChanges(System.Void*)
extern void ARKitImageTrackingSubsystem_UnityARKit_imageTracking_releaseChanges_m79BE2C4F28C174B2D27CCA85FF9C6517F1B9F10A ();
// 0x000000A6 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::RegisterDescriptor()
extern void ARKitImageTrackingSubsystem_RegisterDescriptor_mC8DC8F9919766BB3A242BF200922D81DCC59DE20 ();
// 0x000000A7 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::CreateProvider()
extern void ARKitImageTrackingSubsystem_CreateProvider_mB500E08F02CA3D72D00EF6C1DCACBC857E7F9C6C ();
// 0x000000A8 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem::.ctor()
extern void ARKitImageTrackingSubsystem__ctor_m0E214A0CB94F079C236C8B90D0D3ED0DE8BA8CB1 ();
// 0x000000A9 System.Void UnityEngine.XR.ARKit.ManagedReferenceImage::.ctor(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void ManagedReferenceImage__ctor_m3E6575019476409355D028AE5BF5CF06DBDEBE91_AdjustorThunk ();
// 0x000000AA UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARKit.ManagedReferenceImage::ToReferenceImage()
extern void ManagedReferenceImage_ToReferenceImage_m0B8B4A58D7D7A9E680928D4966568762164E0FC6_AdjustorThunk ();
// 0x000000AB UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARKit.ManagedReferenceImage::AsSerializedGuid(System.Guid)
extern void ManagedReferenceImage_AsSerializedGuid_m1D74B874928ABACCEAA33A8E2B17FD6C88195CE3_AdjustorThunk ();
// 0x000000AC System.Void UnityEngine.XR.ARKit.ManagedReferenceImage::Dispose()
extern void ManagedReferenceImage_Dispose_mE69922AA55D2E38E33DFB72E12C6684051491071_AdjustorThunk ();
// 0x000000AD T UnityEngine.XR.ARKit.ManagedReferenceImage::ResolveGCHandle(System.IntPtr)
// 0x000000AE System.Int32 UnityEngine.XR.ARKit.MemoryLayout::GetHashCode()
extern void MemoryLayout_GetHashCode_m6EC79923E1C318EAC9A2E00E8D9713E58AA37ACD_AdjustorThunk ();
// 0x000000AF System.Boolean UnityEngine.XR.ARKit.MemoryLayout::Equals(UnityEngine.XR.ARKit.MemoryLayout)
extern void MemoryLayout_Equals_m9858ECABD94F69653A020FA372FCFC2F380490CA_AdjustorThunk ();
// 0x000000B0 System.Boolean UnityEngine.XR.ARKit.MemoryLayout::Equals(System.Object)
extern void MemoryLayout_Equals_m09D6C50724B51935065A7CDD96D36AD84CD24495_AdjustorThunk ();
// 0x000000B1 System.Boolean UnityEngine.XR.ARKit.MemoryLayout::op_Equality(UnityEngine.XR.ARKit.MemoryLayout,UnityEngine.XR.ARKit.MemoryLayout)
extern void MemoryLayout_op_Equality_mACCB7C2460D49EAE5BEFF739511B0720D507E148 ();
// 0x000000B2 System.Boolean UnityEngine.XR.ARKit.MemoryLayout::op_Inequality(UnityEngine.XR.ARKit.MemoryLayout,UnityEngine.XR.ARKit.MemoryLayout)
extern void MemoryLayout_op_Inequality_m4CC8BAE44A506F5DF5EE61E45C6FB5C6192C2E93 ();
// 0x000000B3 System.IntPtr UnityEngine.XR.ARKit.NSData::op_Implicit(UnityEngine.XR.ARKit.NSData)
extern void NSData_op_Implicit_m849FF0BB76B860590D7397FD9D6CF1482FC78FFC ();
// 0x000000B4 UnityEngine.XR.ARKit.NSData UnityEngine.XR.ARKit.NSData::CreateWithBytes(System.Void*,System.Int32)
extern void NSData_CreateWithBytes_mD9E781B9122D9E9EED71208F1234EBC3E53075E9 ();
// 0x000000B5 UnityEngine.XR.ARKit.NSData UnityEngine.XR.ARKit.NSData::CreateWithBytesNoCopy(System.Void*,System.Int32,System.Boolean)
extern void NSData_CreateWithBytesNoCopy_m72ADBA64EA4F99900A822CCA4A83986D93DEA991 ();
// 0x000000B6 System.Void UnityEngine.XR.ARKit.NSData::.ctor(System.IntPtr)
extern void NSData__ctor_m9A7FECA65050D513BB19BB46FBD4EA6EAFD3C8FA_AdjustorThunk ();
// 0x000000B7 System.Boolean UnityEngine.XR.ARKit.NSData::get_created()
extern void NSData_get_created_m2D90222AF6028686A8939E8666189D65B4A1919E_AdjustorThunk ();
// 0x000000B8 System.Void* UnityEngine.XR.ARKit.NSData::get_bytes()
extern void NSData_get_bytes_m183DD9311C27C2C02B23B21DF0F105AD79B1AC6B_AdjustorThunk ();
// 0x000000B9 System.Int32 UnityEngine.XR.ARKit.NSData::get_length()
extern void NSData_get_length_m0BA80361ECB165E9A6972E49F5B0F466B62DD3A8_AdjustorThunk ();
// 0x000000BA Unity.Collections.NativeSlice`1<System.Byte> UnityEngine.XR.ARKit.NSData::ToNativeSlice()
extern void NSData_ToNativeSlice_mA623ABA3567BE33488F2E3F1AB94527CF0432B06_AdjustorThunk ();
// 0x000000BB System.Void UnityEngine.XR.ARKit.NSData::Dispose()
extern void NSData_Dispose_m8B1067FDB5CFAF606432A3EEB96812D7CCD8ADDE_AdjustorThunk ();
// 0x000000BC System.Int32 UnityEngine.XR.ARKit.NSData::GetHashCode()
extern void NSData_GetHashCode_m1B57A82FE61E21C2B94ED7623672FE66738D7C70_AdjustorThunk ();
// 0x000000BD System.Boolean UnityEngine.XR.ARKit.NSData::Equals(System.Object)
extern void NSData_Equals_mAEF0CEAB44D1070F793301EBAD51B7356914F7EE_AdjustorThunk ();
// 0x000000BE System.Boolean UnityEngine.XR.ARKit.NSData::Equals(UnityEngine.XR.ARKit.NSData)
extern void NSData_Equals_mC39A17804E12788903FFAB8CFBF70E427671A1F9_AdjustorThunk ();
// 0x000000BF System.Boolean UnityEngine.XR.ARKit.NSData::op_Equality(UnityEngine.XR.ARKit.NSData,UnityEngine.XR.ARKit.NSData)
extern void NSData_op_Equality_m6E656FC4E029E85669056D855E4F8AE6A8B871C5 ();
// 0x000000C0 System.Boolean UnityEngine.XR.ARKit.NSData::op_Inequality(UnityEngine.XR.ARKit.NSData,UnityEngine.XR.ARKit.NSData)
extern void NSData_op_Inequality_mC5B9BD2D944A27B3982DE160A1ABB4F46CBB7F53 ();
// 0x000000C1 System.Void UnityEngine.XR.ARKit.NSData::UnityARKit_CFRelease(System.IntPtr)
extern void NSData_UnityARKit_CFRelease_m6B4F93F8129E2E6F3DEBB5A0A905110AEB414308 ();
// 0x000000C2 System.Void* UnityEngine.XR.ARKit.NSData::UnityARKit_NSData_getBytes(System.IntPtr)
extern void NSData_UnityARKit_NSData_getBytes_mAA5116EF4A60FF192426E7FF4A6C5C8510A13F86 ();
// 0x000000C3 System.Int32 UnityEngine.XR.ARKit.NSData::UnityARKit_NSData_getLength(System.IntPtr)
extern void NSData_UnityARKit_NSData_getLength_m8F463861912690AF701DC4D1F298BAEE6E30871C ();
// 0x000000C4 System.IntPtr UnityEngine.XR.ARKit.NSData::UnityARKit_NSData_createWithBytes(System.Void*,System.Int32)
extern void NSData_UnityARKit_NSData_createWithBytes_m8A57C2C65866569604B9B1880DA65E5A73DA0F18 ();
// 0x000000C5 System.IntPtr UnityEngine.XR.ARKit.NSData::UnityARKit_NSData_createWithBytesNoCopy(System.Void*,System.Int32,System.Boolean)
extern void NSData_UnityARKit_NSData_createWithBytesNoCopy_mA268ACAAF7FFA5917018448B620EE916D27D8D50 ();
// 0x000000C6 System.IntPtr UnityEngine.XR.ARKit.NSMutableData::op_Implicit(UnityEngine.XR.ARKit.NSMutableData)
extern void NSMutableData_op_Implicit_mC4CE5A7B0FF67566F6D2561597F66F2846BA085C ();
// 0x000000C7 System.Void UnityEngine.XR.ARKit.NSMutableData::.ctor(System.Void*,System.Int32)
extern void NSMutableData__ctor_m85F65E0CAF2F4437F80B02F78E4CC6D2B3AB2A06_AdjustorThunk ();
// 0x000000C8 UnityEngine.XR.ARKit.NSData UnityEngine.XR.ARKit.NSMutableData::ToNSData()
extern void NSMutableData_ToNSData_mDA252358836999BE419A4D00E3C49791BACFD83D_AdjustorThunk ();
// 0x000000C9 System.Boolean UnityEngine.XR.ARKit.NSMutableData::get_created()
extern void NSMutableData_get_created_mD218AB8F4C06BA85007F04EA844D63658B110743_AdjustorThunk ();
// 0x000000CA System.Void* UnityEngine.XR.ARKit.NSMutableData::get_bytes()
extern void NSMutableData_get_bytes_m11502AC7249746EF0526CBD7578675BF81C75D6B_AdjustorThunk ();
// 0x000000CB System.Int32 UnityEngine.XR.ARKit.NSMutableData::get_length()
extern void NSMutableData_get_length_mC8FCDD66F7E1605868465B5E59B8E3DE0A2BD81E_AdjustorThunk ();
// 0x000000CC System.IntPtr UnityEngine.XR.ARKit.NSMutableData::get_ptr()
extern void NSMutableData_get_ptr_mDB546A2BA106F9E1D97EEAB680904FF2B9E82128_AdjustorThunk ();
// 0x000000CD System.Void UnityEngine.XR.ARKit.NSMutableData::Append(System.Void*,System.Int32)
extern void NSMutableData_Append_m47A65367547BBC3BBCA1B2C681EE3C73B8B76403_AdjustorThunk ();
// 0x000000CE System.Void UnityEngine.XR.ARKit.NSMutableData::Dispose()
extern void NSMutableData_Dispose_mEAAB4C5FA1BAA2B708A92402C7A336A0DA8E4A3F_AdjustorThunk ();
// 0x000000CF System.Int32 UnityEngine.XR.ARKit.NSMutableData::GetHashCode()
extern void NSMutableData_GetHashCode_mA5D0354E3A022B04ECEC9A0A464F918C16964202_AdjustorThunk ();
// 0x000000D0 System.Boolean UnityEngine.XR.ARKit.NSMutableData::Equals(System.Object)
extern void NSMutableData_Equals_m6B9AA0CA15A5D6D94269F9FB11B631E316E109D3_AdjustorThunk ();
// 0x000000D1 System.Boolean UnityEngine.XR.ARKit.NSMutableData::Equals(UnityEngine.XR.ARKit.NSMutableData)
extern void NSMutableData_Equals_mADF8D53F6EEC9EFB6C8986D14C86331EE1EC0C46_AdjustorThunk ();
// 0x000000D2 System.Boolean UnityEngine.XR.ARKit.NSMutableData::op_Equality(UnityEngine.XR.ARKit.NSMutableData,UnityEngine.XR.ARKit.NSMutableData)
extern void NSMutableData_op_Equality_mDDB5C4F23F8DC5F8E416722FDB8A898D292B38C5 ();
// 0x000000D3 System.Boolean UnityEngine.XR.ARKit.NSMutableData::op_Inequality(UnityEngine.XR.ARKit.NSMutableData,UnityEngine.XR.ARKit.NSMutableData)
extern void NSMutableData_op_Inequality_mA0F73000FB970FF89E66A50B98A9FF7ED301B585 ();
// 0x000000D4 System.Void UnityEngine.XR.ARKit.NSMutableData::UnityARKit_CFRelease(System.IntPtr)
extern void NSMutableData_UnityARKit_CFRelease_m48047B1DD2222EEDB29CD3BD8540C69AF2CBE476 ();
// 0x000000D5 System.Void UnityEngine.XR.ARKit.NSMutableData::UnityARKit_NSMutableData_append(System.IntPtr,System.Void*,System.Int32)
extern void NSMutableData_UnityARKit_NSMutableData_append_mA0BBFF30EABEC8C8BC19420D0EDE1DEE932E05DC ();
// 0x000000D6 System.IntPtr UnityEngine.XR.ARKit.NSMutableData::UnityARKit_NSMutableData_createWithBytes(System.Void*,System.Int32)
extern void NSMutableData_UnityARKit_NSMutableData_createWithBytes_m5A9B649942EE3727276A99F33D5FB6C90D4F88C1 ();
// 0x000000D7 System.Boolean UnityEngine.XR.ARKit.NativeChanges::get_created()
extern void NativeChanges_get_created_mD4E1C38022D90619A4EC6FB00625C836D127E8FB_AdjustorThunk ();
// 0x000000D8 System.Int32 UnityEngine.XR.ARKit.NativeChanges::get_addedLength()
extern void NativeChanges_get_addedLength_m229FB7E43EC8C76BEE7022F67F523A4D1557F481_AdjustorThunk ();
// 0x000000D9 System.Int32 UnityEngine.XR.ARKit.NativeChanges::get_updatedLength()
extern void NativeChanges_get_updatedLength_mBA4E768AE41A7FC6EA6F61CD756F19A6D520325B_AdjustorThunk ();
// 0x000000DA System.Int32 UnityEngine.XR.ARKit.NativeChanges::get_removedLength()
extern void NativeChanges_get_removedLength_m614CEDC349020B00F3192CCF398FA2BE57CAB03E_AdjustorThunk ();
// 0x000000DB UnityEngine.XR.ARKit.MemoryLayout UnityEngine.XR.ARKit.NativeChanges::get_memoryLayout()
extern void NativeChanges_get_memoryLayout_m33CE33857634BFAB2D256E5DFBBD1A34E2778C28_AdjustorThunk ();
// 0x000000DC UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.NativeChanges::get_trackingState()
extern void NativeChanges_get_trackingState_m4B7750E01DE7CC245657EF229D62064E130067A3_AdjustorThunk ();
// 0x000000DD System.Void UnityEngine.XR.ARKit.NativeChanges::Dispose()
extern void NativeChanges_Dispose_m1358BB3C08E567696B02AA0B049D4D214CDC2A6F_AdjustorThunk ();
// 0x000000DE System.Boolean UnityEngine.XR.ARKit.NativeChanges::Equals(System.Object)
extern void NativeChanges_Equals_m567C68E885D53B73943FA163F659AB337548D41C_AdjustorThunk ();
// 0x000000DF System.Int32 UnityEngine.XR.ARKit.NativeChanges::GetHashCode()
extern void NativeChanges_GetHashCode_mC66BD98DE9377530EEC347DDEC884428DAB095F5_AdjustorThunk ();
// 0x000000E0 System.Boolean UnityEngine.XR.ARKit.NativeChanges::Equals(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_Equals_m9BE3F3E0D96499230C43AB8572480E4E83B90D09_AdjustorThunk ();
// 0x000000E1 System.Boolean UnityEngine.XR.ARKit.NativeChanges::op_Equality(UnityEngine.XR.ARKit.NativeChanges,UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_op_Equality_m0E291E9DD72E7B596AA015CB5F39EAC85B09EF9D ();
// 0x000000E2 System.Boolean UnityEngine.XR.ARKit.NativeChanges::op_Inequality(UnityEngine.XR.ARKit.NativeChanges,UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_op_Inequality_m4F1A3A06F7C30F1977BE2ECC6694BCC4FC137F88 ();
// 0x000000E3 System.Int32 UnityEngine.XR.ARKit.NativeChanges::GetAddedLength(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_GetAddedLength_mAE96577C4AFF57B4249B563B5B5DDF42B1616B1B ();
// 0x000000E4 System.Int32 UnityEngine.XR.ARKit.NativeChanges::GetUpdatedLength(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_GetUpdatedLength_m1E6CD83834DE174A4A98CE5A30533B8EF41D0345 ();
// 0x000000E5 System.Int32 UnityEngine.XR.ARKit.NativeChanges::GetRemovedLength(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_GetRemovedLength_mDF5C0941045E74997EC18C3833A0E07D56A86189 ();
// 0x000000E6 UnityEngine.XR.ARKit.MemoryLayout UnityEngine.XR.ARKit.NativeChanges::GetMemoryLayout(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_GetMemoryLayout_m9A9EB356EA334E535B7F2D6ABEFB9989939AB542 ();
// 0x000000E7 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.NativeChanges::GetTrackingState(UnityEngine.XR.ARKit.NativeChanges)
extern void NativeChanges_GetTrackingState_mE845496B0599BCF2DEF4E3625127730305962F88 ();
// 0x000000E8 System.Int32 UnityEngine.XR.ARKit.OSVersion::get_major()
extern void OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6_AdjustorThunk ();
// 0x000000E9 System.Void UnityEngine.XR.ARKit.OSVersion::set_major(System.Int32)
extern void OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84_AdjustorThunk ();
// 0x000000EA System.Int32 UnityEngine.XR.ARKit.OSVersion::get_minor()
extern void OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5_AdjustorThunk ();
// 0x000000EB System.Void UnityEngine.XR.ARKit.OSVersion::set_minor(System.Int32)
extern void OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8_AdjustorThunk ();
// 0x000000EC System.Int32 UnityEngine.XR.ARKit.OSVersion::get_point()
extern void OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74_AdjustorThunk ();
// 0x000000ED System.Void UnityEngine.XR.ARKit.OSVersion::set_point(System.Int32)
extern void OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405_AdjustorThunk ();
// 0x000000EE System.Void UnityEngine.XR.ARKit.OSVersion::.ctor(System.Int32,System.Int32,System.Int32)
extern void OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4_AdjustorThunk ();
// 0x000000EF UnityEngine.XR.ARKit.OSVersion UnityEngine.XR.ARKit.OSVersion::Parse(System.String)
extern void OSVersion_Parse_mDF26500DB38764536C24E82F4659B9B1E0B40AC8 ();
// 0x000000F0 System.Int32 UnityEngine.XR.ARKit.OSVersion::IndexOfFirstDigit(System.String)
extern void OSVersion_IndexOfFirstDigit_m3527F158639800E2A9884F8D83A5F394B3FED5B4 ();
// 0x000000F1 System.Int32 UnityEngine.XR.ARKit.OSVersion::ParseNextComponent(System.String,System.Int32&)
extern void OSVersion_ParseNextComponent_mBDCC4A63E1B916D9B1136864639DA42C8762FC82 ();
// 0x000000F2 System.Int32 UnityEngine.XR.ARKit.OSVersion::GetHashCode()
extern void OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E_AdjustorThunk ();
// 0x000000F3 System.Int32 UnityEngine.XR.ARKit.OSVersion::CompareTo(UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1_AdjustorThunk ();
// 0x000000F4 System.Boolean UnityEngine.XR.ARKit.OSVersion::Equals(System.Object)
extern void OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3_AdjustorThunk ();
// 0x000000F5 System.Boolean UnityEngine.XR.ARKit.OSVersion::Equals(UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E_AdjustorThunk ();
// 0x000000F6 System.Boolean UnityEngine.XR.ARKit.OSVersion::op_LessThan(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_LessThan_m75C71874EFC4F3233EC421034E6599CADB16504F ();
// 0x000000F7 System.Boolean UnityEngine.XR.ARKit.OSVersion::op_GreaterThan(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_GreaterThan_mD04D07551AE4D4C9D08B63884DF80B255047F2B1 ();
// 0x000000F8 System.Boolean UnityEngine.XR.ARKit.OSVersion::op_Equality(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_Equality_mC51EEC8017A1A495CD6DB0E046FA1BCF4D656DB4 ();
// 0x000000F9 System.Boolean UnityEngine.XR.ARKit.OSVersion::op_Inequality(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_Inequality_m0DAF84C7189C1D4A8E2C2ED62E1254262E356843 ();
// 0x000000FA System.Boolean UnityEngine.XR.ARKit.OSVersion::op_GreaterThanOrEqual(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_GreaterThanOrEqual_m5DC241E62216C8897A29CA8BE7F1352528B9F420 ();
// 0x000000FB System.Boolean UnityEngine.XR.ARKit.OSVersion::op_LessThanOrEqual(UnityEngine.XR.ARKit.OSVersion,UnityEngine.XR.ARKit.OSVersion)
extern void OSVersion_op_LessThanOrEqual_mC003D088C0FC11B4C1857B846455594793B11CC8 ();
// 0x000000FC System.String UnityEngine.XR.ARKit.OSVersion::ToString()
extern void OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A_AdjustorThunk ();
// 0x000000FD UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider UnityEngine.XR.ARKit.ARKitParticipantSubsystem::CreateProvider()
extern void ARKitParticipantSubsystem_CreateProvider_m7A52FB02F598655323921124B54E127C1F9B3C73 ();
// 0x000000FE System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem::RegisterDescriptor()
extern void ARKitParticipantSubsystem_RegisterDescriptor_m36BF219BA989608869D12020754327EC6F34F945 ();
// 0x000000FF System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem::.ctor()
extern void ARKitParticipantSubsystem__ctor_m0656DF037CFFF93AE46EA045C34872E282582B97 ();
// 0x00000100 UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::CreateProvider()
extern void ARKitXRPlaneSubsystem_CreateProvider_mF0433C3656285E8CC2EC9F687799AB61C29B8C47 ();
// 0x00000101 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::RegisterDescriptor()
extern void ARKitXRPlaneSubsystem_RegisterDescriptor_m5F27E00E3BBC38D080D558D6B2689AB709ED0123 ();
// 0x00000102 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem::.ctor()
extern void ARKitXRPlaneSubsystem__ctor_m1A4464449DBD1423997334CA3D76D0ABB4E88B2B ();
// 0x00000103 System.Boolean UnityEngine.XR.ARKit.SerializedARCollaborationData::get_created()
extern void SerializedARCollaborationData_get_created_m2E3D4C6854CBD1CE9B3F90464DE40F0C62AC181C_AdjustorThunk ();
// 0x00000104 Unity.Collections.NativeSlice`1<System.Byte> UnityEngine.XR.ARKit.SerializedARCollaborationData::get_bytes()
extern void SerializedARCollaborationData_get_bytes_m14033DBDB8CC6A569A7858B2293F2EE494580059_AdjustorThunk ();
// 0x00000105 System.Void UnityEngine.XR.ARKit.SerializedARCollaborationData::Dispose()
extern void SerializedARCollaborationData_Dispose_m10A79EA8C1211FAE4E72A8B2C087582752D95AAF_AdjustorThunk ();
// 0x00000106 System.Int32 UnityEngine.XR.ARKit.SerializedARCollaborationData::GetHashCode()
extern void SerializedARCollaborationData_GetHashCode_m89BE561C8EAB7A0D69A671315134307523826249_AdjustorThunk ();
// 0x00000107 System.Boolean UnityEngine.XR.ARKit.SerializedARCollaborationData::Equals(System.Object)
extern void SerializedARCollaborationData_Equals_m22E16E2813369F9BC42DFB26638C81E34444E6E5_AdjustorThunk ();
// 0x00000108 System.Boolean UnityEngine.XR.ARKit.SerializedARCollaborationData::Equals(UnityEngine.XR.ARKit.SerializedARCollaborationData)
extern void SerializedARCollaborationData_Equals_m6B612F991B4E51ED3E7681265347A5AC4279BFF3_AdjustorThunk ();
// 0x00000109 System.Boolean UnityEngine.XR.ARKit.SerializedARCollaborationData::op_Equality(UnityEngine.XR.ARKit.SerializedARCollaborationData,UnityEngine.XR.ARKit.SerializedARCollaborationData)
extern void SerializedARCollaborationData_op_Equality_m9BD38364FC1D3828FBB062451408F0CC4C16C809 ();
// 0x0000010A System.Boolean UnityEngine.XR.ARKit.SerializedARCollaborationData::op_Inequality(UnityEngine.XR.ARKit.SerializedARCollaborationData,UnityEngine.XR.ARKit.SerializedARCollaborationData)
extern void SerializedARCollaborationData_op_Inequality_m40BDE8C85A155F6972DC67BE09F22DDEEC08EF78 ();
// 0x0000010B System.Void UnityEngine.XR.ARKit.SerializedARCollaborationData::.ctor(UnityEngine.XR.ARKit.NSData)
extern void SerializedARCollaborationData__ctor_mE5E3DB0B9757DCA77A22D5E582845A4291E77886_AdjustorThunk ();
// 0x0000010C System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::Start()
extern void ARKitProvider_Start_m909A9FE0AED772D065945138072F0C58CAF13E66 ();
// 0x0000010D System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::Stop()
extern void ARKitProvider_Stop_mFE6CDDBE56F66A7520273D7874DDC27367245FE6 ();
// 0x0000010E System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_mFEEF62895226542DCD4EBD2C667CBEDC0EF56105 ();
// 0x0000010F UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_m9FD494D13B72A009A7751182DB8AB50766FF2324 ();
// 0x00000110 System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void ARKitProvider_TryAddAnchor_m42B9138A2279B01E37D999093EAEEA761BB5A9E3 ();
// 0x00000111 System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void ARKitProvider_TryAttachAnchor_mBEFA4EAFE1B4209286CF510EDBB2032D0B9C8B72 ();
// 0x00000112 System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARKitProvider_TryRemoveAnchor_m3C19701F5458BECF81D3394FBA3F6205F3379199 ();
// 0x00000113 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_onStart()
extern void ARKitProvider_UnityARKit_refPoints_onStart_mD48C2FDF1464AABDE11ECF7C46F91BCC90CEFBC8 ();
// 0x00000114 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_onStop()
extern void ARKitProvider_UnityARKit_refPoints_onStop_m7A4366D4FB58FFD78C9843307089A57774E7DDE2 ();
// 0x00000115 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_onDestroy()
extern void ARKitProvider_UnityARKit_refPoints_onDestroy_mC0C108D4D36E43E31BB7D54160220884E3001D43 ();
// 0x00000116 System.Void* UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void ARKitProvider_UnityARKit_refPoints_acquireChanges_m4BD00FBC27D534F01AB15E6A217AD42CFB46BD78 ();
// 0x00000117 System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_releaseChanges(System.Void*)
extern void ARKitProvider_UnityARKit_refPoints_releaseChanges_mEADB262D174F77BFC9C0E8324FF920E4E21EFD39 ();
// 0x00000118 System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_tryAdd(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void ARKitProvider_UnityARKit_refPoints_tryAdd_m2969B475555721927D6B87BB31F202EC2B71ED09 ();
// 0x00000119 System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_tryAttach(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void ARKitProvider_UnityARKit_refPoints_tryAttach_mD0F2154B0CD3A7DB7EDC0198BC426708FC1CFD71 ();
// 0x0000011A System.Boolean UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::UnityARKit_refPoints_tryRemove(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARKitProvider_UnityARKit_refPoints_tryRemove_m46C3B0EADE0C2297E2A49154ABD7DC3860B0554A ();
// 0x0000011B System.Void UnityEngine.XR.ARKit.ARKitAnchorSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_mE014D0F9B44627BC32E1C4211BBCF6BF64192237 ();
// 0x0000011C UnityEngine.Material UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::get_cameraMaterial()
extern void ARKitProvider_get_cameraMaterial_m618301FF821AB3FA4BA2F4ED113A76289D4075B1 ();
// 0x0000011D System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::get_permissionGranted()
extern void ARKitProvider_get_permissionGranted_m396F8E4396480AF248974165484D627A9383C360 ();
// 0x0000011E System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m17954B389E39493C52B0EC8918C820943D550095 ();
// 0x0000011F System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::Start()
extern void ARKitProvider_Start_mEC90123DDA11C6024F62DA39E9873110ACD7600C ();
// 0x00000120 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::Stop()
extern void ARKitProvider_Stop_m9E080CB6E0FB7675ACEEBBCC4956A3030AEB3A00 ();
// 0x00000121 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_m9D9D4C8526F13F29BAFC67F69115C887DF92E32E ();
// 0x00000122 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void ARKitProvider_TryGetFrame_m49FDB983C16F10F3C37A87728AA5508DFC7DB669 ();
// 0x00000123 UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::get_cameraFocusMode()
extern void ARKitProvider_get_cameraFocusMode_m6631A2A95180FC11A2431A7F68C58DB81E731A4C ();
// 0x00000124 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::set_cameraFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void ARKitProvider_set_cameraFocusMode_m7D87F3A3324EB25EE6054EC41342C91A8767B6DD ();
// 0x00000125 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void ARKitProvider_TrySetLightEstimationMode_mA8341986E3571D15EAC602E4DF6C2CFC3F548EDA ();
// 0x00000126 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void ARKitProvider_TryGetIntrinsics_m4D8E7652489E160DE8F72B057D1BF4AC6AB48FF1 ();
// 0x00000127 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void ARKitProvider_GetConfigurations_mD393299D08B98045E06E902C5C27AABC068E3DE0 ();
// 0x00000128 System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::get_currentConfiguration()
extern void ARKitProvider_get_currentConfiguration_m6FE6950DD61DC22EE4A2DA65A118A142C12D65A5 ();
// 0x00000129 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void ARKitProvider_set_currentConfiguration_mD1B03DE1C34DCD8CCE1BDFBE9F58230455236713 ();
// 0x0000012A Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void ARKitProvider_GetTextureDescriptors_m4E627551BE5A781237F6517DEB67B6845A1FA610 ();
// 0x0000012B System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo&)
extern void ARKitProvider_TryAcquireLatestImage_mC72CF5742BD36615F430AAA93B35BDEDD088A747 ();
// 0x0000012C UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::GetAsyncRequestStatus(System.Int32)
extern void ARKitProvider_GetAsyncRequestStatus_mCACD2D1E4D1DABDD649D4744B66C30926004D21B ();
// 0x0000012D System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::DisposeImage(System.Int32)
extern void ARKitProvider_DisposeImage_mCD96A4E7AFF6818ED4716B4B4A189A4537EB971F ();
// 0x0000012E System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::DisposeAsyncRequest(System.Int32)
extern void ARKitProvider_DisposeAsyncRequest_m14B12FB7B00DDC17701173CC7B1602802DCAAFF6 ();
// 0x0000012F System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo&)
extern void ARKitProvider_TryGetPlane_m28439A10DE878269A0122C2CC556E9257E3D0504 ();
// 0x00000130 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::NativeHandleValid(System.Int32)
extern void ARKitProvider_NativeHandleValid_m8EBD3FB23DA716E87E25E3E2036D8F391A457428 ();
// 0x00000131 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void ARKitProvider_TryGetConvertedDataSize_m0AC8023D46234C71774C7432474E63133DFF23D5 ();
// 0x00000132 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32)
extern void ARKitProvider_TryConvert_mE31A78FA0E8F1E91781CA2427812B4548F12CF7A ();
// 0x00000133 System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void ARKitProvider_ConvertAsync_mD002C4A5BCB0D382F9985087CBCCF658043C6324 ();
// 0x00000134 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::TryGetAsyncRequestData(System.Int32,System.IntPtr&,System.Int32&)
extern void ARKitProvider_TryGetAsyncRequestData_m09AC24409C4FC6A6A2AFD9F3A33BD86C1FE0AA0B ();
// 0x00000135 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate,System.IntPtr)
extern void ARKitProvider_ConvertAsync_m026B0421A6ACFBDDD27639696EF0A0098D92962C ();
// 0x00000136 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_ARKitProvider::.cctor()
extern void ARKitProvider__cctor_m0CCAB7EE9003FDDD9165CB7FD8BC09A808E70CA1 ();
// 0x00000137 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_Construct(System.Int32,System.Int32)
extern void NativeApi_UnityARKit_Camera_Construct_m2DF0AEE8AA88CDF96D248F9CD714961C2738216F ();
// 0x00000138 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_Destruct()
extern void NativeApi_UnityARKit_Camera_Destruct_m795FFFF7821CE91E8992070BEFAAE0FFBA6AE084 ();
// 0x00000139 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_Start()
extern void NativeApi_UnityARKit_Camera_Start_m59DA518F90E02C5707FDD049DC3DECD96B65A5A6 ();
// 0x0000013A System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_Stop()
extern void NativeApi_UnityARKit_Camera_Stop_m098821B53BD6527CA00BB02E51D36A7A5C76FF10 ();
// 0x0000013B System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void NativeApi_UnityARKit_Camera_TryGetFrame_m6048991661BFB4AEE17C00EDCF7E65995B7C8691 ();
// 0x0000013C System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_SetFocusMode(UnityEngine.XR.ARSubsystems.CameraFocusMode)
extern void NativeApi_UnityARKit_Camera_SetFocusMode_mEA11062CA8C34DFC4DC3E967B28E26021CAB9DEC ();
// 0x0000013D UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_GetFocusMode()
extern void NativeApi_UnityARKit_Camera_GetFocusMode_m9F0C785F1D3835D11166E0DA19BB321BCF2554DD ();
// 0x0000013E System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TrySetLightEstimationMode(UnityEngine.XR.ARSubsystems.LightEstimationMode)
extern void NativeApi_UnityARKit_Camera_TrySetLightEstimationMode_m2DAF2550DA728382FB0F1B91B02B74FC50D4257E ();
// 0x0000013F System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void NativeApi_UnityARKit_Camera_TryGetIntrinsics_m364B7B8DCA914DD0C1878F56859A5C7D9CC3B045 ();
// 0x00000140 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_IsCameraPermissionGranted()
extern void NativeApi_UnityARKit_Camera_IsCameraPermissionGranted_m972DB9591AA4E5EB4F483280F44DEF5CC8C88EE3 ();
// 0x00000141 System.IntPtr UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_AcquireConfigurations(System.Int32&,System.Int32&)
extern void NativeApi_UnityARKit_Camera_AcquireConfigurations_m6A384C73CC0BE9DA50DC6128284E200678D08771 ();
// 0x00000142 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_ReleaseConfigurations(System.IntPtr)
extern void NativeApi_UnityARKit_Camera_ReleaseConfigurations_mECAE479F28DB8847031DB58670D463D8692C661D ();
// 0x00000143 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetCurrentConfiguration(UnityEngine.XR.ARSubsystems.XRCameraConfiguration&)
extern void NativeApi_UnityARKit_Camera_TryGetCurrentConfiguration_m2B5868976AF970142BB6CDBCBB04329C98FBAA88 ();
// 0x00000144 UnityEngine.XR.ARKit.ARKitCameraSubsystem_CameraConfigurationResult UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TrySetCurrentConfiguration(UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void NativeApi_UnityARKit_Camera_TrySetCurrentConfiguration_m10852C9A985C98A1BF729D17027BE7B96C9FB673 ();
// 0x00000145 System.Void* UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_AcquireTextureDescriptors(System.Int32&,System.Int32&)
extern void NativeApi_UnityARKit_Camera_AcquireTextureDescriptors_mEAFD58446AEBEC88F2E3E55B5C8C3C89B2BA0869 ();
// 0x00000146 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_ReleaseTextureDescriptors(System.Void*)
extern void NativeApi_UnityARKit_Camera_ReleaseTextureDescriptors_m1B1D58FE2C83A8854CD2223602213D302585A0A2 ();
// 0x00000147 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryAcquireLatestImage(UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImageCinfo&)
extern void NativeApi_UnityARKit_Camera_TryAcquireLatestImage_m9EDEF6EF0E4C88B918827C8DFCA3AF3265C04DEE ();
// 0x00000148 UnityEngine.XR.ARSubsystems.AsyncCameraImageConversionStatus UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_GetAsyncRequestStatus(System.Int32)
extern void NativeApi_UnityARKit_Camera_GetAsyncRequestStatus_m7281886403BB594C92DFD3A86ADEE408346923BC ();
// 0x00000149 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_DisposeImage(System.Int32)
extern void NativeApi_UnityARKit_Camera_DisposeImage_m696CD790C79BA59B3B69032D9E331D33F0D8506E ();
// 0x0000014A System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_DisposeAsyncRequest(System.Int32)
extern void NativeApi_UnityARKit_Camera_DisposeAsyncRequest_m60C68C4C527C4F1E0662997D19E991DE8B129AA2 ();
// 0x0000014B System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_CameraImagePlaneCinfo&)
extern void NativeApi_UnityARKit_Camera_TryGetPlane_m1CA36CD2E4A4BCCBE3A3009C0EF6D5D8BA1D85CD ();
// 0x0000014C System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_HandleValid(System.Int32)
extern void NativeApi_UnityARKit_Camera_HandleValid_mD3F58E286478E2DE08802D95C43C63153D77CBB5 ();
// 0x0000014D System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void NativeApi_UnityARKit_Camera_TryGetConvertedDataSize_m1F446C6518799135050CD5FA7CF8E6631821C54A ();
// 0x0000014E System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,System.IntPtr,System.Int32)
extern void NativeApi_UnityARKit_Camera_TryConvert_mADA4D24BA9D27EC2D94778FD0A4B378528EBBBD4 ();
// 0x0000014F System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_CreateAsyncConversionRequest(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams)
extern void NativeApi_UnityARKit_Camera_CreateAsyncConversionRequest_m45FBD7C1BB82A1DA6E8AADCEC8DB2A3825E59A2E ();
// 0x00000150 System.Boolean UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_TryGetAsyncRequestData(System.Int32,System.IntPtr&,System.Int32&)
extern void NativeApi_UnityARKit_Camera_TryGetAsyncRequestData_mB61F95189D5D9C1EC2CFF5253D8D35271667D5C1 ();
// 0x00000151 System.Void UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi::UnityARKit_Camera_CreateAsyncConversionRequestWithCallback(System.Int32,UnityEngine.XR.ARSubsystems.XRCameraImageConversionParams,UnityEngine.XR.ARSubsystems.XRCameraSubsystem_OnImageRequestCompleteDelegate,System.IntPtr)
extern void NativeApi_UnityARKit_Camera_CreateAsyncConversionRequestWithCallback_mA240E3A47077643262D966DC4EF537D924B522B5 ();
// 0x00000152 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m74EA11B1635AAFB7C75610A87527AEADC7AF4D20 ();
// 0x00000153 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::Start()
extern void ARKitProvider_Start_m30E0C9617EA20AE65E375C38674477E84E7BFF5D ();
// 0x00000154 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::Stop()
extern void ARKitProvider_Stop_mD23CA292FD04017A2EB4426D6DB0E8DD4DC0414A ();
// 0x00000155 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_m70663825B404FED505C944EFD6DD1FEEF22FFABF ();
// 0x00000156 System.Void UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::SetAutomaticPlacement(System.Boolean)
extern void ARKitProvider_SetAutomaticPlacement_mF2D03145C110FA3C41ED1171DA0C564E9E9EF759 ();
// 0x00000157 System.Boolean UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::TrySetEnvironmentTextureHDREnabled(System.Boolean)
extern void ARKitProvider_TrySetEnvironmentTextureHDREnabled_m716FDB49019570D0AEC26EE3310301F83F72F045 ();
// 0x00000158 System.Boolean UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void ARKitProvider_TryAddEnvironmentProbe_mDFA308B205CCF8C31AD53AD75D2C88EDDEA46D67 ();
// 0x00000159 System.Boolean UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARKitProvider_RemoveEnvironmentProbe_m7791BEF4D49E8AF96125895FC724ACB46AB51A9A ();
// 0x0000015A UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_mE537EDA473E09E3536CA242240ED672B3A591A1A ();
// 0x0000015B Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARKit.ARKitRaycastSubsystem_ARKitProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARKitProvider_Raycast_m9C4B998808B9588CD132C3952F4B300C3D224DAC ();
// 0x0000015C System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m89E04C05CD3998612E73B973BFAE10B8A3698973 ();
// 0x0000015D System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem_NativeApi::UnityARKit_raycast_acquireHitResults(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,System.Void*&,System.Int32&)
extern void NativeApi_UnityARKit_raycast_acquireHitResults_mA4AC6C358549A8DC05F394AFD56ED90CD03E9C9F ();
// 0x0000015E System.Void UnityEngine.XR.ARKit.ARKitRaycastSubsystem_NativeApi::UnityARKit_raycast_copyAndReleaseHitResults(System.Void*,System.Int32,System.Void*,System.Void*)
extern void NativeApi_UnityARKit_raycast_copyAndReleaseHitResults_m1080A044038B5CF33AAB68D1DD8C4018E1B2D12A ();
// 0x0000015F System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_mE8A2DBDA94E4A4E35736AA2F139ADA09007C78C1 ();
// 0x00000160 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::Resume()
extern void ARKitProvider_Resume_mCE288422DFF1107BC09A818C50303CD7E8DABBC0 ();
// 0x00000161 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::Pause()
extern void ARKitProvider_Pause_m9F19622C31DEA039930913B737B4AF88AD6A6953 ();
// 0x00000162 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void ARKitProvider_Update_m638CAF367EB09C5CA098713D500E085BDDAF27D7 ();
// 0x00000163 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_mA5FE3BCBE02898DCAE38EF64AB4B494B0597F616 ();
// 0x00000164 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::Reset()
extern void ARKitProvider_Reset_mEB1960006C47FD414F58EE336AFBE58006BD6A06 ();
// 0x00000165 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::GetAvailabilityAsync()
extern void ARKitProvider_GetAvailabilityAsync_m5DD8ADCFF0773B54FE8CB2BC8231E04FCD29E89C ();
// 0x00000166 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::InstallAsync()
extern void ARKitProvider_InstallAsync_m939BDBDDA1E171C150F142D1F49509C49F5DBF00 ();
// 0x00000167 System.IntPtr UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_nativePtr()
extern void ARKitProvider_get_nativePtr_m2434FCF2FDC94C2DB761BFB58F0EAE0DE0598773 ();
// 0x00000168 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_trackingState()
extern void ARKitProvider_get_trackingState_m4E6688F25BA0E3BFA8AD38263D3C1ADD0E65F0CA ();
// 0x00000169 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_notTrackingReason()
extern void ARKitProvider_get_notTrackingReason_m45B14B276B268C54B3EBE376BBDAF9985071A406 ();
// 0x0000016A System.Guid UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_sessionId()
extern void ARKitProvider_get_sessionId_m1A4C5FFEE7717335ABB12834D53CC52ABE84D684 ();
// 0x0000016B System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_matchFrameRate()
extern void ARKitProvider_get_matchFrameRate_m7B71690EB1BB0A9C275EA1A661200EB31216FCBB ();
// 0x0000016C System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::set_matchFrameRate(System.Boolean)
extern void ARKitProvider_set_matchFrameRate_m5630834FCE78BE7C16479A423175798E045070CC ();
// 0x0000016D System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_ARKitProvider::get_frameRate()
extern void ARKitProvider_get_frameRate_m57ABC1BBF94026BDCD5999C2E2CCB99653D01B02 ();
// 0x0000016E System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_createWorldMapRequest()
extern void NativeApi_UnityARKit_createWorldMapRequest_m010633C018D940583817758657D3896AB44DD231 ();
// 0x0000016F System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_createWorldMapRequestWithCallback(UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate,System.IntPtr)
extern void NativeApi_UnityARKit_createWorldMapRequestWithCallback_m0D65907CA8F5264B0817FFCAECB09836A6565EEB ();
// 0x00000170 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_worldMapSupported()
extern void NativeApi_UnityARKit_worldMapSupported_m1EDEB43F65D1C8D7A548D1CDA661CA5365560FC7 ();
// 0x00000171 UnityEngine.XR.ARKit.ARWorldMappingStatus UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getWorldMappingStatus()
extern void NativeApi_UnityARKit_session_getWorldMappingStatus_mD1C6F316E1B312DBBBF5A3C388D39C8492AFED3D ();
// 0x00000172 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_applyWorldMap(System.Int32)
extern void NativeApi_UnityARKit_applyWorldMap_m5CCBE77B93748A2B5549BA35E11F91843C40C315 ();
// 0x00000173 System.IntPtr UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getNativePtr()
extern void NativeApi_UnityARKit_session_getNativePtr_m4F7B0EAF0A36CB86D01691442D10550679A32341 ();
// 0x00000174 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_Availability UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getAvailability()
extern void NativeApi_UnityARKit_session_getAvailability_m99887277747C2AAD5A630BF058000B82B97F844D ();
// 0x00000175 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_update()
extern void NativeApi_UnityARKit_session_update_m8C606BC443E0856C3CE057C45A554EAF1DAA8B74 ();
// 0x00000176 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_construct()
extern void NativeApi_UnityARKit_session_construct_mFF6871B50DE774C16170C63CBC1C5A244B6DF7B3 ();
// 0x00000177 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_destroy()
extern void NativeApi_UnityARKit_session_destroy_mED1AC9AA6E6D593699FB6A877AA9B7C015D5C7E6 ();
// 0x00000178 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_resume()
extern void NativeApi_UnityARKit_session_resume_m65F7BC2F836A0DFD9C4ED2F9D9F7B3D7FD8BB6A3 ();
// 0x00000179 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_pause()
extern void NativeApi_UnityARKit_session_pause_m1D55CF790B44AEA02FBBB1D58CE68306AC1ECEA0 ();
// 0x0000017A System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_reset()
extern void NativeApi_UnityARKit_session_reset_m67A0BCBAF9237CEA676E4AE5AFCADA108D7DA87C ();
// 0x0000017B UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getTrackingState()
extern void NativeApi_UnityARKit_session_getTrackingState_mC556510D36A2A09FD80AB564EF433A9740EEEF60 ();
// 0x0000017C UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getNotTrackingReason()
extern void NativeApi_UnityARKit_session_getNotTrackingReason_mDD73A7E193548D8D040F9C147C733072EFA6FF25 ();
// 0x0000017D System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getCollaborationSupported()
extern void NativeApi_UnityARKit_session_getCollaborationSupported_mF2E6538A8E947A80B9BFDC8DD35D7C16C7A7AF98 ();
// 0x0000017E System.IntPtr UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_dequeueCollaborationData()
extern void NativeApi_UnityARKit_session_dequeueCollaborationData_m8BA4E3ADE3AF737995FDD372D90650C8623873DE ();
// 0x0000017F System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getCollaborationDataQueueSize()
extern void NativeApi_UnityARKit_session_getCollaborationDataQueueSize_mA5194859037FCC7D4E50E81FB6BDDB82C7DE4578 ();
// 0x00000180 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_updateWithCollaborationData(System.IntPtr)
extern void NativeApi_UnityARKit_session_updateWithCollaborationData_mECCD8142C621CAEDA2364559BB4BD2FFD6DF03EA ();
// 0x00000181 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getCollaborationEnabled()
extern void NativeApi_UnityARKit_session_getCollaborationEnabled_mD2488ECB355D675BC63D1B87D1D7182B7425AD94 ();
// 0x00000182 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_setCollaborationRequested(System.Boolean)
extern void NativeApi_UnityARKit_session_setCollaborationRequested_mED304CB532D3387B6ADBAF0AD248B17C7D194D8C ();
// 0x00000183 System.Guid UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getSessionId()
extern void NativeApi_UnityARKit_session_getSessionId_m110172205B0A20E0104F0EFCE06292EEFAAABB87 ();
// 0x00000184 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_getMatchFrameRateEnabled()
extern void NativeApi_UnityARKit_session_getMatchFrameRateEnabled_m8F100FB7D6375D28C817BA1F3BE453A6197905E3 ();
// 0x00000185 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_session_setMatchFrameRateEnabled(System.Boolean)
extern void NativeApi_UnityARKit_session_setMatchFrameRateEnabled_m9AD09A23F4C7179C29CFDC5C95D2E75DD3DF751E ();
// 0x00000186 System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_Session_GetFrameRate()
extern void NativeApi_UnityARKit_Session_GetFrameRate_m972076B8E05E0A255E6A2BD428A07FAEA8BF92CF ();
// 0x00000187 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_getActivatesAutomatically()
extern void NativeApi_UnityARKit_coachingOverlay_getActivatesAutomatically_m318B6374D6856938069DC9EEA01A723E4A50BAF1 ();
// 0x00000188 System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_setActivatesAutomatically(System.Boolean)
extern void NativeApi_UnityARKit_coachingOverlay_setActivatesAutomatically_m5C3121BD09BA0F509BE3C637FDBBF4343BCE01A3 ();
// 0x00000189 System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_isActive()
extern void NativeApi_UnityARKit_coachingOverlay_isActive_mC736E2753CC6A08C9FE7211F774873EABA172FE3 ();
// 0x0000018A System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_setGoal(UnityEngine.XR.ARKit.ARCoachingGoal)
extern void NativeApi_UnityARKit_coachingOverlay_setGoal_m12A3CB19A2F7C35484FB7B5A32D293118727F3D1 ();
// 0x0000018B UnityEngine.XR.ARKit.ARCoachingGoal UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_getGoal()
extern void NativeApi_UnityARKit_coachingOverlay_getGoal_m605073A82D7FE71FB0D482AA33927C6525AE26D7 ();
// 0x0000018C System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_setActive(System.Boolean,System.Boolean)
extern void NativeApi_UnityARKit_coachingOverlay_setActive_m5393C3EE8B627C83C8209BDADDA3E791D99B2F01 ();
// 0x0000018D System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_ensureRootViewIsSetup()
extern void NativeApi_UnityARKit_ensureRootViewIsSetup_mE225FB815AA43CEF206A97D455216B3ED4C9C557 ();
// 0x0000018E System.Boolean UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi::UnityARKit_coachingOverlay_isSupported()
extern void NativeApi_UnityARKit_coachingOverlay_isSupported_m4D56FB899E6AB27EE3420A84FAFC65819D5EE085 ();
// 0x0000018F System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::UnityARKit_depth_destroy()
extern void ARKitProvider_UnityARKit_depth_destroy_m6A4D74F885ACE3382A741919678241F0263C85A6 ();
// 0x00000190 System.Void* UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::UnityARKit_depth_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void ARKitProvider_UnityARKit_depth_acquireChanges_m1516593C0ED16CB5373B80145A8F2D151FF80C04 ();
// 0x00000191 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::UnityARKit_depth_releaseChanges(System.Void*)
extern void ARKitProvider_UnityARKit_depth_releaseChanges_mE00BAA1C07118438BCCC80931149478D98737FD1 ();
// 0x00000192 System.Void* UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::UnityARKit_depth_acquirePointCloud(UnityEngine.XR.ARSubsystems.TrackableId,System.Void*&,System.Void*&,System.Int32&)
extern void ARKitProvider_UnityARKit_depth_acquirePointCloud_mBF75060DDA3A390EEE695166FBA734C9F54905B5 ();
// 0x00000193 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::UnityARKit_depth_releasePointCloud(System.Void*)
extern void ARKitProvider_UnityARKit_depth_releasePointCloud_m43EF97D3587FC680FE57E107573BFE8920B415FF ();
// 0x00000194 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRPointCloud,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_m93C38677755047CBEC8888EC166728A155F1D386 ();
// 0x00000195 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_mA9D53428BEF1C15EE6739F0081898F376A809D11 ();
// 0x00000196 UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void ARKitProvider_GetPointCloudData_m08F4534A555CDC857A7525F7690B7B9AA9000C34 ();
// 0x00000197 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m1EE1EC8D2E04581B857FEF2C8B53EBD83C9DF252 ();
// 0x00000198 System.Void UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob::Execute(System.Int32)
extern void TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C_AdjustorThunk ();
// 0x00000199 System.Void UnityEngine.XR.ARKit.ARKitImageDatabase_DeallocateNativeArrayJob`1::Execute()
// 0x0000019A System.Void UnityEngine.XR.ARKit.ARKitImageDatabase_ConvertRGBA32ToARGB32Job::Execute(System.Int32)
extern void ConvertRGBA32ToARGB32Job_Execute_mA531EADD7AD59301639B1C6369985BAA091C5FAA_AdjustorThunk ();
// 0x0000019B System.Void UnityEngine.XR.ARKit.ARKitImageDatabase_AddImageJob::Execute()
extern void AddImageJob_Execute_m15211532BA39BCD74DC8D9FAAB995D8958CA1BC1_AdjustorThunk ();
// 0x0000019C System.Boolean UnityEngine.XR.ARKit.ARKitImageDatabase_AddImageJob::UnityARKit_ImageDatabase_AddImage(System.IntPtr,System.Void*,UnityEngine.TextureFormat,System.Int32,System.Int32,System.Single,UnityEngine.XR.ARKit.ManagedReferenceImage&)
extern void AddImageJob_UnityARKit_ImageDatabase_AddImage_m0241BA575F07E3A8B82792515CE25A3F5CF81614 ();
// 0x0000019D UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void ARKitProvider_CreateRuntimeLibrary_mAB8A0D4F46D0FE04DD8D63862A1355C9E7A2B6B0 ();
// 0x0000019E System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
extern void ARKitProvider_set_imageLibrary_m691388D54857E38CF5E34256E6590419E4924B2E ();
// 0x0000019F UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_m4147331A5C2CF153138902E97229ECB1436E3144 ();
// 0x000001A0 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_m1DDCE5E5D84903F8AFECA244F256B4F632125FD3 ();
// 0x000001A1 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::set_maxNumberOfMovingImages(System.Int32)
extern void ARKitProvider_set_maxNumberOfMovingImages_m1D3225DDD8D2406B1170C776B2F294F935DD8997 ();
// 0x000001A2 System.Void UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_mC4557FF3997401EFDF46BA435B7859457636DAC3 ();
// 0x000001A3 System.Boolean UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::get_created()
extern void ARKitProvider_get_created_mED66AD263607B1393D443D88A7D718BD86C4FC04 ();
// 0x000001A4 System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m620792E932F5D1794CC38F88ECC0E6672364B2DA ();
// 0x000001A5 System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::Start()
extern void ARKitProvider_Start_m7989D59F3272929526D92F904F732920227A86DC ();
// 0x000001A6 System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::Stop()
extern void ARKitProvider_Stop_m8509EE10A3D390955DEE462CDFF2851C634CC443 ();
// 0x000001A7 System.Void UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_m27877ABB9B6FB5B0BF812D93323582BAAA962063 ();
// 0x000001A8 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRParticipant> UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRParticipant,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_mBDE004575A175B2E0207016814CC9B66D2DE9FAF ();
// 0x000001A9 System.IntPtr UnityEngine.XR.ARKit.ARKitParticipantSubsystem_ARKitProvider::UnityARKit_Participant_init()
extern void ARKitProvider_UnityARKit_Participant_init_m5727B98C01813D0DA51AFE3CD2EB7A9A49F289D0 ();
// 0x000001AA System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::Destroy()
extern void ARKitProvider_Destroy_mF5973030EA8900156ADFA608B5B0E0B1FE6E9B7B ();
// 0x000001AB System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::Start()
extern void ARKitProvider_Start_m9DB7CC568BF1ABE39E793EA536BDBE0C6B025807 ();
// 0x000001AC System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::Stop()
extern void ARKitProvider_Stop_mA24DB5AF5FEDD8CC74E69644BE38016951768842 ();
// 0x000001AD System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void ARKitProvider_GetBoundary_m1346438CA4B156993DCBD5A29A4F0C3C029963CD ();
// 0x000001AE UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void ARKitProvider_GetChanges_m44C632F9A10C0F3BCD2DA3383C78B9A26F5220F5 ();
// 0x000001AF System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::set_planeDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void ARKitProvider_set_planeDetectionMode_mDCF459FF96AEDC45F88F47FB7A7099FA26890FA6 ();
// 0x000001B0 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider::.ctor()
extern void ARKitProvider__ctor_m97A7439667349115C58FB37C4AA5F6814F769E26 ();
// 0x000001B1 System.Boolean UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_SupportsClassification()
extern void NativeApi_UnityARKit_planes_SupportsClassification_mD1B96229E99DA916EE152509A67A61F66729C83F ();
// 0x000001B2 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_shutdown()
extern void NativeApi_UnityARKit_planes_shutdown_mD1C2043ADBAB5270B853789C2BA0FF2FA7F461B2 ();
// 0x000001B3 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_start()
extern void NativeApi_UnityARKit_planes_start_m4E5A97BCD4399E84F7DAC9325F42050F75EF739C ();
// 0x000001B4 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_stop()
extern void NativeApi_UnityARKit_planes_stop_m1854477D39BC0BB385A16C04BF99D321CAF5A941 ();
// 0x000001B5 System.Void* UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void NativeApi_UnityARKit_planes_acquireChanges_m483D3C7F49CEED1D7BB70496B6C1D3B2583925E4 ();
// 0x000001B6 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_releaseChanges(System.Void*)
extern void NativeApi_UnityARKit_planes_releaseChanges_m38D9677B1F505B9773102E5BA9CD585BEFE6E044 ();
// 0x000001B7 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_setPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void NativeApi_UnityARKit_planes_setPlaneDetectionMode_m2B39558E40C526CC42FB2F6CA52A8E72820C4730 ();
// 0x000001B8 System.Void* UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_acquireBoundary(UnityEngine.XR.ARSubsystems.TrackableId,System.Void*&,System.Int32&)
extern void NativeApi_UnityARKit_planes_acquireBoundary_m64185E8AF48A2594D17765271ECE51FBFA8075BE ();
// 0x000001B9 System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_NativeApi::UnityARKit_planes_releaseBoundary(System.Void*)
extern void NativeApi_UnityARKit_planes_releaseBoundary_m15D062F6634FEF9BA69690ADCB3BC02BE12B6933 ();
// 0x000001BA System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void OnAsyncConversionCompleteDelegate__ctor_m4BE0A755AAC63D37870EB5636686E53F8A3AB114 ();
// 0x000001BB System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate::Invoke(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr)
extern void OnAsyncConversionCompleteDelegate_Invoke_m02E895D72FDB88BB08F0A654711CE610B2F00816 ();
// 0x000001BC System.IAsyncResult UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate::BeginInvoke(UnityEngine.XR.ARKit.ARWorldMapRequestStatus,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnAsyncConversionCompleteDelegate_BeginInvoke_m2C33525828F103BD6E8F47B18F3193F6AF8FB56A ();
// 0x000001BD System.Void UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate::EndInvoke(System.IAsyncResult)
extern void OnAsyncConversionCompleteDelegate_EndInvoke_mD44E14594FFBDA7691CD4F7D9BE1247CA1EE6AE7 ();
// 0x000001BE System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider_FlipBoundaryWindingJob::Execute()
extern void FlipBoundaryWindingJob_Execute_m558BC1AD243296BF68DCB7877F26578A10310422_AdjustorThunk ();
// 0x000001BF System.Void UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_ARKitProvider_TransformBoundaryPositionsJob::Execute(System.Int32)
extern void TransformBoundaryPositionsJob_Execute_m326C6D99F9D9D6487CC75D232E3326A117AF44DF_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[447] = 
{
	ARCollaborationData__ctor_mA8801622DBC87812580E672FBED6A0E7AA70CBFB_AdjustorThunk,
	ARCollaborationData__ctor_mDC9EFE0861A9160434B1619D5D812313F6BD33E9_AdjustorThunk,
	ARCollaborationData__ctor_mBCE6B3BE0FBCD54FACDC15D9399F9F383542B171_AdjustorThunk,
	ARCollaborationData_get_valid_mE4CAB210F23A37A61E1A521A2FDAAD46F8A6512F_AdjustorThunk,
	ARCollaborationData_get_priority_m697D851F51AA30CCA702C8034E620535BD17BDD6_AdjustorThunk,
	ARCollaborationData_Dispose_m4F408B1A35120FCF93A095EBEA19401BA176889F_AdjustorThunk,
	ARCollaborationData_ToSerialized_mA7D388B24320043601C54F15AFD2AD04889CEEB7_AdjustorThunk,
	ARCollaborationData_GetHashCode_m6CB5AB6E64E20081AB461FD69825A53DDB4F6B40_AdjustorThunk,
	ARCollaborationData_Equals_m2EB8A50C23709F272ACF85A0EA7A34BA4D8DA8FE_AdjustorThunk,
	ARCollaborationData_Equals_mCF87707CDF6A1EE12FF4E73002A6491465EA235C_AdjustorThunk,
	ARCollaborationData_op_Equality_m1E921049437F4E563482D4946FDB909438D69CAD,
	ARCollaborationData_op_Inequality_mBAE1F7A82EF713E3E371ECE54BDE0B935AFAFE68,
	ARCollaborationData__ctor_m743A990ED9B051DA8CEBEBDA6165A3031BA0AFA7_AdjustorThunk,
	ARCollaborationData__ctor_m4F3BF86E1076C66410C7BE6A1A2AE421DD4198EE_AdjustorThunk,
	ARCollaborationData_ValidateAndThrow_m2412260BCDAF65DCDC4C92D69579BAB8E7B3A86E_AdjustorThunk,
	ARCollaborationData_ConstructUnchecked_mE36A07B80180D818F2E489F421950F63BA081A8E,
	ARCollaborationData_ConstructUnchecked_m3E2363E2E0DDCFAFB6AF02D6B0673DD0C98D6BCD,
	ARCollaborationData_UnityARKit_CFRelease_m3FDFA75E2B0D0B02E6608B661E81D3350CC28773,
	ARCollaborationData_UnityARKit_session_deserializeCollaborationDataFromNSData_m7811442A62DA0DD626074F68DEF920DB7D428166,
	ARCollaborationData_UnityARKit_session_serializeCollaborationDataToNSData_m12A6F966BC80BB847759E2C50E4927F509692ED6,
	ARCollaborationData_UnityARKit_session_getCollaborationDataPriority_mE36E65BEA8F9BF6493AAEC17A483D1F1DA0EE6BE,
	ARCollaborationDataBuilder_get_hasData_mDA85E8BE053C940508B6875CF5310FF5FF6578D7_AdjustorThunk,
	ARCollaborationDataBuilder_get_length_m34DEDD9E47CA7FA9065B4F07EEABA2A35614E9AB_AdjustorThunk,
	ARCollaborationDataBuilder_ToCollaborationData_mF5FBFCB380133ECB14B169F5B570528BF053AFF8_AdjustorThunk,
	ARCollaborationDataBuilder_Append_mAAC5FABB782E0D811AD5D6756DFCE152F7A515DA_AdjustorThunk,
	ARCollaborationDataBuilder_Append_m844A03A45AC50DB065D71045C115B7843CC2E199_AdjustorThunk,
	ARCollaborationDataBuilder_Append_mFE88865CACD6EB7450AF4A59DF4863277B6803C6_AdjustorThunk,
	ARCollaborationDataBuilder_Dispose_m8873F4FE4305DABBDFA232AEDD617C7BA2B26D9E_AdjustorThunk,
	ARCollaborationDataBuilder_AppendUnchecked_mC602BCE02D597CD71F308FD9CBB4E490A6D30392_AdjustorThunk,
	ARCollaborationDataBuilder_GetHashCode_m4EE38AEAF1AD7A7A3AE8F4C99BFFD92596C9FBEF_AdjustorThunk,
	ARCollaborationDataBuilder_Equals_mCBF064ACE8FCB2EDBAD48335BB33F7D0091B3DBF_AdjustorThunk,
	ARCollaborationDataBuilder_Equals_m31095618C272CAA89694E0E880A649E0F618AF54_AdjustorThunk,
	ARCollaborationDataBuilder_op_Equality_m33534EFB61AF43BFC80898CAF3311546F820DBD9,
	ARCollaborationDataBuilder_op_Inequality_m5B646EBA82D0882BDEC81490C2FA61A6D48D50D8,
	ARKitAnchorSubsystem_CreateProvider_mD7F43AB383497B78DAD76DC871BCF4147BF142D9,
	ARKitAnchorSubsystem_RegisterDescriptor_m563C875B34BEDCB4973833AFCC1EA1A60E9D0234,
	ARKitAnchorSubsystem__ctor_mFE8F6F6F67B8253619540C0742C75A3134C3E9AE,
	Api_UnityARKit_TrackableProvider_start_m3A9E9F96C54B9D88A81D7BE64DC7905D0CA280A3,
	Api_UnityARKit_TrackableProvider_stop_mB2234DA5EEC25825C3941A26B64D8DA0EEB32AE1,
	Api_UnityARKit_TrackableProvider_acquireChanges_m2559830EE33C4DABF75C059D5EB5CAC3413903C6,
	Api_UnityARKit_TrackableProvider_copyChanges_mE4792AF38E41101B4F60631127E37B6AB392D55B,
	Api_UnityARKit_getWorldMapRequestStatus_mD2F538AB59442DBF7690EE9975FE22ED8ADF4654,
	Api_UnityARKit_disposeWorldMap_mDB310DE6F820E095FB2B2D6ECD586FB7617026B9,
	Api_UnityARKit_disposeWorldMapRequest_m552CC414C42D2EB81B337AFAAA66D509FCD2D609,
	Api_UnityARKit_getWorldMapIdFromRequestId_m501FE31EF9CD0C628AFC54D3790111E340855005,
	Api_UnityARKit_isWorldMapValid_m03ED705763A5E3C562D69409DA5125716F55ECCF,
	Api_UnityARKit_trySerializeWorldMap_m626F02DB778347619427DF1F570702CE6D7F46D2,
	Api_UnityARKit_copyAndReleaseNsData_m1196DA7BA1FD8BDE407C5F4E5595FE5FEE7824C5,
	Api_UnityARKit_deserializeWorldMap_m6F213F566D0E3C21F87F79B61DCCB8FAF590631B,
	Api_CFRelease_m722B29376E99768260B799C5776E0B906B277C09,
	Api_UnityARKit_CFRelease_m048AD7191576F85CD81DD1DE41A41379083DDEA7,
	ARKitCameraSubsystem_get_backgroundShaderName_mF68B51768DCDA3A949F5E5BF4E89688C5170363E,
	ARKitCameraSubsystem_Register_m0B242BF988E92E626979B845EE749A1794E9E642,
	ARKitCameraSubsystem_CreateProvider_m0B81146A5AAC0EFB8C64C1B3F870E9DED4578142,
	ARKitCameraSubsystem__ctor_mE0B24FB4ED5FC603CB22E220A161A00177B9B8B9,
	ARKitEnvironmentProbeRegistration_Register_m99E02E59582A0F880422D128FEEF2BEE553EE20E,
	ARKitEnvironmentProbeSubsystem_CreateProvider_m0546A1ACD5AA45AA8A1C0D64579057FAF95CD5ED,
	ARKitEnvironmentProbeSubsystem__ctor_m957799C8DE48100EF21E8429209271292EADFA95,
	ARKitLoader_get_sessionSubsystem_mA7515BBCDF9232D049CBF1B191BAC905DC66755F,
	ARKitLoader_get_cameraSubsystem_mD12107697F3E7D434978BA85E0B37013E502FDB7,
	ARKitLoader_get_depthSubsystem_mCC4A7A2EA5F0C3929C3E70D2A1868834F60D90DA,
	ARKitLoader_get_planeSubsystem_m9AA4DF25AEAF1D2C3FA0E192344C5E99475BEDA1,
	ARKitLoader_get_anchorSubsystem_mD4ECBB36A396B8F38EBF36253D347168F7F3AC25,
	ARKitLoader_get_raycastSubsystem_mDD7D230E26224F56A80066A06D97A15735F27C12,
	ARKitLoader_get_environmentProbeSubsystem_mE4BF29A12A134D23A8F5E640A596C6CD76549D2B,
	ARKitLoader_get_inputSubsystem_m4F58892B9B2842ACD60D9C633AA2002A4DC65D8A,
	ARKitLoader_get_imageTrackingSubsystem_m6EA1221F3B7DD4A5438AAA4CC58D545025EDCA7A,
	ARKitLoader_get_faceSubsystem_m99A0CBBD032BF9E347C53BA32C80DF8F29F8FCBB,
	ARKitLoader_Initialize_m07F6EAAEBF5DD33535C326D9CEAF0BF9568E6F42,
	ARKitLoader_Start_m64F1E97F17BDD984CF68D372EC73DA7E58FA493D,
	ARKitLoader_Stop_m1050851FB75721A0E9904DBDC1A44943B5C85510,
	ARKitLoader_Deinitialize_m8A9CF38E7A1BA52B4EADE0FCEDA8A60DEEADD486,
	ARKitLoader_GetSettings_mFA9176AE469580FF0BC04BF25B5E608045186FE3,
	ARKitLoader__ctor_mD91F0A69BDCB27DEE299AE30552166FD679EC98A,
	ARKitLoader__cctor_m2F2390E6BE89D5F246595C1733D7A4FC2F0328C4,
	ARKitLoaderSettings_get_startAndStopSubsystems_mC3EC3BC6C40190BF99DA2D9461BCDE1A4A42644F,
	ARKitLoaderSettings_set_startAndStopSubsystems_mE22F3D40AAD40FAB7FCD2E9DFE0FF6D66CA2F6B7,
	ARKitLoaderSettings_Awake_m294A6CE5BBCD6DC1537B10E5F8599AEB2AF0AEDA,
	ARKitLoaderSettings__ctor_m2174ECFDC33B981CB04D95B3622D07D5A2A0F590,
	ARKitLoaderSettings__cctor_m40D2F9FD2EACD15321D90E0AD51E3C3B812C8273,
	ARKitRaycastSubsystem_CreateProvider_mC5E83538555A160082D762DA48ABD7BF87831EFF,
	ARKitRaycastSubsystem_RegisterDescriptor_m1442393B3D2D07C4E6F1FBD355695BCEB4DCF63E,
	ARKitRaycastSubsystem__ctor_m8FE9A2A756806FD0166A60C2347D88F0BB38307F,
	ARKitSessionSubsystem_get_coachingOverlaySupported_m691AF2E35A3F2C3F14AF9AE86F0EBEC2A3FC7E45,
	ARKitSessionSubsystem_get_coachingActivatesAutomatically_m89E0B3DC0DC069BD8001B4F8CB8C807CC6B6F4A4,
	ARKitSessionSubsystem_set_coachingActivatesAutomatically_mE690DEAD6678332A5039C1AB318F27D49D607739,
	ARKitSessionSubsystem_get_coachingGoal_mE0E000624179A69AA375BD935047FF013104236B,
	ARKitSessionSubsystem_set_coachingGoal_mBEFD3504793A2644D37EE00EE410F29FA36A533B,
	ARKitSessionSubsystem_get_coachingActive_m1F3D6A334AF8F481E8B04D741770CB7FA4D024D1,
	ARKitSessionSubsystem_SetCoachingActive_m1180E8D2081C7C4584DA0A2D4225A4028B98046B,
	ARKitSessionSubsystem_GetARWorldMapAsync_m767616E7906AC991039AE185E3ADD5ACB3AE1B10,
	ARKitSessionSubsystem_GetARWorldMapAsync_mF0736B99BBD6949AC7A00FEC9CB1204A5284E6B0,
	ARKitSessionSubsystem_get_worldMapSupported_mBA665DC3901CBA775BABD2DA7F02E9FFAE8BEA20,
	ARKitSessionSubsystem_get_worldMappingStatus_mCCC90F7BE712B10BCD985D7E464AA5755CE3F045,
	ARKitSessionSubsystem_ApplyWorldMap_m43FB6CD517FF823EF08047B8BA4B2B774CFB83E2,
	ARKitSessionSubsystem_get_collaborationEnabled_m1A72B522F81FFF9B4D0DD6F1DD3F638B36F335E2,
	ARKitSessionSubsystem_set_collaborationEnabled_m8D7BAAFEC89962C1BC9E822F4F89066D3E398E7A,
	ARKitSessionSubsystem_get_supportsCollaboration_m32A2801F58B78052A987A18A818191AD857C3F95,
	ARKitSessionSubsystem_get_collaborationDataCount_m518124EDC7BC5530DAE3A9DDA3277718E2913823,
	ARKitSessionSubsystem_DequeueCollaborationData_m52BA35A7996DF823BA6839F87CDD4AD909C372E5,
	ARKitSessionSubsystem_UpdateWithCollaborationData_m4D9C2D04512630C4EDFDF22CD51950034C07E741,
	ARKitSessionSubsystem_CreateProvider_m7F7FBC5E6D8F60E7F8360E750EEE41A342A9C4F5,
	ARKitSessionSubsystem__cctor_mF21044D0935B2CA8F1FCD42BF839812255101E2A,
	ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F,
	ARKitSessionSubsystem_RegisterDescriptor_m359F3EC534DDADAE498376C22BE8BB24813B1CC9,
	ARKitSessionSubsystem__ctor_m551ABE75E099FDA75C75967077DEFE540A41DC19,
	ARWorldMap_Dispose_m79FA6173E594AE9F057F0AEB856A15E2ACB43757_AdjustorThunk,
	ARWorldMap_get_valid_m46E6C15ED1A84309B0B80B17B61025F22D89CBD9_AdjustorThunk,
	ARWorldMap_Serialize_m4DABF47BF52F0FEDBD9DF306B76A20E62C14E1DC_AdjustorThunk,
	ARWorldMap_TryDeserialize_mA27A7AA88E6F36FCBCBFD2E72C2683E980193099,
	ARWorldMap_GetHashCode_m8DDE36BC2ED25796844C59C7A098B07E7A7BA573_AdjustorThunk,
	ARWorldMap_Equals_m8D5C69808F4E3DB20F697D46F85C17A24FDE4688_AdjustorThunk,
	ARWorldMap_Equals_m50211C5B4349C580EC9E3913814FCE02E72C7D3A_AdjustorThunk,
	ARWorldMap_op_Equality_m13BBDD287F1E6CC05115F3CCAFF4DE31A198DF50,
	ARWorldMap_op_Inequality_m40D90E68ED4F82A88B637B39943D5A9579D9C615,
	ARWorldMap__ctor_m51BA4D411B69385E02F7A49B7BA1ECB0D2AD2FD7_AdjustorThunk,
	ARWorldMap_get_nativeHandle_m529E0BB03669BBD9370B50C8F6ED90BA05213F22_AdjustorThunk,
	ARWorldMap_set_nativeHandle_mBE78617799CC9B825C61B179F1E2F35D310740DE_AdjustorThunk,
	ARWorldMapRequest_get_status_mEF94818913FCF6416CF29C6BFBCC84EEF39C3867_AdjustorThunk,
	ARWorldMapRequest_GetWorldMap_m2113D2DECC0E8F9CB67D7A3EC480CBD3F2AA5A4E_AdjustorThunk,
	ARWorldMapRequest_Dispose_mBD67970EA0A71C29390F56841D0357E699987A8B_AdjustorThunk,
	ARWorldMapRequest_GetHashCode_m0257226B87FC520585883D513F8B69D664755003_AdjustorThunk,
	ARWorldMapRequest_Equals_m667C3E4FB1E8F795834A62E93F66A13958C4C4E7_AdjustorThunk,
	ARWorldMapRequest_Equals_m62A44971FAF312C53F44D445377CEC8737720BA1_AdjustorThunk,
	ARWorldMapRequest_op_Equality_m4CE393991E42963EB2464762473D649C52B781E4,
	ARWorldMapRequest_op_Inequality_mA3D7102DF1B3FD71F9C535A4DC9496A14757DBD2,
	ARWorldMapRequest__ctor_m6800C690A81676D3F26CF9D7A4E1099B6BF4DAEF_AdjustorThunk,
	ARWorldMapRequestStatusExtensions_IsDone_mFBAF3BFF35C5A43A711E50C5D6A939BADB89D3E5,
	ARWorldMapRequestStatusExtensions_IsError_m4324D3418C82C5B1C955A72DD0E9BA5805286540,
	ARKitXRDepthSubsystem_CreateProvider_m4D1BD576DBC081378970382782CA3719CD283A5B,
	ARKitXRDepthSubsystem_RegisterDescriptor_mEB1B5F8A8CF06C7E6F00CEE8B8A5D8F2CB86B5E7,
	ARKitXRDepthSubsystem__ctor_m81EFFCC78EA40E790162C25FD9DC64DF4557BD46,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Construct_m389EF901E5A84DE58098B34F3159AFAA1B83C6E5,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Destruct_mA74881A49FA8DB131E3613E414E54215205799C2,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Start_mD464FCD2D9A5C50791D7B9D38AAD43D97ACA6227,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_Stop_m80FF6427ACD878991E6F3649C32A4C948AD834C9,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_SetAutomaticPlacementEnabled_m58CF7C2D525E820536C7143D42E84C7EFC4805A9,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TrySetEnvironmentTextureHDREnabled_mA0623A69CD42A71C9ACB5B3FFDA122774F915D4F,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryAddEnvironmentProbe_m7EF9192A23F0E6CBB565BCB89D9D3AC55024F272,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_TryRemoveEnvironmentProbe_m823D591063A7E9D9F2186D3AE0FB2AA96C03E1D6,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_AcquireChanges_mF3B06C982FBD823D7B03F3DB870E22846FFA834C,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_ReleaseChanges_m7357747C89D2E0CC5380F76F4965A3EECE4D1539,
	EnvironmentProbeApi_UnityARKit_EnvironmentProbeProvider_IsSupported_m2FAAB3704CE8490F8B7C332A4C89EDABC53A37D8,
	ARKitImageDatabase_get_nativePtr_m0627E0F10ED56C59222DD5597F5E0D562C7379D6,
	ARKitImageDatabase_set_nativePtr_m3EB571EADC2FFE172D2F6B49C6F9E85D4C1B737D,
	ARKitImageDatabase_Finalize_m6C9CFDE06F7E6520CA5A5600B5F4CBB1A303359E,
	ARKitImageDatabase__ctor_m577A4B9755C838DD1D7A71F14614DEC22B263FD6,
	ARKitImageDatabase_ScheduleAddImageJobImpl_mAC332D56D90815AF396570124E8D5D121017C01B,
	ARKitImageDatabase_get_supportedTextureFormatCount_m1F27D2C4CC29F345ABFE24F0E71820D0BC8A0A6B,
	ARKitImageDatabase_GetSupportedTextureFormatAtImpl_m2D528D1C9DCBDE029C52747B30554A69B6999002,
	ARKitImageDatabase_GetReferenceImageAt_m32E2DD4E43C4B525821B3ED239FDF04EBA4D93C7,
	ARKitImageDatabase_get_count_m41F4C1B60D3F3DDF656CDC87952490CF5C617801,
	ARKitImageDatabase_UnityARKit_CFRetain_m368A08124CC503F4ABDABC9BE37C61F600186451,
	ARKitImageDatabase_UnityARKit_CFRelease_m08FF854E77AD642DDC7BD585EED991F0AB71F98A,
	ARKitImageDatabase_UnityARKit_ImageDatabase_createEmpty_m033397E063140EE34ECD85964D1C152072C73D83,
	ARKitImageDatabase_UnityARKit_ImageDatabase_tryCreateFromResourceGroup_m79FB16BABD9059F390B4FAAA9A89D56016DD1F80,
	ARKitImageDatabase_UnityARKit_ImageDatabase_GetReferenceImage_m500C9EF31E89D4060082DF8B40B005B0B3E10F90,
	ARKitImageDatabase_UnityARKit_ImageDatabase_GetReferenceImageCount_m8DFA4A6501DEF6C46763560A0CDA7198818C3768,
	ARKitImageDatabase__cctor_m329B57B53D16428AB34ECDD5D50464224D186EEE,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setMaximumNumberOfTrackedImages_mBF0FA67787BE559D28A4949D256D5824B8F6F1F4,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_setDatabase_m03198A009BB5D5BB9005BCFF293556850745FC14,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_stop_mA5E30442ACDDB68089B08B3F6928E3BDC55E85B6,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_destroy_m73470876BEDDE85C215EABCD3555D73571E5F772,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_acquireChanges_mB4BFD5D1B49A8BA6443E7F08FE7BA8EE68A0BCFB,
	ARKitImageTrackingSubsystem_UnityARKit_imageTracking_releaseChanges_m79BE2C4F28C174B2D27CCA85FF9C6517F1B9F10A,
	ARKitImageTrackingSubsystem_RegisterDescriptor_mC8DC8F9919766BB3A242BF200922D81DCC59DE20,
	ARKitImageTrackingSubsystem_CreateProvider_mB500E08F02CA3D72D00EF6C1DCACBC857E7F9C6C,
	ARKitImageTrackingSubsystem__ctor_m0E214A0CB94F079C236C8B90D0D3ED0DE8BA8CB1,
	ManagedReferenceImage__ctor_m3E6575019476409355D028AE5BF5CF06DBDEBE91_AdjustorThunk,
	ManagedReferenceImage_ToReferenceImage_m0B8B4A58D7D7A9E680928D4966568762164E0FC6_AdjustorThunk,
	ManagedReferenceImage_AsSerializedGuid_m1D74B874928ABACCEAA33A8E2B17FD6C88195CE3_AdjustorThunk,
	ManagedReferenceImage_Dispose_mE69922AA55D2E38E33DFB72E12C6684051491071_AdjustorThunk,
	NULL,
	MemoryLayout_GetHashCode_m6EC79923E1C318EAC9A2E00E8D9713E58AA37ACD_AdjustorThunk,
	MemoryLayout_Equals_m9858ECABD94F69653A020FA372FCFC2F380490CA_AdjustorThunk,
	MemoryLayout_Equals_m09D6C50724B51935065A7CDD96D36AD84CD24495_AdjustorThunk,
	MemoryLayout_op_Equality_mACCB7C2460D49EAE5BEFF739511B0720D507E148,
	MemoryLayout_op_Inequality_m4CC8BAE44A506F5DF5EE61E45C6FB5C6192C2E93,
	NSData_op_Implicit_m849FF0BB76B860590D7397FD9D6CF1482FC78FFC,
	NSData_CreateWithBytes_mD9E781B9122D9E9EED71208F1234EBC3E53075E9,
	NSData_CreateWithBytesNoCopy_m72ADBA64EA4F99900A822CCA4A83986D93DEA991,
	NSData__ctor_m9A7FECA65050D513BB19BB46FBD4EA6EAFD3C8FA_AdjustorThunk,
	NSData_get_created_m2D90222AF6028686A8939E8666189D65B4A1919E_AdjustorThunk,
	NSData_get_bytes_m183DD9311C27C2C02B23B21DF0F105AD79B1AC6B_AdjustorThunk,
	NSData_get_length_m0BA80361ECB165E9A6972E49F5B0F466B62DD3A8_AdjustorThunk,
	NSData_ToNativeSlice_mA623ABA3567BE33488F2E3F1AB94527CF0432B06_AdjustorThunk,
	NSData_Dispose_m8B1067FDB5CFAF606432A3EEB96812D7CCD8ADDE_AdjustorThunk,
	NSData_GetHashCode_m1B57A82FE61E21C2B94ED7623672FE66738D7C70_AdjustorThunk,
	NSData_Equals_mAEF0CEAB44D1070F793301EBAD51B7356914F7EE_AdjustorThunk,
	NSData_Equals_mC39A17804E12788903FFAB8CFBF70E427671A1F9_AdjustorThunk,
	NSData_op_Equality_m6E656FC4E029E85669056D855E4F8AE6A8B871C5,
	NSData_op_Inequality_mC5B9BD2D944A27B3982DE160A1ABB4F46CBB7F53,
	NSData_UnityARKit_CFRelease_m6B4F93F8129E2E6F3DEBB5A0A905110AEB414308,
	NSData_UnityARKit_NSData_getBytes_mAA5116EF4A60FF192426E7FF4A6C5C8510A13F86,
	NSData_UnityARKit_NSData_getLength_m8F463861912690AF701DC4D1F298BAEE6E30871C,
	NSData_UnityARKit_NSData_createWithBytes_m8A57C2C65866569604B9B1880DA65E5A73DA0F18,
	NSData_UnityARKit_NSData_createWithBytesNoCopy_mA268ACAAF7FFA5917018448B620EE916D27D8D50,
	NSMutableData_op_Implicit_mC4CE5A7B0FF67566F6D2561597F66F2846BA085C,
	NSMutableData__ctor_m85F65E0CAF2F4437F80B02F78E4CC6D2B3AB2A06_AdjustorThunk,
	NSMutableData_ToNSData_mDA252358836999BE419A4D00E3C49791BACFD83D_AdjustorThunk,
	NSMutableData_get_created_mD218AB8F4C06BA85007F04EA844D63658B110743_AdjustorThunk,
	NSMutableData_get_bytes_m11502AC7249746EF0526CBD7578675BF81C75D6B_AdjustorThunk,
	NSMutableData_get_length_mC8FCDD66F7E1605868465B5E59B8E3DE0A2BD81E_AdjustorThunk,
	NSMutableData_get_ptr_mDB546A2BA106F9E1D97EEAB680904FF2B9E82128_AdjustorThunk,
	NSMutableData_Append_m47A65367547BBC3BBCA1B2C681EE3C73B8B76403_AdjustorThunk,
	NSMutableData_Dispose_mEAAB4C5FA1BAA2B708A92402C7A336A0DA8E4A3F_AdjustorThunk,
	NSMutableData_GetHashCode_mA5D0354E3A022B04ECEC9A0A464F918C16964202_AdjustorThunk,
	NSMutableData_Equals_m6B9AA0CA15A5D6D94269F9FB11B631E316E109D3_AdjustorThunk,
	NSMutableData_Equals_mADF8D53F6EEC9EFB6C8986D14C86331EE1EC0C46_AdjustorThunk,
	NSMutableData_op_Equality_mDDB5C4F23F8DC5F8E416722FDB8A898D292B38C5,
	NSMutableData_op_Inequality_mA0F73000FB970FF89E66A50B98A9FF7ED301B585,
	NSMutableData_UnityARKit_CFRelease_m48047B1DD2222EEDB29CD3BD8540C69AF2CBE476,
	NSMutableData_UnityARKit_NSMutableData_append_mA0BBFF30EABEC8C8BC19420D0EDE1DEE932E05DC,
	NSMutableData_UnityARKit_NSMutableData_createWithBytes_m5A9B649942EE3727276A99F33D5FB6C90D4F88C1,
	NativeChanges_get_created_mD4E1C38022D90619A4EC6FB00625C836D127E8FB_AdjustorThunk,
	NativeChanges_get_addedLength_m229FB7E43EC8C76BEE7022F67F523A4D1557F481_AdjustorThunk,
	NativeChanges_get_updatedLength_mBA4E768AE41A7FC6EA6F61CD756F19A6D520325B_AdjustorThunk,
	NativeChanges_get_removedLength_m614CEDC349020B00F3192CCF398FA2BE57CAB03E_AdjustorThunk,
	NativeChanges_get_memoryLayout_m33CE33857634BFAB2D256E5DFBBD1A34E2778C28_AdjustorThunk,
	NativeChanges_get_trackingState_m4B7750E01DE7CC245657EF229D62064E130067A3_AdjustorThunk,
	NativeChanges_Dispose_m1358BB3C08E567696B02AA0B049D4D214CDC2A6F_AdjustorThunk,
	NativeChanges_Equals_m567C68E885D53B73943FA163F659AB337548D41C_AdjustorThunk,
	NativeChanges_GetHashCode_mC66BD98DE9377530EEC347DDEC884428DAB095F5_AdjustorThunk,
	NativeChanges_Equals_m9BE3F3E0D96499230C43AB8572480E4E83B90D09_AdjustorThunk,
	NativeChanges_op_Equality_m0E291E9DD72E7B596AA015CB5F39EAC85B09EF9D,
	NativeChanges_op_Inequality_m4F1A3A06F7C30F1977BE2ECC6694BCC4FC137F88,
	NativeChanges_GetAddedLength_mAE96577C4AFF57B4249B563B5B5DDF42B1616B1B,
	NativeChanges_GetUpdatedLength_m1E6CD83834DE174A4A98CE5A30533B8EF41D0345,
	NativeChanges_GetRemovedLength_mDF5C0941045E74997EC18C3833A0E07D56A86189,
	NativeChanges_GetMemoryLayout_m9A9EB356EA334E535B7F2D6ABEFB9989939AB542,
	NativeChanges_GetTrackingState_mE845496B0599BCF2DEF4E3625127730305962F88,
	OSVersion_get_major_m78423586F3DAF62CF3FACA972DF71A0794FF42E6_AdjustorThunk,
	OSVersion_set_major_m5DBF19A6CA77CC1DAD43846899A4871548B48A84_AdjustorThunk,
	OSVersion_get_minor_mECC11198E3287B306A2A5B9C2DEAA7113263D7B5_AdjustorThunk,
	OSVersion_set_minor_mA54AFDAB79AF99984AD01893AC99866479877CC8_AdjustorThunk,
	OSVersion_get_point_m15A8472AC862312B14F0FE398C81D538BBF1EF74_AdjustorThunk,
	OSVersion_set_point_mE257BF56FF20203DC23672D35C1B20D6A1454405_AdjustorThunk,
	OSVersion__ctor_mEAB035AA8D379D1C6A42A3CFC1B04F24A8AF2AC4_AdjustorThunk,
	OSVersion_Parse_mDF26500DB38764536C24E82F4659B9B1E0B40AC8,
	OSVersion_IndexOfFirstDigit_m3527F158639800E2A9884F8D83A5F394B3FED5B4,
	OSVersion_ParseNextComponent_mBDCC4A63E1B916D9B1136864639DA42C8762FC82,
	OSVersion_GetHashCode_mF4F18C6F777E716D07DEF052A90CC64C73C3D01E_AdjustorThunk,
	OSVersion_CompareTo_m5102D1398077353643B4784512A599CDCB5590A1_AdjustorThunk,
	OSVersion_Equals_mA70B1E35BBCD9C206367BD8C66F3E0F2DAF51BF3_AdjustorThunk,
	OSVersion_Equals_m99FCBF7670DF0DCC1D6968EC51EE6EF55611471E_AdjustorThunk,
	OSVersion_op_LessThan_m75C71874EFC4F3233EC421034E6599CADB16504F,
	OSVersion_op_GreaterThan_mD04D07551AE4D4C9D08B63884DF80B255047F2B1,
	OSVersion_op_Equality_mC51EEC8017A1A495CD6DB0E046FA1BCF4D656DB4,
	OSVersion_op_Inequality_m0DAF84C7189C1D4A8E2C2ED62E1254262E356843,
	OSVersion_op_GreaterThanOrEqual_m5DC241E62216C8897A29CA8BE7F1352528B9F420,
	OSVersion_op_LessThanOrEqual_mC003D088C0FC11B4C1857B846455594793B11CC8,
	OSVersion_ToString_mC2FB1F8F61AAE40B141FE45C3F3B9408701EA00A_AdjustorThunk,
	ARKitParticipantSubsystem_CreateProvider_m7A52FB02F598655323921124B54E127C1F9B3C73,
	ARKitParticipantSubsystem_RegisterDescriptor_m36BF219BA989608869D12020754327EC6F34F945,
	ARKitParticipantSubsystem__ctor_m0656DF037CFFF93AE46EA045C34872E282582B97,
	ARKitXRPlaneSubsystem_CreateProvider_mF0433C3656285E8CC2EC9F687799AB61C29B8C47,
	ARKitXRPlaneSubsystem_RegisterDescriptor_m5F27E00E3BBC38D080D558D6B2689AB709ED0123,
	ARKitXRPlaneSubsystem__ctor_m1A4464449DBD1423997334CA3D76D0ABB4E88B2B,
	SerializedARCollaborationData_get_created_m2E3D4C6854CBD1CE9B3F90464DE40F0C62AC181C_AdjustorThunk,
	SerializedARCollaborationData_get_bytes_m14033DBDB8CC6A569A7858B2293F2EE494580059_AdjustorThunk,
	SerializedARCollaborationData_Dispose_m10A79EA8C1211FAE4E72A8B2C087582752D95AAF_AdjustorThunk,
	SerializedARCollaborationData_GetHashCode_m89BE561C8EAB7A0D69A671315134307523826249_AdjustorThunk,
	SerializedARCollaborationData_Equals_m22E16E2813369F9BC42DFB26638C81E34444E6E5_AdjustorThunk,
	SerializedARCollaborationData_Equals_m6B612F991B4E51ED3E7681265347A5AC4279BFF3_AdjustorThunk,
	SerializedARCollaborationData_op_Equality_m9BD38364FC1D3828FBB062451408F0CC4C16C809,
	SerializedARCollaborationData_op_Inequality_m40BDE8C85A155F6972DC67BE09F22DDEEC08EF78,
	SerializedARCollaborationData__ctor_mE5E3DB0B9757DCA77A22D5E582845A4291E77886_AdjustorThunk,
	ARKitProvider_Start_m909A9FE0AED772D065945138072F0C58CAF13E66,
	ARKitProvider_Stop_mFE6CDDBE56F66A7520273D7874DDC27367245FE6,
	ARKitProvider_Destroy_mFEEF62895226542DCD4EBD2C667CBEDC0EF56105,
	ARKitProvider_GetChanges_m9FD494D13B72A009A7751182DB8AB50766FF2324,
	ARKitProvider_TryAddAnchor_m42B9138A2279B01E37D999093EAEEA761BB5A9E3,
	ARKitProvider_TryAttachAnchor_mBEFA4EAFE1B4209286CF510EDBB2032D0B9C8B72,
	ARKitProvider_TryRemoveAnchor_m3C19701F5458BECF81D3394FBA3F6205F3379199,
	ARKitProvider_UnityARKit_refPoints_onStart_mD48C2FDF1464AABDE11ECF7C46F91BCC90CEFBC8,
	ARKitProvider_UnityARKit_refPoints_onStop_m7A4366D4FB58FFD78C9843307089A57774E7DDE2,
	ARKitProvider_UnityARKit_refPoints_onDestroy_mC0C108D4D36E43E31BB7D54160220884E3001D43,
	ARKitProvider_UnityARKit_refPoints_acquireChanges_m4BD00FBC27D534F01AB15E6A217AD42CFB46BD78,
	ARKitProvider_UnityARKit_refPoints_releaseChanges_mEADB262D174F77BFC9C0E8324FF920E4E21EFD39,
	ARKitProvider_UnityARKit_refPoints_tryAdd_m2969B475555721927D6B87BB31F202EC2B71ED09,
	ARKitProvider_UnityARKit_refPoints_tryAttach_mD0F2154B0CD3A7DB7EDC0198BC426708FC1CFD71,
	ARKitProvider_UnityARKit_refPoints_tryRemove_m46C3B0EADE0C2297E2A49154ABD7DC3860B0554A,
	ARKitProvider__ctor_mE014D0F9B44627BC32E1C4211BBCF6BF64192237,
	ARKitProvider_get_cameraMaterial_m618301FF821AB3FA4BA2F4ED113A76289D4075B1,
	ARKitProvider_get_permissionGranted_m396F8E4396480AF248974165484D627A9383C360,
	ARKitProvider__ctor_m17954B389E39493C52B0EC8918C820943D550095,
	ARKitProvider_Start_mEC90123DDA11C6024F62DA39E9873110ACD7600C,
	ARKitProvider_Stop_m9E080CB6E0FB7675ACEEBBCC4956A3030AEB3A00,
	ARKitProvider_Destroy_m9D9D4C8526F13F29BAFC67F69115C887DF92E32E,
	ARKitProvider_TryGetFrame_m49FDB983C16F10F3C37A87728AA5508DFC7DB669,
	ARKitProvider_get_cameraFocusMode_m6631A2A95180FC11A2431A7F68C58DB81E731A4C,
	ARKitProvider_set_cameraFocusMode_m7D87F3A3324EB25EE6054EC41342C91A8767B6DD,
	ARKitProvider_TrySetLightEstimationMode_mA8341986E3571D15EAC602E4DF6C2CFC3F548EDA,
	ARKitProvider_TryGetIntrinsics_m4D8E7652489E160DE8F72B057D1BF4AC6AB48FF1,
	ARKitProvider_GetConfigurations_mD393299D08B98045E06E902C5C27AABC068E3DE0,
	ARKitProvider_get_currentConfiguration_m6FE6950DD61DC22EE4A2DA65A118A142C12D65A5,
	ARKitProvider_set_currentConfiguration_mD1B03DE1C34DCD8CCE1BDFBE9F58230455236713,
	ARKitProvider_GetTextureDescriptors_m4E627551BE5A781237F6517DEB67B6845A1FA610,
	ARKitProvider_TryAcquireLatestImage_mC72CF5742BD36615F430AAA93B35BDEDD088A747,
	ARKitProvider_GetAsyncRequestStatus_mCACD2D1E4D1DABDD649D4744B66C30926004D21B,
	ARKitProvider_DisposeImage_mCD96A4E7AFF6818ED4716B4B4A189A4537EB971F,
	ARKitProvider_DisposeAsyncRequest_m14B12FB7B00DDC17701173CC7B1602802DCAAFF6,
	ARKitProvider_TryGetPlane_m28439A10DE878269A0122C2CC556E9257E3D0504,
	ARKitProvider_NativeHandleValid_m8EBD3FB23DA716E87E25E3E2036D8F391A457428,
	ARKitProvider_TryGetConvertedDataSize_m0AC8023D46234C71774C7432474E63133DFF23D5,
	ARKitProvider_TryConvert_mE31A78FA0E8F1E91781CA2427812B4548F12CF7A,
	ARKitProvider_ConvertAsync_mD002C4A5BCB0D382F9985087CBCCF658043C6324,
	ARKitProvider_TryGetAsyncRequestData_m09AC24409C4FC6A6A2AFD9F3A33BD86C1FE0AA0B,
	ARKitProvider_ConvertAsync_m026B0421A6ACFBDDD27639696EF0A0098D92962C,
	ARKitProvider__cctor_m0CCAB7EE9003FDDD9165CB7FD8BC09A808E70CA1,
	NativeApi_UnityARKit_Camera_Construct_m2DF0AEE8AA88CDF96D248F9CD714961C2738216F,
	NativeApi_UnityARKit_Camera_Destruct_m795FFFF7821CE91E8992070BEFAAE0FFBA6AE084,
	NativeApi_UnityARKit_Camera_Start_m59DA518F90E02C5707FDD049DC3DECD96B65A5A6,
	NativeApi_UnityARKit_Camera_Stop_m098821B53BD6527CA00BB02E51D36A7A5C76FF10,
	NativeApi_UnityARKit_Camera_TryGetFrame_m6048991661BFB4AEE17C00EDCF7E65995B7C8691,
	NativeApi_UnityARKit_Camera_SetFocusMode_mEA11062CA8C34DFC4DC3E967B28E26021CAB9DEC,
	NativeApi_UnityARKit_Camera_GetFocusMode_m9F0C785F1D3835D11166E0DA19BB321BCF2554DD,
	NativeApi_UnityARKit_Camera_TrySetLightEstimationMode_m2DAF2550DA728382FB0F1B91B02B74FC50D4257E,
	NativeApi_UnityARKit_Camera_TryGetIntrinsics_m364B7B8DCA914DD0C1878F56859A5C7D9CC3B045,
	NativeApi_UnityARKit_Camera_IsCameraPermissionGranted_m972DB9591AA4E5EB4F483280F44DEF5CC8C88EE3,
	NativeApi_UnityARKit_Camera_AcquireConfigurations_m6A384C73CC0BE9DA50DC6128284E200678D08771,
	NativeApi_UnityARKit_Camera_ReleaseConfigurations_mECAE479F28DB8847031DB58670D463D8692C661D,
	NativeApi_UnityARKit_Camera_TryGetCurrentConfiguration_m2B5868976AF970142BB6CDBCBB04329C98FBAA88,
	NativeApi_UnityARKit_Camera_TrySetCurrentConfiguration_m10852C9A985C98A1BF729D17027BE7B96C9FB673,
	NativeApi_UnityARKit_Camera_AcquireTextureDescriptors_mEAFD58446AEBEC88F2E3E55B5C8C3C89B2BA0869,
	NativeApi_UnityARKit_Camera_ReleaseTextureDescriptors_m1B1D58FE2C83A8854CD2223602213D302585A0A2,
	NativeApi_UnityARKit_Camera_TryAcquireLatestImage_m9EDEF6EF0E4C88B918827C8DFCA3AF3265C04DEE,
	NativeApi_UnityARKit_Camera_GetAsyncRequestStatus_m7281886403BB594C92DFD3A86ADEE408346923BC,
	NativeApi_UnityARKit_Camera_DisposeImage_m696CD790C79BA59B3B69032D9E331D33F0D8506E,
	NativeApi_UnityARKit_Camera_DisposeAsyncRequest_m60C68C4C527C4F1E0662997D19E991DE8B129AA2,
	NativeApi_UnityARKit_Camera_TryGetPlane_m1CA36CD2E4A4BCCBE3A3009C0EF6D5D8BA1D85CD,
	NativeApi_UnityARKit_Camera_HandleValid_mD3F58E286478E2DE08802D95C43C63153D77CBB5,
	NativeApi_UnityARKit_Camera_TryGetConvertedDataSize_m1F446C6518799135050CD5FA7CF8E6631821C54A,
	NativeApi_UnityARKit_Camera_TryConvert_mADA4D24BA9D27EC2D94778FD0A4B378528EBBBD4,
	NativeApi_UnityARKit_Camera_CreateAsyncConversionRequest_m45FBD7C1BB82A1DA6E8AADCEC8DB2A3825E59A2E,
	NativeApi_UnityARKit_Camera_TryGetAsyncRequestData_mB61F95189D5D9C1EC2CFF5253D8D35271667D5C1,
	NativeApi_UnityARKit_Camera_CreateAsyncConversionRequestWithCallback_mA240E3A47077643262D966DC4EF537D924B522B5,
	ARKitProvider__ctor_m74EA11B1635AAFB7C75610A87527AEADC7AF4D20,
	ARKitProvider_Start_m30E0C9617EA20AE65E375C38674477E84E7BFF5D,
	ARKitProvider_Stop_mD23CA292FD04017A2EB4426D6DB0E8DD4DC0414A,
	ARKitProvider_Destroy_m70663825B404FED505C944EFD6DD1FEEF22FFABF,
	ARKitProvider_SetAutomaticPlacement_mF2D03145C110FA3C41ED1171DA0C564E9E9EF759,
	ARKitProvider_TrySetEnvironmentTextureHDREnabled_m716FDB49019570D0AEC26EE3310301F83F72F045,
	ARKitProvider_TryAddEnvironmentProbe_mDFA308B205CCF8C31AD53AD75D2C88EDDEA46D67,
	ARKitProvider_RemoveEnvironmentProbe_m7791BEF4D49E8AF96125895FC724ACB46AB51A9A,
	ARKitProvider_GetChanges_mE537EDA473E09E3536CA242240ED672B3A591A1A,
	ARKitProvider_Raycast_m9C4B998808B9588CD132C3952F4B300C3D224DAC,
	ARKitProvider__ctor_m89E04C05CD3998612E73B973BFAE10B8A3698973,
	NativeApi_UnityARKit_raycast_acquireHitResults_mA4AC6C358549A8DC05F394AFD56ED90CD03E9C9F,
	NativeApi_UnityARKit_raycast_copyAndReleaseHitResults_m1080A044038B5CF33AAB68D1DD8C4018E1B2D12A,
	ARKitProvider__ctor_mE8A2DBDA94E4A4E35736AA2F139ADA09007C78C1,
	ARKitProvider_Resume_mCE288422DFF1107BC09A818C50303CD7E8DABBC0,
	ARKitProvider_Pause_m9F19622C31DEA039930913B737B4AF88AD6A6953,
	ARKitProvider_Update_m638CAF367EB09C5CA098713D500E085BDDAF27D7,
	ARKitProvider_Destroy_mA5FE3BCBE02898DCAE38EF64AB4B494B0597F616,
	ARKitProvider_Reset_mEB1960006C47FD414F58EE336AFBE58006BD6A06,
	ARKitProvider_GetAvailabilityAsync_m5DD8ADCFF0773B54FE8CB2BC8231E04FCD29E89C,
	ARKitProvider_InstallAsync_m939BDBDDA1E171C150F142D1F49509C49F5DBF00,
	ARKitProvider_get_nativePtr_m2434FCF2FDC94C2DB761BFB58F0EAE0DE0598773,
	ARKitProvider_get_trackingState_m4E6688F25BA0E3BFA8AD38263D3C1ADD0E65F0CA,
	ARKitProvider_get_notTrackingReason_m45B14B276B268C54B3EBE376BBDAF9985071A406,
	ARKitProvider_get_sessionId_m1A4C5FFEE7717335ABB12834D53CC52ABE84D684,
	ARKitProvider_get_matchFrameRate_m7B71690EB1BB0A9C275EA1A661200EB31216FCBB,
	ARKitProvider_set_matchFrameRate_m5630834FCE78BE7C16479A423175798E045070CC,
	ARKitProvider_get_frameRate_m57ABC1BBF94026BDCD5999C2E2CCB99653D01B02,
	NativeApi_UnityARKit_createWorldMapRequest_m010633C018D940583817758657D3896AB44DD231,
	NativeApi_UnityARKit_createWorldMapRequestWithCallback_m0D65907CA8F5264B0817FFCAECB09836A6565EEB,
	NativeApi_UnityARKit_worldMapSupported_m1EDEB43F65D1C8D7A548D1CDA661CA5365560FC7,
	NativeApi_UnityARKit_session_getWorldMappingStatus_mD1C6F316E1B312DBBBF5A3C388D39C8492AFED3D,
	NativeApi_UnityARKit_applyWorldMap_m5CCBE77B93748A2B5549BA35E11F91843C40C315,
	NativeApi_UnityARKit_session_getNativePtr_m4F7B0EAF0A36CB86D01691442D10550679A32341,
	NativeApi_UnityARKit_session_getAvailability_m99887277747C2AAD5A630BF058000B82B97F844D,
	NativeApi_UnityARKit_session_update_m8C606BC443E0856C3CE057C45A554EAF1DAA8B74,
	NativeApi_UnityARKit_session_construct_mFF6871B50DE774C16170C63CBC1C5A244B6DF7B3,
	NativeApi_UnityARKit_session_destroy_mED1AC9AA6E6D593699FB6A877AA9B7C015D5C7E6,
	NativeApi_UnityARKit_session_resume_m65F7BC2F836A0DFD9C4ED2F9D9F7B3D7FD8BB6A3,
	NativeApi_UnityARKit_session_pause_m1D55CF790B44AEA02FBBB1D58CE68306AC1ECEA0,
	NativeApi_UnityARKit_session_reset_m67A0BCBAF9237CEA676E4AE5AFCADA108D7DA87C,
	NativeApi_UnityARKit_session_getTrackingState_mC556510D36A2A09FD80AB564EF433A9740EEEF60,
	NativeApi_UnityARKit_session_getNotTrackingReason_mDD73A7E193548D8D040F9C147C733072EFA6FF25,
	NativeApi_UnityARKit_session_getCollaborationSupported_mF2E6538A8E947A80B9BFDC8DD35D7C16C7A7AF98,
	NativeApi_UnityARKit_session_dequeueCollaborationData_m8BA4E3ADE3AF737995FDD372D90650C8623873DE,
	NativeApi_UnityARKit_session_getCollaborationDataQueueSize_mA5194859037FCC7D4E50E81FB6BDDB82C7DE4578,
	NativeApi_UnityARKit_session_updateWithCollaborationData_mECCD8142C621CAEDA2364559BB4BD2FFD6DF03EA,
	NativeApi_UnityARKit_session_getCollaborationEnabled_mD2488ECB355D675BC63D1B87D1D7182B7425AD94,
	NativeApi_UnityARKit_session_setCollaborationRequested_mED304CB532D3387B6ADBAF0AD248B17C7D194D8C,
	NativeApi_UnityARKit_session_getSessionId_m110172205B0A20E0104F0EFCE06292EEFAAABB87,
	NativeApi_UnityARKit_session_getMatchFrameRateEnabled_m8F100FB7D6375D28C817BA1F3BE453A6197905E3,
	NativeApi_UnityARKit_session_setMatchFrameRateEnabled_m9AD09A23F4C7179C29CFDC5C95D2E75DD3DF751E,
	NativeApi_UnityARKit_Session_GetFrameRate_m972076B8E05E0A255E6A2BD428A07FAEA8BF92CF,
	NativeApi_UnityARKit_coachingOverlay_getActivatesAutomatically_m318B6374D6856938069DC9EEA01A723E4A50BAF1,
	NativeApi_UnityARKit_coachingOverlay_setActivatesAutomatically_m5C3121BD09BA0F509BE3C637FDBBF4343BCE01A3,
	NativeApi_UnityARKit_coachingOverlay_isActive_mC736E2753CC6A08C9FE7211F774873EABA172FE3,
	NativeApi_UnityARKit_coachingOverlay_setGoal_m12A3CB19A2F7C35484FB7B5A32D293118727F3D1,
	NativeApi_UnityARKit_coachingOverlay_getGoal_m605073A82D7FE71FB0D482AA33927C6525AE26D7,
	NativeApi_UnityARKit_coachingOverlay_setActive_m5393C3EE8B627C83C8209BDADDA3E791D99B2F01,
	NativeApi_UnityARKit_ensureRootViewIsSetup_mE225FB815AA43CEF206A97D455216B3ED4C9C557,
	NativeApi_UnityARKit_coachingOverlay_isSupported_m4D56FB899E6AB27EE3420A84FAFC65819D5EE085,
	ARKitProvider_UnityARKit_depth_destroy_m6A4D74F885ACE3382A741919678241F0263C85A6,
	ARKitProvider_UnityARKit_depth_acquireChanges_m1516593C0ED16CB5373B80145A8F2D151FF80C04,
	ARKitProvider_UnityARKit_depth_releaseChanges_mE00BAA1C07118438BCCC80931149478D98737FD1,
	ARKitProvider_UnityARKit_depth_acquirePointCloud_mBF75060DDA3A390EEE695166FBA734C9F54905B5,
	ARKitProvider_UnityARKit_depth_releasePointCloud_m43EF97D3587FC680FE57E107573BFE8920B415FF,
	ARKitProvider_GetChanges_m93C38677755047CBEC8888EC166728A155F1D386,
	ARKitProvider_Destroy_mA9D53428BEF1C15EE6739F0081898F376A809D11,
	ARKitProvider_GetPointCloudData_m08F4534A555CDC857A7525F7690B7B9AA9000C34,
	ARKitProvider__ctor_m1EE1EC8D2E04581B857FEF2C8B53EBD83C9DF252,
	TransformPositionsJob_Execute_mB106298BC698C628A7389A30320D14D7A001ED2C_AdjustorThunk,
	NULL,
	ConvertRGBA32ToARGB32Job_Execute_mA531EADD7AD59301639B1C6369985BAA091C5FAA_AdjustorThunk,
	AddImageJob_Execute_m15211532BA39BCD74DC8D9FAAB995D8958CA1BC1_AdjustorThunk,
	AddImageJob_UnityARKit_ImageDatabase_AddImage_m0241BA575F07E3A8B82792515CE25A3F5CF81614,
	ARKitProvider_CreateRuntimeLibrary_mAB8A0D4F46D0FE04DD8D63862A1355C9E7A2B6B0,
	ARKitProvider_set_imageLibrary_m691388D54857E38CF5E34256E6590419E4924B2E,
	ARKitProvider_GetChanges_m4147331A5C2CF153138902E97229ECB1436E3144,
	ARKitProvider_Destroy_m1DDCE5E5D84903F8AFECA244F256B4F632125FD3,
	ARKitProvider_set_maxNumberOfMovingImages_m1D3225DDD8D2406B1170C776B2F294F935DD8997,
	ARKitProvider__ctor_mC4557FF3997401EFDF46BA435B7859457636DAC3,
	ARKitProvider_get_created_mED66AD263607B1393D443D88A7D718BD86C4FC04,
	ARKitProvider__ctor_m620792E932F5D1794CC38F88ECC0E6672364B2DA,
	ARKitProvider_Start_m7989D59F3272929526D92F904F732920227A86DC,
	ARKitProvider_Stop_m8509EE10A3D390955DEE462CDFF2851C634CC443,
	ARKitProvider_Destroy_m27877ABB9B6FB5B0BF812D93323582BAAA962063,
	ARKitProvider_GetChanges_mBDE004575A175B2E0207016814CC9B66D2DE9FAF,
	ARKitProvider_UnityARKit_Participant_init_m5727B98C01813D0DA51AFE3CD2EB7A9A49F289D0,
	ARKitProvider_Destroy_mF5973030EA8900156ADFA608B5B0E0B1FE6E9B7B,
	ARKitProvider_Start_m9DB7CC568BF1ABE39E793EA536BDBE0C6B025807,
	ARKitProvider_Stop_mA24DB5AF5FEDD8CC74E69644BE38016951768842,
	ARKitProvider_GetBoundary_m1346438CA4B156993DCBD5A29A4F0C3C029963CD,
	ARKitProvider_GetChanges_m44C632F9A10C0F3BCD2DA3383C78B9A26F5220F5,
	ARKitProvider_set_planeDetectionMode_mDCF459FF96AEDC45F88F47FB7A7099FA26890FA6,
	ARKitProvider__ctor_m97A7439667349115C58FB37C4AA5F6814F769E26,
	NativeApi_UnityARKit_planes_SupportsClassification_mD1B96229E99DA916EE152509A67A61F66729C83F,
	NativeApi_UnityARKit_planes_shutdown_mD1C2043ADBAB5270B853789C2BA0FF2FA7F461B2,
	NativeApi_UnityARKit_planes_start_m4E5A97BCD4399E84F7DAC9325F42050F75EF739C,
	NativeApi_UnityARKit_planes_stop_m1854477D39BC0BB385A16C04BF99D321CAF5A941,
	NativeApi_UnityARKit_planes_acquireChanges_m483D3C7F49CEED1D7BB70496B6C1D3B2583925E4,
	NativeApi_UnityARKit_planes_releaseChanges_m38D9677B1F505B9773102E5BA9CD585BEFE6E044,
	NativeApi_UnityARKit_planes_setPlaneDetectionMode_m2B39558E40C526CC42FB2F6CA52A8E72820C4730,
	NativeApi_UnityARKit_planes_acquireBoundary_m64185E8AF48A2594D17765271ECE51FBFA8075BE,
	NativeApi_UnityARKit_planes_releaseBoundary_m15D062F6634FEF9BA69690ADCB3BC02BE12B6933,
	OnAsyncConversionCompleteDelegate__ctor_m4BE0A755AAC63D37870EB5636686E53F8A3AB114,
	OnAsyncConversionCompleteDelegate_Invoke_m02E895D72FDB88BB08F0A654711CE610B2F00816,
	OnAsyncConversionCompleteDelegate_BeginInvoke_m2C33525828F103BD6E8F47B18F3193F6AF8FB56A,
	OnAsyncConversionCompleteDelegate_EndInvoke_mD44E14594FFBDA7691CD4F7D9BE1247CA1EE6AE7,
	FlipBoundaryWindingJob_Execute_m558BC1AD243296BF68DCB7877F26578A10310422_AdjustorThunk,
	TransformBoundaryPositionsJob_Execute_m326C6D99F9D9D6487CC75D232E3326A117AF44DF_AdjustorThunk,
};
static const int32_t s_InvokerIndices[447] = 
{
	26,
	35,
	2326,
	89,
	10,
	23,
	2327,
	10,
	9,
	2328,
	2329,
	2329,
	7,
	2330,
	23,
	2331,
	2332,
	25,
	1015,
	1015,
	164,
	89,
	10,
	2333,
	35,
	26,
	2326,
	23,
	64,
	10,
	9,
	2334,
	2335,
	2335,
	14,
	3,
	23,
	1015,
	1015,
	2340,
	2341,
	21,
	173,
	173,
	21,
	46,
	2342,
	2343,
	2344,
	17,
	25,
	4,
	3,
	14,
	23,
	3,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	89,
	89,
	89,
	89,
	14,
	23,
	3,
	89,
	31,
	23,
	23,
	3,
	14,
	3,
	23,
	49,
	89,
	31,
	10,
	32,
	89,
	837,
	2355,
	26,
	49,
	10,
	2356,
	89,
	31,
	49,
	10,
	2333,
	2357,
	14,
	3,
	2358,
	3,
	23,
	23,
	89,
	2362,
	2363,
	10,
	9,
	2364,
	2365,
	2365,
	32,
	10,
	32,
	10,
	2366,
	23,
	10,
	9,
	2367,
	2368,
	2368,
	32,
	46,
	46,
	14,
	3,
	23,
	3,
	3,
	3,
	3,
	866,
	5,
	2370,
	2339,
	2371,
	25,
	49,
	15,
	7,
	23,
	26,
	1952,
	10,
	37,
	1951,
	10,
	25,
	25,
	776,
	2372,
	2373,
	164,
	3,
	173,
	25,
	3,
	3,
	2336,
	17,
	3,
	14,
	23,
	2226,
	1955,
	2375,
	23,
	-1,
	10,
	2376,
	9,
	2377,
	2377,
	2378,
	2379,
	2380,
	7,
	89,
	8,
	10,
	2381,
	23,
	10,
	9,
	2382,
	2383,
	2383,
	25,
	612,
	164,
	2331,
	2384,
	2385,
	64,
	2386,
	89,
	8,
	10,
	15,
	64,
	23,
	10,
	9,
	2387,
	2388,
	2388,
	25,
	2389,
	2331,
	89,
	10,
	10,
	10,
	2390,
	10,
	23,
	9,
	10,
	2391,
	2392,
	2392,
	2393,
	2393,
	2393,
	2394,
	2393,
	10,
	32,
	10,
	32,
	10,
	32,
	38,
	2395,
	94,
	648,
	10,
	2396,
	9,
	2397,
	2398,
	2398,
	2398,
	2398,
	2398,
	2398,
	14,
	14,
	3,
	23,
	14,
	3,
	23,
	89,
	2381,
	23,
	10,
	9,
	2400,
	2401,
	2401,
	2330,
	23,
	23,
	23,
	1839,
	1836,
	1837,
	1838,
	3,
	3,
	3,
	2336,
	17,
	2337,
	2338,
	2339,
	23,
	14,
	89,
	23,
	23,
	23,
	23,
	1883,
	10,
	32,
	30,
	859,
	1889,
	1881,
	1882,
	1890,
	859,
	37,
	32,
	32,
	898,
	30,
	1885,
	1886,
	1887,
	1385,
	1888,
	3,
	174,
	3,
	3,
	3,
	2345,
	173,
	106,
	46,
	394,
	49,
	2346,
	25,
	394,
	2347,
	456,
	17,
	394,
	21,
	173,
	173,
	2348,
	46,
	2349,
	2350,
	2351,
	2342,
	2352,
	23,
	23,
	23,
	23,
	31,
	223,
	1929,
	1838,
	1931,
	1997,
	23,
	2353,
	2354,
	23,
	23,
	23,
	2004,
	23,
	23,
	14,
	14,
	15,
	10,
	10,
	728,
	89,
	31,
	10,
	106,
	1006,
	49,
	106,
	173,
	776,
	106,
	3,
	3,
	3,
	3,
	3,
	3,
	106,
	106,
	49,
	776,
	106,
	25,
	49,
	866,
	439,
	49,
	866,
	106,
	49,
	866,
	49,
	173,
	106,
	2359,
	3,
	49,
	3,
	2336,
	17,
	2369,
	17,
	1905,
	23,
	1904,
	23,
	32,
	-1,
	32,
	23,
	2374,
	28,
	26,
	1959,
	23,
	32,
	23,
	89,
	23,
	23,
	23,
	23,
	1977,
	776,
	23,
	23,
	23,
	1944,
	1985,
	32,
	23,
	49,
	3,
	3,
	3,
	2336,
	17,
	173,
	2399,
	17,
	124,
	2360,
	2361,
	26,
	23,
	32,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x06000068, 10,  (void**)&ARKitSessionSubsystem_OnAsyncConversionComplete_mF9F38F09B38CC00967BD472D09D8584299F1B04F_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000AD, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 24385 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARKitCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARKitCodeGenModule = 
{
	"Unity.XR.ARKit.dll",
	447,
	s_methodPointers,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
