﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 EventManager EventManager::get_instance()
extern void EventManager_get_instance_m428764A3D4E4F271524B9391D28FFE6809114374 ();
// 0x0000000A System.Void EventManager::Init()
extern void EventManager_Init_m8BA016A50E13BA7391402F08D5E02342BFF9E3F4 ();
// 0x0000000B System.Void EventManager::StartListening(System.String,UnityEngine.Events.UnityAction)
extern void EventManager_StartListening_mA0D8BD9D0552443760FED2B041F011DB448A6BA1 ();
// 0x0000000C System.Void EventManager::StopListening(System.String,UnityEngine.Events.UnityAction)
extern void EventManager_StopListening_m8B453A35FD4DCDA255099729FFFA3CF8632AF26B ();
// 0x0000000D System.Void EventManager::TriggerEvent(System.String)
extern void EventManager_TriggerEvent_m226A5FF47528DA8EBA47B08C05BAF3313603D7B5 ();
// 0x0000000E System.Void EventManager::.ctor()
extern void EventManager__ctor_m78D54CF3F2AC07AEB77FE83DDB2AD9DAB8DA77DF ();
// 0x0000000F System.Void GameManager::Awake()
extern void GameManager_Awake_m12E1357322F99B1544DAD9C099CBFA6E63529A49 ();
// 0x00000010 System.Void GameManager::LoadGameScene(System.String)
extern void GameManager_LoadGameScene_m64DDBE62354ABBEF29BBB7959286DB9693CB5981 ();
// 0x00000011 System.Void GameManager::.ctor()
extern void GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358 ();
// 0x00000012 T Singleton`1::get_Instance()
// 0x00000013 System.Void Singleton`1::OnApplicationQuit()
// 0x00000014 System.Void Singleton`1::OnDestroy()
// 0x00000015 System.Void Singleton`1::.ctor()
// 0x00000016 System.Void Singleton`1::.cctor()
// 0x00000017 System.Void TrackedImage::Awake()
extern void TrackedImage_Awake_m5C032A4268A546E3AB4C12A36135248C91F57AD5 ();
// 0x00000018 System.Void TrackedImage::OnEnable()
extern void TrackedImage_OnEnable_mAA428110C2F30214B6EF40A6063EFB45AAD63B36 ();
// 0x00000019 System.Void TrackedImage::OnDisable()
extern void TrackedImage_OnDisable_m49FD040F763DA05AFAA73DFECB26A34904D181C9 ();
// 0x0000001A System.Void TrackedImage::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImage_OnTrackedImagesChanged_m9E600B0CA973D25919F73288634314AA2F900A63 ();
// 0x0000001B System.Void TrackedImage::UpdateGameObject(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void TrackedImage_UpdateGameObject_m49E349B2E3C12EA75965744759616169579FE480 ();
// 0x0000001C System.Void TrackedImage::.ctor()
extern void TrackedImage__ctor_m13FC3BEB9B4388143ADE48807370055A06E3DE32 ();
// 0x0000001D UnityEngine.Camera TrackedImageInfoManager::get_worldSpaceCanvasCamera()
extern void TrackedImageInfoManager_get_worldSpaceCanvasCamera_mCAABAD549BA48A96CDEC3A0FEFB4F314FE5258C7 ();
// 0x0000001E System.Void TrackedImageInfoManager::set_worldSpaceCanvasCamera(UnityEngine.Camera)
extern void TrackedImageInfoManager_set_worldSpaceCanvasCamera_mC3EB890AD2003574E06416D071946A5043DD5EFE ();
// 0x0000001F UnityEngine.Texture2D TrackedImageInfoManager::get_defaultTexture()
extern void TrackedImageInfoManager_get_defaultTexture_m1176A7A2F63BA29DAC02C52FC53224C2777548D2 ();
// 0x00000020 System.Void TrackedImageInfoManager::set_defaultTexture(UnityEngine.Texture2D)
extern void TrackedImageInfoManager_set_defaultTexture_mE07C609573CF89B52C76B593F1D68FF32C0AD1B3 ();
// 0x00000021 System.Void TrackedImageInfoManager::Awake()
extern void TrackedImageInfoManager_Awake_m7E3BFA101DD5C9A4F2DE15F09EA8D41B0A9462AA ();
// 0x00000022 System.Void TrackedImageInfoManager::OnEnable()
extern void TrackedImageInfoManager_OnEnable_m45B7BBFF4EBCE1219F3D92FD266E9E0ACA061A54 ();
// 0x00000023 System.Void TrackedImageInfoManager::OnDisable()
extern void TrackedImageInfoManager_OnDisable_m7A7B891203F05471A8D8463AF0EEB9A04DED8BFC ();
// 0x00000024 System.Void TrackedImageInfoManager::UpdateInfo(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void TrackedImageInfoManager_UpdateInfo_m2A20A4C15009AFB37A4C303D326D6D342B18C476 ();
// 0x00000025 System.Void TrackedImageInfoManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoManager_OnTrackedImagesChanged_m1F3D7A5052C233BC3EBF4ABC0F7F50CCA409421F ();
// 0x00000026 System.Void TrackedImageInfoManager::.ctor()
extern void TrackedImageInfoManager__ctor_m6188AAB9675D2D7DE6788D0F7058FD705A10D451 ();
// 0x00000027 System.Void Rotate::Start()
extern void Rotate_Start_mFF56EB83C79B5DEC931AA33F269381DD6A526FEC ();
// 0x00000028 System.Void Rotate::Update()
extern void Rotate_Update_mA3B26220EF9616351F003F9E3CA799FEB234B9E3 ();
// 0x00000029 System.Void Rotate::SetRotationSpeed(System.String)
extern void Rotate_SetRotationSpeed_mAF2C142212847B496B52AAB0AC622DE96689D87A ();
// 0x0000002A System.Void Rotate::.ctor()
extern void Rotate__ctor_m6E97A5DAE2DF20704A0A006973DF805DCA460F5F ();
// 0x0000002B MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426 ();
// 0x0000002C T MessageHandler::getData()
// 0x0000002D System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mEF0C5F957F8FAC2E9EA73C4749BFAF9B727547FF ();
// 0x0000002E System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C ();
// 0x0000002F System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6 ();
// 0x00000030 System.Void NativeAPI::onUnityMessage(System.String)
extern void NativeAPI_onUnityMessage_m981A178E4C85C082B76F5F86A4EFAC19425867BB ();
// 0x00000031 System.Void NativeAPI::.ctor()
extern void NativeAPI__ctor_m6AF8C586F2C4674EE98EA0EEC7E4DE11D26FD038 ();
// 0x00000032 System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846 ();
// 0x00000033 UnityMessageManager UnityMessageManager::get_Instance()
extern void UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16 ();
// 0x00000034 System.Void UnityMessageManager::set_Instance(UnityMessageManager)
extern void UnityMessageManager_set_Instance_mFD260C3BC291C3924C61F0724C6969AB0336716F ();
// 0x00000035 System.Void UnityMessageManager::add_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4 ();
// 0x00000036 System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07 ();
// 0x00000037 System.Void UnityMessageManager::add_OnFlutterMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_m7FD992E772A26B34DD346B04C3D68A0B1F92805A ();
// 0x00000038 System.Void UnityMessageManager::remove_OnFlutterMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_m623F9C78BDD8E605C5EEE5AC1400C4A2F0FFF892 ();
// 0x00000039 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5 ();
// 0x0000003A System.Void UnityMessageManager::Awake()
extern void UnityMessageManager_Awake_m1C1E9A8AFB3258EB62A404F13644CBB101AC02DA ();
// 0x0000003B System.Void UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_mC8D4CB3F70B56F758CC34B3246EE018AF6E4805A ();
// 0x0000003C System.Void UnityMessageManager::SendMessageToFlutter(UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m32B9FFB546C6D09C86CF79E046F7D00BB5AECD6E ();
// 0x0000003D System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413 ();
// 0x0000003E System.Void UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m7DF88A89869A1FF528735694C4855BD8C0DD74F0 ();
// 0x0000003F System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2 ();
// 0x00000040 System.Void UnityMessageManager_MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479 ();
// 0x00000041 System.Void UnityMessageManager_MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09 ();
// 0x00000042 System.IAsyncResult UnityMessageManager_MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1 ();
// 0x00000043 System.Void UnityMessageManager_MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F ();
// 0x00000044 System.Void UnityMessageManager_MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A ();
// 0x00000045 System.Void UnityMessageManager_MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF ();
// 0x00000046 System.IAsyncResult UnityMessageManager_MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994 ();
// 0x00000047 System.Void UnityMessageManager_MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5 ();
static Il2CppMethodPointer s_methodPointers[71] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EventManager_get_instance_m428764A3D4E4F271524B9391D28FFE6809114374,
	EventManager_Init_m8BA016A50E13BA7391402F08D5E02342BFF9E3F4,
	EventManager_StartListening_mA0D8BD9D0552443760FED2B041F011DB448A6BA1,
	EventManager_StopListening_m8B453A35FD4DCDA255099729FFFA3CF8632AF26B,
	EventManager_TriggerEvent_m226A5FF47528DA8EBA47B08C05BAF3313603D7B5,
	EventManager__ctor_m78D54CF3F2AC07AEB77FE83DDB2AD9DAB8DA77DF,
	GameManager_Awake_m12E1357322F99B1544DAD9C099CBFA6E63529A49,
	GameManager_LoadGameScene_m64DDBE62354ABBEF29BBB7959286DB9693CB5981,
	GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TrackedImage_Awake_m5C032A4268A546E3AB4C12A36135248C91F57AD5,
	TrackedImage_OnEnable_mAA428110C2F30214B6EF40A6063EFB45AAD63B36,
	TrackedImage_OnDisable_m49FD040F763DA05AFAA73DFECB26A34904D181C9,
	TrackedImage_OnTrackedImagesChanged_m9E600B0CA973D25919F73288634314AA2F900A63,
	TrackedImage_UpdateGameObject_m49E349B2E3C12EA75965744759616169579FE480,
	TrackedImage__ctor_m13FC3BEB9B4388143ADE48807370055A06E3DE32,
	TrackedImageInfoManager_get_worldSpaceCanvasCamera_mCAABAD549BA48A96CDEC3A0FEFB4F314FE5258C7,
	TrackedImageInfoManager_set_worldSpaceCanvasCamera_mC3EB890AD2003574E06416D071946A5043DD5EFE,
	TrackedImageInfoManager_get_defaultTexture_m1176A7A2F63BA29DAC02C52FC53224C2777548D2,
	TrackedImageInfoManager_set_defaultTexture_mE07C609573CF89B52C76B593F1D68FF32C0AD1B3,
	TrackedImageInfoManager_Awake_m7E3BFA101DD5C9A4F2DE15F09EA8D41B0A9462AA,
	TrackedImageInfoManager_OnEnable_m45B7BBFF4EBCE1219F3D92FD266E9E0ACA061A54,
	TrackedImageInfoManager_OnDisable_m7A7B891203F05471A8D8463AF0EEB9A04DED8BFC,
	TrackedImageInfoManager_UpdateInfo_m2A20A4C15009AFB37A4C303D326D6D342B18C476,
	TrackedImageInfoManager_OnTrackedImagesChanged_m1F3D7A5052C233BC3EBF4ABC0F7F50CCA409421F,
	TrackedImageInfoManager__ctor_m6188AAB9675D2D7DE6788D0F7058FD705A10D451,
	Rotate_Start_mFF56EB83C79B5DEC931AA33F269381DD6A526FEC,
	Rotate_Update_mA3B26220EF9616351F003F9E3CA799FEB234B9E3,
	Rotate_SetRotationSpeed_mAF2C142212847B496B52AAB0AC622DE96689D87A,
	Rotate__ctor_m6E97A5DAE2DF20704A0A006973DF805DCA460F5F,
	MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426,
	NULL,
	MessageHandler__ctor_mEF0C5F957F8FAC2E9EA73C4749BFAF9B727547FF,
	MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C,
	UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6,
	NativeAPI_onUnityMessage_m981A178E4C85C082B76F5F86A4EFAC19425867BB,
	NativeAPI__ctor_m6AF8C586F2C4674EE98EA0EEC7E4DE11D26FD038,
	UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846,
	UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16,
	UnityMessageManager_set_Instance_mFD260C3BC291C3924C61F0724C6969AB0336716F,
	UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4,
	UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07,
	UnityMessageManager_add_OnFlutterMessage_m7FD992E772A26B34DD346B04C3D68A0B1F92805A,
	UnityMessageManager_remove_OnFlutterMessage_m623F9C78BDD8E605C5EEE5AC1400C4A2F0FFF892,
	UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5,
	UnityMessageManager_Awake_m1C1E9A8AFB3258EB62A404F13644CBB101AC02DA,
	UnityMessageManager_SendMessageToFlutter_mC8D4CB3F70B56F758CC34B3246EE018AF6E4805A,
	UnityMessageManager_SendMessageToFlutter_m32B9FFB546C6D09C86CF79E046F7D00BB5AECD6E,
	UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413,
	UnityMessageManager_onFlutterMessage_m7DF88A89869A1FF528735694C4855BD8C0DD74F0,
	UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2,
	MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479,
	MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09,
	MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1,
	MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F,
	MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A,
	MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF,
	MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994,
	MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5,
};
static const int32_t s_InvokerIndices[71] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	23,
	137,
	137,
	163,
	23,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	2402,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	26,
	2402,
	23,
	23,
	23,
	26,
	23,
	0,
	-1,
	40,
	26,
	23,
	163,
	23,
	106,
	4,
	163,
	26,
	26,
	26,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	23,
	124,
	26,
	214,
	26,
	124,
	26,
	214,
	26,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x02000005, { 21, 4 } },
	{ 0x0600002C, { 25, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)2, 24836 },
	{ (Il2CppRGCTXDataType)3, 19506 },
	{ (Il2CppRGCTXDataType)2, 24837 },
	{ (Il2CppRGCTXDataType)3, 19507 },
	{ (Il2CppRGCTXDataType)3, 19508 },
	{ (Il2CppRGCTXDataType)2, 24838 },
	{ (Il2CppRGCTXDataType)3, 19509 },
	{ (Il2CppRGCTXDataType)3, 19510 },
	{ (Il2CppRGCTXDataType)2, 24839 },
	{ (Il2CppRGCTXDataType)3, 19511 },
	{ (Il2CppRGCTXDataType)3, 19512 },
	{ (Il2CppRGCTXDataType)2, 24840 },
	{ (Il2CppRGCTXDataType)3, 19513 },
	{ (Il2CppRGCTXDataType)3, 19514 },
	{ (Il2CppRGCTXDataType)3, 19515 },
	{ (Il2CppRGCTXDataType)3, 19516 },
	{ (Il2CppRGCTXDataType)3, 19517 },
	{ (Il2CppRGCTXDataType)2, 24421 },
	{ (Il2CppRGCTXDataType)2, 24422 },
	{ (Il2CppRGCTXDataType)2, 24423 },
	{ (Il2CppRGCTXDataType)2, 24424 },
	{ (Il2CppRGCTXDataType)2, 24841 },
	{ (Il2CppRGCTXDataType)1, 24435 },
	{ (Il2CppRGCTXDataType)2, 24435 },
	{ (Il2CppRGCTXDataType)3, 19518 },
	{ (Il2CppRGCTXDataType)3, 19519 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	71,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
};
